<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragrma", "no-cache");
	response.setDateHeader("Expires", 0);
%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>WiFun Console</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta HTTP-EQUIV="pragma" content="no-cache">
<meta HTTP-EQUIV="Cache-Control" content="no-store, must-revalidate">
<meta HTTP-EQUIV="expires" content="0">
<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
<style type="text/css">
table { //
	width: 100px;
	table-layout: fixed; /* 只有定义了表格的布局算法为fixed，下面td的定义才能起作用。 */
}

td { //
	width: 100%;
	word-break: keep-all; /* 不换行 */
	white-space: nowrap; /* 不换行 */
	overflow: hidden; /* 内容超出宽度时隐藏超出部分的内容 */
	text-overflow: ellipsis;
	/* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用*/
}
</style>

<script type="text/javascript" src="/js/echarts/echarts.js"></script>
<script type="text/javascript" src="/js/echarts/esl.js"></script>

</head>

<body>
	<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
	<div class="container-fluid">
		<div class="row-fluid">

			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
				<s:param name="type">13</s:param>
			</s:action>
			<!-- left menu ends -->

			<div id="content" class="span10">
				<!-- content starts -->
				<div>
					<ul class="breadcrumb">
						<li><a href="#">Report Management</a> <span class="divider">/</span>
						</li>
						<li><a href="#">Movie PVUV</a>
						</li>
					</ul>
				</div>
				
				<!-- search starts -->
<s:if test="#session['account_session'].type==1">
				<div>
					<div class="span12 my-search">
						<div>
							<h3>Search By</h3>
							<div class="my-filters">
							<form action="/pvuvReport/report-pvuv-search.action"
								method="post" id="queryForm">
								<table>
									<tr>
										<td class="control-label"><label class="control-label">Company</label>
										</td>
										<td class="control-label">
											<div class="controls">
												<select id="companyId" name="companyId" onchange="onCompanyChange(this.value);">
													<s:iterator value="companyList">
														<option value="${id}"
															<s:if test="id == companyId">selected="selected"</s:if>>${name}</option>
													</s:iterator>
												</select>
											</div>
										</td>
										<td><label class="control-label">Media List</label></td>
										<td>
											<div class="controls">
												<select id="mediaListId" name="mediaListId" value="${mediaListId }">
													<s:iterator value="mediaLists">
														<option value="${id}" <s:if test="id == mediaListId">selected="selected"</s:if>>${title}</option>
													</s:iterator>
												</select>
											</div>
										</td>
										<td>
											<button type="submit" class="btn">Search</button>
										</td>
									</tr>
								</table>
							</form>
							</div>
						</div>
					</div>
				</div>
</s:if>
				<div class="row-fluid sortable">


					<div class="box span12">
						<div class="box-content">
<s:if test="vos==null or vos.size()==0">
								<div style="padding: 20px;" id="nullScheduleAlert">
									<h3>There are no matching datas in the system.</h3>
								</div>
</s:if>
<s:if test="vos.size>0">
							<table id="tbl"
								class="table table-striped table-bordered bootstrap-datatable datatable">
								<thead>
									<tr>
										<th width="30%">Movie</th>
										<th>PV Total</th>
										<th>UV Total</th>
										<th>PV Today</th>
										<th>UV Today</th>
										<th>PV Yesterday</th>
										<th>PV Yesterday</th>
									</tr>
								</thead>
								<tbody>
									<s:iterator value="vos">

										<tr>
											<td width="30%">${title}</td>
											<td>${totalPv}</td>
											<td>${totalUv}</td>
											<td>${currentPv}</td>
											<td>${currentUv}</td>
											<td>${lastPv}</td>
											<td>${lastUv}</td>
										</tr>
									</s:iterator>
								</tbody>
							</table>
</s:if>
						</div>

					</div>
					<!--/span-->

				</div>
				<!--/row-->
				<!-- content ends -->

<s:if test="vos.size>0">
				<div class="row-fluid sortable">
					<div class="box span12">
						<div class="box-content">


							<div id="main" style=" height:800px"></div>
							<script type="text/javascript">
								require.config({
									packages : [ {
										name : 'echarts',
										location : '/js/echarts/src',
										main : 'echarts'
									}, {
										name : 'zrender',
										location : '/js/zrender/src',// zrender与echarts在同一级目录
										main : 'zrender'
									} ]
								});

								// 使用
								require(
										[ 'echarts', 'echarts/chart/bar' // 使用柱状图就加载bar模块，按需加载
										],
										function(ec) {
											// 基于准备好的dom，初始化echarts图表
											var myChart = ec.init(document
													.getElementById('main'));

											var option = {
												title : {
													text : '',
													subtext : ''
												},
												tooltip : {
													trigger : 'axis'
												},
												legend : {
													data : [ 'pv', 'uv' ]
												},
												toolbox : {
													show : true,
													feature : {

														saveAsImage : {
															show : true
														}
													}
												},
												//calculable : true,

												yAxis : [ {
													type : 'category',
													data : [ <s:property value="xdata"/> ],
													axisLabel : {
														show : true,
														interval : 0, // {number}
														rotate : 25,
														margin : 0

													}

												} ],
												xAxis : [ {
													type : 'value'
												} ],
												series : [ <s:property value="seriesStr"/> ]
											};

											// 为echarts对象加载数据 
											myChart.setOption(option);
										});
							</script>
						</div>
					</div>
				</div>
</s:if>
			</div>
			<!--/#content.span10-->
		</div>
		<!--/fluid-row-->

		<hr>
		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

	<script>
		$(document).ready(function() {

		});
		
		//查询
		function search(){
			if ($("#companyId").val() == 1) {
				alert("please select a company.");
				return;
			}
			$("#queryForm").submit();
		}
		
		function setMediaList(mediaList) {

			if (!mediaList)
				return;

			var mediaListSelect = $("#mediaListId");
			if (!mediaListSelect)
				return;

			mediaListSelect.html("");
			///添加默认分组
			for( var i = 0; i < mediaList.length; i++) {
				mediaListSelect.append("<option value='"+mediaList[i].id+"'>" + mediaList[i].title
						+ "</option>");
			}
		};

		function onCompanyChange(companyId) {

			loadCompanyMediaList(companyId, setMediaList);
		};
		
	</script>



</body>
</html>
