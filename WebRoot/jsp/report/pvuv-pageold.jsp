<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragrma", "no-cache");
	response.setDateHeader("Expires", 0);
%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>WiFun Console</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta HTTP-EQUIV="pragma" content="no-cache">
<meta HTTP-EQUIV="Cache-Control" content="no-store, must-revalidate">
<meta HTTP-EQUIV="expires" content="0">
<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
<style type="text/css">
table { //
	width: 100px;
	table-layout: fixed; /* 只有定义了表格的布局算法为fixed，下面td的定义才能起作用。 */
}

td { //
	width: 100%;
	word-break: keep-all; /* 不换行 */
	white-space: nowrap; /* 不换行 */
	overflow: hidden; /* 内容超出宽度时隐藏超出部分的内容 */
	text-overflow: ellipsis;
	/* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用*/
}
</style>

<script type="text/javascript" src="/js/echarts/echarts.js"></script>
<script type="text/javascript" src="/js/echarts/esl.js"></script>

</head>

<body>
	<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
	<div class="container-fluid">
		<div class="row-fluid">

			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
				<s:param name="type">18</s:param>
			</s:action>
			<!-- left menu ends -->

			<div id="content" class="span10">
				<!-- content starts -->
				<div>
					<ul class="breadcrumb">
						<li><a href="#">Report Management</a> <span class="divider">/</span>
						</li>
						<li><a href="#">Page PVUV</a>
						</li>
					</ul>
				</div>

				<div class="row-fluid sortable">

					<div class="box span12">
						<font color="red"><s:property value="errCode" /> </font>
						<div class="box-content">
<s:if test="#session['account_session'].type==1">
							<form action="/pvuvPageReport/report-pagepvuv-search.action"
								method="post" id="queryForm">
								<table>
									<tr>

											<td class="control-label"><label class="control-label">Company</label>
											</td>
											<td class="control-label">
												<div class="controls">
													<select id="companyId" name="companyId">
														<s:iterator value="companyList">
															<option value="${id}"
																<s:if test="id == companyId">selected="selected"</s:if>>${name}</option>
														</s:iterator>
													</select>
												</div></td>

										<td>
											<button type="submit" class="btn">Search</button>
										</td>
									</tr>
								</table>
							</form>
</s:if>
							<table id="tbl"
								class="table table-striped table-bordered bootstrap-datatable datatable">
								<thead>
									<tr>
										<th>Type</th>
										<th>PV Total</th>
										<th>UV Total</th>
										<th>PV Current</th>
										<th>UV Current</th>
										<th>PV Yesterday</th>
										<th>PV Yesterday</th>
									</tr>
								</thead>
								<tbody>
									<s:iterator value="vos">

										<tr>
										
											<td>${type}</td>
											<td>${totalPv}</td>
											<td>${totalUv}</td>
											<td>${currentPv}</td>
											<td>${currentUv}</td>
											<td>${lastPv}</td>
											<td>${lastPv}</td>
										</tr>
									</s:iterator>
								</tbody>
							</table>
						</div>

					</div>
					<!--/span-->

				</div>
				<!--/row-->
				<!-- content ends -->


			</div>
			<!--/#content.span10-->
		</div>
		<!--/fluid-row-->

		<hr>
		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

	<script>
		$(document).ready(function() {

		});
		
		//查询
		function search(){
			if ($("#companyId").val() == 1) {
				alert("please select a company.");
				return;
			}
			$("#queryForm").submit();
		}
	</script>



</body>
</html>
