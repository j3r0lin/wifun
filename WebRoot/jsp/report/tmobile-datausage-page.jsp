<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragrma", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta HTTP-EQUIV="pragma" content="no-cache">
<meta HTTP-EQUIV="Cache-Control" content="no-store, must-revalidate">
<meta HTTP-EQUIV="expires" content="0">
<style>
.newtable {
	margin: 20px;
	width: 600px;
	border-collapse: collapse;
	border-spacing: 0;
	text-align: center;
	font-size: 12px;
	font-family: Arial;
}

.newtable th {
	font-weight: bold;
}

.newtable td,.newtable th {
	height: 30px;
	line-height: 30px;
	border: 1px solid #999;
}

.newtable td {
	cursor: pointer;
}

.newtable td label {
	height: 30px;
	line-height: 30px;
	cursor: pointer;
}
</style>
<title>WiFun Console</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>

</head>

<body>
	<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
	<div class="container-fluid">
		<div class="row-fluid">

			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
				<s:param name="type">19</s:param>
			</s:action>
			<!-- left menu ends -->



			<div id="content" class="span10">
				<!-- content starts -->


				<div>
					<ul class="breadcrumb">
						<li><a href="#">T-mobile data usage Report</a></li>
					</ul>
				</div>
				<!-- search starts -->
				<div>
					<div class="span12 my-search">
						<div>
							<h3>Search By</h3>
							<div class="my-filters">
								<form action="/TmobileDataUsageReport/report-tmoible-datausage-search.action"
									method="post" id="queryForm">
									<table>
										<tr>
										
											<td>
												<label class="control-label" style="float: right;">Date</label>
											</td>
											<td>
												<div class="controls">
													<input id="date" name="date" style="width:130px" type="text" class="datepicker" value="${date}">
												</div>
											</td>
											<td>
												<button type="submit" class="btn">Search</button></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- /search ends -->
				<div class="row-fluid sortable">
					<div class="box span12">
<!-- 						<font color="red"><s:property value="errCode" /> </font> -->
						<div class="box-content">
							<s:if test="vos==null or vos.size()==0">
								<div style="padding: 20px;" id="nullScheduleAlert">
									<h3>There are no matching datas in the system.</h3>
								</div>
							</s:if>
							<s:if test="vos.size>0">

								<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<th rowspan="2">Phone num</th>
											
											<th rowspan="2">Data usage total</th>
											<th rowspan="2">Current usage date</th>
											<th rowspan="2">Bill cycle</th>
											<th rowspan="2">Days remain in cycle</th>											
											
											
											<th colspan="4"  style="text-align: center;">Included data</th>
											
											<th colspan="4" style="text-align: center;">Reduced-speed data</th>
											
											
										</tr>
										<tr>
											
											<th>Used</th>
											<th>Included</th>	
											<th>Remaining</th>
											<th>charges</th>
											
											<th>Used</th>
											<th>Included</th>	
											<th>Remaining</th>
											<th>charges</th>
											
										</tr>
										
										
									</thead>
									<tbody>
										<s:iterator value="vos">

											<tr>
												<td>${phoneNum}</td>
												<td>${dataUsageAll}</td>
												<td>${currentUsageTime}</td>
												<td>${billCycle }</td>
												<td>${daysRemainingInCycle}</td>
												
												<td>${usedInclude}</td>
												<td>${includedInclude}</td>
												<td>${remainingInclude}</td>
												<td>${chargesInclude}</td>
												
												<td>${usedReduced}</td>
												<td>${includedReduced}</td>
												<td>${remainingReduced}</td>
												<td>${chargesReduced}</td>
												
											</tr>
										</s:iterator>
									</tbody>
								</table>

							</s:if>
						</div>
					</div>
					<!--/span-->
				</div>
				<!--/row-->
				<!-- content ends -->
			</div>
			<!--/#content.span10-->
		</div>
		<!--/fluid-row-->
		
		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->
	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	
	<script type="text/javascript">
		function setGroupList(groupList) {

			if (!groupList)
				return;

			var groupSelect = $("#groupId");
			if (!groupSelect)
				return;

			groupSelect.html("");
			///添加默认分组
			groupSelect.append("<option value='-1'>All</option>");
			///添加默认分组
			groupSelect.append("<option value='0'>default group</option>");
			for( var i = 0; i < groupList.length; i++) {
				groupSelect.append("<option value='"+groupList[i].id+"'>" + groupList[i].name
						+ "</option>");
			}
		};

		function onCompanyChange(companyId) {

			loadCompanyGroups(companyId, setGroupList);
		};
	</script>
</body>
</html>
