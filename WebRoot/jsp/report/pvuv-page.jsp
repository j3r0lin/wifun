<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragrma", "no-cache");
	response.setDateHeader("Expires", 0);
%>

<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>WiFun Console</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta HTTP-EQUIV="pragma" content="no-cache">
<meta HTTP-EQUIV="Cache-Control" content="no-store, must-revalidate">
<meta HTTP-EQUIV="expires" content="0">
<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
<style type="text/css">
table { //
	width: 100px;
	table-layout: fixed; /* 只有定义了表格的布局算法为fixed，下面td的定义才能起作用。 */
}

td { //
	width: 100%;
	word-break: keep-all; /* 不换行 */
	white-space: nowrap; /* 不换行 */
	overflow: hidden; /* 内容超出宽度时隐藏超出部分的内容 */
	text-overflow: ellipsis;
	/* 当对象内文本溢出时显示省略标记(...) ；需与overflow:hidden;一起使用*/
}

.chart-loading{
	  width: 100%;
	  height: 100%;
	  position: absolute;
	  top: 0;
	  left: 0;
	  z-index: 100;
	  text-align: center;
	  background-color: rgba(0, 0, 0, 0.5);
}
</style>
<script type="text/javascript" src="/js/echarts/echarts-all.js"></script>
<%-- <script type="text/javascript" src="/js/echarts/echarts.js"></script> --%>
<%-- <script type="text/javascript" src="/js/echarts/esl.js"></script> --%>

</head>

<body>
	<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
	<div class="container-fluid">
		<div class="row-fluid">

			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
				<s:param name="type">18</s:param>
			</s:action>
			<!-- left menu ends -->

			<div id="content" class="span10">
				<!-- content starts -->
				<div>
					<ul class="breadcrumb">
						<li><a href="#">Report Management</a> <span class="divider">/</span>
						</li>
						<li><a href="#">Page PVUV</a></li>
					</ul>
				</div>

				<!-- search starts -->
				<s:if test="#session['account_session'].type==1">
					<div>
						<div class="span12 my-search">
							<div>
								<h3>Search By</h3>
								<div class="my-filters">
									<form action="/pvuvPageReport/report-pagepvuv-search.action"
										method="post" id="queryForm">
										<table>
											<tr>

												<td class="control-label"><label class="control-label">Company</label>
												</td>
												<td class="control-label">
													<div class="controls">
														<select id="companyId" name="companyId">
															<s:iterator value="companyList">
																<option value="${id}"
																	<s:if test="id == companyId">selected="selected"</s:if>>${name}</option>
															</s:iterator>
														</select>
													</div>
												</td>
												<td>
												<button type="submit" class="btn">Search</button></td>
											</tr>
										</table>
									</form>
								</div>
							</div>
						</div>
					</div>
				</s:if>
				<div class="row-fluid sortable">


					<div class="box span12">
						<div class="box-content">
							<s:if test="vos==null or vos.size()==0">
								<div style="padding: 20px;" id="nullScheduleAlert">
									<h3>There are no matching datas in the system.</h3>
								</div>
							</s:if>
							<s:if test="vos.size>0">
								<table id="tbl" class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>Checked</th>
											<th>Type</th>
											<th>PV Total</th>
											<th>UV Total</th>
											<th>PV Today</th>
											<th>UV Today</th>
											<th>PV Yesterday</th>
											<th>UV Yesterday</th>
										</tr>
									</thead>
									<tbody>
										<s:iterator value="vos">
											<tr>
												<td>
													<input type="radio" name="checkType" value="${type}" />
												</td>
												<td sid="type">${type}</td>
												<td>${totalPv}</td>
												<td>${totalUv}</td>
												<td>${currentPv}</td>
												<td>${currentUv}</td>
												<td>${lastPv}</td>
												<td>${lastUv}</td>
											</tr>
										</s:iterator>
									</tbody>
								</table>
							</s:if>
						</div>
					</div>
				</div>
				<div id="pvuc-rowfluid" class="row-fluid sortable" style="display:none">
					<div class="box span12" style="display: block;position: relative;">
					
						<div id="chart-title" class="box-header well"><H2>PVUV Chart</H2></div>
						<div id="nullChartData"  class="box-content" style="height:464px;display:none">
								<h3>There are no matching datas in the system.</h3>
						</div>
						
						<div id="chart-loading" class="chart-loading">
							<div style=" display:inline-block;margin-top: 282px;">
								<img src="/img/ajax-loaders/ajax-loader-5.gif" title="img/ajax-loaders/ajax-loader-1.gif">
								<br/>
								<span style="color:#fff">Loading chart....</span>
							</div>
						</div>
						<div id="chart-box" class="box-content">
							<div id="queryForm" style="padding-bottom:10px">
								<table>
									<tr>
										<td class="control-group">
											<label class="control-label" style="float: right;">Start Date</label>
										</td>
										<td>
											<div class="controls">
												<input id="startDate" style="width:130px" type="text"
													class="datepicker" name="startDate"
													value="${startDate}">
											</div></td>
										<td><label class="control-label" style="float: right;">End Date</label>
										</td>
										<td>
											<div class="controls">
												<input id="endDate" style="width:130px" type="text"
													class="datepicker"  name="endDate"
													value="${endDate}">
											</div>
										</td>
										<td>
											<div class="controls">
												<input id="btnLoadChart" style="width:130px" type="button"  name="startDate"
												 class="btn"	value="Load Chart">
											</div>
										</td>
									</tr>
								</table>
							</div>

							<div id="lineChart" style="width:100%;height:400px"></div>
						</div>
					</div>
				</div>
				<!--/row-->
				<!-- content ends -->

			</div>
			<!--/#content.span10-->
		</div>
		<!--/fluid-row-->

		<hr>
		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

	<script>
		//查询
		function search() {
			if ($("#companyId").val() == 1) {
				alert("please select a company.");
				return;
			}
			$("#queryForm").submit();
		}

		$("#tbl tbody tr").click(function() {
		
			$("#tbl tbody").find(".radio span").removeClass("checked");
			$(this).find(".radio span").addClass("checked");
			
			var type = $(this).find("td[sid=type]").text();
			//$("#chart-title h2").html(type+" PVUV Chart");
			loadChart(type);
		});
		
		$("#btnLoadChart").click(function(){
			var type = $("#tbl tbody tr .radio span").find("input[type=radio]").val();
			loadChart(type);
		});
	</script>

	<script>
		
		if($("#nullScheduleAlert").length<=0){
			$("#pvuc-rowfluid").show();
		}
		
		var radio = $("#tbl").find("tbody tr").first().find("input[type=radio]");
		radio.attr("checked","checked");
		//$("#chart-title h2").html(radio.val()+" PVUV Chart");
		
		loadChart(null);
		
		function loadChart(type) {
		
			showChartLoading();
			
			var startDate = $("#startDate").val();
			if (startDate == undefined) {
				alert("startDate is empty");
				return;
			}

			var endDate = $("#endDate").val();
			if (startDate == undefined) {
				alert("endDate is empty");
				return;
			}
			
			if(startDate>endDate){
				alert("start date bigger than end date.");
			}

			if (type == null) {
				type = $("#tbl").find("tbody tr").first().find("td[sid=type]").text();
			}

			var companyId = $("#companyId").val();

			if(!companyId || companyId.length<1){
				companyId = -1;
			}

			$.ajax({
				type : "get",
				url : "/pvuvPageReport/page-pvuv-detail.action",
				data : {
					startDate : startDate,
					endDate : endDate,
					type : type,
					companyId : companyId
				},
				async : true,
				success : function(data) {
					hideChartLoading();
					showChartData();
					if (!data) {
						return;
					}
					dataList = JSON.parse(data).pvuvList;

					var names = [];
					var pvs = [];
					var uvs = [];

					if (dataList) {
						$.each(dataList, function(index, item) {
							names.push(item.date);
							pvs.push(item.pv);
							uvs.push(item.uv);
						});
					}
					
					if(!dataList || dataList.length<1){
						showNullChartData();
						return;
					}
					
					initChart(names, pvs, uvs);

				},
				error : function() {
					showNullChartData();
				}
			});

		}

		function initChart(names, pvs, uvs) {
		
			showChartData();
			var myChart = echarts.init(document.getElementById('lineChart'));

			var option = {
				title : {
					//text : '未来一周气温变化',
					subtext : ''
				},
				tooltip : {
					trigger : 'axis'
				},
				legend : {
					data : [ 'pv', 'uv' ]
				},
				toolboxs : {
					show : true,
					feature : {
						mark : {
							show : true
						},
						dataView : {
							show : true,
							readOnly : false
						},
						magicType : {
							show : true,
							type : [ 'line', 'bar' ]
						},
						restore : {
							show : true
						},
						saveAsImage : {
							show : true
						}
					}
				},
				calculable : true,
				xAxis : [ {
					type : 'category',
					boundaryGap : false,
					data : names,
				} ],
				yAxis : [ {
					type : 'value',
					axisLabel : {
						formatter : '{value}'
					}
				} ],
				series : [ {
					name : 'pv',
					type : 'line',
					data : pvs,
					markPoint : {
						data : [ {
							type : 'max',
							name : 'maxVaue'
						}, {
							type : 'min',
							name : 'minValue'
						} ]
					},
					markLine : {
						data : [ {
							type : 'average',
							name : 'averageValue'
						} ]
					}
				}, {
					name : 'uv',
					type : 'line',
					data : uvs,
					markPoint : {
						data : [ {
							type : 'max',
							name : 'maxVaue'
						}, {
							type : 'min',
							name : 'minValue'
						} ]
					},
					markLine : {
						data : [ {
							type : 'average',
							name : 'averageValue'
						} ]
					}
				} ]
			};

			// 为echarts对象加载数据 
			myChart.setOption(option);
		}
		
		function showChartData(){
			$("#nullChartData").hide();	
			$("#chart-box").show();
		}
		function showNullChartData(){
		
			$("#chart-box").hide();
			$("#nullChartData").show();
		}
		
		function showChartLoading(){
			$("#chart-loading").show();
		}
		
		function hideChartLoading(){
			$("#chart-loading").hide();
		};
	</script>
</body>
</html>
