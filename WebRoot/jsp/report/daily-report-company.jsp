<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragrma", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta HTTP-EQUIV="pragma" content="no-cache">
<meta HTTP-EQUIV="Cache-Control" content="no-store, must-revalidate">
<meta HTTP-EQUIV="expires" content="0">
<style>
.newtable {
	margin: 20px;
	width: 600px;
	border-collapse: collapse;
	border-spacing: 0;
	text-align: center;
	font-size: 12px;
	font-family: Arial;
}

.newtable th {
	font-weight: bold;
}

.newtable td,.newtable th {
	height: 30px;
	line-height: 30px;
	border: 1px solid #999;
}

.newtable td {
	cursor: pointer;
}

.newtable td label {
	height: 30px;
	line-height: 30px;
	cursor: pointer;
}
</style>
<title>WiFun Console</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>

</head>

<body>
	<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
	<div class="container-fluid">
		<div class="row-fluid">

			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
				<s:param name="type">15</s:param>
			</s:action>
			<!-- left menu ends -->



			<div id="content" class="span10">
				<!-- content starts -->


				<div>
					<ul class="breadcrumb">
						<li><a href="#">Daily Report</a></li>
					</ul>
				</div>
				<!-- search starts -->
				<div>
					<div class="span12 my-search">
						<div>
							<h3>Search By</h3>
							<div class="my-filters">
								<form action="/report/daily-report-company-search.action"
									method="post" id="queryForm">
									<table>
										<tr>
											<td>
												<label class="control-label" style="float: right;">Date</label>
											</td>
											<td>
												<div class="controls">
													<input id="date" name="date" style="width:130px" type="text" class="datepicker" value="${date}">
												</div>
											</td>
											<td>
												<button type="submit" class="btn">Search</button></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- /search ends -->
				<div class="row-fluid sortable">
					<div class="box span12">
<!-- 						<font color="red"><s:property value="errCode" /> </font> -->
						<div class="box-content">
							<s:if test="dailyList==null or dailyList.size()==0">
								<div style="padding: 20px;" id="nullScheduleAlert">
									<h3>There are no matching datas in the system.</h3>
								</div>
							</s:if>
							<s:if test="dailyList.size>0">

								<table class="table table-striped table-bordered">
									<thead>
										<tr>
											<th>Company</th>
											
											<th>Home PV</th>
											<th>Home UV</th>
											<th>Main PV</th>
											<th>Main UV</th>
											<th>Movie PV</th>
											<th>Movie UV</th>
											
											<th># of new discovered mobile device</th>
											<th>Total # of mobile device since on-board</th>
											<th># of mobile device connected longer than 1 hour</th>
											
											<th># of new mobile device request to surf</th>
											<th>Total # of device request to surf</th>
											
											<th>Traffic Total</th>
<!-- 											<th>Action</th> -->
										</tr>
									</thead>
									<tbody>
										<s:iterator value="dailyList">

											<tr>
												<td>
													<a href="javascript:void(0);" onclick="showDetail(${companyId});">${companyName}</a>
												</td>
												
												<td>${pvHome}</td>
												<td>${uvHome}</td>
												<td>${pvMain}</td>
												<td>${uvMain}</td>
												<td>${pvMovie}</td>
												<td>${uvMovie}</td>
												
												<td>${deviceDailyAdd}</td>
												<td>${deviceTotal}</td>
												<td>${deviceOneHourOnline}</td>
												
												<td>${userDailyAdd}</td>
												<td>${userTotal}</td>
												
												<td>
<!-- 													${trafficTotal}MB -->
													<s:property value="@com.wifun.admin.util.MathUtil@bytes2MB(trafficTotal,3)" /> MB
												</td>
<!-- 												<td class="center"> -->
<!-- 													<a class="btn btn-success" href="javascript:void(0);" onclick="showDetail(${companyId});"> <i class="icon-list icon-white"></i> -->
<!-- 													Detail </a>  -->
<!-- 												</td> -->
											</tr>
										</s:iterator>
									</tbody>
								</table>

							</s:if>
						</div>
					</div>
					<!--/span-->
				</div>
				<!--/row-->
				<!-- content ends -->
			</div>
			<!--/#content.span10-->
		</div>
		<!--/fluid-row-->
		
		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->
	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	
	<script type="text/javascript">
	
		function showDetail(companyId) {
			
			var date = $("#date").val();
			
			window.location.href = "/report/daily-report-search.action?groupId=-1&companyId="+companyId+"&date="+date;
		}
	</script>
</body>
</html>
