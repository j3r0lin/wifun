<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%
	response.setHeader("Cache-Control", "no-store");
	response.setHeader("Pragrma", "no-cache");
	response.setDateHeader("Expires", 0);
%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta HTTP-EQUIV="pragma" content="no-cache">
<meta HTTP-EQUIV="Cache-Control" content="no-store, must-revalidate">
<meta HTTP-EQUIV="expires" content="0">
<style>
.newtable {
	margin: 20px;
	width: 600px;
	border-collapse: collapse;
	border-spacing: 0;
	text-align: center;
	font-size: 12px;
	font-family: Arial;
}

.newtable th {
	font-weight: bold;
}

.newtable td,.newtable th {
	height: 30px;
	line-height: 30px;
	border: 1px solid #999;
}

.newtable td {
	cursor: pointer;
}

.newtable td label {
	height: 30px;
	line-height: 30px;
	cursor: pointer;
}
</style>
<title>WiFun Console</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>

</head>

<body>
	<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
	<div class="container-fluid">
		<div class="row-fluid">

			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
				<s:param name="type">14</s:param>
			</s:action>
			<!-- left menu ends -->
			<div id="content" class="span10">
				<!-- content starts -->
				<div>
					<ul class="breadcrumb">
						<li><a href="#">Report traffic</a>
						</li>
					</ul>
				</div>
				<!-- search starts -->
				<div>
					<div class="span12 my-search">
						<div>
							<h3>Search By</h3>
							<div class="my-filters">
								<form action="/trafficReport/trafficSum-search.action"
									method="post" id="queryForm">
									<table>
										<tr>
											<s:if test="#session['account_session'].type==1">
												<td class="control-label"><label class="control-label">Company</label>
												</td>
												<td class="control-label">
													<div class="controls">
														<select id="companyId" name="companyId">
															<s:iterator value="companyList">
																<option value="${id}"
																	<s:if test="id == companyId">selected="selected"</s:if>>${name}</option>
															</s:iterator>
														</select>
													</div></td>
											</s:if>
											<td><label class="control-label">Group</label></td>
											<td>
												<div class="controls">
													<select id="groupId" name="groupId" value="${groupId}">
														<option value="-1" <s:if test="-1 == groupId">selected="selected"</s:if>>All</option>
														<option value="0" <s:if test="0 == groupId">selected="selected"</s:if>>default group</option>
														<s:iterator value="groupList">
															<option value="${id}" <s:if test="id == groupId">selected="selected"</s:if>>${name}</option>
														</s:iterator>
													</select>
												</div>
											</td>
											<td><label class="control-label"> Month:</label>
											</td>
											<td>
												<div class="controls">
													<input id="month" name="month" type="text" value="${month}"
														placeholder="input month like 201507">
												</div>
											</td>
											<td>
												<button type="button" onclick="search();" class="btn">Search</button>
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- /search ends -->
				<div class="row-fluid sortable">

					<div class="box span12">
						<div class="box-content">

							<s:if test="vos==null or vos.size()==0">
								<div style="padding: 20px;" id="nullScheduleAlert">
									<h3>There are no matching datas in the system.</h3>
								</div>
							</s:if>
							<s:if test="vos.size>0">

								<table id="tbl"
									class="table table-striped table-bordered bootstrap-datatable datatable">
									<thead>
										<tr>
											<th>Device Name</th>
											<th>Group Name</th>
											<th>Send</th>
											<th>Receive</th>
											<th>Total</th>
											<th>Max Daily</th>
										</tr>
									</thead>
									<tbody>
										<s:iterator value="vos">

											<tr class="btn-setting" id="${inhandId}"
												onclick="showDailyDetail(this.id,$('#month').val());">
												<td>${deviceName}</td>
												<td>${groupName}</td>
												<td>${send}MB</td>
												<td>${receive}MB</td>
												<td>${total}MB</td>
												<td>${max}MB</td>
											</tr>
										</s:iterator>
									</tbody>
								</table>

							</s:if>
						</div>
					</div>
					<!--/span-->

				</div>
				<!--/row-->
				<!-- content ends -->
			</div>
			<!--/#content.span10-->
		</div>
		<!--/fluid-row-->

		<hr>

		<div class="modal hide fade" id="myModal" style="width: 700px">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Traffic Daily Detail</h3>
			</div>
			<div class="modal-body">

				<table class="newtable">
					<thead>
						<tr>
							<th>Date</th>
							<th>Send Daily</th>
							<th>Receive Daily</th>
							<th>Total Daily</th>
						</tr>
					</thead>

					<tbody>
					</tbody>
				</table>

			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	<script type="text/javascript">
		function showDailyDetail(inhandId, month) {
			//alert("asdfsdfas=="+deviceID);

			//输入校验
			if (month.replace(/(^s*)|(s*$)/g, "").length == 0) {
				alert('Please enter month.');
			}

			var modalView = $("#myModal");

			modalView.find("table tbody").empty();
			$.ajax({
						type : "POST",
						url : "/trafficReport/trafficDailyDetail.action",
						data : {
							inhandId : inhandId,
							month : month,
							companyId:$('#companyId').val()
						},
						dataType : 'json',
						success : function(data) {
							if (data != null && data.dailyList != null
									&& data.dailyList.length > 0) {
								var dailyList = data.dailyList;
								//alert(JSON.stringify(dailyList));
								for ( var index in dailyList) {

									var trData = dailyList[index];
									//alert(trData);
									new TRObject(trData, index, modalView);
								}
								//modalView.find("tbody").append("<tr><td colspan=3>There are no File.</td></tr>");
							} else {
								modalView
										.find("table tbody")
										.append(
												"<tr><td colspan=4>There are no datas.</td></tr>");
							}

						},
						error : function(e) {
						}
					});
		}
	</script>

	<script type="text/javascript">
		var TRObject = function(data, index, container) {

			this.date = data.date;
			this.send = data.send;
			this.recv = data.receive;
			this.total = data.total;

			this.view;
			this.container = container;

			this.init();
		};

		TRObject.prototype.init = function() {

			this.view = $("<tr role='row' class='odd'></tr>");
			if (this.index % 2 == 0)
				this.view.addClass("odd");
			else
				this.view.addClass("even");

			this.view.append("<td>" + this.date + "</td>");
			this.view.append("<td>" + this.send + "MB</td>");
			this.view.append("<td>" + this.recv + "MB</td>");
			this.view.append("<td>" + this.total + "MB</td>");
			//alert(this.view);
			this.container.find("tbody").append(this.view);

			//this.view.bind("click", this, this.onFileClick);
		};
	</script>
	<script type="text/javascript">

		function search() {
			if ($("#companyId").val() == 1) {
				alert("please select a company.");
				return;
			}
			
			//输入校验
			var mon=$('#month').val();
			 mon=$.trim(mon);//去掉空格
			 $('#month').val(mon);
			
			//var mon=trim(mon);
			if ( (mon.length == 0) || (mon.length!=6))
			{
				alert('Please enter valid month.');
				return;
			}
			else 
			{
			for(var i=0; i<mon.length; i++)
			{
			  if(mon.charAt(i)<'0' || mon.charAt(i)>'9')
				{
				alert("Please enter valid month");
				return ;
				}
			}

			  var monStr=mon.substr(4, 2);
			  var tmpInt2= parseInt(monStr);
			 
			  if ((tmpInt2>12)||(tmpInt2==0))
			  {
			   alert("month must between 1 to 12");
			   return;
			  }
			    
			  
			}
			

			$("#queryForm").submit();
		}
	</script>
	
	<script>
	function setGroupList(groupList) {

		if (!groupList)
			return;

		var groupSelect = $("#groupId");
		if (!groupSelect)
			return;

		groupSelect.html("");
		///添加默认分组
		groupSelect.append("<option value='-1'>All</option>");
		///添加默认分组
		groupSelect.append("<option value='0'>default group</option>");
		for( var i = 0; i < groupList.length; i++) {
			groupSelect.append("<option value='"+groupList[i].id+"'>" + groupList[i].name
					+ "</option>");
		}
	};

	function onCompanyChange(companyId) {
		loadCompanyGroups(companyId, setGroupList);
	};
	
	$("#companyId").change(function(){
		var companyId = $(this).val();
		onCompanyChange(companyId);
	});
	</script>


</body>
</html>
