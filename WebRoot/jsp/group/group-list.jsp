<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
				<s:action name="menu" executeResult="true">
					<s:param name="type">2</s:param>
				</s:action>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Group Management</a>
					</li>
				</ul>
			</div>
<!-- search starts -->
<div>
	<div class="span12 my-search">
		<div>
			<h3>Search By</h3>
			<div class="my-filters">
				<form action="/group/group-search.action" method="post" id="queryForm">
					<table>
						<tr>
<s:if test="#session['account_session'].type==1">
							<td>
							<label class="control-label">Company</label>
							</td>
							<td>
								<div class="controls">
								  <select id="companyId" name="companyId">
									<s:iterator value="companyList">
									 <option value="${id}" <s:if test="id == companyId">selected="selected"</s:if>>${name}</option>
									 </s:iterator>
								  </select>
								</div>
							</td>
</s:if>
							<td>
								<label class="control-label"> Group Name</label>
							</td>
							<td>
								<div class="controls">
									<input id="groupName" type="text" name="groupName" value="${groupName }">
								</div>
							</td>
							<td>
								<button type="button" onclick="$('#queryForm').submit();" style="margin-bottom: 10px;" class="btn">
									Search
								</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /search ends -->   								  
       <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Groups</h2>
						<div style="float:right;">
									<a class="btn btn-primary"  href="/group/group-add.action" >
									<i class="icon icon-white icon-plus"></i>&nbsp;Add Group </a>
						</div>	
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>ID</th>
								  <th>Group Name</th>
								  <th>Company</th>
								  <th>Devices</th>
								  <th>Status</th>
								  <th>Create Date</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
 <s:iterator value="groupList" >
							<tr>
								<td>${id}</td>
								<td>${name}</td>
								<td>${company.name}</td>
								<td class="center">
									<a href="/device/device-list.action?companyId=${companyId }&groupId=${id}">
										<span class="label label-success">${deviceNormalNum }</span>
										/
										<span class="label label-success">${deviceTotalNum }</span>
									</a> 
								</td>
								<td class="center">
									<s:if test="status==1">
										<span class="label label-success">Active</span>
									</s:if> <s:else>
										<span class="label label-warning"> Inactive </span>
									</s:else>
								</td>
								<td class="center">
									<s:property value="@com.wifun.admin.util.TimeUtils@DateToString(createDate,'MM/dd/yy HH:mm')" />
								</td>
								<td class="center">
									<a class="btn btn-info" href="/group/group-edit.action?id=${id}">
										 <i class="icon-edit icon-white"></i> Edit </a>
									<a class="btn btn-danger" href="/jsp/group/group-delete-confirm.jsp?id=${id}">
										<i class="icon-trash icon-white"></i> 
										Delete
									</a>
								</td>
							</tr>
</s:iterator>						
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	
	
</body>
</html>
