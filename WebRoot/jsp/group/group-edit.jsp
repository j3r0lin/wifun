<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>WiFun</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
	<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
	<div class="container-fluid">
		<div class="row-fluid">

			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
				<s:param name="type">2</s:param>
			</s:action>
			<!-- left menu ends -->

			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>
						You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
						enabled to use this site.
					</p>
				</div>
			</noscript>

			<div id="content" class="span10">
				<!-- content starts -->


				<div>
					<ul class="breadcrumb">
						<li><a href="#">Group Edit</a> </li>
					</ul>
				</div>


				<div class="row-fluid sortable">
					<div class="box span12">
						<div class="box-header well" data-original-title>
							<h2>
								<i class="icon-edit"></i> Group Info
							</h2>
							<div class="box-icon" style="padding-right: 20px;">
								<a class="back" href="javascript:history.go(-1);" title="Back"><span
									class="icon32 icon-blue icon-undo"></span> </a>
							</div>
						</div>

						<div class="box-content">
							<form class="form-horizontal" id="subForm" name="subForm"
								action="/group/group-edit-save.action" method="post">
								<input type="hidden" id="id" name="id" value="${id}" />
								<input type="hidden" id="companyId" name="companyId" value="${vo.companyId}" />
								<fieldset>
									<div class="control-group" id="group_name">
										<label class="control-label">Name</label>
										<div class="controls">
											<input id="name" type="text" name="vo.name" value="${vo.name}" onblur="outBlur(this.id);"
												msg="Please enter group name" maxlength="64">
											<span class="help-inline" id="msg_name"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Status</label>
										<div class="controls">
											<select id="status" name="vo.status">
												<option value="1" <s:if test="vo.status == 1">selected="selected"</s:if>>Active</option>
												<option value="0" <s:if test="vo.status == 0">selected="selected"</s:if>>Inactive</option>
											</select>
										</div>
									</div>

									<div class="form-actions">
										<button id="submit-btn" type="button" onclick="submitForm();" class="btn btn-primary">
											Save</button>
										<button class="btn" type="reset"
											onclick="window.location.href='javascript:history.go(-1);'">Cancel</button>
									</div>
								</fieldset>
							</form>

						</div>
					</div>
					<!--/span-->

				</div>
				<!--/row-->

				<!-- content ends -->
			</div>
			<!--/#content.span10-->
		</div>
		<!--/fluid-row-->

		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a> <a href="#" class="btn btn-primary">Save
					changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	<script type="text/javascript">
		function submitForm() {

			alertTogger(false);
			$("#name").val($.trim($("#name").val()));
			if (!outBlur("name")) {
				alertTogger(true);
				return;
			}
			//alert("submit");
			$("#subForm").submit();
		};

		//失去焦点
		function outBlur(id) {

			if ($.trim($("#" + id).val()) == '') {
				$("#group_" + id).addClass("error");
				var msg = $("#" + id).attr("msg");
				$("#msg_" + id).text(msg);
			} else {
				$("#group_" + id).removeClass("error");
				$("#msg_" + id).text("");
				return true;
			}
			return false;
		};

		function alertTogger(swt) {

			if (!swt) {
				//禁用提交按钮
				$("#submit-btn").attr("disabled", true);
				//展示loading 
				$(".waitting").show();
				//隐藏提示信息
				$("#result-alert").hide();
			} else {
				$("#submit-btn").attr("disabled", false);
				//展示loading 
				$(".waitting").hide();
				//展示提示信息
				$("#result-alert").show();
			}
		};
	</script>
</body>
</html>
