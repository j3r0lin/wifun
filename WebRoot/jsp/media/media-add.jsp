<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
					<s:param name="type">9</s:param>
				</s:action>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#"> Media Add </a>
					</li>
				</ul>
			</div>
								  
      
			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-edit"></i> Media Info</h2>
						<div class="box-icon" style="padding-right: 20px;">
								<a class="back" href="javascript:history.go(-1);" title="Back"><span
									class="icon32 icon-blue icon-undo"></span> </a>
							</div>
					</div>
					
					<div class="box-content">
						<form class="form-horizontal" id="subForm" name="subForm"
									action="/media/media-add-save.action" method="post">
							<fieldset>
							  <div class="control-group" id="group_title">
								<label class="control-label">Title</label>
								<div class="controls">
								  <input id="title" name="vo.title" type="text" onblur="outBlur(this.id);"  msg="Please enter title">
								  <span class="help-inline" id="msg_title"></span>
								</div>
							  </div>
 							  <div class="control-group" id="group_titleCn"> 
 								<label class="control-label">Title(CN)</label>
 								<div class="controls"> 
 								  <input id="titleCn" name="vo.titleCn" type="titleCn" onblur="outBlur(this.id);"  msg="Please enter title(CN)"> 
 								  <span class="help-inline" id="msg_titleCn"></span> 
 								</div> 
 							  </div> 
 							  <div class="control-group" id="group_copyright">
									<label class="control-label" for="focusedInput"> Copyright </label>
									<div class="controls">
										<select id="copyright" name="vo.copyright" value="0">
											<option value="0">United States</option>
											<option value="1">China</option>
										</select>
										<span class="help-inline" id="msg_copyright"></span>
									</div>
							  </div>
							  <div class="control-group" id="group_language">
									<label class="control-label" for="focusedInput"> Language </label>
									<div class="controls">
										<select id="language" name="vo.language" value="0">
											<option value="0">English</option>
											<option value="1">Chinese</option>
											<option value="2">English&Chinese</option>
										</select>
										<span class="help-inline" id="msg_language"></span>
									</div>
								</div>
							  <div class="control-group">
								<label class="control-label">Category</label>
								<div class="controls">
								  <select id="mediaCategory" name="mediaCategory" multiple data-rel="chosen">
								  <s:iterator value="categoryList" >
								  	<option value="${id }">${title}/${titleCn}</option>
								  </s:iterator>
								  </select>
								</div>
							  </div>
							  <div class="control-group" id="group_entertainment">
								<label class="control-label">Entertainment</label>
								<div class="controls">
								  <input id="entertainment" name="vo.entertainment" type="text" onblur="outBlur(this.id);"  msg="Please enter entertainment">
								  <span class="help-inline" id="msg_entertainment"></span>
								</div>
							  </div>
 							  <div class="control-group" id="group_entertainmentCn"> 
 								<label class="control-label">Entertainment(CN)</label> 
 								<div class="controls"> 
 								  <input id="entertainmentCn" name="vo.entertainmentCn" type="text" onblur="outBlur(this.id);"  msg="Please enter entertainment(CN)"> 
 								  <span class="help-inline" id="msg_entertainmentCn"></span> 
 								</div> 
 							  </div> 
							  <div class="control-group">
								<label class="control-label">Year</label>
								<div class="controls">
								  <select id="vo.year" name="vo.year">
									<option value="1990">1990</option>
									<option value="1991">1991</option>
									<option value="1992">1992</option>
									<option value="1993">1993</option>
									<option value="1994">1994</option>
									<option value="1995">1995</option>
									<option value="1996">1996</option>
									<option value="1997">1997</option>
									<option value="1998">1998</option>
								  	<option value="1999">1999</option>
									<option value="2000">2000</option>
									<option value="2001">2001</option>
									<option value="2002">2002</option>
									<option value="2003">2003</option>
									<option value="2004">2004</option>
									<option value="2005">2005</option>
									<option value="2006">2006</option>
									<option value="2007">2007</option>
									<option value="2008">2008</option>
									<option value="2009">2009</option>
									<option value="2010">2010</option>
									<option value="2011">2011</option>
									<option value="2012">2012</option>
									<option value="2013">2013</option>
									<option value="2014">2014</option>
									<option value="2015">2015</option>
								  </select>
								</div>
							  </div>
							 <div class="control-group" id="group_length">
								<label class="control-label">Length</label>
								<div class="controls">
								  <input id="length" name="vo.length" type="number" onblur="outBlur(this.id);"  msg="Please enter length">
								  <span class="help-inline" id="msg_length"></span>
								</div>
							  </div>
							  <div class="control-group" id="group_writer">
								<label class="control-label">Writer</label>
								<div class="controls">
								  <input id="writer" name="vo.writer" type="text" onblur="outBlur(this.id);"  msg="Please enter Writer">
								   <span class="help-inline" id="msg_writer"></span>
								</div>
							  </div>
 							  <div class="control-group" id="group_writerCn"> 
 								<label class="control-label">Writer(CN)</label> 
 								<div class="controls"> 
 								  <input id="writerCn" name="vo.writerCn" type="text" onblur="outBlur(this.id);"  msg="Please enter Writer(CN)"> 
 								   <span class="help-inline" id="msg_writerCn"></span> 
 								</div> 
 							  </div> 
							  <div class="control-group" id="group_director">
								<label class="control-label">Director</label>
								<div class="controls">
								  <input id="director" name="vo.director" type="text" onblur="outBlur(this.id);"  msg="Please enter Director">
								   <span class="help-inline" id="msg_director"></span>
								</div>
							  </div>
 							  <div class="control-group" id="group_directorCn"> 
 								<label class="control-label">Director(CN)</label> 
 								<div class="controls"> 
 								  <input id="directorCn" name="vo.directorCn" type="text" onblur="outBlur(this.id);"  msg="Please enter Director(CN)"> 
 								   <span class="help-inline" id="msg_directorCn"></span> 
 								</div> 
 							  </div> 
							  <div class="control-group" id="group_starring">
								<label class="control-label">Stars</label>
								<div class="controls">
								  <input id="starring" name="vo.starring" type="text" onblur="outBlur(this.id);"  msg="Please enter Stars">
								   <span class="help-inline" id="msg_starring"></span>
								</div>
							  </div>
 							  <div class="control-group" id="group_starringCn"> 
 								<label class="control-label">Stars(CN)</label> 
 								<div class="controls"> 
 								  <input id="starringCn" name="vo.starringCn" type="text" onblur="outBlur(this.id);"  msg="Please enter Stars(CN)"> 
 								   <span class="help-inline" id="msg_starringCn"></span> 
 								</div> 
 							  </div> 
							  <div class="control-group" id="group_showIndex">
								<label class="control-label">Show Index</label>
								<div class="controls">
								  <input id="showIndex" name="vo.showIndex" type="number" onblur="outBlur(this.id);"  msg="Please enter Show Index">
								   <span class="help-inline" id="msg_showIndex"></span>
								</div>
							  </div>
							  <div class="control-group" id="group_introduction">
								<label class="control-label">Introduction</label>
								<div class="controls">
								   <textarea  id="introduction" name="vo.introduction"  class="autogrow" onblur="outBlur(this.id);"  msg="Please enter Introduction"></textarea>
								    <span class="help-inline" id="msg_introduction"></span>
								</div>
							  </div>
 							  <div class="control-group" id="group_introductionCn"> 
 								<label class="control-label">Introduction(CN)</label> 
 								<div class="controls"> 
 								   <textarea  id="introductionCn" name="vo.introductionCn"  class="autogrow" onblur="outBlur(this.id);"  msg="Please enter Introduction(CN)"></textarea>
 								    <span class="help-inline" id="msg_introductionCn"></span> 
 								</div> 
 							  </div> 
							  <div class="control-group" id="group_isActive">
									<label class="control-label" for="focusedInput"> Status </label>
									<div class="controls">
										<select id="isActive" name="vo.isActive" value="true">
											<option value="true">Active</option>
											<option value="false">Inactive</option>
										</select>
										<span class="help-inline" id="msg_isActive"></span>
									</div>
								</div>
							  
							  <div class="form-actions">
								<button  id="submit-btn" type="button" onclick="submitForm();" class="btn btn-primary">Add</button>
								<button class="btn" type="reset" onclick="window.location.href='javascript:history.go(-1);'">Cancel</button>
							  </div>
							</fieldset>
						  </form>
					
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
	
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	<script>
	function submitForm(){ 
		$("#title").val($.trim($("#title").val()));
		$("#titleCn").val($.trim($("#titleCn").val()));
		
		var language = $("#language").val();
		
		if(language == 0 || language == 2) {
		
			if (!outBlur("title")) {
				return;
			}
			if (!outBlur("entertainment")) {
				return;
			}
			if (!outBlur("writer")) {
				return;
			}
			if (!outBlur("director")) {
				return;
			}
			if (!outBlur("starring")) {
				return;
			}
			if (!outBlur("introduction")) {
				return;
			}
		}
		
		if(language == 1 || language == 2) {
		
			if (!outBlur("titleCn")) {
				return;
			}
			if (!outBlur("entertainmentCn")) {
				return;
			}
			if (!outBlur("writerCn")) {
				return;
			}
			if (!outBlur("directorCn")) {
				return;
			}
			if (!outBlur("starringCn")) {
				return;
			}
			if (!outBlur("introductionCn")) {
				return;
			}
		}
		
		if (!outBlur("length")) {
			return;
		}
		if (!outBlur("showIndex")) {
			return;
		}
		
		$("#subForm").submit();
	}
	
	//失去焦点
	function outBlur(id) {

		if ($.trim($("#" + id).val()) == '') {
			$("#group_" + id).addClass("error");
			var msg = $("#" + id).attr("msg");
			$("#msg_" + id).text(msg);
		} else {
			$("#group_" + id).removeClass("error");
			$("#msg_" + id).text("");
			return true;
		}
		return false;
	};			
	</script>
</body>
</html>
