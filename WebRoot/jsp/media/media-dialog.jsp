<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
				<s:action name="menu" executeResult="true">
					<s:param name="type">9</s:param>
				</s:action>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Media Management</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Media Management</a>
					</li>
				</ul>
			</div>
<!-- search starts -->
<div>
	<div class="span12 my-search">
		<div>
			<h3>Search By</h3>
			<div class="my-filters">
				<form action="/city/city-list.action" method="post" id="subForm">
					<table>
						<tr>
							<td class="<%= session.getAttribute("currentrole")  %>">
							<label class="control-label">Company</label>
							</td>
							<td class="<%= session.getAttribute("currentrole")  %>">
								<div class="controls">
								  <select id="selectError3">
									<option>Eastern Bus</option>
									<option>Western Bus</option>
								  </select>
								</div>
							</td>
							<td>
								<label class="control-label"> Group Name</label>
							</td>
							<td>
								<div class="controls">
									<input id="focusedInput" type="text">
								</div>
							</td>
							<td>
								<button type="button" onclick="alert('Search');" style="margin-bottom: 10px;" class="btn">
									Search
								</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /search ends -->   								  
       <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Medias</h2>
						<div style="float:right;">
									<a class="btn btn-primary"  href="/media/media-add.action" >
									<i class="icon icon-white icon-plus"></i>&nbsp;Add Media </a>
						</div>	
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>ID</th>
								  <th>Title</th>
								  <th>Category</th>
								  <th>Entertainment</th>
								  <th>Year</th>
								  <th>Length</th>
								  <th>Movie</th>
								  <th>Hori-Poster</th>	
								  <th>Vert-Poster</th>
								  <th>Director</th>
								  <th>Starring</th>
								  <th>Show Index</th>
								  <th>Introduction</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
 <s:iterator value="mediaList" >
							<tr>
								<td>${id }</td>
								<td>${title }</td>
								<td>${category }</td>
								<td>${entertainment }</td>
								<td>${year }</td>
								<td>${length }</td>
								<td class="center">
									<a class="btn btn-success" href="#" onclick="$('#myModal').modal('show');">
										<i class="icon-scissors"></i> 
										View
									</a>
								</td>
								<td class="center">
									<span class="label label-warning">
										Empty
									</span>
								</td>
								<td class="center">
									<span class="label label-warning">
										Empty
									</span>
								</td>
								<td>${director }</td>
								<td>${starring }</td>
								<td>${showIndex }</td>
								<td>${introduction }</td>
								<td class="center">
									<a class="btn btn-info" href="/media/media-edit.action?id=${id}">
										 <i class="icon-edit icon-white"></i> Edit </a>
									<a class="btn btn-danger" href="/media/media-delete.action?id=${id}">
										<i class="icon-trash icon-white"></i> 
										Delete
									</a>
									<a class="btn btn-primary btn-movie-resource" href="#">
										<i class="icon-upload icon-white"></i> 
										Movie
									</a>
									<a class="btn btn-primary btn-poster-resource" href="#">
										<i class="icon-upload icon-white"></i> 
										HPoster
									</a>
									<a class="btn btn-primary btn-poster-resource" href="#">
										<i class="icon-upload icon-white"></i> 
										VPoster
									</a>
								</td>
							</tr>
</s:iterator>						
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	
	
</body>
</html>
