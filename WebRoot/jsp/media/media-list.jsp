<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<style>
	    .newtable{
	        margin: 20px;
	        width: 620px;
	        border-collapse: collapse;
	        border-spacing: 0;
	        text-align: center;
	        font-size: 12px;
	        font-family: Arial;
	    }
	    .newtable th{
	        font-weight: bold;
	    }
	    .newtable td,.newtable th{
	        height: 30px;
	        line-height: 30px;
	        border: 1px solid #999;
	    }
	    .newtable td{
	        cursor:pointer;
	    }
	    
	    .newtable td label{
	    	height: 30px;
	        line-height: 30px;
	        cursor:pointer;
	    }
	</style>
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
				<s:action name="menu" executeResult="true">
					<s:param name="type">9</s:param>
				</s:action>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Media Management</a>
					</li>
				</ul>
			</div>
<!-- search starts -->
<div>
	<div class="span12 my-search">
		<div>
			<h3>Search By</h3>
			<div class="my-filters">
				<form action="/media/media-search.action" method="post" id="queryForm">
					<table>
						<tr>
							<td>
								<label class="control-label">Category</label>
							</td>
							<td>
								<div class="controls">
								  <select id="categoryId" name="categoryId">
								   <option value="0">All</option>
									<s:iterator value="categoryList">
									 <option value="${id}" <s:if test="id == categoryId">selected="selected"</s:if>>${title}</option>
									 </s:iterator>
								  </select>
								</div>
							</td>
							<td>
								<label class="control-label"> Title </label>
							</td>
							<td>
								<div class="controls">
									<input id="title" type="text" name="title" value="${title }">
								</div>
							</td>
							<td>
								<button type="button" onclick="$('#queryForm').submit();" style="margin-bottom: 10px;" class="btn">
									Search
								</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /search ends -->   								  
       <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Medias</h2>
						<div style="float:right;">
									<a class="btn btn-primary"  href="/media/media-add.action" >
									<i class="icon icon-white icon-plus"></i>&nbsp;Add Media </a>
						</div>	
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered">
						  <thead>
							  <tr>
<!-- 								  <th>ID</th> -->
								  <th>CODE</th>
<!--  								  <th>Title(EN)</th>  -->
<!--  								  <th>Title(CN)</th>  -->
								  <th>Title</th>
								  <th>Language</th>
								  <th>Movie</th>
								  <th>HPoster</th>	
								  <th>VPoster</th>
								  <th>Status</th>
<!--                              <th>Version</th> -->
								  <th>Zip Status</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
 <s:iterator value="mediaList" >
							<tr>
<!-- 								<td>${id }</td> -->
								<td>${code }</td>
<!--  								<td>${title }</td>  -->
<!--  								<td>${titleCn }</td>  -->

								<td>
									<s:if test="%{language==0}">
										${title } 
									</s:if> 
									<s:elseif test="%{language==1}">
										${titleCn }
									</s:elseif> 
									<s:elseif test="%{language==2}">
										${title }
									</s:elseif> 
								</td>

								<td class="center">
									<s:if test="%{language==0}">
										EN
									</s:if> 
									<s:elseif test="%{language==1}">
										CN
									</s:elseif> 
									<s:elseif test="%{language==2}">
										EN&CN
									</s:elseif> 
								</td>
								<td class="center">
									<s:if test="mediaUrl==null">
								  		<span class="label label-warning">
											Empty
										</span>
									</s:if>
    							   	<s:else>
    							   		<span class="label label-success">
											Active
										</span>
    							   </s:else>
								</td>
								<td class="center">
								  <s:if test="hposterUrl==null">
								  <span class="label label-warning">
										Empty
									</span>
								  </s:if>
    							   <s:else>
    							   		<span class="label label-success">
											Active
										</span>
    							   </s:else>
								</td>
								<td class="center">
									<s:if test="vposterUrl==null">
								  		<span class="label label-warning">
											Empty
										</span>
								  	</s:if>
    							   	<s:else>
    							   		<span class="label label-success">
											Active
										</span>
    							   </s:else>
								</td>
								<td class="center">
									<s:if test="isActive">
								  		<span class="label label-success">
											Active
										</span>
								  	</s:if>
    							   	<s:else>
    							   		<span class="label label-warning">
											Inactive
										</span>
    							   </s:else>
								</td>
<!-- 								<td>${version }</td> -->
								<td>
									<s:if test="%{mediaVersion==null}">
										<span class="label label-warning">
											No zip
										</span>
									</s:if> 
									<s:elseif test="%{mediaVersion.status==0}">
										<span class="label label-info">
											Zipping
										</span>
									</s:elseif> 
									<s:elseif test="%{mediaVersion.status==1}">
										<span class="label label-success">
											Success
										</span>
									</s:elseif> 
									<s:elseif test="%{mediaVersion.status==2}">
										<span class="label label-warning">
											Error
										</span>
									</s:elseif> 
								</td>
								<td class="center">
									<a class="btn btn-info" href="/media/media-edit.action?id=${id}">
										 Edit 
									</a>
									<a class="btn btn-primary btn-movie-resource btn-setting" href="#" onclick="showTempFileDialog('${id}', '0')">
										Movie
									</a>
									<a class="btn btn-primary btn-poster-resource btn-setting" href="#" onclick="showTempFileDialog('${id}', '1')">
										HPoster
									</a>
									<a class="btn btn-primary btn-poster-resource btn-setting" href="#" onclick="showTempFileDialog('${id}', '2')">
										VPoster
									</a>
									<a class="btn btn-primary btn-poster-resource" href="#" onclick="zipMedia('${id}')">
										Zip
									</a>
								</td>
							</tr>
</s:iterator>						
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal" style="width: 700px">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Select file</h3>
			</div>
			<div class="modal-body">
				<input type="hidden" id="mediaId"/>
				<input type="hidden" id="type"/>
					<table class="newtable">
						<thead>
							<tr>
								<th>Select</th>
								<th>File Name</th>
								<th>File Path</th>
							</tr>
						</thead>
							
						<tbody>
						</tbody>
					</table>
					<input type="hidden" id="result" />
<!-- 					<div> -->
<!-- 						<b>Select file: <span id="result"></span></b> -->
<!-- 					</div> -->
			</div>
			<div class="modal-footer">
				
				<span id="result-alert"></span>
				<div class="waitting" style="display: none;">
					<img src="/img/spinner-mini.gif"> &nbsp;&nbsp; <span>Processing......</span>
				</div>
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" id="selectBtn" class="btn btn-primary" onclick="selectFile()">Select</a>
			</div>
		</div>
		
			
		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	
	<script type="text/javascript">
	
		function zipMedia(mediaId) {
		
			$.ajax({
				type: "POST",
			  	url: "/media/media-zip.action",
			  	data: {
			  		id : mediaId
			  	}, 
			  	dataType:'json',				  
			  	success: function(data){
			  		if(data.code == 0) {
// 			  			alert("Begin to compress the media resource!");
			  			window.location.href="/jsp/media/media-zip-ok.jsp";
			  		} else {
// 			  			alert(data.msg);
			  			window.location.href="/jsp/media/media-zip-error.jsp?msg="+data.msg;
			  		}
			  	},
			  	error:function(e){ 
			  		window.location.href="/jsp/media/media-zip-error.jsp";
			  	}
			});
		
		}
	
		function showTempFileDialog(mediaId, type) {
			var modalView = $("#myModal");
			$("#result").val("");
			$("#result-alert").html("");
			modalView.find("#type").val(type);
			modalView.find("#mediaId").val(mediaId);
			modalView.find("table tbody").empty();
			$.ajax({
				type: "POST",
			  	url: "/file/file-list-load.action",
			  	data: {
			  		type : type
			  	}, 
			  	dataType:'json',				  
			  	success: function(data){
			  		
			  		if(data!=null && data.fileList!=null && data.fileList.length > 0) {
			  			var fileList = data.fileList;
			  			for(var index in fileList) {
			  				var fileData = fileList[index];
			  				new FileObject(fileData, index, modalView);
			  			}
			  		} else {
			  			modalView.find("tbody").append("<tr><td colspan=3>There are no File.</td></tr>");
			  		}
			  	},
			  	error:function(e){ 
			  	}
			});
		}
		
		function selectFile() {
			
			var modalView = $("#myModal");
			
			var selectFilePath = $("#result").val();
			var type = modalView.find("#type").val();
			var mediaId = modalView.find("#mediaId").val();
			if(!selectFilePath) {
				$("#result-alert").html("Please select a file!");
				return;
			}
			
			alertTogger(false);
			
			$.ajax({
				type: "POST",
			  	url: "/media/media-file-select.action",
			  	data: {
			  		id : mediaId,
			  		type : type,
			  		path : selectFilePath
			  	}, 
			  	dataType:'json',				  
			  	success: function(data){
			  		
			  		if(data.result == 0) {
			  			location.reload();
			  		} else {
			  			$("#result-alert").html("Select file error!");
			  			alertTogger(true);
			  		}
			  	},
			  	error:function(e){ 
			  		$("#result-alert").html("Select file error!");
			  		alertTogger(true);
			  	}
			});
		}
		
		function alertTogger(swt) {

			if (!swt) {
				//禁用提交按钮
				$("#selectBtn").attr("disabled", true);
				//展示loading 
				$(".waitting").show();
				//隐藏提示信息
				$("#result-alert").hide();
			} else {
				$("#selectBtn").attr("disabled", false);
				//展示loading 
				$(".waitting").hide();
				//展示提示信息
				$("#result-alert").show();
			}
		};
	</script>
	
	<script type="text/javascript">
	
		var FileObject = function(data, index, container) {
		
			this.name = data.name;
			this.path = data.path;
			this.index = index;
			
			this.view;
			this.container = container;
			
			this.init();
		};
		
		FileObject.prototype.init = function() {
		
			this.view = $("<tr role='row' class='odd'></tr>"); 
			if(this.index%2 == 0)
				this.view.addClass("odd");
			else
				this.view.addClass("even");
			
			this.view.append("<td><input id='radio_"+this.index+"' type='radio' name='radio_group'/></td>");
			this.view.append("<td><label for='radio_"+this.index+"'>"+this.name+"</label></td>");
			this.view.append("<td style='text-align: left'><label for='radio_"+this.index+"'>"+this.path+"</label></td>");
			this.container.find("tbody").append(this.view);
			
			this.view.bind("click", this, this.onFileClick);
		};
		
		FileObject.prototype.onFileClick = function(e) {
		
			var self = e.data;
			$("#result-alert").html("");
			$("#result").val(self.path);
		};
		
	</script>
</body>
</html>
