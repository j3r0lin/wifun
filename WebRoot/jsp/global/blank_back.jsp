<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>iLIKEBUS</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
    <meta name="author" content="Muhammad Usman">

	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>

</head>

<body>

	<jsp:include page="/jsp/global/header.jsp"></jsp:include>

		
<div class="ch-container">
	<!--/fluid-row Start--> 
    <div class="row">

		<s:action name="menu" executeResult="true">
			<s:param name="type">2</s:param>
		</s:action>
	
	  <!--/#content.Start-->			
      <div id="content" class="col-lg-10 col-sm-10">
            <!-- content starts -->
	         <div>
		        <ul class="breadcrumb">
		            <li>
		                <a href="#">Home</a>
		            </li>
		            <li>
		                <a href="#">Tables</a>
		            </li>
		        </ul>
		      </div>

    	<div class="row">
			    <!-- box col Start -->
			    <div class="box col-md-12">
					    <!-- box-inner Start -->
					    <div class="box-inner">
							    <div class="box-header well" data-original-title="">
							        <h2><i class="glyphicon glyphicon-user"></i> Datatable + Responsive</h2>
							
							        <div class="box-icon">
							            <a href="#" class="btn btn-setting btn-round btn-default"><i class="glyphicon glyphicon-cog"></i></a>
							            <a href="#" class="btn btn-minimize btn-round btn-default"><i
							                    class="glyphicon glyphicon-chevron-up"></i></a>
							            <a href="#" class="btn btn-close btn-round btn-default"><i class="glyphicon glyphicon-remove"></i></a>
							        </div>
							    </div>
						   		<!-- box-content Start -->
							    <div class="box-content">
									    <table class="table table-striped table-bordered bootstrap-datatable datatable responsive">
									    <thead>
									    <tr>
									        <th>Username</th>
									        <th>Date registered</th>
									        <th>Role</th>
									        <th>Status</th>
									        <th>Actions</th>
									    </tr>
									    </thead>
									    <tbody>
									    <tr>
									        <td>Ahemd Saruar</td>
									        <td class="center">2012/03/01</td>
									        <td class="center">Member</td>
									        <td class="center">
									            <span class="label-warning label label-default">Pending</span>
									        </td>
									        <td class="center">
									            <a class="btn btn-success" href="#">
									                <i class="glyphicon glyphicon-zoom-in icon-white"></i>
									                View
									            </a>
									            <a class="btn btn-info" href="#">
									                <i class="glyphicon glyphicon-edit icon-white"></i>
									                Edit
									            </a>
									            <a class="btn btn-danger" href="#">
									                <i class="glyphicon glyphicon-trash icon-white"></i>
									                Delete
									            </a>
									        </td>
									    </tr>
									    </tbody>
									    </table>
							    </div><!-- box-content End -->
						    
					    </div><!-- box-inner End -->
			    </div><!-- box col End -->
    			<!--/span-->
  		</div><!--/row-->
  		
      <!-- content ends -->
    </div><!--/#content.col-md-0-->
</div>	<!--/fluid-row End-->   

   <jsp:include page="/jsp/global/footer.jsp"></jsp:include>

</div><!--/.fluid-container-->


</body>
</html>
