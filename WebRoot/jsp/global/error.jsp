<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%><%@ taglib
	prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>iLIKEBUS</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>

		<jsp:include page="/jsp/global/header.jsp"></jsp:include>

		<div class="container-fluid">
			<div class="row-fluid">

				<div class="alert alert-error center" style="width: 50%;margin-top: 150px;margin-bottom: 150px;height: 150px;">
							<h4 class="alert-heading">System Error!</h4>
							<p>Sorry, please try again or contact the administrator.</p>
							<P><a href="#" style="">Back</a></P>
						</div>
				<!--/fluid-row-->
				<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

			</div>
		</div>
		<!--/.fluid-container-->
		<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	</body>
</html>

