<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- topbar starts -->
<div class="navbar">
	<div class="navbar-inner">
		<div class="container-fluid">
			<a class="btn btn-navbar" data-toggle="collapse"
				data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse"> <span class="icon-bar"></span>
				<span class="icon-bar"></span> <span class="icon-bar"></span> </a> <a class="brand"
				href="#" target="_blank"> <img alt="Charisma Logo"
				src="/img/wifun.png" /></a>

			<!-- user dropdown starts -->
			<div class="btn-group pull-right">
				<a class="btn dropdown-toggle" data-toggle="dropdown" href="#"> <i class="icon-user"></i><span
					class="hidden-phone">${sessionScope.account_session.account}</span> <span class="caret"></span>
				</a>
				<ul class="dropdown-menu">
<!-- 					<li><a href="/system/account-repwd.action">Reset Password</a></li> -->
<!-- 					<li class="divider"></li> -->
					<li><a href="/system/login.action">Logout</a></li>
				</ul>
			</div>
			<!-- user dropdown ends -->
			<div class="pull-right">
				<a class="btn dropdown-toggle"
					style="background-color: transparent;background-image: none;color: white;text-shadow:none;">
					<!-- border-color:transparent;box-shadow:none; -->${sessionScope.account_session.companyName}</a>
			</div>
			<div class="top-nav nav-collapse">
				<ul class="nav">
					<li><a href="#" target="_blank">Visit Site</a></li>
					<!-- 
						<li>
							<form class="navbar-search pull-left">
								<input placeholder="Search" class="search-query span2" name="query" type="text">
							</form>
						</li>
						 -->
				</ul>
			</div>
			<!--/.nav-collapse -->
		</div>
	</div>
</div>
<!-- topbar ends -->
