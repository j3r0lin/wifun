<%@ page language="java" pageEncoding="utf-8"%><%@page import="com.wifun.admin.util.Page"%><%@ taglib uri="/struts-tags" prefix="s" %>
<s:if test="page.totalPage > 1">
					   <div class="pagination pagination-centered">
							  <ul>
								<li><s:if test="!page.hasFirst"><a title="Prev" href="javascript:void(0);" style="color: #999999;">Prev</a></s:if><s:else><a title="Prev" href="${param.url}pn=${page.currentPage-1}" ${param.useJs?"onclick='turnToPage(this); return false;'": ""}>Prev</a></s:else></li>
								<%
						    	    Page p=(Page)request.getAttribute("page");    	    
						    	    
						    	    int showPage=10;//10个页码
						    	    int left=(showPage)/2-1;//一边4个	  
						    	    int rigth=(showPage)/2;//5个
						    	      	    
						    	    int total=p.getTotalPage();
						    	    int c=p.getCurrentPage(); 
						    	    
						    	    int start=c-left>0?c-left:1;  
						    	   
						    	    int end=c+rigth<=total?c+rigth:total;  
						    	    if(end==total)
						    	    {
						    	       start=total-(showPage-1)>0?total-(showPage-1):1;
						    	    }
						    	    if(start==1)
						    	    {
						    	       end=showPage<=total?showPage:total;
						    	    }	   	 
						    	    for(int i=start;i<=end;i++)
						    	    {
						    	       if(i==p.getCurrentPage())
						    	       {
						    	       %>	    	       
						    	       <li class="active" onmouseover="this.style.cursor='hand'"><a href="#"><%=i%></a></li>	       
					    	            <%
						    	        }else
						    	        {
					    	            %>
						    	        <li onmouseover="this.style.cursor='hand'"><a href="${param.url}pn=<%=i%>" ${param.useJs?"onclick='turnToPage(this); return false;'":""}><%=i%></a></li>	       	    	         
						    	         <%
						    	        }	    	         
						    	    }	    	    
	    	  					 %>								
								<li><s:if test="!page.hasNext"><a href="javascript:void(0);" style="color: #999999;" title="Next">Next</a></s:if><s:else><a href="${param.url}pn=${page.currentPage+1}" ${param.useJs?"onclick='turnToPage(this); return false;'":""}>Next</a></s:else></li>
							  </ul>
						</div>
</s:if>

          
          