<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
<script type="text/javascript">
	var deleteId="";
	function recordDeleteMedia(id)
	{
		deleteId=id;
	}
	
	function deleteMedia(){ 	        
	        $("#closeBtn").trigger("click");
		    $.ajax({
				  type: "POST",
				  url: "/media/media-list-delete.action",
				  data:{id:deleteId}, 
				  dataType:'json',
				  success: function(data){	
				      	       
					   // 0,系统错误,1 成功,3参数错误
				       if(data.result==1)
				       {  
				          window.document.location.reload();
				       }else if(data.result==0)
				       {				          
				        alert("Delete error!");
				        window.document.location.reload();
				       }
				  },
				  error:function(e)
				  { 
				  	alert("System error!");					    
				  }
			});	      
	  	}
</script>	
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
				<s:action name="menu" executeResult="true">
					<s:param name="type">10</s:param>
				</s:action>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Media List Management</a>
					</li>
				</ul>
			</div>
<!-- search starts -->
<div>
	<div class="span12 my-search">
		<div>
			<h3>Search By</h3>
			<div class="my-filters">
				<form action="/media/media-list-list.action" method="post" id="subForm">
					<table>
						<tr>
							<td>
								<label class="control-label"> Title: </label>
							</td>
							<td>
								<div class="controls">
									<input id="title" type="text" name="title" value="${title}">
								</div>
							</td>
							<td>
								<button type="submit" style="margin-bottom: 10px;" class="btn">
									Search
								</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /search ends -->   								  
       <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Media List</h2>
						<div style="float:right;">
									<a class="btn btn-primary"  href="/media/media-list-add.action" >
									<i class="icon icon-white icon-plus"></i>&nbsp;Add Media List</a>
						</div>	
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>ID</th>
								  <th>Title</th>
								  <th>Size</th>
<s:if test="#session['account_session'].type==1">								  								 
								  <th>Company</th>
</s:if>								  
								  <th>Create Date</th>
								  <!-- 
								  <th>Creator</th>
								   -->
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
 <s:iterator value="list" >
							<tr>
								<td>${id }</td>
								<td>${title }</td>
								<td>${listSize}</td>
<s:if test="#session['account_session'].type==1">								
								<td class="center">
<s:if test="companyId==1">
								All
</s:if>						
<s:else>		
								${company.name}
</s:else>								
								</td>
</s:if>								
								<td class="center">
									<s:property value="@com.wifun.admin.util.TimeUtils@DateToString(createDate,'yyyy-MM-dd')" />
								</td>
								<!-- 
								<td>
									<span class="label label-success">${creator}</span>
								</td>
								-->
								<td class="center">
									<a class="btn btn-success" href="/media/media-list-detail.action?mlid=${id}">
										<i class="icon-list icon-white"></i>  
										Detail                                            
									</a>
									<a class="btn btn-primary btn-choose-media" href="/media/media-list-add-media.action?mlid=${id}">
										<i class="icon-white icon-plus"></i>  
										Add Media                                            
									</a>
									<a class="btn btn-info" href="/media/media-list-edit.action?id=${id}">
										 <i class="icon-edit icon-white"></i> Edit </a>
									<a class="btn btn-danger btn-setting" href="javascript:void(0);" onclick="recordDeleteMedia(${id});">
										<i class="icon-trash icon-white"></i> 
										Delete
									</a>
								</td>
							</tr>
</s:iterator>						
						  </tbody>
					  </table>
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>
<!-- 
		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>
 -->
		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	
	
</body>
</html>
<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Alert</h3>
			</div>
			<div class="modal-body">
				<p>Are you sure delete the media list?</p>
			</div>
			<div class="modal-footer">
				<a href="javascript:void(0);" class="btn" id="closeBtn" data-dismiss="modal">No</a>
				<a href="javascript:deleteMedia();" class="btn btn-primary">Yes</a>
			</div>
</div>