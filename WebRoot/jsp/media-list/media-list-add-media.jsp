<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
				<s:action name="menu" executeResult="true">
					<s:param name="type">10</s:param>
				</s:action>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Media List Add Media</a>
					</li>
				</ul>
			</div>
			  								  
       <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Medias</h2>
						<div class="box-icon" style="padding-right: 20px;">
								<a class="back" href="javascript:history.go(-1);" title="Back"><span
									class="icon32 icon-blue icon-undo"></span> </a>
						</div>
					</div>
					<form class="form-horizontal" id="subForm" name="subForm" action="/media/media-list-add-media-save.action" method="post">
							<input type="hidden" id="mlid" name="mlid" value="${mlid}"/> 
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
							  	  <th></th>
								  <th>ID</th>
								  <th>Language</th>
								  <th>Title</th>
<!-- 								  <th>Title(CN)</th> -->
								  <th>Category</th>
								  <th>Movie</th>
								  <th>Hori-Poster</th>	
								  <th>Vert-Poster</th>
							  </tr>
						  </thead>   
						  <tbody>
 <s:iterator value="mediaList" >
							<tr>
								<td> <input type="checkbox" id="media_${id }" name="mediaIdList" value="${id }" > </td>
								<td>${id }</td>
								<td class="center">
									<s:if test="%{language==0}">
										EN
									</s:if> 
									<s:elseif test="%{language==1}">
										CN
									</s:elseif> 
									<s:elseif test="%{language==2}">
										EN&CN
									</s:elseif> 
								</td>
								<td>
									<s:if test="%{language==0}">
										${title }
									</s:if> 
									<s:elseif test="%{language==1}">
										${titleCn }
									</s:elseif> 
									<s:elseif test="%{language==2}">
										${title }
									</s:elseif> 
									
								</td>
<!-- 								<td>${titleCn }</td> -->
								<td>
<s:iterator value="categorys">								
								${category.title}
</s:iterator>	
								</td>
								<td class="center">
									<s:if test="mediaUrl==null">
								  		<span class="label label-warning">
											Empty
										</span>
									</s:if>
    							   	<s:else>
    							   		<span class="label label-success">
											Active
										</span>
    							   </s:else>
								</td>
								<td class="center">
								  <s:if test="hposterUrl==null">
								  	<span class="label label-warning">
										Empty
									</span>
								  </s:if>
    							   <s:else>
    							   		<span class="label label-success">
											Active
										</span>
    							   </s:else>
								</td>
								<td class="center">
									<s:if test="vposterUrl==null">
								  		<span class="label label-warning">
											Empty
										</span>
								  	</s:if>
    							   	<s:else>
    							   		<span class="label label-success">
											Active
										</span>
    							   </s:else>
								</td>
								<!-- 
								<td class="center">
									<a class="btn btn-danger" href="/media/media-list-media-delete.action?id=${id}">
										<i class="icon-trash icon-white"></i> 
										Remove
									</a>
								</td>
								 -->
							</tr>
</s:iterator>						
						  </tbody>
					  </table>   
					  <div class="form-actions">
								<button id="submit-btn" type="button"
									onclick="submitForm();" class="btn btn-primary">
									Save
								</button>
								<button class="btn" type="reset"
									onclick="window.location.href='javascript:history.go(-1);'">
									Cancel
								</button>
							  </div>         
					</div>
					</form>
				</div><!--/span-->
			
			</div><!--/row-->
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
		<script type="text/javascript">		
	
	function submitForm(){  
		
		$("#subForm").submit();
	}
	</script>
	
</body>
</html>
