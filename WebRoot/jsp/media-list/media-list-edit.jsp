<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
					<s:param name="type">10</s:param>
				</s:action>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#"> Media List Edit </a>
					</li>
				</ul>
			</div>
								  
      
			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-edit"></i> Media List Info</h2>
						<div class="box-icon" style="padding-right: 20px;">
								<a class="back" href="javascript:history.go(-1);" title="Back"><span
									class="icon32 icon-blue icon-undo"></span> </a>
							</div>
					</div>
					
					<div class="box-content">
						<form class="form-horizontal" id="subForm" name="subForm" action="/media/media-list-edit-save.action" method="post">
							<input type="hidden" id="id" name="id" value="${bo.id}"/> 
<s:if test="#session['account_session'].type==1">							  
							  <div class="control-group">
								<label class="control-label">Company</label>
								<div class="controls">
								  <select id="companyId" name="companyId">
									<option value="1" <s:if test="bo.companyId==0">selected="selected"</s:if>>All</option>
<s:iterator value="companyLsit">
									<option value="${id}" <s:if test="bo.companyId==id">selected="selected"</s:if>>
										${name}
									</option>
</s:iterator>									
								  </select>
								</div>
							  </div>
</s:if>							
							<fieldset>
							  <div class="control-group" id="group_title">
									<label class="control-label" for="focusedInput"> Title </label>
									<div class="controls">
										<input class="input-xlarge focused" id="title" name="bo.title" value="${bo.title}" onblur="outBlur(this.id);" type="text"
											name="name" msg="Please enter title!">
										<span class="help-inline" id="msg_title"></span>
									</div>
								</div>
							  <div class="form-actions">
								<button id="submit-btn" type="button"
									onclick="submitForm();" class="btn btn-primary">
									Save
								</button>
								<button class="btn" type="reset"
									onclick="window.location.href='javascript:history.go(-1);'">
									Cancel
								</button>
							  </div>
							</fieldset>
						  </form>
					
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
	
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	<script>
			function submitForm(){ 
			
				if (!outBlur("title")) {
					alertTogger(true);
					return;
				}
				$("#subForm").submit();
			}
			
			//失去焦点
			function outBlur(id) {
		
				if ($.trim($("#" + id).val()) == '') {
					$("#group_" + id).addClass("error");
					var msg = $("#" + id).attr("msg");
					$("#msg_" + id).text(msg);
				} else {
					$("#group_" + id).removeClass("error");
					$("#msg_" + id).text("");
					return true;
				}
				return false;
			}
			
	</script>
</body>
</html>
