<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
					<s:param name="type">6</s:param>
				</s:action>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Company Management</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Add Portal</a>
					</li>
				</ul>
			</div>
								  
      
			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-edit"></i> Portal Info</h2>
						<div class="box-icon" style="padding-right: 20px;">
								<a class="back" href="javascript:history.go(-1);" title="Back"><span
									class="icon32 icon-blue icon-undo"></span> </a>
							</div>
					</div>
					
					<div class="box-content">
						<form class="form-horizontal" id="subForm" name="subForm"
									action="/portal/portal-add-save.action" enctype="multipart/form-data" method="post">
							<fieldset>
<s:if test="#session['account_session'].type==1">
							  <div class="control-group">
								<label class="control-label">Company</label>
								<div class="controls">
								  <select id="companyId" name="companyId">
								  	<option value="1" >ALL</option>
									<s:iterator value="companyList">
									 <option value="${id}" >${name}</option>
									 </s:iterator>
								  </select>
								</div>
							  </div>
</s:if>
							  <div class="control-group" id="group_portalName">
									<label class="control-label" for="focusedInput"> Portal Name </label>
									<div class="controls">
										<input class="input-xlarge focused" id="portalName" name="portalName" onblur="outBlur(this.id);" type="text"
											 msg="Please enter portal name">
										<span class="help-inline" id="msg_portalName"></span>
									</div>
								</div>
							  <div class="control-group" id="group_portalPackage">
								<label class="control-label"  for="focusedInput">Package</label>
								<div class="controls">
								 <s:file class="input-file uniform_on" id="portalPackage" name="portalPackage" msg="Please select portal package"/>
								 <span class="help-inline" id="msg_portalPackage"></span>
								</div>
								<div class="control-group" id="group_description">
									<label class="control-label" for="focusedInput"> Description </label>
									<div class="controls">
										<textarea  id="description" name="description" cols="10" rows="6" msg="Please enter Description"></textarea>
										<span class="help-inline" id="msg_description"></span>
									</div>
								</div>
							  </div>
								<div class="form-actions">
									<div class="actions">
										<button id="submit-btn" type="button" onclick="submitForm();" class="btn btn-primary">Add</button>
										<button class="btn" type="reset" onclick="window.location.href='javascript:history.go(-1);'">Cancel</button>
									</div>


									<div class="alert alert-error result" id="result-alert" style="display: none;">
										<span></span>
									</div>
									<div class="waitting" style="display: none;">
										<img src="/img/spinner-mini.gif"> &nbsp;&nbsp; <span>Processing......</span>
									</div>
								</div>
							</fieldset>
						  </form>
					
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
	
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	<script>
		
		function submitForm() {

			if (!outBlur("portalName")) {
				alertTogger(true);
				return;
			}
			if (!outBlur("portalPackage")) {
				alertTogger(true);
				return;
			}
			/**禁用提交按钮 展示loading 图片 **/
			alertTogger(false);

			$("#subForm").submit();
		}

		function alertTogger(swt) {
			if (!swt) {
				//禁用提交按钮
				$("#submit-btn").attr("disabled", true);
				//展示loading 
				$(".waitting").show();
				//隐藏提示信息
// 				$("#result-alert").hide();
			} else {
				$("#submit-btn").attr("disabled", false);
				//展示loading 
				$(".waitting").hide();
				//展示提示信息
// 				$("#result-alert").show();
			}
		}
		//失去焦点
		function outBlur(id) {

			if ($.trim($("#" + id).val()) == '') {
				$("#group_" + id).addClass("error");
				var msg = $("#" + id).attr("msg");
				$("#msg_" + id).text(msg);
			} else {
				$("#group_" + id).removeClass("error");
				$("#msg_" + id).text("");
				return true;
			}
			return false;
		}
	</script>
</body>
</html>
