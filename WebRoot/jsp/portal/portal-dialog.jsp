<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
			<div id="content" class="span10">
			<!-- content starts -->
			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Group List</a> 
					</li>
				</ul>
			</div>
<!-- search starts -->
<div>
	<div class="span12 my-search">
		<div>
			<h3>Search By</h3>
			<div class="my-filters">
				<form action="/city/city-list.action" method="post" id="subForm">
					<table>
						<tr>
							<td class="<%= session.getAttribute("currentrole")  %>">
							<label class="control-label">Company</label>
							</td>
							<td class="<%= session.getAttribute("currentrole")  %>">
								<div class="controls">
								  <select id="selectError3">
									<option>Eastern Bus</option>
									<option>Western Bus</option>
								  </select>
								</div>
							</td>
							<td>
								<label class="control-label"> Group Name</label>
							</td>
							<td>
								<div class="controls">
									<input id="focusedInput" type="text">
								</div>
							</td>
							<td>
								<button type="button" onclick="alert('Search');" style="margin-bottom: 10px;" class="btn">
									Search
								</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /search ends -->   								  
       <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Groups</h2>
						<div style="float:right;">
									<a class="btn btn-primary"  href="/portal/portal-add.action" >
									<i class="icon icon-white icon-plus"></i>&nbsp;Add Group </a>
						</div>	
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
							  <th>Choose</th>
							  <th>ID</th>
							  <th>Group Name</th>
							  <th>Company</th>
							  <th>Devices</th>
							  </tr>
						  </thead>   
						  <tbody>
 <s:iterator value="portalList" >
							<tr>
								<td>
									<label class="radio">
									<input type="radio" name="portalRadios" id="portal_${id }" value="${id }">
									</label>
								</td>
								<td>${id}</td>
								<td>${name}</td>
								<td>${company.name}</td>
								<td class="center">
									<a href="device.jsp"><span class="label label-success">5</span>/<span class="label label-important">5</span></a>
								</td>
							</tr>
</s:iterator>						
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	
	
</body>
</html>
