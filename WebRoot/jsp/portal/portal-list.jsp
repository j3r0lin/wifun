<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>WiFun Console</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
	<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
	<div class="container-fluid">
		<div class="row-fluid">

			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
				<s:param name="type">6</s:param>
			</s:action>
			<!-- left menu ends -->

			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>
						You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
						enabled to use this site.
					</p>
				</div>
			</noscript>

			<div id="content" class="span10">
				<!-- content starts -->


				<div>
					<ul class="breadcrumb">
						<li><a href="#">Portal Management</a></li>
					</ul>
				</div>
				<!-- search starts -->
				<div>
					<div class="span12 my-search">
						<div>
							<h3>Search By</h3>
							<div class="my-filters">
								<form action="/portal/portal-search.action" method="post" id="queryForm">
									<table>
										<tr>
<s:if test="#session['account_session'].type==1">
											<td class="control-label">
												<label class="control-label">Company</label>
											</td>
											<td class="control-label">
												<div class="controls">
													<select id="companyId" name="companyId">
														<option value="1" <s:if test="1 == companyId">selected="selected"</s:if>>ALL</option>
														<s:iterator value="companyList">
															<option value="${id}" <s:if test="id == companyId">selected="selected"</s:if>>${name}</option>
														</s:iterator>
													</select>
												</div>
											</td>
</s:if>
											<td><label class="control-label"> Portal Name</label></td>
											<td>
												<div class="controls">
													<input id="portalName" name="portalName" type="text" value="${portalName}">
												</div></td>
											<td>
												<button type="submit" style="margin-bottom: 10px;" class="btn">Search</button></td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- /search ends -->
				<div class="row-fluid sortable">
					<div class="box span12">
						<div class="box-header well" data-original-title>
							<h2>
								<i class="icon-user"></i> Portals
							</h2>
							<div style="float:right;">
								<a class="btn btn-primary" href="/portal/portal-add.action"> <i
									class="icon icon-white icon-plus"></i>&nbsp;Add Portal </a>
							</div>
						</div>
						<div class="box-content">
							<table class="table table-striped table-bordered bootstrap-datatable datatable">
								<thead>
									<tr>
										<th>ID</th>
										<th>Company</th>
										<th>Name</th>
										<th>Status</th>
										<th>Create Time</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<s:iterator value="portalList">
										<tr>
											<td>${id}</td>
											<td>
												<s:if test="%{companyId==1}">
													ALL
												</s:if> 
												<s:else>
													${company.name}
												</s:else>
											
											</td>
											<td>${name}</td>
											<td>
												<s:if test="isActive">
													<span class="label label-success">Active</span>
												</s:if> <s:else>
													<span class="label label-warning"> Inactive </span>
												</s:else>
											</td>
											<td class="center">
												<s:property value="@com.wifun.admin.util.TimeUtils@DateToString(createTime,'MM/dd/yy HH:mm')" />
											</td>
											<td class="center">
<!-- 												<a class="btn btn-danger" -->
<!-- 													href="/jsp/portal/portal-delete-confirm.jsp?id=${id}"> <i class="icon-trash icon-white"></i> -->
<!-- 													Delete </a> -->
													<s:if test="isActive">
														<a class="btn btn-danger" href="/portal/portal-pause.action?id=${id}"> 
	<!-- 														<i class="icon-trash icon-white"></i> -->
															  Pause
														</a>
													</s:if> <s:else>
														<a class="btn btn-success" href="/portal/portal-active.action?id=${id}"> 
	<!-- 														<i class="icon-trash icon-white"></i> -->
															  Active
														</a>
													</s:else>
													
													<a class="btn btn-success" href="/portal/portal-download.action?id=${id}"> 
														  Download
													</a>
													
													
											</td>
										</tr>
									</s:iterator>
								</tbody>
							</table>
						</div>
					</div>
					<!--/span-->

				</div>
				<!--/row-->
				<!-- content ends -->
			</div>
			<!--/#content.span10-->
		</div>
		<!--/fluid-row-->

		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a> <a href="#" class="btn btn-primary">Save
					changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>


</body>
</html>
