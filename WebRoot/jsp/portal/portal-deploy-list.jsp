<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>WiFun Console</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
	<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
	<div class="container-fluid">
		<div class="row-fluid">

			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
				<s:param name="type">7</s:param>
			</s:action>
			<!-- left menu ends -->

			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>
						You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
						enabled to use this site.
					</p>
				</div>
			</noscript>

			<div id="content" class="span10">
				<!-- content starts -->


				<div>
					<ul class="breadcrumb">
						<li><a href="#">Portal Management</a></li>
					</ul>
				</div>
				<!-- search starts -->
				<div>
					<div class="span12 my-search">
						<div>
							<h3>Search By</h3>
							<div class="my-filters">
								<form action="/portal/portal-deploy-search.action" method="post" id="queryForm">
									<table>
										<tr>
											<td><label class="control-label" style="float: right;"> Deploy Date</label>
											</td>
											<td>
												<div class="controls">
													<input type="text" class="datepicker" id="startDate" name="startDate" value="${startDate }">
												</div>
											</td>
											<td><label class="control-label" style="float: right;"> to</label>
											</td>
											<td>
												<div class="controls">
													<input type="text" class="datepicker" id="endDate" name="endDate" value="${endDate }">
												</div>
											</td>
										</tr>
										<tr>
											<td><label class="control-label" style="float: right;"> Description</label>
											</td>
											<td>
												<div class="controls">
													<input id="description" type="text" name="description" value="${description }">
												</div>
											</td>
											<td class="controls" style="float: right;">
<s:if test="#session['account_session'].type==1">
												<label class="control-label">Company</label>
											</td>
											<td class="controls">
												<div class="controls">
													<select id="companyId" name="companyId">
														<s:iterator value="companyList">
															<option value="${id}" <s:if test="id == companyId">selected="selected"</s:if>>${name}</option>
														</s:iterator>
													</select>
												</div>
</s:if>
											</td>
											<td>
												<button type="submit" style="margin-bottom: 10px;" class="btn">Search</button>
											</td>
										</tr>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- /search ends -->
				<div class="row-fluid sortable">
					<div class="box span12">
						<div class="box-header well" data-original-title>
							<h2>
								<i class="icon-user"></i> Portals
							</h2>
							<div style="float:right;">
								<a class="btn btn-primary" href="/portal/portal-deploy-add.action"> <i
									class="icon icon-white icon-plus"></i>&nbsp;New Deploy </a>
							</div>
						</div>
						<div class="box-content">
							<table class="table table-striped table-bordered bootstrap-datatable datatable">
								<thead>
									<tr>
										<th>ID</th>
										<th>Company</th>
										<th>Group</th>
										<th>Portal</th>
										<th>Description</th>
										<th>Status</th>
										<th>Deploy Time</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<s:iterator value="deployList">
										<tr>
											<td>${id}</td>
											<td>${company.name}</td>
											<td>${group.name}</td>
											<td>${portalName}</td>
											<td>${description}</td>
											<td>
												<s:if test="%{status==0}">
													<span class="label label-info">Waiting</span>
												</s:if> 
												<s:elseif test="%{status==1}">
													<span class="label label-info">Deploying</span>
												</s:elseif> 
												<s:elseif test="%{status==2}">
													<span class="label label-success">Success</span>
												</s:elseif> 
												<s:elseif test="%{status==3}">
													<span class="label label-warning"> Failure </span>
												</s:elseif> 
											</td>
											<td class="center"><s:property value="@com.wifun.admin.util.TimeUtils@DateToString(deployTime,'MM/dd/yy HH:mm')" /></td>
											<td class="center"><a class="btn btn-primary"
												href="/portal/portal-deploy-detail.action?deployId=${id}"> <i
													class="icon-list icon-white"></i> Detail </a></td>
										</tr>
									</s:iterator>
								</tbody>
							</table>
						</div>
					</div>
					<!--/span-->

				</div>
				<!--/row-->
				<!-- content ends -->
			</div>
			<!--/#content.span10-->
		</div>
		<!--/fluid-row-->

		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a> <a href="#" class="btn btn-primary">Save
					changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>


</body>
</html>
