
<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>WiFun--Console</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
	</head>

	<body>
		<jsp:include page="/jsp/global/header.jsp"></jsp:include>

		<div class="container-fluid">
			<div class="row-fluid">

				<s:action name="menu" executeResult="true">
					<s:param name="type">6</s:param>
				</s:action>

				<div id="content" class="span10">
					<!-- content starts -->
					<div class="row-fluid">
						<div class="box span12">
							<div class="box-header well">
							<h2><li class="icon-bullhorn"></li>&nbsp;Alerts</h2>
							</div>
							<div class="box-content">
								<form class="form-horizontal">
									<p class="green">
										Portal save succeeded!
									</p>
									<p>
										<button id="submit-btn" type="button" onclick="window.location.href='/portal/portal-list.action'" class="btn btn-primary">
											Go Back
										</button>
									</p>
								</form>
							</div>
						</div>
					</div>

					<!-- content ends -->
				</div>
				<!--/#content.span10-->
			</div>
			<!--/fluid-row-->

			<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

		</div>
		<!--/.fluid-container-->

		<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

	</body>
</html>
