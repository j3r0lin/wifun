<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
					<s:param name="type">8</s:param>
				</s:action>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Category Add</a> 
					</li>
				</ul>
			</div>
								  
      
			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-edit"></i> Category Info</h2>
						<div class="box-icon" style="padding-right: 20px;">
								<a class="back" href="javascript:history.go(-1);" title="Back"><span
									class="icon32 icon-blue icon-undo"></span> </a>
							</div>
					</div>
					
					<div class="box-content">
						<form class="form-horizontal" id="subForm" name="subForm"
									action="/media/category-add-save.action" method="post">
							<fieldset>
							  <div class="control-group">
								<label class="control-label">Title</label>
								<div class="controls">
								  <input id="title" name="bo.title" type="text" >
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label">Title(CN)</label>
								<div class="controls">
								  <input id="titleCn" name="bo.titleCn" type="text" >
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label">Status</label>
								<div class="controls">
								  <select id="status" name="bo.status">
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								  </select>
								</div>
							  </div>
							  <div class="form-actions">
								<button  id="submit-btn" type="button" onclick="submitForm();" class="btn btn-primary">Add</button>
								<button class="btn" type="reset" onclick="window.location.href='javascript:history.go(-1);'">Cancel</button>
							  </div>
							</fieldset>
						  </form>
					
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
	
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	<script>
			function submitForm(){ 
				$("#subForm").submit();
			}
	</script>
</body>
</html>
