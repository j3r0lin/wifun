<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- left menu starts -->
<div class="span2 main-menu-span">
	<div class="well nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li class="nav-header hidden-tablet">Company</li>
						<li class="${type==2?'active':''}"><a class="ajax-link" href="/group/group-list.action"><i class="icon-inbox"></i><span class="hidden-tablet"> Group</span></a></li>
						<li class="nav-header hidden-tablet">Device</li>
						<li class="${type==3?'active':''}"><a class="ajax-link" href="/device/device-list.action"><i class="icon-hdd"></i><span class="hidden-tablet"> Device</span></a></li>
						<li class="nav-header hidden-tablet">Portal</li>
						<li class="${type==6?'active':''}"><a class="ajax-link" href="/portal/portal-list.action"><i class="icon-globe"></i><span class="hidden-tablet"> Portal</span></a></li>
<!-- 						<li class="${type==7?'active':''}"><a class="ajax-link" href="/portal/portal-deploy-list.action"><i class="icon-wrench"></i><span class="hidden-tablet">  Deploy</span></a></li> -->
						<li class="nav-header hidden-tablet">Media </li>
						<li class="${type==10?'active':''}"><a class="ajax-link" href="/media/media-list-list.action"><i class="icon-list"></i><span class="hidden-tablet"> Media List </span></a></li>
<!-- 						<li class="${type==11?'active':''}"><a class="ajax-link" href="/media/media-deploy-list.action"><i class="icon-wrench"></i><span class="hidden-tablet"> Deploy</span></a></li> -->
						<li class="nav-header hidden-tablet">Report </li>
						<li class="${type==15?'active':''}"><a class="ajax-link" href="/report/daily-report.action"><i class="icon-list-alt"></i><span class="hidden-tablet"> Daily Report</span></a></li>
						<li class="${type==13?'active':''}"><a class="ajax-link" href="/pvuvReport/report-pvuv.action"><i class="icon-list-alt"></i><span class="hidden-tablet"> PVUV Movie</span></a></li>
						<li class="${type==18?'active':''}"><a class="ajax-link" href="/pvuvPageReport/report-pagepvuv.action"><i class="icon-list"></i><span class="hidden-tablet"> PVUV Pages</span></a></li>
						<li class="${type==14?'active':''}"><a class="ajax-link" href="/trafficReport/trafficSum-list.action"><i class="icon-list-alt"></i><span class="hidden-tablet"> Traffic Report</span></a></li>
						<li class="nav-header hidden-tablet">Location </li>
						<li class="${type==16?'active':''}"><a class="ajax-link" href="/location/device-location.action"><i class="icon-list-alt"></i><span class="hidden-tablet"> Device Location </span></a></li>
						<li class="${type==17?'active':''}"><a class="ajax-link" href="/route/device-routeIndex.action"><i class="icon-list-alt"></i><span class="hidden-tablet"> Device Route </span></a></li>
					</ul>
				</div><!--/.well -->
</div><!--/span-->
<!-- left menu ends -->
