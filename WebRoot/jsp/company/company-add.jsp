<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>WiFun</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>

</head>

<body>

	<jsp:include page="/jsp/global/header.jsp"></jsp:include>

	<div class="container-fluid">
		<div class="row-fluid">

			<s:action name="menu" executeResult="true">
				<s:param name="type">1</s:param>
			</s:action>

			<div id="content" class="span10">
				<!-- content starts -->
				<div class="row-fluid">
					<div class="box span12">
						<div class="box-header well">
							<h2>
								<i class="icon-user"></i> Company Add
							</h2>
							<div class="box-icon" style="padding-right: 20px;">
								<a class="back" href="javascript:history.go(-1);" title="Back"><span
									class="icon32 icon-blue icon-undo"></span> </a>
							</div>
						</div>
						<div class="box-content">
							<fieldset>
								<form class="form-horizontal" id="subForm" name="subForm"
									action="/company/company-add-save.action" method="post">

									<div class="control-group" id="group_name">
										<legend> Company Information </legend>
										<label class="control-label" for="focusedInput"> Company Name </label>
										<div class="controls">
											<input class="input-xlarge focused" id="name" onblur="outBlur(this.id);" type="text"
												name="name" msg="Please enter your company name" maxlength="64">
											<span class="help-inline" id="msg_name"></span>
										</div>
									</div>
									<div class="control-group" id="group_account">
										<label class="control-label"> Account </label>
										<div class="controls">
											<input id="account" type="text" onblur="outBlur(this.id);" name="account"
												msg="Please enter 2 - 20 characters(A-Z,a-z,0-9 only)"  maxlength="20"/>
											<span class="help-inline" id="msg_account"></span>
										</div>
									</div>
									<div class="control-group" id="group_contact">
										<label class="control-label" for="focusedInput"> Contact </label>
										<div class="controls">
											<input class="input-xlarge focused" id="contact" onblur="outBlur(this.id);" type="text"
												name="contact" msg="Please enter contact name" maxlength="32">
											<span class="help-inline" id="msg_contact"></span>
										</div>
									</div>
									<div class="control-group" id="group_phone">
										<label class="control-label" for="focusedInput"> Phone </label>
										<div class="controls">
											<input class="input-xlarge focused" id="phone" onblur="outBlur(this.id);" type="text"
												name="phone" msg="Please enter phone number" maxlength="15">
											<span class="help-inline" id="msg_phone"></span>
										</div>
									</div>
									<div class="control-group" id="group_email">
										<label class="control-label" for="focusedInput"> Email </label>
										<div class="controls">
											<input class="input-xlarge focused" id="email" onblur="outBlur(this.id);" type="text"
												name="email" msg="Please enter email adress" maxlength="45">
											<span class="help-inline" id="msg_email"></span>
										</div>
									</div>
									<div class="control-group" id="group_isActive">
										<label class="control-label" for="focusedInput"> status </label>
										<div class="controls">
											<select id="isActive" name="isActive" value="true">
												<option value="true">Active</option>
												<option value="false">Inactive</option>
											</select>
											<span class="help-inline" id="msg_isActive"></span>
										</div>
									</div>

									<div class="form-actions">
										<div class="actions">
											<button id="submit-btn" type="button" onclick="submitForm();" class="btn btn-primary">
												Save</button>
											<button class="btn" type="reset"
												onclick="window.location.href='javascript:history.go(-1);'">Cancel</button>
										</div>
									</div>
								</form>
							</fieldset>
						</div>
					</div>
				</div>
				<!--/row-->

				<div class="row-fluid"></div>

				<!-- content ends -->
			</div>
			<!--/#content.span10-->
		</div>
		<!--/fluid-row-->

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->


	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

</body>
<script type="text/javascript">
	function submitForm() {

		alertTogger(false);
		$("#name").val($.trim($("#name").val()));
		if (!outBlur("name")) {
			alertTogger(true);
			return;
		}
		$("#account").val($.trim($("#account").val()));
		if (!outBlur("account")) {
			alertTogger(true);
			return;
		}
		$("#contact").val($.trim($("#contact").val()));
		if (!outBlur("contact")) {
			alertTogger(true);
			return;
		}
		$("#phone").val($.trim($("#phone").val()));
		if (!outBlur("phone")) {
			alertTogger(true);
			return;
		}
		$("#email").val($.trim($("#email").val()));
		if (!outBlur("email")) {
			alertTogger(true);
			return;
		}
		if (!validLoginname($("#account").val())) {
			$("#group_account").addClass("error");
			var msg = $("#account").attr("msg");
			$("#msg_account").text(msg);
			alertTogger(true);
			return;
		}
		if (!validEmail($("#email").val())) {
			$("#group_email").addClass("error");
			$("#msg_email").text("Email Format Error!");
			alertTogger(true);
			return;
		}
		
		$("#subForm").submit();
	};
	
	//失去焦点
	function outBlur(id) {

		if ($.trim($("#" + id).val()) == '') {
			$("#group_" + id).addClass("error");
			var msg = $("#" + id).attr("msg");
			$("#msg_" + id).text(msg);
		} else {
			$("#group_" + id).removeClass("error");
			$("#msg_" + id).text("");
			return true;
		}
		return false;
	};

	function alertTogger(swt) {

		if (!swt) {
			//禁用提交按钮
			$("#submit-btn").attr("disabled", true);
			//展示loading 
			$(".waitting").show();
			//隐藏提示信息
			$("#result-alert").hide();
		} else {
			$("#submit-btn").attr("disabled", false);
			//展示loading 
			$(".waitting").hide();
			//展示提示信息
			$("#result-alert").show();
		}
	};
	
	//验证账号格式
	function validLoginname(value) {

		var regex = /^[a-zA-Z0-9]{1,20}$/;
		return regex.test(value);
	};
	
	function validEmail(value) {
		
		var emailFormat = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z0-9]+$/; // 邮箱格式校验
		if(!emailFormat.test(value))
			return false;
			
		return true;
	}
</script>

</html>
