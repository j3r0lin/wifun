<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>WiFun</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
	</head>

	<body>

		<jsp:include page="/jsp/global/header.jsp"></jsp:include>
		<div class="container-fluid">
			<div class="row-fluid">
				<s:action name="menu" executeResult="true">
					<s:param name="type">1</s:param>
				</s:action>

				<div id="content" class="span10">
					<!-- content starts -->
					<div>
						<ul class="breadcrumb">
							<li>
								<a href="#">Company</a>
							</li>
						</ul>
					</div>
<!-- search starts -->
<div>
	<div class="span12 my-search">
		<div>
			<h3>Search By</h3>
			<div class="my-filters">
				<form action="/company/company-search.action" method="post" id="queryForm">
					<table>
						<tr>
							
							<td>
								<label class="control-label"> Company Name</label>
							</td>
							<td>
								<div class="controls">
									<input id="name" type="text" name="name" value="${name}">
								</div>
							</td>
							<td>
								<input type="checkbox" id="active" name="active" <s:if test="active">checked="checked"</s:if> value="true" style="padding-right:90px;">
								<span>Show Active Only</span>
							</td>
							<td>
								<button type="button" onclick="$('#queryForm').submit();" style="margin-bottom: 10px;" class="btn">
									Search
								</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /search ends -->   
					<div class="row-fluid">
						<div class="box span12">
							<div class="box-header well">
								<h2>
									<i class="icon-edit"></i> Company List
								</h2>
								<div style="float:right;">
									<a class="btn btn-primary"  href="/company/company-add.action" >
									<i class="icon icon-white icon-plus"></i>&nbsp;Add Company </a>
								</div>	
							</div>
							<div class="box-content">
								<form class="form-horizontal">
									<table
										class="table table-striped table-bordered datatable dataTable">
										<thead>
										<tr>
											<th>ID</th>
											  <th>Company Name</th>
											  <th>Account</th>
											  <th>Contact</th>
											  <th>Phone</th>
											  <th>Email</th>
											  <th>Groups</th>
											  <th>Devices</th>								 
											  <th>Status</th>
											  <th>Create Time</th>
											  <th>Actions</th>
										</tr>
										</thead>

										<tbody>
<s:iterator value="comList" >
											<tr>
												<td>
													${id}
												</td>
												<td >
													${name }
												</td>
												<td>
													${account}
												</td>
												<td>
													${contact}
												</td>
												<td>
													${phone}
												</td>
												<td >
													${email}
												</td>
												<td >
													
													<a href="/group/group-search.action?companyId=${id }"><span class="label label-success">${groupNum}</span></a>
												</td>
												<td >
													<a href="/device/device-search.action?companyId=${id }">
														<span class="label label-success">${deviceNormalNum }</span>
														/
														<span class="label label-success">${deviceTotalNum }</span>
													</a> 
												</td>
												<td >
													<s:if test="isActive">
														<span class="label label-success">Active</span>
													</s:if> <s:else>
														<span class="label label-warning"> Inactive </span>
													</s:else>
												</td>
												<td>
													<s:property value="@com.wifun.admin.util.TimeUtils@DateToString(createDate,'MM/dd/yy HH:mm')" />
												</td>
												<td >
													<a class="btn btn-info" href="/company/company-edit.action?id=${id}"> <i
														class="icon-edit icon-white"></i> Edit </a>
													<a class="btn btn-success" href="/company/company-pwd-reset.action?companyId=${id}"> <i
														class="icon-zoom-in icon-white"></i> Reset Password </a>
												</td>
											</tr>
</s:iterator>
										</tbody>
									</table>
								</form>

							</div>
						</div>
						<!--/span-->
					</div>
					<!--/row-->
					<!-- content ends -->
				</div>
				<!--/#content.span10-->

			</div>
			<!--/fluid-row-->
			<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		</div>
		<!--/.fluid-container-->
		<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

	</body>
</html>
