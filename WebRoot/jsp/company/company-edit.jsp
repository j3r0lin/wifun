<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>WiFun</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
<script type="text/javascript">
	function submitForm() {

		alertTogger(false);
		$("#contact").val($.trim($("#contact").val()));
		if (!outBlur("contact")) {
			alertTogger(true);
			return;
		}
		$("#phone").val($.trim($("#phone").val()));
		if (!outBlur("phone")) {
			alertTogger(true);
			return;
		}
		//alert("submit");
		$("#subForm").submit();
	};
	//失去焦点
	function outBlur(id) {

		if ($.trim($("#" + id).val()) == '') {
			$("#group_" + id).addClass("error");
			var msg = $("#" + id).attr("msg");
			$("#msg_" + id).text(msg);
		} else {
			$("#group_" + id).removeClass("error");
			$("#msg_" + id).text("");
			return true;
		}
		return false;
	};
	function alertTogger(swt) {

		if (!swt) {
			//禁用提交按钮
			$("#submit-btn").attr("disabled", true);
			//展示loading 
			$(".waitting").show();
			//隐藏提示信息
			$("#result-alert").hide();
		} else {
			$("#submit-btn").attr("disabled", false);
			//展示loading 
			$(".waitting").hide();
			//展示提示信息
			$("#result-alert").show();
		}
	};
</script>
</head>

<body>

	<jsp:include page="/jsp/global/header.jsp"></jsp:include>

	<div class="container-fluid">
		<div class="row-fluid">

			<s:action name="menu" executeResult="true">
				<s:param name="type">1</s:param>
			</s:action>

			<div id="content" class="span10">
				<!-- content starts -->
				<div class="row-fluid">
					<div class="box span12">
						<div class="box-header well">
							<h2>
								<i class="icon-user"></i> Company Edit
							</h2>
							<div class="box-icon" style="padding-right: 20px;">
								<a class="back" href="javascript:history.go(-1);" title="Back"><span
									class="icon32 icon-blue icon-undo"></span> </a>
							</div>
						</div>
						<div class="box-content">
							<fieldset>
								<form class="form-horizontal" id="subForm" name="subForm"
									action="/company/company-edit-save.action" method="post">
									<input type="hidden" id="vo.id" name="vo.id" value="${vo.id}" />
									<div class="control-group" id="group_name">
										<label class="control-label" for="focusedInput"> Company Name </label>
										<div class="controls">
											<input class="input-xlarge focused readonly" id="name"
												msg="Please enter your company name" type="text" name="vo.name" value="${vo.name }"
												disabled="disabled">
											<span class="help-inline" id="msg_name"></span>
										</div>
									</div>
									<div class="control-group" id="group_account">
										<label class="control-label" for="focusedInput"> Account </label>
										<div class="controls">
											<input class="input-xlarge focused readonly" id="account"
												msg="Please enter company account" type="text" name="vo.account" value="${vo.account }"
												disabled="disabled">
											<span class="help-inline" id="msg_account"></span>
										</div>
									</div>
									<div class="control-group" id="group_contact">
										<label class="control-label" for="focusedInput"> Contact </label>
										<div class="controls">
											<input class="input-xlarge focused" id="contact" onblur="outBlur(this.id);"
												msg="Please enter contact name" type="text" name="vo.contact" value="${vo.contact }" maxlength="20">
											<span class="help-inline" id="msg_contact"></span>
										</div>
									</div>
									<div class="control-group" id="group_phone">
										<label class="control-label" for="focusedInput"> Phone </label>
										<div class="controls">
											<input class="input-xlarge focused" id="phone" onblur="outBlur(this.id);" type="text"
												name="vo.phone" value="${vo.phone }" msg="Please enter phone number" maxlength="15">
											<span class="help-inline" id="msg_phone"></span>
										</div>
									</div>
									<div class="control-group" id="group_email">
										<label class="control-label" for="focusedInput"> Email </label>
										<div class="controls">
											<input class="input-xlarge focused readonly" id="email" type="text" name="vo.email"
												value="${vo.email}" disabled="disabled">
											<span class="help-inline" id="msg_email"></span>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Status</label>
										<div class="controls">
											<select id="isActive" name="vo.isActive" value="${vo.isActive}">

												<option value="true" <s:if test="vo.isActive">selected="selected"</s:if>>Active</option>
												<option value="false" <s:if test="!vo.isActive">selected="selected"</s:if>>Inactive</option>
											</select>
										</div>
									</div>

									<div class="form-actions">
										<div class="actions">
											<button id="submit-btn" type="button" onclick="submitForm();" class="btn btn-primary">
												Save</button>
											<button class="btn" type="reset"
												onclick="window.location.href='javascript:history.go(-1);'">Cancel</button>
										</div>
									</div>
								</form>
							</fieldset>
						</div>
					</div>
				</div>
				<!--/row-->

				<div class="row-fluid"></div>

				<!-- content ends -->
			</div>
			<!--/#content.span10-->
		</div>
		<!--/fluid-row-->

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->


	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	<script src="/form/jquery.form.js"></script>

</body>
</html>
