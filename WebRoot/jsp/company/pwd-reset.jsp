<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>iLIKEBUS</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>

	</head>

	<body>

		<jsp:include page="/jsp/global/header.jsp"></jsp:include>

		<div class="container-fluid">
			<div class="row-fluid">

				<s:action name="menu" executeResult="true">
					<s:param name="type">1</s:param>
				</s:action>

				<div id="content" class="span10">
					<!-- content starts -->
					<div class="row-fluid">
						<div class="box span12">
							<div class="box-header well">
								<h2>
									<i class="icon-user"></i> Reset Password
								</h2>
								<div class="box-icon" style="padding-right: 20px;">
									<a class="back" href="javascript:history.go(-1);" title="Back"><span
										class="icon32 icon-blue icon-undo"></span> </a>
								</div>
							</div>
							<div class="box-content">
								<fieldset>
									<form class="form-horizontal" id="subForm" name="subForm" method="post" action="/company/company-pwd-save.action">
										<input type="hidden" value="${vo.id}" name="id">
										<div class="control-group" id="account">
											<label class="control-label" for="focusedInput">
												Account:
											</label>
											<div class="controls">
												<input class="input-xlarge" id="name" type="text" name="name" value="${vo.account}">
												<span class="help-inline" id="msg_pwd" ></span>
											</div>
										</div>	
										<div class="control-group" id="group_pwd">
											<label class="control-label" for="focusedInput">
												Old Password:
											</label>
											<div class="controls">
												<input class="input-xlarge focused" id="pwd" type="text" disabled="disabled" value="${vo.password}">
												<span class="help-inline" id="msg_pwd" ></span>
											</div>
										</div>													
										<div class="control-group" id="group_pwd">
											<label class="control-label" for="focusedInput">
												New Password
											</label>
											<div class="controls">
												<input class="input-xlarge focused" id="password" type="password" name="password">
											</div>
										</div>
										<div class="control-group" id="group_pwd">
											<label class="control-label" for="focusedInput">
												Re Password
											</label>
											<div class="controls">
												<input class="input-xlarge focused" id="repassword" type="password" name="repassword">
												<span class="help-inline" id="msg_re_pwd" ></span>
											</div>
										</div>												
										<div class="form-actions">
											<div class="actions">
												<button id="submit-btn" type="button" onclick="javascript:submitData();" class="btn btn-primary">
													Save
												</button>
												<button class="btn" type="reset"
													onclick="window.location.href='javascript:history.go(-1);'">
													Cancel
												</button>
											</div>
										</div>
									</form>
								</fieldset>
							</div>
						</div>
					</div>
					<!--/row-->

					<div class="row-fluid">

					</div>

					<!-- content ends -->
				</div>
				<!--/#content.span10-->
			</div>
			<!--/fluid-row-->

			<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

		</div>
		<!--/.fluid-container-->

		
		<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
		<script src="/form/jquery.form.js"></script>
		<script>
			function submitData() {
				var newPasswd = $("#password").val();
				var rePasswd = $("#repassword").val();
				if(newPasswd != rePasswd) {
					$("#msg_re_pwd").append("The new password input is not the same.");
					return;
				}
				$("#subForm").submit();
			}
		</script>
	</body>
</html>
