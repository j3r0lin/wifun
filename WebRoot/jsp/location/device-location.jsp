<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>WiFun</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>

<style>
.cloud-table-online {
	width: 14px;
	height: 14px;
	margin: 3px 0 0 3px;
	background-image: url(../../img/googlemap/cloud-table-online.png);
}

.cloud-table-offline {
	width: 14px;
	height: 14px;
	margin: 3px 0 0 3px;
	background-image: url(../../img/googlemap/cloud-table-offline.png);
}
.map-loading{
	  width: 100%;
	  height: 100%;
	  position: absolute;
	  top: 0;
	  left: 0;
	  z-index: 100;
	  text-align: center;
	  background-color: rgba(0, 0, 0, 0.5);
}
</style>
</head>

<body>
	<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
	<div class="container-fluid">
		<div class="row-fluid">

			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
				<s:param name="type">16</s:param>
			</s:action>
			<!-- left menu ends -->

			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>
						You need to have <a href="http://en.wikipedia.org/wiki/JavaScript"
							target="_blank">JavaScript</a> enabled to use this site.
					</p>
				</div>
			</noscript>

			<!--/#content.span10-->
			<div id="content" class="span10">
				<div>
					<ul class="breadcrumb">
						<li><a href="#">Device Location</a>
						</li>
					</ul>
				</div>

				<div class="row-fluid sortable">
					<!-- device list -->
					<div id="device" class="box span3">
						<div class="box-header well">
							<h2>
								<i class="icon-tasks"></i> Device List
							</h2>
						</div>
						<div class="box-content">
							<div id="queryForm" style="padding-bottom:10px">
								<table>
									<tr>
										<s:if test="#session['account_session'].type==1">
											<td><label class="control-label">Company</label>
											</td>
											<td>
												<div class="controls">
													<input id="hiddenCompany" type="hidden" value="${companyId}" />
													<select sid="companyId" name="companyId" style="width:130px">
														<s:iterator value="companyList">
															<option value="${id}"
																<s:if test="id == companyId">selected="selected"</s:if>>${name}</option>
														</s:iterator>
													</select>
												</div>
											</td>
										</s:if>
									</tr>
									
								</table>
							</div>

							<table id="deviceTable"
								class="table table-striped table-bordered bootstrap-datatable"
								style="cursor:pointer;width:100%">
							</table>

							<div id="table-loading" class="table table-striped"
								style="display:none">
								<div style="margin:0 auto;">
									<img src="/img/ajax-loaders/ajax-loader-1.gif"
										title="img/ajax-loaders/ajax-loader-1.gif">
									<p>loading data</p>
								</div>
							</div>
						</div>
					</div>

					<!-- device location -->
					<div id="device" class="box span9">
						<div class="box-header well">
							<h2>
								<i class="icon-th"></i> Device Location
							</h2>
						</div>
						<div class="box-content" style="padding: 0px; display: block;position: relative;">
							<div id="map-canvas" style="width:100%;height:600px"></div>
							<div id="map-loading" class="map-loading" style="display:none">
								<div style=" display:inline-block;margin-top: 282px;">
									<img src="/img/ajax-loaders/ajax-loader-5.gif" title="img/ajax-loaders/ajax-loader-1.gif">
									<br/>
									<span style="color:#fff">Loading Map....</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!--/fluid-row-->

		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Alert</h3>
			</div>
			<div class="modal-body">
				<p>Are you sure delete the device?</p>
			</div>
			<div class="modal-footer">
				<a href="javascript:void(0);" class="btn" id="closeBtn"
					data-dismiss="modal">No</a> <a href="javascript:deleteDevice();"
					class="btn btn-primary">Yes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->
	<div id="infoWindowTemplate" style="display:none">
		<div sid="infoWindow">
			<div sid="siteNotice"></div>
			<h1 sid="firstHeading" class="firstHeading"></h1>
			<div sid="bodyContent">
				<p>this is a test content.</p>
			</div>
		</div>
	</div>


	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

	<script type="text/javascript"
		src="http://maps.google.cn/maps/api/js?key=#需要替换#&language=en_US"></script>
	<script type="text/javascript" src="/jsp/location/js/devicelocation.js"></script>

</body>
</html>
