<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>WiFun</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>

<style>
.cloud-table-online {
	width: 14px;
	height: 14px;
	margin: 3px 0 0 3px;
	background-image: url(../../img/googlemap/cloud-table-online.png);
}

.cloud-table-offline {
	width: 14px;
	height: 14px;
	margin: 3px 0 0 3px;
	background-image: url(../../img/googlemap/cloud-table-offline.png);
}
.map-loading{
	  width: 100%;
	  height: 100%;
	  position: absolute;
	  top: 0;
	  left: 0;
	  z-index: 100;
	  text-align: center;
	  background-color: rgba(0, 0, 0, 0.5);
}
</style>
</head>

<body>
	<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
	<div class="container-fluid">
		<div class="row-fluid">

			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
				<s:param name="type">17</s:param>
			</s:action>
			<!-- left menu ends -->

			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>
						You need to have <a href="http://en.wikipedia.org/wiki/JavaScript"
							target="_blank">JavaScript</a> enabled to use this site.
					</p>
				</div>
			</noscript>

			<!--/#content.span10-->
			<div id="content" class="span10">
				<div>
					<ul class="breadcrumb">
						<li><a href="#">Device Location</a>
						</li>
					</ul>
				</div>

				<div class="row-fluid sortable">
					<!-- device list -->
					<div id="device" class="box span3">
						<div class="box-header well">
							<h2>
								<i class="icon-tasks"></i> Device List
							</h2>
						</div>
						<div class="box-content">
							<div id="queryForm" style="padding-bottom:10px">
								<table>
									<tr>
										<s:if test="#session['account_session'].type==1">
											<td><label class="control-label">Company</label>
											</td>
											<td>
												<div class="controls">
												    <input id="hiddenCompany" type="hidden" value="${companyId}" />
													<select id="companyId" name="companyId" style="width:130px">
														<s:iterator value="companyList">
															<option value="${id}"
																<s:if test="id == companyId">selected="selected"</s:if>>${name}</option>
														</s:iterator>
													</select>
												</div>
											</td>
										</s:if>
										<td>
											
										</td>
									</tr>
									<tr>
										<td>
											<label class="control-label" style="float: right;">Date</label>
										</td>
										<td >
											<div class="controls">
												<input id="inputDate" style="width:130px" type="text" class="datepicker" id="startDate" name="startDate" value="${date}">
											</div>
										</td>
									</tr>
									<tr>
											<td>
												<label class="control-label" style="float: right;">Time</label>
											</td>
											
											<td>
												<div class="controls">
													<select id="startTime" style="width:55px">
														<option value='0'>0</option>
														<option value='1'>1</option>
														<option value='2'>2</option>
														<option value='3'>3</option>
														<option value='4'>4</option>
														<option value='5'>5</option>
														<option value='6'>6</option>
														<option value='7'>7</option>
														<option value='8'>8</option>
														<option value='9'>9</option>
														<option value='10'>10</option>
														<option value='11'>11</option>
														<option value='12'>12</option>
														<option value='13'>13</option>
														<option value='14'>14</option>
														<option value='15'>15</option>
														<option value='16'>16</option>
														<option value='17'>17</option>
														<option value='18'>18</option>
														<option value='19'>19</option>
														<option value='20'>20</option>
														<option value='21'>21</option>
														<option value='22'>22</option>
														<option value='23'>23</option>
													</select>-
													<select id="endTime" style="width:55px">
														<option value='1'>1</option>
														<option value='2'>2</option>
														<option value='3'>3</option>
														<option value='4'>4</option>
														<option value='5'>5</option>
														<option value='6'>6</option>
														<option value='7'>7</option>
														<option value='8'>8</option>
														<option value='9'>9</option>
														<option value='10'>10</option>
														<option value='11'>11</option>
														<option value='12'>12</option>
														<option value='13'>13</option>
														<option value='14'>14</option>
														<option value='15'>15</option>
														<option value='16'>16</option>
														<option value='17'>17</option>
														<option value='18'>18</option>
														<option value='19'>19</option>
														<option value='20'>20</option>
														<option value='21'>21</option>
														<option value='22'>22</option>
														<option value='23'>23</option>
														<option value='24' selected>24</option>
													</select>
												</div>
											</td>
									</tr>
									
								</table>
							</div>

							<table id="deviceTable" class="table table-striped table-bordered bootstrap-datatable"
								style="cursor:pointer;width:100%">
							</table>
							
							<div id="table-loading" class="table table-striped" style="display:none">
								<div style="margin:0 auto;">
									<img src="/img/ajax-loaders/ajax-loader-1.gif" title="img/ajax-loaders/ajax-loader-1.gif">
									<p>loading data</p>
								</div>
							</div>
						</div>
					</div>

					<!-- device location -->
					<div id="device" class="box span9">
						<div class="box-header well">
							<h2>
								<i class="icon-th"></i> Device Route
							</h2>
						</div>
						<div id="map-box" class="box-content" style="padding: 0px; display: block;position: relative;">
							<div id="map-canvas" style="width:100%;height:600px;z-index:1"></div>
							
							<div id="map-noData" style="padding: 20px;display:none;height:600px">
								<h3>There are no matching datas in the system.</h3>
							</div>
							
							<div id="map-loading" class="map-loading">
								<div style=" display:inline-block;margin-top: 282px;">
									<img src="/img/ajax-loaders/ajax-loader-5.gif" title="img/ajax-loaders/ajax-loader-1.gif">
									<br/>
									<span style="color:#fff">Loading Map....</span>
								</div>
							</div>
						</div>
						
					</div>
				</div>
			</div>
		</div>
		<!--/fluid-row-->
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Alert</h3>
			</div>
			<div class="modal-body">
				<p>Are you sure delete the device?</p>
			</div>
			<div class="modal-footer">
				<a href="javascript:void(0);" class="btn" id="closeBtn"
					data-dismiss="modal">No</a> <a href="javascript:deleteDevice();"
					class="btn btn-primary">Yes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->
	<div id="infoWindowTemplate" style="display:none">
		<div sid="infoWindow" >
			<div sid="siteNotice"></div>
			<h1 sid="firstHeading" class="firstHeading"></h1>
			<div sid="bodyContent">
				<p>this is a test content.</p>
			</div>
		</div>
	</div>

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

	<script type="text/javascript"
		src="http://maps.google.cn/maps/api/js?key=#需要替换#&language=en_US"></script>
	<script type="text/javascript" src="/jsp/location/js/deviceroute.js"></script>

</body>
</html>
