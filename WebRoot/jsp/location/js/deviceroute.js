//map value
var map=null;
var markers = [];
var infoWindow = null;
//路线相关
var directionsService = null;
var directionsDisplay = null;
//device value
var deviceList = null;
//location value
var locationList = [];
var showLocationList = [];

//当前寻中的device
var selectedDevice = null;

$(document).ready(function(){
	
	LoadParam();
	//加载数据
	initDeviceData({companyId:$("#hiddenCompany").val()});
	//加载地图
	initializeMap();
	
	//绑定行单击事件
	$("#deviceTable").on("click","tr",rowClick);
	//绑定search按钮事件
	$("#companyId").change(function(){
		var param = {
			companyId:$("#companyId").val()
		};
		//alert(param.companyId);
		initDeviceData(param);
	});
	
	//增加日期，时间选择联动
	$("#inputDate").change(function(){
		if(!selectedDevice){return;}
		
		loadPath(selectedDevice,$("#inputDate").val());
	});
	
	$("#startTime").change(function(){
		if(!selectedDevice){return;}
		
		loadPath(selectedDevice,$("#inputDate").val());
	});
	$("#endTime").change(function(){
		if(!selectedDevice){return;}
		
		loadPath(selectedDevice,$("#inputDate").val());
	});
});
function LoadParam(){
	var companyId = getUrlParam("companyId");
	if(companyId>0) {
		$("#hiddenCompany").val(companyId);
		$("#companyId").val(companyId);
	}
	
	var deviceId = getUrlParam("deviceId");
	if(deviceId){
		var deviceItem = {deviceId:deviceId};
		loadPath(deviceItem,$("#inputDate").val());
	}
	
	selectedDevice = {deviceId:deviceId};
}
//加载设备数据
function initDeviceData(param){
	
	showTableLoading();
	
	var deviceUrl = "/route/device-list.action";
	
	$.ajax({
		type:"get",
		url:deviceUrl,
		data:param,
		async:true,
		success:function(data){
			
			hideTableLoading();
			deviceList  = JSON.parse(data).deviceList;
			if(!deviceList || deviceList.length<1){
				deviceList= [];
			}
			//deviceList 根据status排序
			deviceList.sort(function(a,b){
				if(a.online>b.online){
					return -1;
				}
				if(a.online<b.online){
					return 1;
				}
				return 0;
			});
			//加载数据表格
			loadTable(deviceList);
		},
		error:function(){
			
			deviceList= [];
			loadTable(deviceList);
			hideTableLoading();
			hideMapLoading();
		}
	});
};
//初始化标签数据
function loadTable(deviceList){
	
	$('#deviceTable').dataTable({
		destroy: true,
		autoWidth:true,
	    data: deviceList,
	    paging:false,
	    searching:false,
	    columns: [
            {Title:"Checked",name:"checked",width:'20%',render:function(data, type, full, meta){
            	if(selectedDevice!=undefined && full.deviceId == selectedDevice.deviceId){
            		selectedDevice=full;
            		return '<input type="radio" name="checked" checked="checked" />';
            	}
            	return '<input type="radio" name="checked" />';
            }},
            {title:"Status",name:"status",data: 'online',width:'20%',render:statusRender},
	        {title:"Device Name",name:"devicename", data:'name', width:'80%' }
	    ]
	});
};
function statusRender(data, type, full, meta){
    	var div = $("<div></div>");
    	if(data==0){
    		div.removeClass("cloud-table-online").addClass("cloud-table-offline");
    	}
    	else{
    		div.removeClass("cloud-table-offline").addClass("cloud-table-online");
    	}
    	return div.prop("outerHTML");
}

function rowClick(eventHander){
	var tr = this;
	var rowIndex = $(tr).index();
	var itemData = deviceList[rowIndex];
	
	if(!itemData){
		return;
	}
	
	//设置选中标签
	$(this).find("input[type=radio]").attr("checked","checked");
	
	selectedDevice = itemData;
	
	map.setCenter(new google.maps.LatLng(itemData.latitude,itemData.longitude));
	
	
	var date = $("#inputDate").val();
	if(!date || date==''){
		alert("Please choose Date");
		return;
	}
	
	//下载路线
	loadPath(itemData,date);
};
//初始化地图
function initializeMap() {

	showMapLoading();
	
	var mapOptions = {
		mapTypeId:google.maps.MapTypeId.ROADMAP,//地图的类型（MapTypeId.HYBRID：卫星图（含地名），MapTypeId.SATELLITE：卫星图，MapTypeId.ROADMAP：交通地图，MapTypeId.TERRAIN：地形图）
		center: {
			lat: 38.8993488,
			lng: -77.0145665
		},
		panControl:true,
		zoomControl:true,
		mapTypeControl:true,
		scaleControl:true,
		streetViewControls:true,
		overviewMapControl:true,
		zoom: 5 //缩放比例
	};
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	
	//初始化infoWIndow
	infowindow= new google.maps.InfoWindow({
	      //content: $("#infoWindow").prop("innerHTML")
    });
	
	setTimeout(function(){
		hideMapLoading();
	},2000);
};

//创建需要显示的点路线
function createShowDeviceList(deviceRout){
	//显示的location点数组
	showLocationList = [];
	
	var startHour = parseInt($("#startTime").val());
	var endHour = parseInt($("#endTime").val());

	if(endHour<startHour){
		alert("endHour less then startHour");
		return;
	}
	
	var tempList  = getLocationListInTime(startHour,endHour,deviceRout);
	
	if(tempList.length<=8){
		showLocationList = tempList;
		return;
	}
	
	showLocationList.push(tempList[0]);
		
	var limit = Math.ceil(tempList.length/6);
	for(var i=1;i< tempList.length-1;i++){
		
		if(i%limit!=1){
			continue;
		}
		
		var item = tempList[i];
		showLocationList.push(item);
	}
	
	showLocationList.push(tempList[tempList.length-1]);
}
//获取符合时间点内的deviceList
function getLocationListInTime(startHour,endHour,locationList){
	
	var returnList = [];
	$.each(locationList,function(index,item){
		var time = item.time;
		var hour = new Date(time).getHours();
		
		if(hour<endHour && hour>=startHour){
			returnList.push(item);
		}
	});
	
	return returnList;
}

//google地图路线相关
function loadPath(deviceItem,date){

	showMapLoading();
	clearMarkers();
	var param = {};
	
	param.deviceId = deviceItem.deviceId;
	param.date = date;
	
	var deviceRout = null;
	var deviceRouteUrl= "/route/device-route.action";
	$.ajax({
		type:"get",
		url:deviceRouteUrl,
		data:param,
		async:true,
		success:function(data){
			deviceRout  = JSON.parse(data).deviceRoute;
			if(!deviceRout || deviceRout.length<1){
				showMapNoData();
				return;
			}
			
			locationList = deviceRout;
			//创建用于显示的deviceList
			createShowDeviceList(deviceRout);
			
			if(!showLocationList || showLocationList.length<1){
				showMapNoData();
				return;
			}
			
			//显示路线
			showPath(showLocationList);
			//设置Markers
			setMakers(showLocationList);
			
			setTimeout(function(){
				hideMapLoading();
				showMapDiv();
			},2000);
		},
		error:function(){
			$("#map-noData").html("<h3>ajax map data error.</h3>");
			hideMapLoading();
		}
	});
};


function showMapLoading(){
	$("#map-loading").show();
}

function hideMapLoading(){
	$("#map-loading").hide();
}

function showMapNoData(){
	hideMapLoading();
	$("#map-canvas").hide();
	$("#map-box").show();
	$("#map-noData").show();
}

function showMapDiv(){
	hideMapLoading();
	$("#map-box").show();
	$("#map-canvas").show();
	$("#map-noData").hide();
}

function showPath(deviceRouteList) {

	clearPath();
	if(deviceRouteList.length<2){
		return;
	}
	
	directionsService = new google.maps.DirectionsService(); //实例构造
    directionsDisplay = new google.maps.DirectionsRenderer({
		markerOptions: {
			'map': map
		} //行车路线
	});
	directionsDisplay.setMap(map);
	//初始化起点
	var start = deviceRouteList[0];
	var origin = new google.maps.LatLng(start.latitude,start.longitude);
	//初始化终点
	var end = deviceRouteList[deviceRouteList.length-1];
	var destination = new google.maps.LatLng(end.latitude,end.longitude);
	//构建中间点
	var waypoints= [];
	
	//商业版最多23个点绘制路径
	//免费版最多8个点画路径
	for(var i=1;i< deviceRouteList.length-1;i++){
		var item = deviceRouteList[i];
		var point =  new google.maps.LatLng(item.latitude,item.longitude);
		waypoints.push({location:point,stopover:false});
	}
	
	var request = {
		origin: origin, //起点
		destination: destination, //终点
		optimizeWaypoints: false, //为true，重新排列中间路标顺序，最大程度降低路线整体成本
		waypoints: waypoints, //中间经过的点
		travelMode: google.maps.TravelMode.DRIVING, //驾车路线
		unitSystem: google.maps.UnitSystem.METRIC //单位为米
	};

	directionsService.route(request, function(response, status) {
		if (status == google.maps.DirectionsStatus.OK) {
			directionsDisplay.setDirections(response); //将返回的路线信息赋给directionsDisplay，显示在地图上
		} else {
			console.log(status);
		}
	});
}
//清除路径
function clearPath(){
	directionsService = null;
	if(directionsDisplay){
		directionsDisplay.setMap(null);
	}
}


//设置地图标签
function setMakers(deviceList){
	
	if(markers && markers.length>0){
		clearMarkers();
		markers = [];
	}
	if(deviceList.length<1){return;}
	
	var data = deviceList[0];
	map.setCenter(new google.maps.LatLng(data.latitude,data.longitude));
	
	//清空marker列表
	
	
	$.each(deviceList,function(index,item){
		
		var position = new google.maps.LatLng(item.latitude,item.longitude);
		
		var img= "";
		if(index==0){
			img = "/img/googlemap/1.png";
		}else if (index == deviceList.length-1){
			img = "/img/googlemap/3.png";
		}
		else{
			img = "/img/googlemap/4.png";
		}
		
		var beachMarker = new google.maps.Marker({
			title:item.time,
			position: position,
			animation: google.maps.Animation.DROP, //动画
			icon: img,
			dataid:item.time
		});
		beachMarker.setMap(map);
		markers.push(beachMarker);
		//marker点击显示
		google.maps.event.addListener(beachMarker, 'click', markerClick);
	});
};

function clearMarkers(){
	$.each(markers,function(index,marker){
		marker.setMap(null);
	});
};

function markerClick(latlng){
	var marker = this;
	// 此处可以对contontstring进行编辑
	showInfoWindow(marker);
};


var infoTemplate = $("#infoWindowTemplate").html();;

function showInfoWindow(marker){
	
   var dataId = marker.dataid;
   var itemData;
   $.each(showLocationList,function(index,item){
		if (item.time == dataId){
			itemData = item;
			return true;
		}
   });
	
	if(!itemData){return;}
	
	var contentCom = $(infoTemplate);
	contentCom.find("[sid=firstHeading]").html(itemData.name);
	contentCom.find("[sid=bodyContent]").html(
			"Time : "+itemData.time);
	
	var contentString = contentCom.prop("outerHTML");
	infowindow.setContent(contentString);
	infowindow.open(map,marker);
};

function showTableLoading(){
	$("#deviceTable").hide();
	$("#deviceTable_info").hide();
	$("#table-loading").show();
};
function hideTableLoading(){
	$("#deviceTable").show();
	$("#deviceTable_info").show();
	$("#table-loading").hide();
};

function getUrlParam(name) {
    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)"); //构造一个含有目标参数的正则表达式对象
    var r = window.location.search.substr(1).match(reg);  //匹配目标参数
    if (r != null) return unescape(r[2]); return null; //返回参数值
}

