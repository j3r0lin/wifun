//bus设备页面
var map=null;
var markers = [];
var deviceList = null;
var infoWindow = null;
var table = null;
$(document).ready(function(){
	
	//加载数据
	initDeviceData({companyId:$("#hiddenCompany").val()});
	//加载地图
	initializeMap();
	//绑定行单击事件
	$("#deviceTable").on("click","tr",rowClick);
	//绑定search按钮事件
	$("select[sid=companyId]").change(function(){
		var param = {
			companyId:$("select[sid=companyId]").val()
		};
		//alert(param.companyId);
		initDeviceData(param);
	});
});

//加载设备数据
function initDeviceData(param){
	
	showTableLoading();
	
	var deviceUrl = "/location/device-list.action";
	
	$.ajax({
		type:"get",
		url:deviceUrl,
		data:param,
		async:true,
		success:function(data){
			hideTableLoading();
			deviceList  = JSON.parse(data).deviceList;
			if(!deviceList || deviceList.length<1){
				deviceList= [];
			}
			//deviceList 排序
			deviceList.sort(function(a,b){
				if(a.online>b.online){
					return -1;
				}
				if(a.online<b.online){
					return 1;
				}
				return 0;
			});
			
			//加载数据表格
			loadTable(deviceList);
			//设置地图maker
			setMakers(deviceList);
		},
		error:function(){
			deviceList = [];
			loadTable(deviceList);
			hideTableLoading();
		}
	});
};
//初始化标签数据
function loadTable(deviceList){
	
	//先将表格对象销毁，不然报错
	table = $('#deviceTable').dataTable({
		destroy: true,
		autoWidth:true,
	    data: deviceList,
	    paging:false,
	    searching:false,
	    columns: [
            {title:"Status",data: 'online',width:'30%',render:statusRender},
	        {title:"Device Name",data:'name', width:'40%' },
	        {width:'30%',render:routeRender}
	    ]
	
	});
};
function statusRender(data, type, full, meta){
    	var div = $("<div></div>");
    	if(data==0){
    		div.removeClass("cloud-table-online").addClass("cloud-table-offline");
    	}
    	else{
    		div.removeClass("cloud-table-offline").addClass("cloud-table-online");
    	}
    	return div.prop("outerHTML");
}

function routeRender(data,type,full,meta){
	var companyId = full.companyId;
	var deviceId = full.deviceId;
	
	return '<a href="/route/device-routeIndex.action?companyId='+companyId+'&deviceId='+deviceId+'">device route</a>';
}

function rowClick(eventHander){
	var tr = this;
	var rowIndex = $(tr).index();
	var itemData = deviceList[rowIndex];
	if(!itemData){
		return;
	}
	map.setCenter(new google.maps.LatLng(itemData.latitude,itemData.longitude));
	
	if(markers){
		showInfoWindow(markers[rowIndex]);
	}
};
//初始化地图
function initializeMap() {
	
	showMapLoading();
	
	var mapOptions = {
		mapTypeId:google.maps.MapTypeId.ROADMAP,//地图的类型（MapTypeId.HYBRID：卫星图（含地名），MapTypeId.SATELLITE：卫星图，MapTypeId.ROADMAP：交通地图，MapTypeId.TERRAIN：地形图）
		center: {
			lat: 38.8993488,
			lng: -77.0145665
		},
		panControl:true,
		zoomControl:true,
		mapTypeControl:true,
		scaleControl:true,
		streetViewControls:true,
		overviewMapControl:true,
		zoom: 5 //缩放比例
	};
	map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
	
	//初始化infoWIndow
	infowindow= new google.maps.InfoWindow({});
	
	setTimeout(function(){
		hideMapLoading();
	},2000);
	
};
//设置地图标签
function setMakers(deviceList){

	showMapLoading();
	var data = deviceList[0];
	if(!data){
		hideMapLoading();
		return;
	}
	map.setCenter(new google.maps.LatLng(data.latitude,data.longitude));
	
	//清空marker列表
	markers = [];
	$.each(deviceList,function(index,item){
		var position = new google.maps.LatLng(item.latitude,item.longitude);
		var img = "/img/googlemap/ui-gis-setting2.png";
		if(item.online==0){
			img = "/img/googlemap/ui-gis-setting4.png";
		}
		
		var beachMarker = new google.maps.Marker({
			position: position,
			//title:"it",
			animation: google.maps.Animation.DROP, //动画
	        //map: map,
			icon: img,
			dataid:item.sn
		});
		beachMarker.setMap(map);
		markers.push(beachMarker);
		google.maps.event.addListener(beachMarker, 'click', markerClick);
	});
	
	setTimeout(function(){
		hideMapLoading();
	},1000);
};

function markerClick(latlng){
	var marker = this;
	
    //alert(self.dataid);
	// 此处可以对contontstring进行编辑
	showInfoWindow(marker);
};


var infoTemplate = $("#infoWindowTemplate").html();
function showInfoWindow(marker){
	var dataId = marker.dataid;
	
	var itemData;
	$.each(deviceList,function(index,item){
		if (item.sn == dataId){
			itemData = item;
			return true;
		}
	});
	
	var contentCom = $(infoTemplate);
	contentCom.find("[sid=firstHeading]").html(itemData.name);
	contentCom.find("[sid=bodyContent]").html(
			"SN : "+itemData.sn 
			+"<br/> Group : "
			+itemData.groupName);
	
	var contentString = contentCom.prop("outerHTML");
	infowindow.setContent(contentString);
	infowindow.open(map,marker);
}

//table loading 
function showTableLoading(){
	$("#deviceTable").hide();
	$("#deviceTable_info").hide();
	$("#table-loading").show();
};
function hideTableLoading(){
	$("#deviceTable").show();
	$("#deviceTable_info").show();
	$("#table-loading").hide();
};
//maploading
function showMapLoading(){
	$("#map-loading").show();
}
function hideMapLoading(){
	$("#map-loading").hide();
}






