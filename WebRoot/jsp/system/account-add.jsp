<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>System Account- iLIKEBUS</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>

</head>

<body>

	<jsp:include page="/jsp/global/header.jsp"></jsp:include>

	<div class="container-fluid">
		<div class="row-fluid">

			<s:action name="menu" executeResult="true">
				<s:param name="type">22</s:param>
			</s:action>

			<div id="content" class="span10">
				<!-- content starts -->
				<div class="row-fluid">
					<div class="box span12">
						<div class="box-header well">
							<h2>
								<i class="icon-user"></i> Account Add
							</h2>
							<div class="box-icon" style="padding-right: 20px;">
								<a class="back" href="javascript:history.go(-1);" title="Back"><span
									class="icon32 icon-blue icon-undo"></span> </a>
							</div>
						</div>
						<div class="box-content">
							<fieldset>
								<form class="form-horizontal" id="subForm" name="subForm"
									action="/system/system-account-add-save.action" method="post">
									<div class="control-group" id="group_username">
										<label class="control-label"> Account Name </label>
										<div class="controls">
											<input id="username" type="text" onblur="checkVality();" name="username"
												msg="Please enter 6 - 20 characters(A-Z,a-z,0-9 only)" />
											<span class="help-inline" id="msg_username"></span>
										</div>
									</div>
									<div class="control-group" id="group_password">
										<label class="control-label" for="focusedInput"> Account Password </label>
										<div class="controls">
											<input onblur="outBlur(this.id);" id="password" type="text" name="password"
												value="123456" msg="Please enter your Password">
											<span class="help-inline" id="msg_password"></span>
										</div>
									</div>
									<div class="control-group" id="group_isActive">
										<label class="control-label" for="focusedInput"> IsActive </label>
										<div class="controls">
											<input class="input-xlarge focused" id="isActive" type="checkbox" name="valid"
												value="true" checked="checked">
											<span class="help-inline" id="msg_isActive"></span>
										</div>
									</div>
									<div class="form-actions">
										<div class="actions">
											<button id="submit-btn" type="button" onclick="submitForm();" class="btn btn-primary">
												Save</button>
											<button class="btn" type="reset"
												onclick="window.location.href='javascript:history.go(-1);'">Cancel</button>
										</div>
									</div>
								</form>
							</fieldset>
						</div>
					</div>
				</div>
				<!--/row-->

				<div class="row-fluid"></div>

				<!-- content ends -->
			</div>
			<!--/#content.span10-->
		</div>
		<!--/fluid-row-->

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->


	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

</body>
<script type="text/javascript">		
	
	function submitForm(){ 
		if(!outBlur("username")){
			return;
		}
		$("#subForm").submit();
	}		
	//失去焦点
	function outBlur(id){
	   if($.trim($("#"+id).val())==''){	   
		     $("#group_"+id).addClass("error");
		     var msg = $("#"+id).attr("msg");
		     $("#msg_"+id).text(msg);	    
		 }else{	     
			 $("#group_"+id).removeClass("error");
			 $("#msg_"+id).text("");
			 return true;		 
	   	}	
	    return false;
	} 	

	//检查用户名有效性
	function checkVality(){
	    
	    var name = $.trim($("#username").val());	    
	    	    
	    if(name!=""){
	    	//检查账户格式 Please enter 6 - 20 characters(A-Z,a-z,0-9 only)
	    	if(!validLoginname(name))
	    	{
	    	  	var msg = $("#username").attr("msg");
		    	$("#msg_username").text(msg);
		    	$("#group_username").addClass("error");
		    	return false;
	    	}
	    	
	    	// 是否占用
	        if(!checkUserName(name)){
		       $("#msg_username").text("This account is in use, Please use a new account.");
		       $("#group_username").addClass("error");
		    }else{
		       $("#msg_username").text("");
		       $("#group_username").removeClass("error");	
		       return true; 
		    }
		}else{ 
		    var msg = $("#username").attr("msg");
		    $("#msg_username").text(msg);
		    $("#group_username").addClass("error");
		}
		return false;
	}
	
	//检查用户名
	function checkUserName(username){		
			if(username == null ||jQuery.trim(username).length==0){
			   return false;
			 }
			 var flag=false;			 
			 $.ajax({
			    type: "POST",
			    url:"/system/validate-system-account.action",
			    async:false,
			    data:{username:username,random:Math.random()},
			    dataType:"json",
			    success:function(data){
			      if(data.result==1){
		             flag = true;
		          }
			    }
			 });
			 return flag;
	}			
	
	//验证账号格式
	function validLoginname(value) {	
	           var regex =/^[a-zA-Z0-9]{6,20}$/;		            
			   return regex.test(value); 
	}	
</script>

</html>
