<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>System Account- iLIKEBUS</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
<meta name="author" content="Muhammad Usman">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>
<body>
	<div class="container-fluid">
		<div class="row-fluid">
			<div id="content" class="span10">
				<!-- content starts -->
				<div>
					<ul class="breadcrumb">
						<li><a href="#">Home</a> <span class="divider">/</span>
						</li>
						<li><a href="#">File Manager</a>
						</li>
					</ul>
				</div>

				<div class=" row-fluid">
					<div class="box span12">
						<div class="box-header well">
							<h2>
								<i class="icon-edit"></i> File List
							</h2>
							
						</div>
						<div class="box-content">
							<form class="form-horizontal" id="subForm" name="subForm"
									action="/system/file-list.action" method="post">
								<input type="hidden" id="movieId" name="movieId" value="${movieId}"/> 
								<input type="hidden" id="type" name="type" value="${type}"/> 
								<p>you choose file:</p><input style="width:300" id="filePath" name="filePath" value="${filePath} " readonly="readonly"/> 
								
								 <div class="form-actions">
								<button id="submit-btn" type="button"
									onclick="confirmPath();" class="btn btn-primary">
									Confirm
								</button>
								<button class="btn" type="reset"
									onclick="window.location.href='javascript:history.go(-1);'">
									Back;
								</button>
							  </div>
							</form>

						</div>
					</div>
					<!--/span-->
				</div>
				<!--/row-->
				<!-- content ends -->
			</div>
			<!--/#content.span10-->

		</div>
		<!--/fluid-row-->
		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
	</div>
	<!--/.fluid-container-->
	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3 id="pop_title">Alert</h3>
		</div>
		<div class="modal-body">
			<p id="pauseP">Are you sure you want to pause the Account?</p>
			<p id="activeP">Are you sure you want to active the Account?</p>
			<p id="resetP">
				User Name:<br />
				<input type="text" id="reset_name" />
				<br /> Password:<br />
				<input type="text" id="reset_pwd" />
			</p>
			<p id="okP">Reset the Account's password succeeded!</p>
		</div>
		<div class="modal-footer">
			<a href="javascript:void(0);" class="btn" id="closeBtn" data-dismiss="modal">No</a><a
				href="javascript:resetAccount();" class="btn btn-primary" id="resetBtn">Yes</a> <a
				href="javascript:pauseAccount();" class="btn btn-primary" id="pauseBtn">Yes</a> <a
				href="javascript:activeAccount();" class="btn btn-primary" id="activeBtn">Yes</a>
		</div>
	</div>
	<script type="text/javascript">		
	
		function confirmPath(){ 
			var movieId = $("#movieId").val();
			var type = $("#type").val();
			var filePath = $("#filePath").val();
			var typeKey;
			if ("0" == type)
			{
				typeKey = "bo.mediaUrl";
			}
			else if ("1" == type)
			{
				typeKey = "bo.hposterUrl";
			}
			else if ("2" == type)
			{
				typeKey = "bo.vposterUrl";
			}
			else
			{
				alert("parameter invalid");
				return;
			}
			
			
			var options = {
				type : "post",
				url : "/media/media-url-confirm.action",
				data : {"bo.id":movieId},
				dataType : "text",
				async : false,
				success : function(response) {
				
					if ("success" == response)
						window.close();
				} };
				
			
			options.data[typeKey] = filePath;
			
		
			$.ajax(options);
		
	
		}	
	</script>
</body>
</html>
