<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>WiFun-Console</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
<script type="text/javascript">
	function checkUser() {

	};
	//if (top.location != location)
	//top.location.href = self.location;
</script>
</head>

<body>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="row-fluid">
				<div class="span12 center login-header">
					<h2>Welcome to WiFun Platform</h2>
				</div>
				<!--/span-->
			</div>
			<!--/row-->

			<div class="row-fluid">
				<div class="well span5 center login-box">
					<s:if test="result=='error'">
						<div class="alert alert-error">Username or Password error! Please incorrect and try
							again.</div>
					</s:if>
					<s:else>
						<div class="alert alert-info">Please login with your Username and Password.</div>
					</s:else>
					<form class="form-horizontal" action="/system/do-login.action" method="post">
						<fieldset>
							<div class="input-prepend" title="Username" data-rel="tooltip">
								<span class="add-on" style="margin-top:2px"><i class="icon-user"></i></span><input class="input-large span10" onBlur="checkUser()" value="${username }" name="username"
									id="username" type="text" />
							</div>
							<div class="clearfix"></div>

							<div class="input-prepend" title="Password" data-rel="tooltip">
								<span class="add-on" style="margin-top:2px"><i class="icon-lock"></i></span><input class="input-large span10" name="password" id="password" type="password" />
							</div>
							<div class="clearfix" id="msg">${msg }</div>

							<!-- div class="input-prepend">
							<label class="remember" for="remember"><input type="checkbox" id="remember" />Remember me</label>
							</div -->
							<div class="clearfix"></div>

							<p class="center span5">
								<button type="submit" class="btn btn-primary">Login</button>
							</p>
						</fieldset>
					</form>
				</div>
				<!--/span-->
			</div>
			<!--/row-->
		</div>
		<!--/fluid-row-->
	</div>
	<!--/.fluid-container-->
	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

</body>
</html>
