<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>System Account- iLIKEBUS</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description"
	content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
<meta name="author" content="Muhammad Usman">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
<script type="text/javascript">

	var pauseId = "";
	function pauseAccountId(id) {

		$('#myModal').modal('show');
		pauseId = id;
		$("#pauseP").show();
		$("#pauseBtn").show();
		$("#activeP").hide();
		$("#activeBtn").hide();
		$("#resetP").hide();
		$("#resetBtn").hide();
		$("#okP").hide();
		$("#closeBtn").html("NO");
		$("#pop_title").html("Alert");
	};
	
	var activeId = "";
	function activeAccountId(id) {

		$('#myModal').modal('show');
		activeId = id;
		$("#pauseP").hide();
		$("#pauseBtn").hide();
		$("#activeP").show();
		$("#activeBtn").show();
		$("#resetP").hide();
		$("#resetBtn").hide();
		$("#okP").hide();
		$("#closeBtn").html("NO");
		$("#pop_title").html("Alert");
	};
	
	var resetId = "";
	function resetAccountId(id, name, pwd) {

		$('#myModal').modal('show');
		resetId = id;
		$("#pauseP").hide();
		$("#pauseBtn").hide();
		$("#activeP").hide();
		$("#activeBtn").hide();
		$("#resetP").show();
		$("#resetBtn").show();
		$("#okP").hide();
		$("#closeBtn").html("NO");
		$("#pop_title").html("Reset Password");
		$("#reset_name").val(name);
		$("#reset_pwd").val(pwd);
	};
	
	function pauseAccount() {

		$("#closeBtn").trigger("click");
		$.ajax({
			type : "POST",
			url : "/system/pause-account.action",
			data : {
				id : pauseId
			},
			dataType : 'json',
			success : function(data) {

				// 0,系统错误,1 成功,3参数错误
				if (data.result == 1) {
					//window.document.location.reload();
					submitFormSelect();
				} else if (data.result == 0) {
					$("#sysErrorAlert").trigger("click");

				} else if (data.result == 3) {
					$("#paramErrorAlert").trigger("click");
				}
			},
			error : function(e) {

				$("#connectionAlert").trigger("click");
			}
		});
	};
	
	function activeAccount() {

		$("#closeBtn").trigger("click");
		$.ajax({
			type : "POST",
			url : "/system/active-account.action",
			data : {
				id : activeId
			},
			dataType : 'json',
			success : function(data) {

				// 0,系统错误,1 成功,3参数错误
				if (data.result == 1) {
					//window.document.location.reload();
					submitFormSelect();
				} else if (data.result == 0) {
					$("#sysErrorAlert").trigger("click");

				} else if (data.result == 3) {
					$("#paramErrorAlert").trigger("click");
				}
			},
			error : function(e) {

				$("#connectionAlert").trigger("click");
			}
		});
	};

	function resetAccount() {
		var newName = $("#reset_name").val();
		var newPwd = $("#reset_pwd").val();
		$("#closeBtn").trigger("click");
		$.ajax({
			type : "POST",
			url : "/system/reset-account.action",
			data : {
				id : resetId,
				name : newName,
				pwd : newPwd
			},
			dataType : 'json',
			success : function(data) {

				// 0,系统错误,1 成功,3参数错误
				if (data.result == 1) {
					//window.document.location.reload();
					//alert("Reset account password success!");
					//setTimeout("showOk()",1000);
					submitFormSelect();
				} else if (data.result == 0) {
					$("#sysErrorAlert").trigger("click");

				} else if (data.result == 3) {
					$("#paramErrorAlert").trigger("click");
				}
			},
			error : function(e) {

				$("#connectionAlert").trigger("click");
			}
		});
	};
	
	function showOk() {

		$('#myModal').modal('show');
		$("#pauseP").hide();
		$("#pauseBtn").hide();
		$("#activeP").hide();
		$("#activeBtn").hide();
		$("#resetP").hide();
		$("#resetBtn").hide();
		$("#okP").show();
		$("#closeBtn").html("OK");
	};
	function submitFormSelect() {

		window.location.reload();
	};
</script>
</head>

<body>

	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<div class="container-fluid">
		<div class="row-fluid">
			<s:action name="menu" executeResult="true">
				<s:param name="type">22</s:param>
			</s:action>

			<div id="content" class="span10">
				<!-- content starts -->
				<div>
					<ul class="breadcrumb">
						<li><a href="#">Home</a> <span class="divider">/</span>
						</li>
						<li><a href="#">System Account</a>
						</li>
					</ul>
				</div>

				<div class=" row-fluid">
					<div class="box span12">
						<div class="box-header well">
							<h2>
								<i class="icon-edit"></i> Account List
							</h2>
							<div style="float:right;">
								<a class="btn btn-primary" href="/system/system-account-add.action"> <i
									class="icon icon-white icon-plus"></i>&nbsp;Add Account </a>
							</div>
						</div>
						<div class="box-content">
							<form class="form-horizontal">
								<table class="table table-striped table-bordered datatable dataTable">
									<thead>
										<tr>
											<th>User Name</th>
											<th>Create Time</th>
											<th>Last Login Time</th>
											<th>IsActive</th>
											<th>Action</th>
										</tr>
									</thead>

									<tbody>
										<s:iterator value="accountList">
											<tr>
												<td>${username}</td>
												<td><s:property
														value="@com.ilikebus.admin.util.TimeUtils@DateToString(createTime,'MM/dd/yy HH:mm')" />
												</td>
												<td><s:property
														value="@com.ilikebus.admin.util.TimeUtils@DateToString(lastLoginDate,'MM/dd/yy HH:mm')" />
												</td>
												<td><s:if test="valid">
														<span class="icon icon-green icon-check"></span>
													</s:if> <s:else>
														<span class="icon icon-red icon-close"></span>
													</s:else>
												</td>
												<td><s:if test="valid">
														<a id="pause" class="btn btn-danger btn-setting" onclick="pauseAccountId(${id});"
															href="javascript:void(0);"><i class="icon-ok-sign icon-white"></i> Pause </a>
													</s:if> <s:else>
														<a id="active" class="btn btn-danger btn-setting" onclick="activeAccountId(${id});"
															href="javascript:void(0);"><i class="icon-ok-sign icon-white"></i> Active </a>
													</s:else> <a id="active" class="btn btn-success btn-setting" onclick="resetAccountId(${id},'${username}','${password}')"
													href="javascript:void(0);"><i class="icon-zoom-in icon-white"></i> Reset Password </a>
												</td>
											</tr>
										</s:iterator>
									</tbody>
								</table>
							</form>

						</div>
					</div>
					<!--/span-->
				</div>
				<!--/row-->
				<!-- content ends -->
			</div>
			<!--/#content.span10-->

		</div>
		<!--/fluid-row-->
		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
	</div>
	<!--/.fluid-container-->
	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

	<div class="modal hide fade" id="myModal">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal">×</button>
			<h3 id="pop_title">Alert</h3>
		</div>
		<div class="modal-body">
			<p id="pauseP">Are you sure you want to pause the Account?</p>
			<p id="activeP">Are you sure you want to active the Account?</p>
			<p id="resetP">
				User Name:<br />
				<input type="text" id="reset_name" />
				<br /> Password:<br />
				<input type="text" id="reset_pwd" />
			</p>
			<p id="okP">Reset the Account's password succeeded!</p>
		</div>
		<div class="modal-footer">
			<a href="javascript:void(0);" class="btn" id="closeBtn" data-dismiss="modal">No</a><a
				href="javascript:resetAccount();" class="btn btn-primary" id="resetBtn">Yes</a> <a
				href="javascript:pauseAccount();" class="btn btn-primary" id="pauseBtn">Yes</a> <a
				href="javascript:activeAccount();" class="btn btn-primary" id="activeBtn">Yes</a>
		</div>
	</div>
</body>
</html>
