<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>Reset Password- iLIKEBUS</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>

</head>

<body>

	<jsp:include page="/jsp/global/header.jsp"></jsp:include>

	<div class="container-fluid">
		<div class="row-fluid">

			<s:action name="menu" executeResult="true">
				<s:param name="type">33</s:param>
			</s:action>

			<div id="content" class="span10">
				<!-- content starts -->
				<div class="row-fluid">
					<div class="box span12">
						<div class="box-header well">
							<h2>
								<i class="icon-user"></i> Reset Password
							</h2>
							<div class="box-icon" style="padding-right: 20px;">
								<a class="back" href="javascript:history.go(-1);" title="Back"><span
									class="icon32 icon-blue icon-undo"></span> </a>
							</div>
						</div>
						<div class="box-content">
							<fieldset>
								<form class="form-horizontal" id="subForm" name="subForm"
									action="/system/account-repwd-save.action" method="post">

									<div class="control-group" id="group_old_password">
										<label class="control-label" for="focusedInput"> Old Password </label>
										<div class="controls">
											<input onblur="outBlur(this.id);" id="old_password" type="password" name="old_password"
												value="" msg="Please enter your old password">
											<span class="help-inline" id="msg_old_password"></span>
										</div>
									</div>
									<div class="control-group" id="group_new_password">
										<label class="control-label" for="focusedInput"> New Password </label>
										<div class="controls">
											<input onblur="outBlur(this.id);" id="new_password" type="password" name="new_password"
												value="" msg="Please enter your New Password">
											<span class="help-inline" id="msg_new_password"></span>
										</div>
									</div>
									<div class="control-group" id="group_re_password">
										<label class="control-label" for="focusedInput"> Confirm New Password </label>
										<div class="controls">
											<input onblur="outBlur(this.id);" id="re_password" type="password" name="re_password"
												value="" msg="Please confirm your New Password">
											<span class="help-inline" id="msg_re_password"></span>
										</div>
									</div>
									<div class="control-group" id="group_re_password">
										<div class="controls clearfix" id="msg" style="color: red">${msg }</div>
									</div>
									<div class="form-actions">
										<div class="actions">
											<button id="submit-btn" type="button" onclick="submitForm();" class="btn btn-primary">
												Change</button>
										</div>
									</div>
								</form>
							</fieldset>
						</div>
					</div>
				</div>
				<!--/row-->

				<div class="row-fluid"></div>

				<!-- content ends -->
			</div>
			<!--/#content.span10-->
		</div>
		<!--/fluid-row-->

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->


	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

</body>
<script type="text/javascript">
	function submitForm() {

		if (!outBlur("old_password")) {
			return;
		}
		if (!outBlur("new_password")) {
			return;
		}
		if (!outBlur("re_password")) {
			return;
		}
		if ($.trim($("#new_password").val()) != $.trim($("#re_password").val())) {
			$("#msg_re_password").text("Your passwords do not match.Please try again");
			return;
		}
		$("#subForm").submit();
	}
	//失去焦点
	function outBlur(id) {
		$("#msg").text("");
		if ($.trim($("#" + id).val()) == '') {
			$("#group_" + id).addClass("error");
			var msg = $("#" + id).attr("msg");
			$("#msg_" + id).text(msg);
		} else {
			$("#group_" + id).removeClass("error");
			$("#msg_" + id).text("");
			return true;
		}
		return false;
	}
</script>

</html>
