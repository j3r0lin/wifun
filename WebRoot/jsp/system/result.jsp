<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>iLIKEBUS</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
	<div class="container-fluid">
		<div class="row-fluid">
			<div class="row-fluid">
				<div class="span12 center login-header">
					<h2>Welcome to iLIKEBUS Platform</h2>
				</div>
				<!--/span-->
			</div>
			<!--/row-->

			<div class="row-fluid">
				<div class="well span5 center login-box">
					<fieldset>
						<div class="clearfix" id="msg">${msg }</div>
					</fieldset>
				</div>
				<!--/span-->
			</div>
			<!--/row-->
		</div>
		<!--/fluid-row-->
	</div>
	<!--/.fluid-container-->
	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

</body>
</html>
