<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
				<s:action name="menu" executeResult="true">
					<s:param name="type">12</s:param>
				</s:action>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Media Management</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Deploy Detail</a>
					</li>
				</ul>
			</div>
			
       <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Detail</h2>
						
						
						<div style="float:right;">
							<a class="btn btn-success"  href="/media/vlist-download.action?deployId=${deployId}">
							Download vlist</a>&nbsp;&nbsp;
							
							<a class="btn btn-primary"  href="/media/sd-deploy-list.action" >
							Back</a>
						</div>	
					</div>
					
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Media</th>
								  <th>Media(CN)</th>	
								  <th>Media Version</th>								 
								  <th>Deploy Time</th>
								  <th>Action</th>
							  </tr>
						  </thead>   
						  <tbody>
 <s:iterator value="detailList" >
							<tr>
								<td>${mediaTitle }</td>
								<td>${mediaTitleCn }</td>
								<td>${mediaVersion }</td>
								<td class="center">
									<s:property value="@com.wifun.admin.util.TimeUtils@DateToString(deployTime,'MM/dd/yy HH:mm')" />
								</td>
								<td class="center">
									<a class="btn btn-success" href="/media/media-download.action?mediaId=${mediaId}&mediaVersion=${mediaVersion}">
										<i class="icon-list icon-white"></i> 
										Download
									</a>
									<a class="btn btn-primary" href="/media/sd-deploy-device-list.action?deployId=${deployId}&mediaId=${mediaId}">
										<i class="icon-list icon-white"></i> 
										Detail
									</a>
								</td>
							</tr>
</s:iterator>						
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	
	
</body>
</html>
