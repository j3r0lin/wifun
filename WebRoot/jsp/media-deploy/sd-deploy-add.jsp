<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>WiFun Console</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body onload="init()">
	<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
	<div class="container-fluid">
		<div class="row-fluid">

			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
				<s:param name="type">12</s:param>
			</s:action>
			<!-- left menu ends -->

			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>
						You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
						enabled to use this site.
					</p>
				</div>
			</noscript>

			<div id="content" class="span10">
				<!-- content starts -->


				<div>
					<ul class="breadcrumb">
						<li><a href="#">Media Management</a> <span class="divider">/</span></li>
						<li><a href="#">New Deployment</a></li>
					</ul>
				</div>


				<div class="row-fluid sortable">
					<div class="box span12">
						<div class="box-header well" data-original-title>
							<h2>
								<i class="icon-edit"></i> Portal Deploy Info
							</h2>
							<div class="box-icon" style="padding-right: 20px;">
								<a class="back" href="javascript:history.go(-1);" title="Back"><span
									class="icon32 icon-blue icon-undo"></span> </a>
							</div>
						</div>

						<div class="box-content">
							<form class="form-horizontal" id="subForm" name="subForm"
								action="/media/sd-deploy-save.action" enctype="multipart/form-data" method="post">
								<fieldset>
									<s:if test="#session['account_session'].type==1">
										<div class="control-group">
											<label class="control-label">Company</label>
											<div class="controls">
												<select id="companyId" name="mediaDeploy.companyId" onchange="onComanyChange()">
													<s:iterator value="companyList">
														<option value="${id}">${name}</option>
													</s:iterator>
												</select>
											</div>
										</div>
									</s:if>
									<s:else>
										<input type="hidden" id="companyId" value="${companyId }">
									</s:else>
									<div class="control-group">
										<label class="control-label">Group</label>
										<div class="controls">
											<select id="groupId" name="mediaDeploy.groupId" onchange="onGroupChange()">
											</select>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Devices</label>
										<div class="controls" id="devices_div"></div>
									</div>
									<div class="control-group">
										<label class="control-label">Media List</label>
										<div class="controls">
											<select id="mediaListId" name="mediaDeploy.mediaListId">
											</select>
										</div>
									</div>
									<div class="control-group">
										<label class="control-label">Description</label>
										<div class="controls">
											<input id="description" type="text" name="mediaDeploy.description">
										</div>
									</div>

									<div class="form-actions">
										<button id="submit-btn" type="button" onclick="submitForm();" class="btn btn-primary">Deploy</button>
										<button class="btn" type="reset"
											onclick="window.location.href='javascript:history.go(-1);'">Cancel</button>
									</div>
								</fieldset>
							</form>

						</div>
					</div>
					<!--/span-->

				</div>
				<!--/row-->

				<!-- content ends -->
			</div>
			<!--/#content.span10-->
		</div>
		<!--/fluid-row-->

		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a> <a href="#" class="btn btn-primary">Save
					changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

	<script>
		function submitForm() {

			var devices = $("[name=devices]");
			var count = 0;
			devices.each(function(e) {

				if ($(this).attr("checked")) {
					count = count + 1;
				}
			});

			if (count == 0) {
				alert("Please select devices.");
				return;
			}
			$("#subForm").submit();

		};

		function setGroupList(groupList) {

			if (!groupList)
				return;

			var groupSelect = $("#groupId");
			if (!groupSelect)
				return;
			groupSelect.empty();
			///添加默认分组
			groupSelect.append("<option value='0'>default group</option>");
			for( var i = 0; i < groupList.length; i++) {
				groupSelect.append("<option value='"+groupList[i].id+"'>" + groupList[i].name
						+ "</option>");
			}
			onGroupChange();
		};

		function setMediaList(list) {

			var mediaListSelect = $("#mediaListId");
			if (!mediaListSelect)
				return;
			mediaListSelect.empty();
			if (!list)
				return;
			for( var i = 0; i < list.length; i++) {
				mediaListSelect.append("<option value='"+list[i].id+"'>" + list[i].title
						+ "</option>");
			}
		};

		function setDeviceList(list) {

			var devices = $("#devices_div");
			if (!devices)
				return;
			devices.empty();
			if (!list)
				return;

			for( var i = 0; i < list.length; i++) {
				devices
						.append("<label><input type='checkbox' name='devices' value='"+list[i].id+"'>"
								+ list[i].name + "</input></label>");
			}
			$("[name=devices]").uniform();
		};

		function onComanyChange() {

			var companyId = $("#companyId").val();
			loadCompanyGroups(companyId, setGroupList);
			loadCompanyMediaList(companyId, setMediaList);
		};

		function onGroupChange() {

			var companyId = $("#companyId").val();
			var groupId = $("#groupId").val();
			loadCompanyGroupDevices(companyId, groupId, setDeviceList);
		};

		function init() {

			////加载初始化的分组和Portal列表 
			if ($("#companyId")) {
				onComanyChange();
			}
		};
	</script>
</body>
</html>
