<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>WiFun</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
	<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
	<div class="container-fluid">
		<div class="row-fluid">

			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
				<s:param name="type">3</s:param>
			</s:action>
			<!-- left menu ends -->

			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>
						You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a>
						enabled to use this site.
					</p>
				</div>
			</noscript>

			<div id="content" class="span10">
				<!-- content starts -->
				<div>
					<ul class="breadcrumb">
						<li><a href="#">Device Management</a></li>
					</ul>
				</div>
				<!-- search starts -->
				<div>
					<div class="span12 my-search">
						<div>
							<h3>Search By</h3>
							<div class="my-filters">
								<form action="/device/device-search.action" method="post" id="queryForm">
									<table>
										<tr>
											<td><label class="control-label"> SN </label></td>
											<td>
												<div class="controls">
													<input id="sn" type="text" name="sn" value="${sn }">
												</div>
											</td>
											<td><label class="control-label"> Name </label></td>
											<td>
												<div class="controls">
													<input id="name" type="text" name="name" value="${name }">
												</div>
											</td>
											<td></td>
										</tr>
<s:if test="#session['account_session'].type==1">
										<tr>
											<td><label class="control-label">Company</label></td>
											<td>
											
												<div class="controls">
													<select id="companyId" name="companyId" onchange="onCompanyChange(this.value);">
														<s:iterator value="companyList">
															<option value="${id}" <s:if test="id == companyId">selected="selected"</s:if>>${name}</option>
														</s:iterator>
													</select>
												</div>
											</td>
											
											<td><label class="control-label">Group</label></td>
											<td>
												<div class="controls">
													<select id="groupId" name="groupId" value="${vo.groupId }">
														<option value="-1" <s:if test="-1 == groupId">selected="selected"</s:if>>All</option>
														<option value="0" <s:if test="0 == groupId">selected="selected"</s:if>>default
															group</option>
														<s:iterator value="groupList">
															<option value="${id}" <s:if test="id == groupId">selected="selected"</s:if>>${name}</option>
														</s:iterator>
													</select>
												</div>
											</td>
											<td>
												<input type="checkbox" id="active" name="active" <s:if test="active">checked="checked"</s:if> value="true" style="padding-right:90px;">
												<span>Show Active Only</span>
												<button type="submit" style="margin-bottom: 10px;" class="btn">Search</button>
											</td>	
										</tr>							
</s:if>
<s:else>
									<tr>
										<td><label class="control-label">Group</label></td>
										<td>
											<div class="controls">
												<select id="groupId" name="groupId" value="${vo.groupId }">
													<option value="-1" <s:if test="-1 == groupId">selected="selected"</s:if>>All</option>
													<option value="0" <s:if test="0 == groupId">selected="selected"</s:if>>default
														group</option>
													<s:iterator value="groupList">
														<option value="${id}" <s:if test="id == groupId">selected="selected"</s:if>>${name}</option>
													</s:iterator>
												</select>
											</div>
										</td>
										<td></td>
										<td>
											<input type="checkbox" id="active" name="active" <s:if test="active">checked="checked"</s:if> value="true" style="padding-right:90px;">
											<span>Show Active Only</span>
											<button type="submit" style="margin-bottom: 10px;" class="btn">Search</button>
										</td>
										<td></td>	
									</tr>
</s:else>
									</table>
								</form>
							</div>
						</div>
					</div>
				</div>
				<!-- /search ends -->
				<div class="row-fluid sortable">
					<div class="box span12">
						<div class="box-header well" data-original-title>
							<h2>
								<i class="icon-user"></i> Device
							</h2>
<s:if test="#session['account_session'].type==1">
							<div style="float:right;">
								<a class="btn btn-primary" href="/device/device-add.action"> <i
									class="icon icon-white icon-plus"></i>&nbsp;Add Device </a>
							</div>
</s:if>
						</div>
						<div class="box-content">
							<table class="table table-striped table-bordered bootstrap-datatable datatable">
								<thead>
									<tr>
										<th>ID</th>
										<th>Name</th>
										<th>SN</th>
										<th>Model</th>
										<th>HardWare Version</th>
										<th>Company</th>
										<th>Group</th>
										<th>Work Status</th>
										<th>Start Date</th>
										<th>Update Time</th>
										<!-- 								  <th>Carrier</th> -->
										<!-- 								  <th>IMSI</th> -->
										<!-- 								  <th>Bus Info</th> -->
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<s:iterator value="deviceList">
										<tr>
											<td>${id}</td>
											<td>${name}</td>
											<td>${sn}</td>
											<td>${model}</td>
											<td>${hardwareVersion}</td>
											<td>${companyName}</td>
											<td>${groupName}</td>
											<td class="center"><s:if test="workStatus==1">
													<span class="label label-success">Active</span>
												</s:if> <s:else>
													<span class="label label-warning"> Inactive </span>
												</s:else></td>
											<td class="center"><s:property
													value="@com.wifun.admin.util.TimeUtils@DateToString(startDate,'MM/dd/yy')" /></td>
											<td class="center"><s:property
													value="@com.wifun.admin.util.TimeUtils@DateToString(updateDate,'MM/dd/yy HH:mm')" /></td>
											<!-- 								<td>${carrier} </td> -->
											<!-- 								<td>${imsi} </td> -->
											<!-- 								<td>${busInfo} </td> -->
											<td class="center">
												<a class="btn btn-info" href="/device/device-edit.action?id=${id}"> <i class="icon-edit icon-white"></i>
													Edit </a> 
												<a class="btn btn-danger btn-setting" href="javascript:void(0);" onclick="recordDeleteDevice(${id})"> <i class="icon-trash icon-white"></i> 
													Delete </a></td>
										</tr>
									</s:iterator>
								</tbody>
							</table>
						</div>
					</div>
					<!--/span-->

				</div>
				<!--/row-->
				<!-- content ends -->
			</div>
			<!--/#content.span10-->
		</div>
		<!--/fluid-row-->

		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Alert</h3>
			</div>
			<div class="modal-body">
				<p>Are you sure delete the device?</p>
			</div>
			<div class="modal-footer">
				<a href="javascript:void(0);" class="btn" id="closeBtn" data-dismiss="modal">No</a>
				<a href="javascript:deleteDevice();" class="btn btn-primary">Yes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

	</div>
	<!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

	<script type="text/javascript">
	var deleteId="";
	function recordDeleteDevice(id) {
		deleteId=id;
	}
	
	function deleteDevice(){ 	        
	        $("#closeBtn").trigger("click");
		    $.ajax({
				  type: "POST",
				  url: "/device/device-delete.action",
				  data:{id:deleteId}, 
				  dataType:'json',
				  success: function(data){	
				      	       
					   // 0,系统错误,1 成功,3参数错误
				       if(data.result==1)
				       {  
				          window.document.location.reload();
				       }else if(data.result==0)
				       {				          
				        alert("Delete error!");
				        window.document.location.reload();
				       }
				  },
				  error:function(e)
				  { 
				  	alert("System error!");					    
				  }
			});	      
	  	}
		function setGroupList(groupList) {

			if (!groupList)
				return;

			var groupSelect = $("#groupId");
			if (!groupSelect)
				return;

			groupSelect.html("");
			///添加默认分组
			groupSelect.append("<option value='-1'>All</option>");
			///添加默认分组
			groupSelect.append("<option value='0'>default group</option>");
			for( var i = 0; i < groupList.length; i++) {
				groupSelect.append("<option value='"+groupList[i].id+"'>" + groupList[i].name
						+ "</option>");
			}
		};

		function onCompanyChange(companyId) {

			loadCompanyGroups(companyId, setGroupList);
		};
	</script>

</body>
</html>
