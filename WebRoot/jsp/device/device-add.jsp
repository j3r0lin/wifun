<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<s:action name="menu" executeResult="true">
					<s:param name="type">3</s:param>
				</s:action>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Device Add</a> 
					</li>
				</ul>
			</div>
								  
      
			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-edit"></i> Device Info</h2>
						<div class="box-icon" style="padding-right: 20px;">
								<a class="back" href="javascript:history.go(-1);" title="Back"><span
									class="icon32 icon-blue icon-undo"></span> </a>
							</div>
					</div>
					
					<div class="box-content">
						<form class="form-horizontal" id="subForm" name="subForm"
									action="/device/device-add-save.action" method="post">
							<fieldset>
							  <div class="control-group" id="group_name">
								<label class="control-label"> Name</label>
								<div class="controls">
								  <input id="name" type="text" name="bo.name" value="" onblur="outBlur(this.id);"  msg="Please enter name">
								   <span class="help-inline" id="msg_name"></span>
								</div>
							  </div>
							  <div class="control-group" id="group_sn">
								<label class="control-label"> SN</label>
								<div class="controls">
								  <input id="sn" type="text" name="bo.sn" value="" onblur="outBlur(this.id);"  msg="Please enter SN">
								  <span class="help-inline" id="msg_sn"></span>
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label">Model</label>
								<div class="controls">
								  <select id="bo.model" name="bo.model">
									<option value="IP3012L">IP3012L</option>
								  </select>
								</div>
							  </div>
<!-- 							  <div class="control-group" id="group_hardwareVersion"> -->
<!-- 								<label class="control-label">HardWare Version</label> -->
<!-- 								<div class="controls"> -->
<!-- 								  <input type="text" id="hardwareVersion" name="bo.hardwareVersion" onblur="outBlur(this.id);"  msg="Please enter HardWare Version"> -->
<!-- 								  <span class="help-inline" id="msg_hardwareVersion"></span> -->
<!-- 								</div> -->
<!-- 							  </div> -->
							  <div class="control-group">
								<label class="control-label">Company</label>
								<div class="controls">
								  <select id="bo_companyId" name="bo.companyId" onchange="onCompanyChange(this.value)">
									<s:iterator value="companyList">
									 <option value="${id}" >${name}</option>
									 </s:iterator>
								  </select>
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label">Group</label>
								<div class="controls">
								  <select id="bo_groupId" name="bo.groupId">
									
								  </select>
								</div>
							  </div>
							   <div class="control-group">
								<label class="control-label">Work Status</label>
								<div class="controls">
								  <select id="bo.workStatus" name="bo.workStatus">
									<option value="1">Active</option>
									<option value="0">Inactive</option>
								  </select>
								</div>
							  </div>
							  <div class="control-group" id="group_startDate">
								<label class="control-label">Start Date</label>
								<div class="controls">
								  <input type="text" class="datepicker" autocomplete="off" id="startDate" name="startDate" onblur="outBlur(this.id);"  msg="Please enter Start Date">
								  <span class="help-inline" id="msg_startDate"></span>
								</div>
							  </div>
							  <div class="control-group" id="group_carrier">
								<label class="control-label">Carrier</label>
								<div class="controls">
								  <input type="text" id="carrier" name="bo.carrier" msg="Please enter carrier">
								  <span class="help-inline" id="msg_carrier"></span>
								</div>
							  </div>
<!-- 							   <div class="control-group" id="group_imsi"> -->
<!-- 								<label class="control-label">IMSI</label> -->
<!-- 								<div class="controls"> -->
<!-- 								  <input type="text" id="imsi" name="bo.imsi" onblur="outBlur(this.id);"  msg="Please enter IMSI"> -->
<!-- 								  <span class="help-inline" id="msg_imsi"></span> -->
<!-- 								</div> -->
<!-- 							  </div> -->
							  
							  <div class="control-group" id="group_imei">
								<label class="control-label">IMEI</label>
								<div class="controls">
								  <input type="text" id="imei" name="bo.imei" msg="Please enter IMEI">
								  <span class="help-inline" id="msg_imei"></span>
								</div>
							  </div>
							  <div class="control-group" id="group_phoneNumber">
								<label class="control-label">Phone Number</label>
								<div class="controls">
								  <input type="text" id="phoneNumber" name="bo.phoneNumber" msg="Please enter Phone Number">
								  <span class="help-inline" id="msg_phoneNumber"></span>
								</div>
							  </div>
							  
							   <div class="control-group" id="group_busInfo">
								<label class="control-label">Bus Info</label>
								<div class="controls">
								  <input type="text" id="busInfo" name="bo.busInfo" onblur="outBlur(this.id);"  msg="Please enter Bus Info">
								  <span class="help-inline" id="msg_busInfo"></span>
								</div>
							  </div>
							 
							  <div class="form-actions">
								<button  id="submit-btn" type="button" onclick="submitForm();" class="btn btn-primary">Add</button>
								<button class="btn" type="reset" onclick="window.location.href='javascript:history.go(-1);'">Cancel</button>
							  </div>
							</fieldset>
						  </form>
					
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
	
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	<script>
			function submitForm(){ 
			
				$("#name").val($.trim($("#name").val()));
				if (!outBlur("name")) {
					return;
				}
				$("#sn").val($.trim($("#sn").val()));
				if (!outBlur("sn")) {
					return;
				}
// 				$("#hardwareVersion").val($.trim($("#hardwareVersion").val()));
// 				if (!outBlur("hardwareVersion")) {
// 					return;
// 				}
				$("#startDate").val($.trim($("#startDate").val()));
				if (!outBlur("startDate")) {
					return;
				}
// 				$("#carrier").val($.trim($("#carrier").val()));
// 				if (!outBlur("carrier")) {
// 					return;
// 				}
// 				$("#imsi").val($.trim($("#imsi").val()));
// 				if (!outBlur("imsi")) {
// 					return;
// 				}
				$("#busInfo").val($.trim($("#busInfo").val()));
				if (!outBlur("busInfo")) {
					return;
				}
				$("#subForm").submit();
			}

			function setGroupList(groupList)
			{
				if (!groupList) return;
				
				var groupSelect = $("#bo_groupId");
				if (!groupSelect)
					return ;
				
				groupSelect.html("");
				///添加默认分组
				groupSelect.append("<option value='0'>default group</option>");	
				for (var i = 0; i < groupList.length;i++)
				{
					groupSelect.append("<option value='"+groupList[i].id+"'>"+groupList[i].name+"</option>");
				}
			}
			
			
			function onCompanyChange(companyId)
			{
				loadCompanyGroups(companyId, setGroupList);
			}
			
			////加载初始化的分组
			if ($("#bo_companyId"))
			{
				onCompanyChange($("#bo_companyId").val());
			}
			//失去焦点
			function outBlur(id) {
		
				if ($.trim($("#" + id).val()) == '') {
					$("#group_" + id).addClass("error");
					var msg = $("#" + id).attr("msg");
					$("#msg_" + id).text(msg);
				} else {
					$("#group_" + id).removeClass("error");
					$("#msg_" + id).text("");
					return true;
				}
				return false;
			};			
	</script>
</body>
</html>
