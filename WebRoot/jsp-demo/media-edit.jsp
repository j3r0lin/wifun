<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<jsp:include page="/jsp/global/menu.jsp"></jsp:include>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Media Management</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Edit Media</a>
					</li>
				</ul>
			</div>
								  
      
			<div class="row-fluid sortable">
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-edit"></i> Media Info</h2>
						<div class="box-icon" style="padding-right: 20px;">
								<a class="back" href="javascript:history.go(-1);" title="Back"><span
									class="icon32 icon-blue icon-undo"></span> </a>
							</div>
					</div>
					
					<div class="box-content">
						<form class="form-horizontal">
							<fieldset>
							  <div class="control-group">
								<label class="control-label">Title</label>
								<div class="controls">
								  <input   type="text" Value="X-Men(2000)">
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label">Category</label>
								<div class="controls">
								  <select id="selectCategory" multiple data-rel="chosen">
									<option selected="">Action</option>
									<option>Comedy</option>
									<option>Love</option>
									<option selected="">Science</option>
									<option>War</option>
								  </select>
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label">Entertainment</label>
								<div class="controls">
								  <input   type="text" value="FOX">
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label">Year</label>
								<div class="controls">
								  <select id="selectYear" >
									<option selected>2000</option>
									<option>2001</option>
									<option>2002</option>
									<option>2003</option>
									<option>2004</option>
									<option>2005</option>
									<option>2006</option>
									<option>2007</option>
									<option>2008</option>
									<option>2009</option>
									<option>2010</option>
									<option>2011</option>
									<option>2012</option>
									<option>2013</option>
									<option>2014</option>
									<option>2015</option>
								  </select>
								</div>
							  </div>
							 <div class="control-group">
								<label class="control-label">Length</label>
								<div class="controls">
								  <input   type="text" value="104m">
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label">Writer</label>
								<div class="controls">
								  <input   type="text" value="Bryan Singer">
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label">Director</label>
								<div class="controls">
								  <input   type="text" value="Bryan Singer">
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label">Starring</label>
								<div class="controls">
								  <input   type="text" value="Hugh Jackman,Patrick Stewart">
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label">Show Index</label>
								<div class="controls">
								  <input   type="text" value="1">
								</div>
							  </div>
							  <div class="control-group">
								<label class="control-label">Introduction</label>
								<div class="controls">
								   <textarea class="autogrow">X-Men(2000) may need a long description</textarea>
								</div>
							  </div>
							  <div class="form-actions">
								<button type="button" class="btn btn-primary" onclick="window.location.href='javascript:history.go(-1);'">Save</button>
								<button class="btn" type="reset" onclick="window.location.href='javascript:history.go(-1);'">Cancel</button>
							  </div>
							</fieldset>
						  </form>
					
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
	
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
</body>
</html>
