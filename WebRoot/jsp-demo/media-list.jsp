<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<jsp:include page="/jsp/global/menu.jsp"></jsp:include>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Media Management</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Media List</a>
					</li>
				</ul>
			</div>
<!-- search starts -->
<div>
	<div class="span12 my-search">
		<div>
			<h3>Search By</h3>
			<div class="my-filters">
				<form action="/city/city-list.action" method="post" id="subForm">
					<table>
						<tr>
							<td>
								<label class="control-label"> Title</label>
							</td>
							<td>
								<div class="controls">
									<input id="focusedInput" type="text">
								</div>
							</td>
							<td>
								<button type="button" onclick="alert('Search');" style="margin-bottom: 10px;" class="btn">
									Search
								</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /search ends -->   								  
       <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Meida List</h2>
						<div style="float:right;">
									<a class="btn btn-primary"  href="media-list-add.jsp" >
									<i class="icon icon-white icon-plus"></i>&nbsp;Add List </a>
						</div>	
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>ID</th>
								  <th>Title</th>
								  <th>Size</th>									 
								  <th>Sharing</th>
								  <th>Create Date</th>
								  <th>Creator</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
							<tr>
								<td>1</td>
								<td>Action Series</td>
								<td>7</td>
								<td class="center">
									<span class="label label-success">Share</span>
								</td>
								<td class="center">2015/01/01</td>
								<td>
									<span class="label label-success">super</span>
								</td>
								<td class="center">
									<a class="btn btn-info" href="#" onclick="$('#mediaDetailDiv').removeClass('hide');">
										<i class="icon-list icon-white"></i>  
										Detail                                            
									</a>
									<a class="btn btn-info btn-choose-media" href="#">
										<i class="icon-add icon-white"></i>  
										Add Media                                            
									</a>
									<a class="btn btn-danger" href="#" onclick="deleteTableRow(this)">
										<i class="icon-trash icon-white"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>Comedy Series</td>
								<td>5</td>
								<td class="center">
									<span class="label label-success">Share</span>
								</td>
								<td class="center">2015/01/01</td>
								<td>
									<span class="label label-success">super</span>
								</td>
								<td class="center">
									<a class="btn btn-info" href="#" onclick="$('#mediaDetailDiv').removeClass('hide');">
										<i class="icon-list icon-white"></i>  
										Detail                                            
									</a>
									<a class="btn btn-info btn-choose-media" href="#">
										<i class="icon-add icon-white"></i>  
										Add Media                                            
									</a>
									<a class="btn btn-danger" href="#" onclick="deleteTableRow(this)">
										<i class="icon-trash icon-white"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>3</td>
								<td>Science Series</td>
								<td>10</td>
								<td class="center">
									<span class="label label-warning">Exclusive</span>
								</td>
								<td class="center">2015/01/01</td>
								<td>
									<span class="label label-warning">eastern_bus</span>
								</td>
								<td class="center">
									<a class="btn btn-info" href="#" onclick="$('#mediaDetailDiv').removeClass('hide');">
										<i class="icon-list icon-white"></i>  
										Detail                                            
									</a>
									<a class="btn btn-info btn-choose-media" href="#">
										<i class="icon-add icon-white"></i>  
										Add Media                                            
									</a>
									<a class="btn btn-danger" href="#" onclick="deleteTableRow(this)">
										<i class="icon-trash icon-white"></i> 
										Delete
									</a>
								</td>
							</tr>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
			<div id="mediaDetailDiv" class="row-fluid hide sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Medias</h2>
						
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>ID</th>
								  <th>Title</th>
								  <th>Year</th>
								  <th>Url</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
							<tr>
								<td>1</td>
								<td>X-Men</td>
								<td>2000</td>
								<td>http://wifun.com/vedio/Zip/xmen2000.zip</td>
								<td class="center">
									<a class="btn btn-info" href="#">
										<i class="icon-trash icon-white"></i>  
										Remove                                            
									</a>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>X-Men</td>
								<td>2011</td>
								<td>http://wifun.com/vedio/Zip/xmen2011.zip</td>
								<td class="center">
									<a class="btn btn-info" href="#">
										<i class="icon-trash icon-white"></i>  
										Remove                                            
									</a>
								</td>
							</tr>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	
	<jsp:include page="/jsp/dialog/choose-media.jsp"></jsp:include>	
</body>
</html>
