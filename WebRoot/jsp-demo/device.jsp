<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<jsp:include page="/jsp/global/menu.jsp"></jsp:include>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Device Management</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Device Management</a>
					</li>
				</ul>
			</div>
						
<!-- search starts -->
<div>
	<div class="span12 my-search">
		<div>
			<h3>Search By</h3>
			<div class="my-filters">
				<form action="/city/city-list.action" method="post" id="subForm">
					<table>
						<tr>
							<td>
								<label class="control-label">Company</label>
							</td>
							<td>
								<div class="controls">
									<select id="selectError3">
										<option>Eastern Bus</option>
										<option>Western Bus</option>
									</select>
								</div>
							</td>
							<td>
								<label class="control-label">Group</label>
							</td>
							<td>
								<div class="controls">
									<select id="selectError3">
										<option>NY-DC</option>
										<option>NY-CS</option>
									</select>
								</div>
							</td>
							<td>
								<label class="control-label"> SN</label>
							</td>
							<td>
								<div class="controls">
									<input id="focusedInput" type="text">
								</div>
							</td>
							<td>
								<button type="button" onclick="alert('Search')" style="margin-bottom: 10px;" class="btn">
									Search
								</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /search ends -->
					
       <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Device</h2>
						 <% if ("super".equals( session.getAttribute("currentrole") )) {%>
						<div style="float:right;">
									<a class="btn btn-primary"  href="device-add.jsp" >
									<i class="icon icon-white icon-plus"></i>&nbsp;Add Device </a>
						</div>	
						<% } %>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>ID</th>
								  <th>SN</th>
								  <th>Model</th>
								  <th class="company">HardWare Version</th>
								  <th class="<%= session.getAttribute("currentrole")  %>">Company</th>
								  <th>Group</th>
								  <th>Work Status</th>
								  <th>Start Date</th>	
								  <th>Update Date</th>
								  <th>Carrier</th>
								  <th>IMEI</th>
								  <th>Bus Info</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
							<tr>
								<td>1</td>
								<td>PL3121503310001</td>
								<td>IP8XX_WCDMA</td>
								<td class="company">3.52</td>
								<td class="<%= session.getAttribute("currentrole")  %>">Eastern Bus</td>
								<td>NY-DC</td>
								<td class="center">
									<span class="label label-success">Active</span>
								</td>
								<td class="center">2015/05/01</td>
								<td class="center">2015/05/01</td>
								<td>AT&T</td>
								<td>400128001</td>
								<td>Bus Card:NC005</td>
								<td class="center">
									<a class="btn btn-info" href="device-edit.jsp">
										<i class="icon-edit icon-white"></i>  
										Edit                                            
									</a>
									<a class="btn btn-danger" href="#" onclick="deleteTableRow(this)">
										<i class="icon-trash icon-white"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>PP3121504259273</td>
								<td>IP8XX_WCDMA</td>
								<td class="company">3.52</td>
								<td class="<%= session.getAttribute("currentrole")  %>">Eastern Bus</td>
								<td>NY-DC</td>
								<td class="center">
									<span class="label label-success">Active</span>
								</td>
								<td class="center">2015/05/01</td>
								<td class="center">2015/05/01</td>
								<td>AT&T</td>
								<td>400128002</td>
								<td>Bus Card:BN125</td>
								<td class="center">
									<a class="btn btn-info" href="device-edit.jsp">
										<i class="icon-edit icon-white"></i>  
										Edit                                            
									</a>
									<a class="btn btn-danger" href="#" onclick="deleteTableRow(this)">
										<i class="icon-trash icon-white"></i> 
										Delete
									</a>
								</td>
							</tr>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
</body>
</html>
