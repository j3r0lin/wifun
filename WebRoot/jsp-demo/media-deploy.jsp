<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<jsp:include page="/jsp/global/menu.jsp"></jsp:include>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Media Management</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Deploy Management</a>
					</li>
				</ul>
			</div>
	<!-- search starts -->
<div>
	<div class="span12 my-search">
		<div>
			<h3>Search By</h3>
			<div class="my-filters">
				<form action="/city/city-list.action" method="post" id="subForm">
					<table>
						<tr>
							<td class="<%= session.getAttribute("currentrole")  %>">
							<label class="control-label">Company</label>
							</td>
							<td class="<%= session.getAttribute("currentrole")  %>">
								<div class="controls">
								  <select id="selectError3">
									<option>Eastern Bus</option>
									<option>Western Bus</option>
								  </select>
								</div>
							</td>
							<td>
								<label class="control-label"> Description</label>
							</td>
							<td>
								<div class="controls">
									<input id="focusedInput" type="text">
								</div>
							</td>
							<td>
								<label class="control-label"> Deploy Time</label>
							</td>
							<td>
								<div class="controls">
									<input type="text" class="datepicker" id="date01" value="04/16/15">
								</div>
							</td>
							<td>
								<label class="control-label"> To</label>
							</td>
							<td>
								<div class="controls">
									<input type="text" class="datepicker" id="date02" value="04/26/15">
								</div>
							</td>
							<td>
								<button type="button" onclick="alert('Search');" style="margin-bottom: 10px;" class="btn">
									Search
								</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /search ends -->   							  
           <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Deploy List</h2>
						<div style="float:right;">
									<a class="btn btn-primary"  href="media-deploy-add.jsp" >
									<i class="icon icon-white icon-plus"></i>&nbsp;Deploy </a>
						</div>	
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>ID</th>
								  <th class="<%= session.getAttribute("currentrole")  %>">Company</th>
								  <th>Group</th>	
								  <th>Media List</th>
								  <th>Deploy Time</th>
								  <th>Inhand Status</th>
								  <th>Device Progress</th>	
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
							<tr>
								<td>1</td>
								<td class="<%= session.getAttribute("currentrole")  %>">Eastern Bus</td>
								<td>NY-DC</td>
								<td>Action Series</td>
								<td class="center">04/01/2015 10:00:00</td>
								<td>
									<span class="label label-success">Success</span>
								</td>
								<td>
									<span class="label label-success">5</span>/<span class="label label-important">5</span>
								</td>
								<td class="center">
									<a class="btn btn-info" href="#" onclick="$('#deviceListDiv').removeClass('hide');">
										<i class="icon-list icon-white"></i>  
										View                                            
									</a>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td class="<%= session.getAttribute("currentrole")  %>">Eastern Bus</td>
								<td>NY-DC</td>
								<td>Comedy Series</td>
								<td class="center">04/05/2015 10:00:00</td>
								<td>
									<span class="label label-warning">Sync</span>
								</td>
								<td>
									<span class="label label-success">3</span>/<span class="label label-important">5</span>
								</td>
								<td class="center">
									<a class="btn btn-info" href="#" onclick="$('#deviceListDiv').removeClass('hide');">
										<i class="icon-list icon-white"></i>  
										View                                            
									</a>
								</td>
							</tr>
							<tr>
								<td>3</td>
								<td class="<%= session.getAttribute("currentrole")  %>">Eastern Bus</td>
								<td>NY-CS</td>
								<td>Comedy Series</td>
								<td class="center">04/05/2015 10:00:00</td>
								<td>
									<span class="label label-success">Success</span>
								</td>
								<td>
									<span class="label label-success">2</span>/<span class="label label-important">2</span>
								</td>
								<td class="center">
									<a class="btn btn-info" href="#" onclick="$('#deviceListDiv').removeClass('hide');">
										<i class="icon-list icon-white"></i>  
										View                                            
									</a>
								</td>
							</tr>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
			<div id="deviceListDiv" class="row-fluid hide sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Devices</h2>
						
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Device ID</th>
								  <th>Device SN</th>
								  <th>Media Progess</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
							<tr>
								<td>1</td>
								<td>PL3121503310001</td>
								<td>
									<span class="label label-success">2</span>/<span class="label label-important">3</span>
								</td>
								<td class="center">
									<a class="btn btn-info btn-device-detail" href="#">
										<i class="icon-trash icon-white"></i>  
										Detail                                            
									</a>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>PP3121504259273</td>
								<td>
									<span class="label label-success">5</span>/<span class="label label-important">5</span>
								</td>
								<td class="center">
									<a class="btn btn-info btn-device-detail" href="#">
										<i class="icon-trash icon-white"></i>  
										Detail                                            
									</a>
								</td>
							</tr>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

<jsp:include page="/jsp/dialog/media.jsp"></jsp:include>
	
		
</body>
</html>
