<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<jsp:include page="/jsp/global/menu.jsp"></jsp:include>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Media Management</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Media Management</a>
					</li>
				</ul>
			</div>
<!-- search starts -->
<div>
	<div class="span12 my-search">
		<div>
			<h3>Search By</h3>
			<div class="my-filters">
				<form action="/city/city-list.action" method="post" id="subForm">
					<table>
						<tr>
							<td>
								<label class="control-label"> Category</label>
							</td>
							<td>
								<div class="controls">
									<select id="selectCategory" data-rel="chosen">
									<option>Action</option>
									<option>Comedy</option>
									<option>Love</option>
									<option>Science</option>
									<option>War</option>
								  </select>
								</div>
							</td>
							<td>
								<label class="control-label"> Title</label>
							</td>
							<td>
								<div class="controls">
									<input id="focusedInput" type="text">
								</div>
							</td>
							<td>
								<label class="control-label"> Year</label>
							</td>
							<td>
								<div class="controls">
									<select id="selectYear" >
									<option>2000</option>
									<option>2001</option>
									<option>2002</option>
									<option>2003</option>
									<option>2004</option>
									<option>2005</option>
									<option>2006</option>
									<option>2007</option>
									<option>2008</option>
									<option>2009</option>
									<option>2010</option>
									<option>2011</option>
									<option>2012</option>
									<option>2013</option>
									<option>2014</option>
									<option>2015</option>
								  </select>
								</div>
							</td>
							<td>
								<button type="button" onclick="alert('Search');" style="margin-bottom: 10px;" class="btn">
									Search
								</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /search ends -->   								  
       		 <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Medias</h2>
						<div style="float:right;">
									<a class="btn btn-primary"  href="media-add.jsp" >
									<i class="icon icon-white icon-plus"></i>&nbsp;Add Media </a>
						</div>	
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>ID</th>
								  <th>Title</th>
								  <th>Category</th>
								  <th>Entertainment</th>
								  <th>Year</th>
								  <th>Length</th>
								  <th>Movie</th>
								  <th>Hori-Poster</th>	
								  <th>Vert-Poster</th>
								  <th>Director</th>
								  <th>Starring</th>
								  <th>Show Index</th>
								  <th class="company">Introduction</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
							<tr>
								<td>1</td>
								<td>X-MEN(2000)</td>
								<td>Science, Action</td>
								<td>FOX</td>
								<td>2000</td>
								<td>104m</td>
								<td class="center">
									<a class="btn btn-success" href="#" onclick="$('#myModal').modal('show');">
										<i class="icon-scissors"></i> 
										View
									</a>
								</td>
								<td class="center">
									<span class="label label-warning">
										Empty
									</span>
								</td>
								<td class="center">
									<span class="label label-warning">
										Empty
									</span>
								</td>
								<td>Bryan Singer</td>
								<td>Hugh Jackman,Patrick Stewart</td>
								<td>1</td>
								<td class="company">X-Men 2000</td>
								<td class="center">
									<a class="btn btn-info" href="media-edit.jsp">
										<i class="icon-edit icon-white"></i>  
										Edit                                            
									</a>
									<a class="btn btn-primary btn-movie-resource" href="#">
										<i class="icon-upload icon-white"></i> 
										Movie
									</a>
									<a class="btn btn-primary btn-poster-resource" href="#">
										<i class="icon-upload icon-white"></i> 
										HPoster
									</a>
									<a class="btn btn-primary btn-poster-resource" href="#">
										<i class="icon-upload icon-white"></i> 
										VPoster
									</a>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>X-MEN(2011)</td>
								<td>Science, Action</td>
								<td>FOX</td>
								<td>2011</td>
								<td>132m</td>
								<td class="center">
									<a class="btn btn-success" href="#" onclick="$('#myModal').modal('show');">
										<i class="icon-archive"></i> 
										View
									</a>
								</td>
								<td class="center">
									<a class="btn btn-success" href="#" onclick="$('#myModal').modal('show');">
										<i class="icon-archive"></i> 
										View
									</a>
								</td>
								<td class="center">
									<a class="btn btn-success" href="#" onclick="$('#myModal').modal('show');">
										<i class="icon-archive"></i> 
										View
									</a>
								</td>
								<td>Bryan Singer</td>
								<td>Hugh Jackman,Patrick Stewart</td>
								<td>5</td>
								<td class="company">X-Men 2011</td>
								<td class="center">
									<a class="btn btn-info" href="media-edit.jsp">
										<i class="icon-edit icon-white"></i>  
										Edit                                            
									</a>
									<a class="btn btn-primary btn-movie-resource" href="#">
										<i class="icon-upload icon-white"></i> 
										Movie
									</a>
									<a class="btn btn-primary btn-poster-resource" href="#">
										<i class="icon-upload icon-white"></i> 
										HPoster
									</a>
									<a class="btn btn-primary btn-poster-resource" href="#">
										<i class="icon-upload icon-white"></i> 
										VPoster
									</a>
								</td>
							</tr>
							<tr>
								<td>3</td>
								<td>X-MEN(2014)</td>
								<td>Science, Action</td>
								<td>FOX</td>
								<td>2014</td>
								<td>130m</td>
								<td class="center">
									<span class="label label-warning">
										Empty
									</span>
								</td>
								<td class="center">
									<a class="btn btn-success" href="#" onclick="$('#myModal').modal('show');">
										<i class="icon-archive icon-white"></i> 
										View
									</a>
								</td>
								<td class="center">
									<a class="btn btn-success" href="#" onclick="$('#myModal').modal('show');">
										<i class="icon-archive icon-white"></i> 
										View
									</a>
								</td>
								<td>Bryan Singer</td>
								<td>Hugh Jackman,Patrick Stewart</td>
								<td>6</td>
								<td class="company">X-Men 2014</td>
								<td class="center">
									<a class="btn btn-info" href="media-edit.jsp">
										<i class="icon-edit icon-white"></i>  
										Edit                                            
									</a>
									<a class="btn btn-primary btn-movie-resource" href="#">
										<i class="icon-upload icon-white"></i> 
										Movie
									</a>
									<a class="btn btn-primary btn-poster-resource" href="#">
										<i class="icon-upload icon-white"></i> 
										HPoster
									</a>
									<a class="btn btn-primary btn-poster-resource" href="#">
										<i class="icon-upload icon-white"></i> 
										VPoster
									</a>
								</td>
							</tr>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Preview</h3>
			</div>
			<div class="modal-body">
							<li id="image-5" class="thumbnail">
								<a style="background:url(img/5.jpg)" title="Sample Image 5" href="img/5.jpg"><img class="grayscale" src="img/5.jpg" alt="Sample Image 5"></a>
							</li>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
			</div>
		</div>
		
		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	
	<jsp:include page="/jsp/dialog/free-movie.jsp"></jsp:include>
	<jsp:include page="/jsp/dialog/free-poster.jsp"></jsp:include>
</body>
</html>
