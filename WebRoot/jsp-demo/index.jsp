<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
		String userName = request.getParameter("username");
	
	if (null != userName)
	{
		if ("super".equals(userName))
		{
			session.setAttribute("currentrole","super");
		}
		else
		session.setAttribute("currentrole","company");
	}
	
	if (null == session.getAttribute("currentrole"))
	{
		response.sendRedirect("/login.html");//redirect to login.html
		return;
	}
%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>


</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<jsp:include page="/jsp/global/menu.jsp"></jsp:include>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Home</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Dashboard</a>
					</li>
				</ul>
			</div>
								  
       this is a workbench page!
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	
		
</body>
</html>
