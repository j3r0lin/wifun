<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
	if (!"super".equals( session.getAttribute("currentrole") ))
	{
		response.sendRedirect("noright.jsp");//路径001  
		return;
	}

%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<jsp:include page="/jsp/global/menu.jsp"></jsp:include>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Company Management</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Company Management</a>
					</li>
				</ul>
			</div>
								  
 <!-- search starts -->
<div>
	<div class="span12 my-search">
		<div>
			<h3>Search By</h3>
			<div class="my-filters">
				<form action="/city/city-list.action" method="post" id="subForm">
					<table>
						<tr>
							<td>
								<label class="control-label"> Company Name</label>
							</td>
							<td>
								<div class="controls">
									<input id="focusedInput" type="text">
								</div>
							</td>
							<td>
								<button type="button" onclick="alert('Search');" style="margin-bottom: 10px;" class="btn">
									Search
								</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /search ends -->      			
			<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Companys</h2>
						<div style="float:right;">
									<a class="btn btn-primary"  href="company-add.jsp" >
									<i class="icon icon-white icon-plus"></i>&nbsp;Add Company </a>
						</div>	
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>ID</th>
								  <th>Company Name</th>
								  <th>Account</th>
								  <th>Contact</th>
								  <th>Phone</th>
								  <th>Email</th>
								  <th>Groups</th>
								  <th>Devices</th>								 
								  <th>Status</th>
								  <th>Create Date</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
							<tr>
								<td>1001</td>
								<td>Eastern Bus</td>
								<td>eastern_bus</td>
								<td>Jack</td>
								<td>417000027</td>
								<td>easternbus@ilikebus.com</td>
								<td class="center">
									<a href="group.jsp"><span class="label label-success">3</span></a>
								</td>
								<td class="center">
									<a href="device.jsp"><span class="label label-success">8</span>/<span class="label label-important">10</span></a> 
								</td>
								<td class="center">
									<span class="label label-success">Active</span>
								</td>
								<td class="center">2015/01/01</td>
								<td class="center">
									<a class="btn btn-info" href="company-edit.jsp">
										<i class="icon-edit icon-white"></i>  
										Edit                                            
									</a>
									<a class="btn btn-danger" href="#" onclick="alert('reset password success!');">
										<i class="icon-refresh icon-white"></i> 
										Reset Pwd
									</a>
								</td>
							</tr>
							<tr>
								<td>1002</td>
								<td>Western Bus</td>
								<td>western_bus</td>
								<td>Tom</td>
								<td>417000029</td>
								<td>westernbus@ilikebus.com</td>
								<td class="center">
									<a href="group.jsp"><span class="label label-success">2</span></a>
								</td>
								<td class="center">
									<a href="device.jsp"><span class="label label-success">6</span>/<span class="label label-important">6</span></a>
								</td>
								<td class="center">
									<span class="label label-success">Active</span>
								</td>
								<td class="center">2015/01/01</td>
								<td class="center">
									<a class="btn btn-info" href="company-edit.jsp">
										<i class="icon-edit icon-white"></i>  
										Edit                                            
									</a>
									<a class="btn btn-danger" href="#" onclick="alert('reset password success!');">
										<i class="icon-refresh icon-white"></i> 
										Reset Pwd
									</a>
								</td>
							</tr>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Setting</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	
		
</body>
</html>
