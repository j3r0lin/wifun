<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!--
        	作者：city0532@126.com
        	时间：2015-04-28
        	描述：分组选择框开始
        -->
		<div class="modal hide fade" id="groupModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Choose Group</h3>
			</div>
			<div class="modal-body">
				<!-- search starts -->
<div>
		<div>
			<h3>Search By</h3>
			<div>
				<form action="/city/city-list.action" method="post" id="subForm">
					<table>
						<tr>
							<td class="<%= session.getAttribute("currentrole")  %>">
							<label class="control-label">Company</label>
							</td>
							<td class="<%= session.getAttribute("currentrole")  %>">
								<div class="controls">
								  <select id="selectError3">
									<option>Eastern Bus</option>
									<option>Western Bus</option>
								  </select>
								</div>
							</td>
							<td>
								<label class="control-label"> Group Name</label>
							</td>
							<td>
								<div class="controls">
									<input id="focusedInput" type="text">
								</div>
							</td>
							<td>
								<button type="button" onclick="alert('Search');" style="margin-bottom: 10px;" class="btn">
									Search
								</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
</div>
<!-- /search ends -->  
				 <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Groups</h2>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Choose</th>
								  <th>Group Name</th>
								  <th>Company</th>
								  <th>Devices</th>
								  <th>Status</th>
							  </tr>
						  </thead>   
						  <tbody>
							<tr>
								<td>
									<label class="radio">
									<input type="radio" name="groupRadios" id="group1" value="1" checked="">
									</label>
								</td>
								<td>NY-DC</td>
								<td>Eastern Bus</td>
								<td class="center">
									<span class="label label-success">5</span>/<span class="label label-important">5</span>
								</td>
								<td class="center">
									<span class="label label-success">Active</span>
								</td>
							</tr>
							<tr>
								<td>
									<label class="radio">
									<input type="radio" name="groupRadios" id="group2" value="2">
									</label>
								</td>
								<td>NY-CS</td>
								<td>Eastern Bus</td>
								<td class="center">
									<span class="label label-success">3</span>/<span class="label label-important">3</span>
								</td>
								<td class="center">
									<span class="label label-success">Active</span>
								</td>
							</tr>
							<tr>
								<td>
									<label class="radio">
									<input type="radio" name="groupRadios" id="group3" value="3">
									</label>
								</td>
								<td>NY-GA</td>
								<td>Eastern Bus</td>
								<td class="center">
									<span class="label label-success">3</span>/<span class="label label-important">3</span>
								</td>
								<td class="center">
									<span class="label label-success">Active</span>
								</td>
							</tr>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary" data-dismiss="modal">OK</a>
			</div>
		</div>
		<!-- Choose Group Dialog End -->
		
		<script>
		
			$('.btn-choose-group').click(function(e){
				e.preventDefault();
				$('#groupModal').modal('show');
			});

	</script>
