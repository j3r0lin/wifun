<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
		<!-- Choose Media List Dialog Begin -->
		<div class="modal hide fade" id="medialistModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Choose Media List</h3>
			</div>
			<div class="modal-body">
				  <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Meida List</h2>
						
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>
								  	 Choose
								  </th>
								  <th>Title</th>
								  <th>Size</th>									 
							  </tr>
						  </thead>   
						  <tbody>
							<tr>
								<td>
									<label class="radio">
									<input type="radio" name="medialistRadios" id="medialist1" value="1" checked="true">
									</label>
								</td>
								<td>Action Series</td>
								<td>7</td>
							</tr>
							<tr>
								<td>
									<label class="radio">
									<input type="radio" name="medialistRadios" id="medialist2" value="2">
									</label>
								</td>
								<td>Comedy Series</td>
								<td>5</td>
							</tr>
							<tr>
								<td>
									<label class="radio">
									<input type="radio" name="medialistRadios" id="medialist3" value="3">
									</label>
								</td>
								<td>Science Series</td>
								<td>8</td>
							</tr>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary" data-dismiss="modal">OK</a>
			</div>
		</div>
<!-- Choose Media List Dialog End -->
<script>

			$('.btn-choose-medialist').click(function(e){
				e.preventDefault();
				$('#medialistModal').modal('show');
			});
	</script>
