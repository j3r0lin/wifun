<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="modal hide fade" id="mediaDialog">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Media Detail</h3>
			</div>
			<div class="modal-body">
				<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Devices</h2>
						
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable">
						  <thead>
							  <tr>
								  <th>Choose</th>
								  <th>Title</th>
								  <th>Category</th>
								  <th>Year</th>
							  </tr>
						  </thead>   
						  <tbody>
						  	<tr>
								<td>
									<label class="checkbox">
									<input type="checkbox" id="optionsCheckbox2" >
								  </label>
								</td>
								<td>X-Men(2011)</td>
								<td>
									Science, Action
								</td>
								<td class="center">
									2011
								</td>
							</tr>
							<tr>
								<td>
									<label class="checkbox">
									<input type="checkbox" id="optionsCheckbox2" >
								  </label>
								</td>
								<td>X-Men(2000)</td>
								<td>
									Science, Action
								</td>
								<td class="center">
									2000
								</td>
							</tr>
							<tr>
								<td>
									<label class="checkbox">
									<input type="checkbox" id="optionsCheckbox2" >
								  </label>
								</td>
								<td>X-Men(2014)</td>
								<td>
									Science, Action
								</td>
								<td class="center">
									2014
								</td>
							</tr>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary" data-dismiss="modal">OK</a>
			</div>
		</div>
		
	<script>
		
			$('.btn-choose-media').click(function(e){
				e.preventDefault();
				$('#mediaDialog').modal('show');
			});

	</script>