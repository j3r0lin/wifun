<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="modal hide fade" id="deviceDialog">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Media Detail</h3>
			</div>
			<div class="modal-body">
				<div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Devices</h2>
						
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable">
						  <thead>
							  <tr>
								  <th>Device SN</th>
								  <th>Status</th>
								  <th>Update Time</th>
							  </tr>
						  </thead>   
						  <tbody>
						  	<tr>
								<td>PL3121503310001</td>
								<td class="center">
									<span class="label label-warning">Sync</span>
								</td>
								<td class="center">
									
								</td>
							</tr>
							<tr>
								<td>PP3121504259273</td>
								<td class="center">
									<span class="label label-success">Success</span>
								</td>
								<td class="center">
									04/05/2015 10:00:00
								</td>
							</tr>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
			</div>
		</div>
		
<script>
		
			$('.btn-device-detail').click(function(e){
				e.preventDefault();
				$('#deviceDialog').modal('show');
			});

	</script>