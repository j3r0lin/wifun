<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!--
        	作者：city0532@126.com
        	时间：2015-04-28
        	描述：分组选择框开始
        -->
		<div class="modal hide fade" id="movieModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Choose Movie</h3>
			</div>
			<div class="modal-body">
				 <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Movie Resource</h2>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Choose</th>
								  <th>File</th>
							  </tr>
						  </thead>   
						  <tbody>
							<tr>
								<td>
									<label class="radio">
									<input type="radio" name="movieRadios" id="group1" value="1" checked>
									</label>
								</td>
								<td>x-men-2000.mp4</td>
							</tr>
							<tr>
								<td>
									<label class="radio">
									<input type="radio" name="movieRadios" id="group2" value="2">
									</label>
								</td>
								<td>x-men-2011.mp4</td>
							</tr>
							<tr>
								<td>
									<label class="radio">
									<input type="radio" name="movieRadios" id="group3" value="3">
									</label>
								</td>
								<td>x-men-2014.mp4</td>
							</tr>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary" data-dismiss="modal">OK</a>
			</div>
		</div>
		<!-- Choose Group Dialog End -->
		
		<script>
		
			$('.btn-movie-resource').click(function(e){
				e.preventDefault();
				$('#movieModal').modal('show');
			});

	</script>
