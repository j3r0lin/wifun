<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!--
        	作者：city0532@126.com
        	时间：2015-04-28
        	描述：分组选择框开始
        -->
		<div class="modal hide fade" id="portalModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Choose Portal</h3>
			</div>
			<div class="modal-body">
				 <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Portal Packages</h2>
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>Choose</th>
								  <th>Title</th>
								  <th>Package</th>
								  <th>Note</th>
							  </tr>
						  </thead>   
						  <tbody>
							<tr>
								<td>
									<label class="radio">
									<input type="radio" name="groupRadios" id="group1" value="1" checked="">
									</label>
								</td>
								<td>Eastern Bus Portal(V1.0)</td>
								<td>portal_eastern_v1.0.zip</td>
								<td>
									portal note 1
								</td>
							</tr>
							<tr>
								<td>
									<label class="radio">
									<input type="radio" name="groupRadios" id="group2" value="2">
									</label>
								</td>
								<td>Eastern Bus Portal(V2.0)</td>
								<td>portal_eastern_v2.0.zip</td>
								<td>
									portal note 2
								</td>
							</tr>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary" data-dismiss="modal">OK</a>
			</div>
		</div>
		<!-- Choose Group Dialog End -->
		
		<script>
			$('.btn-choose-portal').click(function(e){
				e.preventDefault();
				$('#portalModal').modal('show');
			});

	</script>
