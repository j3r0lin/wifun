<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<title>iLIKEBUS</title>
		<meta name="viewport" content="width=device-width, initial-scale=1.0">

		<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
	</head>

	<body>

		<jsp:include page="/jsp/global/header.jsp"></jsp:include>

		<div class="container-fluid">
			<div class="row-fluid">
				


				<div id="content" class="span10">
					<!-- content starts -->
					<div class="row-fluid">
						<div class="box span12">
							<div class="box-header well">
								<h2><li class="icon-bullhorn"></li>&nbsp;Alerts</h2>
							</div>
							<div class="box-content">
								<form class="form-horizontal">
									<p class="red">
										Sorry, You do not have permission.
									</p>
									<p>
									<button id="submit-btn" type="button" onclick="window.location.href='javascript:history.go(-1);'" class="btn btn-primary">
										Back
									</button>
										<!--<a href="/jsp/station/station-list.action">Back</a>
									--></p>
								</form>
							</div>
						</div>
					</div>

					<!-- content ends -->
				</div>
				<!--/#content.span10-->
			</div>
			<!--/fluid-row-->

			<jsp:include page="/jsp/global/footer.jsp"></jsp:include>

		</div>
		<!--/.fluid-container-->

		<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>

	</body>
</html>
