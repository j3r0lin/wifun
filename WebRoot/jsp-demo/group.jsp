<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>WiFun Console</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	
	<jsp:include page="/jsp/global/base-css.jsp"></jsp:include>
</head>

<body>
		<!-- topbar starts -->
	<jsp:include page="/jsp/global/header.jsp"></jsp:include>
	<!-- topbar ends -->
		<div class="container-fluid">
		<div class="row-fluid">
				
			<!-- left menu starts -->
			<jsp:include page="/jsp/global/menu.jsp"></jsp:include>
			<!-- left menu ends -->
			
			<noscript>
				<div class="alert alert-block span10">
					<h4 class="alert-heading">Warning!</h4>
					<p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id="content" class="span10">
			<!-- content starts -->
			

			<div>
				<ul class="breadcrumb">
					<li>
						<a href="#">Company Management</a> <span class="divider">/</span>
					</li>
					<li>
						<a href="#">Group Management</a>
					</li>
				</ul>
			</div>
<!-- search starts -->
<div>
	<div class="span12 my-search">
		<div>
			<h3>Search By</h3>
			<div class="my-filters">
				<form action="/city/city-list.action" method="post" id="subForm">
					<table>
						<tr>
							<td class="<%= session.getAttribute("currentrole")  %>">
							<label class="control-label">Company</label>
							</td>
							<td class="<%= session.getAttribute("currentrole")  %>">
								<div class="controls">
								  <select id="selectError3">
									<option>Eastern Bus</option>
									<option>Western Bus</option>
								  </select>
								</div>
							</td>
							<td>
								<label class="control-label"> Group Name</label>
							</td>
							<td>
								<div class="controls">
									<input id="focusedInput" type="text">
								</div>
							</td>
							<td>
								<button type="button" onclick="alert('Search');" style="margin-bottom: 10px;" class="btn">
									Search
								</button>
							</td>
						</tr>
					</table>
				</form>
			</div>
		</div>
	</div>
</div>
<!-- /search ends -->   								  
       <div class="row-fluid sortable">		
				<div class="box span12">
					<div class="box-header well" data-original-title>
						<h2><i class="icon-user"></i> Groups</h2>
						<div style="float:right;">
									<a class="btn btn-primary"  href="group-add.jsp" >
									<i class="icon icon-white icon-plus"></i>&nbsp;Add Group </a>
						</div>	
					</div>
					<div class="box-content">
						<table class="table table-striped table-bordered bootstrap-datatable datatable">
						  <thead>
							  <tr>
								  <th>ID</th>
								  <th>Group Name</th>
								  <th class="<%= session.getAttribute("currentrole")  %>">Company</th>
								  <th>Devices</th>
								  <th>Status</th>
								  <th>Create Date</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
							<tr>
								<td>1</td>
								<td>NY-DC</td>
								<td class="<%= session.getAttribute("currentrole")  %>">Eastern Bus</td>
								<td class="center">
									<a href="device.jsp"><span class="label label-success">5</span>/<span class="label label-important">5</span></a>
								</td>
								<td class="center">
									<span class="label label-success">Active</span>
								</td>
								<td class="center">2015/01/01</td>
								<td class="center">
									<a class="btn btn-info" href="group-edit.jsp">
										<i class="icon-edit icon-white"></i>  
										Edit                                            
									</a>
									<a class="btn btn-danger" href="#" onclick="deleteTableRow(this)">
										<i class="icon-trash icon-white"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>2</td>
								<td>NY-CS</td>
								<td class="<%= session.getAttribute("currentrole")  %>">Eastern Bus</td>
								<td class="center">
									<a href="device.jsp"><span class="label label-success">3</span>/<span class="label label-important">3</span></a>
								</td>
								<td class="center">
									<span class="label label-success">Active</span>
								</td>
								<td class="center">2015/01/01</td>
								<td class="center">
									<a class="btn btn-info" href="group-edit.jsp">
										<i class="icon-edit icon-white"></i>  
										Edit                                            
									</a>
									<a class="btn btn-danger" href="#" onclick="deleteTableRow(this)">
										<i class="icon-trash icon-white"></i> 
										Delete
									</a>
								</td>
							</tr>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
				
		<hr>

		<div class="modal hide fade" id="myModal">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">×</button>
				<h3>Settings</h3>
			</div>
			<div class="modal-body">
				<p>Here settings can be configured...</p>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn" data-dismiss="modal">Close</a>
				<a href="#" class="btn btn-primary">Save changes</a>
			</div>
		</div>

		<jsp:include page="/jsp/global/footer.jsp"></jsp:include>
		
	</div><!--/.fluid-container-->

	<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<jsp:include page="/jsp/global/base-js.jsp"></jsp:include>
	
	
</body>
</html>
