<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
		<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<!-- jQuery -->
	<script src="<%= request.getContextPath() %>/js/jquery-1.7.2.min.js"></script>
	<!-- jQuery UI -->
	<script src="<%= request.getContextPath() %>/js/jquery-ui-1.8.21.custom.min.js"></script>
	<!-- transition / effect library -->
	<script src="<%= request.getContextPath() %>/js/bootstrap-transition.js"></script>
	<!-- alert enhancer library -->
	<script src="<%= request.getContextPath() %>/js/bootstrap-alert.js"></script>
	<!-- modal / dialog library -->
	<script src="<%= request.getContextPath() %>/js/bootstrap-modal.js"></script>
	<!-- custom dropdown library -->
	<script src="<%= request.getContextPath() %>/js/bootstrap-dropdown.js"></script>
	<!-- scrolspy library -->
	<script src="<%= request.getContextPath() %>/js/bootstrap-scrollspy.js"></script>
	<!-- library for creating tabs -->
	<script src="<%= request.getContextPath() %>/js/bootstrap-tab.js"></script>
	<!-- library for advanced tooltip -->
	<script src="<%= request.getContextPath() %>/js/bootstrap-tooltip.js"></script>
	<!-- popover effect library -->
	<script src="<%= request.getContextPath() %>/js/bootstrap-popover.js"></script>
	<!-- button enhancer library -->
	<script src="<%= request.getContextPath() %>/js/bootstrap-button.js"></script>
	<!-- accordion library (optional, not used in demo) -->
	<script src="<%= request.getContextPath() %>/js/bootstrap-collapse.js"></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script src="<%= request.getContextPath() %>/js/bootstrap-carousel.js"></script>
	<!-- autocomplete library -->
	<script src="<%= request.getContextPath() %>/js/bootstrap-typeahead.js"></script>
	<!-- tour library -->
	<script src="<%= request.getContextPath() %>/js/bootstrap-tour.js"></script>
	<!-- library for cookie management -->
	<script src="<%= request.getContextPath() %>/js/jquery.cookie.js"></script>
	<!-- calander plugin -->
	<script src='<%= request.getContextPath() %>/js/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='<%= request.getContextPath() %>/js/jquery.dataTables.min.js'></script>

	<!-- chart libraries start -->
	<script src="<%= request.getContextPath() %>/js/excanvas.js"></script>
	<script src="<%= request.getContextPath() %>/js/jquery.flot.min.js"></script>
	<script src="<%= request.getContextPath() %>/js/jquery.flot.pie.min.js"></script>
	<script src="<%= request.getContextPath() %>/js/jquery.flot.stack.js"></script>
	<script src="<%= request.getContextPath() %>/js/jquery.flot.resize.min.js"></script>
	<!-- chart libraries end -->

	<!-- select or dropdown enhancer -->
	<script src="<%= request.getContextPath() %>/js/jquery.chosen.min.js"></script>
	<!-- checkbox, radio, and file input styler -->
	<script src="<%= request.getContextPath() %>/js/jquery.uniform.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="<%= request.getContextPath() %>/js/jquery.colorbox.min.js"></script>
	<!-- rich text editor library -->
	<script src="<%= request.getContextPath() %>/js/jquery.cleditor.min.js"></script>
	<!-- notification plugin -->
	<script src="<%= request.getContextPath() %>/js/jquery.noty.js"></script>
	<!-- file manager library -->
	<script src="<%= request.getContextPath() %>/js/jquery.elfinder.min.js"></script>
	<!-- star rating plugin -->
	<script src="<%= request.getContextPath() %>/js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="<%= request.getContextPath() %>/js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="<%= request.getContextPath() %>/js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="<%= request.getContextPath() %>/js/jquery.uploadify-3.1.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="<%= request.getContextPath() %>/js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<script src="<%= request.getContextPath() %>/js/charisma.js"></script>
	
		<script>
		function deleteTableRow(obj)
		{
			var killRow = $(obj).parent();
			while("TR" != killRow[0].tagName)
			{
				killRow = killRow.parent();
			}
			
			killRow.remove();
		}
		
		///innerHtml to text
		function parseHtmlText(element)
		{
			if (0 == element.children.length )
			{
				return element.innerHTML.replace(/(^\s*)|(\s*$)/g,'');  
			}
				
			
			var htmlValue = "";
			while (element.children.length > 0)
			{
				htmlValue += parseHtmlText(element.children[0]);
				element.children[0].remove();
			}
			htmlValue += element.innerHTML;
			return htmlValue.replace(/(^\s*)|(\s*$)/g,'');
		}
		
		function onEditTableRow(url, obj)
		{
			var editRow = $(obj).parent();
			while("TR" != editRow[0].tagName)
			{
				editRow = editRow.parent();
			}
			
			var obj = new Object(); 
			
			for (var i = 0; i < editRow[0].cells.length; i++)
			{
				obj['td'+i] = parseHtmlText( editRow[0].cells[i] );
			}
			
			window.location = url + "?type=Save&obj="+JSON.stringify(obj); ;
		}
	</script>