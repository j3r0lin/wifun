<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!-- left menu starts -->
<div class="span2 main-menu-span">
	<div class="well nav-collapse sidebar-nav">
		<ul class="nav nav-tabs nav-stacked main-menu">
		
			<li class="nav-header">Admin Main</li>
			<li class="${type==16?'active':''}"><a class="ajax-link" href="/company/company-list.action"><i class="icon-globe"></i><span class="hidden-tablet"> Company </span></a></li> 
			<li class="${type==2 ?'active':''}"><a class="ajax-link" href="/city/city-list.action"><i class="icon-flag"></i><span class="hidden-tablet"> Busline Cities </span></a></li>
			<li class="${type==17?'active':''}"><a class="ajax-link" href="/schedule/checkException-list.action"><i class="icon-plane"></i><span class="hidden-tablet"> Check Exception </span></a></li> 
			<li class="${type==12?'active':''}"><a class="ajax-link" href="/pricing/pricing-list.action"><i class="icon-tags"></i><span class="hidden-tablet"> Pricing Plans </span></a></li> 
<!-- 			<li class="${type==21?'active':''}"><a class="ajax-link" href="/refund/refund-search.action"><i class="icon-tags"></i><span class="hidden-tablet"> Refund </span></a></li> -->
			
			<li class="nav-header">Bookings</li>
			<li class="${type==8 ?'active':''}"><a class="ajax-link" href="/booking/booking-list.action"><i class="icon-book"></i><span class="hidden-tablet"> Bookings</span></a></li>
			<li class="${type==32 ?'active':''}"><a class="ajax-link" href="/booking/today-booking-list.action"><i class="icon-book"></i><span class="hidden-tablet"> Today's Bookings</span></a></li>
			<li class="${type==31 ?'active':''}"><a class="ajax-link" href="/booking/today-order-list.action"><i class="icon-book"></i><span class="hidden-tablet"> Today's Order</span></a></li>
			<li class="${type==5 ?'active':''}"><a class="ajax-link" href="/ticket/ticket-list.action"><i class="icon-barcode"></i><span class="hidden-tablet"> Tickets</span></a></li>
			<li class="${type==4 ?'active':''}"><a class="ajax-link" href="/schedule/schedule.action"><i class="icon-calendar"></i><span class="hidden-tablet"> Schedules</span></a></li>
			<li class="${type==6 ?'active':''}"><a class="ajax-link" href="/schedule/sub-schedule.action"><i class="icon-calendar"></i><span class="hidden-tablet"> Sub Schedules</span></a></li>
			<li class="${type==18 ?'active':''}"><a class="ajax-link" href="/schedule/change-pricing.action"><i class="icon-random"></i><span class="hidden-tablet"> Adjust Pricing</span></a></li>
			
			<li class="nav-header">Credits</li>
			<li class="${type==19 ?'active':''}"><a class="ajax-link" href="/coupon/issue-coupons.action"><i class="icon-gift"></i><span class="hidden-tablet"> Issue Credits</span></a></li>
			<li class="${type==20 ?'active':''}"><a class="ajax-link" href="/coupon/coupon-logs.action"><i class="icon-gift"></i><span class="hidden-tablet"> Credits Logs</span></a></li>
			
			<li class="nav-header">Reports</li>
			<li class="${type==9 ?'active':''}"><a class="ajax-link" href="/report/daily-sales.action"><i class="icon-time"></i><span class="hidden-tablet"> Daily Sales</span></a></li>
			<li class="${type==10?'active':''}"><a class="ajax-link" href="/report/sales.action"><i class="icon-tasks"></i><span class="hidden-tablet"> Sales</span></a></li>
			<li class="${type==11?'active':''}"><a class="ajax-link" href="/report/sales-by-city.action"><i class="icon-bullhorn"></i><span class="hidden-tablet"> Sales By City</span></a></li> 
			<li class="${type==13?'active':''}"><a class="ajax-link" href="/report/sales-reward.action"><i class="icon-briefcase"></i><span class="hidden-tablet"> Reward Sales</span></a></li> 
			<!-- <li class="${type==14?'active':''}"><a class="ajax-link" href="/admin/report/international-sales.action"><i class=" icon-adjust"></i><span class="hidden-tablet"> International Sales </span></a></li> --> 
		
			<li class="nav-header">Buslines</li>
			<li class="${type==3 ?'active':''}"><a class="ajax-link" href="/station/station-list.action"><i class="icon-plane"></i><span class="hidden-tablet"> Bus Stations</span></a></li>
			<li class="${type==7 ?'active':''}"><a class="ajax-link" href="/busline/busline-list.action"><i class="icon-road"></i><span class="hidden-tablet"> Buslines</span></a></li>
									
			<li class="nav-header">Member</li>
			<li class="${type==22 ?'active':''}"><a class="ajax-link" href="/system/account-list.action"><i class="icon-user"></i><span class="hidden-tablet"> System Accounts</span></a></li>
			<li class="${type==1 ?'active':''}"><a class="ajax-link" href="/company/account-list.action"><i class="icon-user"></i><span class="hidden-tablet"> Company Accounts</span></a></li>
			<li class="${type==33 ?'active':''}"><a class="ajax-link" href="/system/account-repwd.action"><i class="icon-user"></i><span class="hidden-tablet"> Reset Password</span></a></li>
		</ul>
		<!--<label id="for-is-ajax" class="hidden-tablet" for="is-ajax"><input id="is-ajax" type="checkbox"> Ajax on menu</label>-->
	</div><!--/.well -->
</div><!--/span-->
<!-- left menu ends -->

