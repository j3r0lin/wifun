<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
	<!-- The styles -->
	<link id="bs-css" href="<%= request.getContextPath() %>/css/bootstrap-cerulean.css" rel="stylesheet">
	<style type="text/css">
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	  
	  .company {
	  	display:none;
	  }
	  
	  .my-search {
		min-height: 20px;
		padding: 5px 10px;
		margin-bottom: 18px;
		margin-top: 0px;
		border: 1px solid #eee;
	}
	.my-filters{
			margin-top:5px;
	}
	
	.my-filters form{
			margin: 0px; 
	}
	</style>
	<link href="<%= request.getContextPath() %>/css/bootstrap-responsive.css" rel="stylesheet">
	<link href="<%= request.getContextPath() %>/css/charisma-app.css" rel="stylesheet">
	<link href="<%= request.getContextPath() %>/css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
	<link href='<%= request.getContextPath() %>/css/fullcalendar.css' rel='stylesheet'>
	<link href='<%= request.getContextPath() %>/css/fullcalendar.print.css' rel='stylesheet'  media='print'>
	<link href='<%= request.getContextPath() %>/css/chosen.css' rel='stylesheet'>
	<link href='<%= request.getContextPath() %>/css/uniform.default.css' rel='stylesheet'>
	<link href='<%= request.getContextPath() %>/css/colorbox.css' rel='stylesheet'>
	<link href='<%= request.getContextPath() %>/css/jquery.cleditor.css' rel='stylesheet'>
	<link href='<%= request.getContextPath() %>/css/jquery.noty.css' rel='stylesheet'>
	<link href='<%= request.getContextPath() %>/css/noty_theme_default.css' rel='stylesheet'>
	<link href='<%= request.getContextPath() %>/css/elfinder.min.css' rel='stylesheet'>
	<link href='<%= request.getContextPath() %>/css/elfinder.theme.css' rel='stylesheet'>
	<link href='<%= request.getContextPath() %>/css/jquery.iphone.toggle.css' rel='stylesheet'>
	<link href='<%= request.getContextPath() %>/css/opa-icons.css' rel='stylesheet'>
	<link href='<%= request.getContextPath() %>/css/uploadify.css' rel='stylesheet'>
	<link href='<%= request.getContextPath() %>/css/dataTables.tableTools.css' rel='stylesheet'>
	

	<!-- The fav icon -->
	<link rel="shortcut icon" href="/img/favicon.ico">	