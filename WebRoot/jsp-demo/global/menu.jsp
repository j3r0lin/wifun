<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<div class="span2 main-menu-span">
				<div class="well nav-collapse sidebar-nav">
					<ul class="nav nav-tabs nav-stacked main-menu">
						<li class="nav-header hidden-tablet"> Main</li>
						<li><a class="ajax-link" href="<%= request.getContextPath() %>/jsp/index.jsp"><i class="icon-home"></i><span class="hidden-tablet"> Dashboard</span></a></li>
						<li class="nav-header hidden-tablet">Company</li>
						<% if ("super".equals( session.getAttribute("currentrole") )) {%>
						<li><a class="ajax-link" href="<%= request.getContextPath() %>/jsp/company.jsp"><i class="icon-flag"></i><span class="hidden-tablet"> Company</span></a></li>
						<% } %>
						<li><a class="ajax-link" href="<%= request.getContextPath() %>/jsp/group.jsp"><i class="icon-inbox"></i><span class="hidden-tablet"> Group</span></a></li>
						<li class="nav-header hidden-tablet">Device</li>
						<li><a class="ajax-link" href="<%= request.getContextPath() %>/jsp/device.jsp"><i class="icon-hdd"></i><span class="hidden-tablet"> Device</span></a></li>
						<li class="nav-header hidden-tablet">Portal</li>
						<li><a class="ajax-link" href="<%= request.getContextPath() %>/jsp/portal.jsp"><i class="icon-globe"></i><span class="hidden-tablet"> Portal</span></a></li>
						<li><a class="ajax-link" href="<%= request.getContextPath() %>/jsp/portal-deploy.jsp"><i class="icon-wrench"></i><span class="hidden-tablet">  Deploy</span></a></li>
						<li class="nav-header hidden-tablet">Media </li>
						<li><a class="ajax-link" href="<%= request.getContextPath() %>/jsp/category.jsp"><i class="icon-tags"></i><span class="hidden-tablet"> Category </span></a></li>
						<li><a class="ajax-link" href="<%= request.getContextPath() %>/jsp/media.jsp"><i class="icon-film"></i><span class="hidden-tablet"> Media </span></a></li>
						<li><a class="ajax-link" href="<%= request.getContextPath() %>/jsp/media-list.jsp"><i class="icon-list"></i><span class="hidden-tablet"> Media List </span></a></li>
						<li><a class="ajax-link" href="<%= request.getContextPath() %>/jsp/media-deploy.jsp"><i class="icon-wrench"></i><span class="hidden-tablet"> Deploy</span></a></li>
					</ul>
				</div><!--/.well -->
			</div><!--/span-->