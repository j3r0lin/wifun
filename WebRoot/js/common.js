function loadCompanyGroups(companyId, func) {

	$.ajax({
		type : "post",
		url : "/group/company-group-list.action",
		data : {
			"companyId" : companyId
		},
		dataType : "json",
		async : false,
		success : function(response) {

			if ("function" == typeof func && response.groupList)
				func.call(func, response.groupList);
		}
	});
};

function loadCompanyGroupDevices(companyId, groupId, func) {

	$.ajax({
		type : "post",
		url : "/device/company-group-device-list.action",
		data : {
			"companyId" : companyId,
			"groupId" : groupId
		},
		dataType : "json",
		async : false,
		success : function(response) {

			if ("function" == typeof func && response.deviceList)
				func.call(func, response.deviceList);
		}
	});
};

function loadCompanyPortals(companyId, func) {

	$.ajax({
		type : "post",
		url : "/portal/company-portal-list.action",
		data : {
			"companyId" : companyId
		},
		dataType : "json",
		async : false,
		success : function(response) {

			if ("function" == typeof func && response.portalList)
				func.call(func, response.portalList);
		}
	});
};

function loadCompanyMediaList(companyId, func) {

	$.ajax({
		type : "post",
		url : "/media/company-media-list.action",
		data : {
			"companyId" : companyId
		},
		dataType : "json",
		async : false,
		success : function(response) {

			if ("function" == typeof func && response.list)
				func.call(func, response.list);
		}
	});
};