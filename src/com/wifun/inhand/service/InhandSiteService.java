package com.wifun.inhand.service;


import org.apache.commons.lang.StringUtils;

import net.sf.json.JSONObject;

import com.wifun.inhand.request.site.QuerySiteListRequest;
import com.wifun.inhand.response.site.SiteListResponse;
import com.wifun.inhand.util.InhandHttpClient;

/**
 * 车辆接口服务类
 * 
 * @File wifun-admin/com.wifun.inhand.service.InhandSiteService.java
 * @Desc
 */
public class InhandSiteService {

	public static SiteListResponse querySites(String token) {

		QuerySiteListRequest request = new QuerySiteListRequest();
		request.setVerbose("100");
		request.setToken(token);
		String rs = InhandHttpClient.getInstance().httpGet(request);

		if (StringUtils.isBlank(rs))
			return null;

		JSONObject jsonObject = JSONObject.fromObject(rs);
		SiteListResponse response = new SiteListResponse();
		response.fromJsonObject(jsonObject);

		return response;
	}
}
