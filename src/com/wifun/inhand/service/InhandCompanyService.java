package com.wifun.inhand.service;

import org.apache.commons.lang.StringUtils;

import net.sf.json.JSONObject;

import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.model.Company;
import com.wifun.admin.util.SysConfig;
import com.wifun.inhand.model.Organization;
import com.wifun.inhand.request.company.CreateCompanyRequest;
import com.wifun.inhand.request.company.DeleteCompanyRequest;
import com.wifun.inhand.request.company.EditCompanyRequest;
import com.wifun.inhand.request.company.GetCompanyRequest;
import com.wifun.inhand.response.company.OrganizationResponse;
import com.wifun.inhand.util.InhandHttpClient;
import com.wifun.inhand.util.MD5Util;

/**
 * 公司接口服务类
 * 
 * @File wifun-admin/com.wifun.inhand.service.InhandCompanyService.java
 * @Desc
 */
public class InhandCompanyService {

	/**
	 * 新建公司
	 * 
	 * @param c
	 * @return
	 */
	public static OrganizationResponse addCompany(Company c) {

		CreateCompanyRequest ccr = new CreateCompanyRequest();
		ccr.setToken(SessionHelper.getToken(1));
		Organization company = new Organization();
		company.setName(c.getName());
		company.setUsername(c.getAccount());
		company.setEmail(c.getEmail());
		company.setPassword(MD5Util.string2MD5(SysConfig.getSetting("inhand.api.company.password")));
		ccr.setCompany(company);

		String rs = InhandHttpClient.getInstance().httpPost(ccr);

		if (StringUtils.isBlank(rs))
			return null;

		JSONObject jsonObject = JSONObject.fromObject(rs);
		OrganizationResponse response = new OrganizationResponse();
		response.fromJsonObject(jsonObject);
		return response;
	}

	/**
	 * 修改公司
	 * 
	 * @param c
	 * @return
	 */
	public static OrganizationResponse editCompany(Company c) {

		EditCompanyRequest ecr = new EditCompanyRequest();
		ecr.setToken(SessionHelper.getToken(1));
		Organization company = new Organization();
		company.set_id(c.getInhandId());
		company.setName(c.getName());
		company.setUsername(c.getAccount());
		ecr.setCompany(company);

		String rs = InhandHttpClient.getInstance().httpPut(ecr);

		if (StringUtils.isBlank(rs))
			return null;

		JSONObject jsonObject = JSONObject.fromObject(rs);
		OrganizationResponse response = new OrganizationResponse();
		response.fromJsonObject(jsonObject);
		return response;
	}

	/**
	 * 删除公司
	 * 
	 * @param token
	 * @param inhandId
	 * @return
	 */
	public static OrganizationResponse deleteCompany(String token, String inhandId) {

		DeleteCompanyRequest ccr = new DeleteCompanyRequest();
		ccr.setToken(token);
		ccr.setOid(inhandId);

		String rs = InhandHttpClient.getInstance().httpDelete(ccr);
		if (StringUtils.isBlank(rs))
			return null;

		JSONObject jsonObject = JSONObject.fromObject(rs);
		OrganizationResponse response = new OrganizationResponse();
		response.fromJsonObject(jsonObject);
		return response;
	}

	/**
	 * 查询公司
	 * 
	 * @param inhandId
	 * @return
	 */
	public static OrganizationResponse getCompany(String inhandId) {

		GetCompanyRequest ccr = new GetCompanyRequest();
		ccr.setToken(SessionHelper.getToken(1));
		ccr.setOid(inhandId);

		String rs = InhandHttpClient.getInstance().httpGet(ccr);

		if (StringUtils.isBlank(rs))
			return null;

		JSONObject jsonObject = JSONObject.fromObject(rs);
		OrganizationResponse response = new OrganizationResponse();
		response.fromJsonObject(jsonObject);
		return response;
	}
}
