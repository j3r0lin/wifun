package com.wifun.inhand.service;

import net.sf.json.JSONObject;

import com.wifun.inhand.request.publish.FileUploadRequest;
import com.wifun.inhand.util.InhandHttpClient;

/**
 * portal上传接口类
 * 
 * @File wifun-admin/com.wifun.inhand.service.InhandPortalDeployService.java
 * @Desc
 */
public class InhandPortalDeployService {

	/**
	 * 上传portal文件
	 * 
	 * @param token
	 * @param filePath
	 * @return
	 */
	public static String portalUpload(String token, String filePath) {

		FileUploadRequest fur = new FileUploadRequest();
		fur.setToken(token);
		fur.setFilePath(filePath);
		fur.setFileName("html.zip");
		fur.setSuffix("zip");

		String rs = InhandHttpClient.getInstance().httpPost(fur);
		JSONObject jsObject = JSONObject.fromObject(rs);
		if (jsObject.containsKey("result"))
			return jsObject.getString("result");

		return null;
	}
}
