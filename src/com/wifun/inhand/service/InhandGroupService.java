package com.wifun.inhand.service;

import org.apache.commons.lang.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.wifun.admin.domain.model.Group;
import com.wifun.inhand.model.Tag;
import com.wifun.inhand.request.tag.CreateTagRequest;
import com.wifun.inhand.request.tag.DeleteTagRequest;
import com.wifun.inhand.request.tag.EditTagRequest;
import com.wifun.inhand.request.tag.QueryTagRequest;
import com.wifun.inhand.response.tag.TagResponse;
import com.wifun.inhand.util.InhandHttpClient;

/**
 * 标签接口服务类
 * 
 * @File wifun-admin/com.wifun.inhand.service.InhandGroupService.java
 * @Desc
 */
public class InhandGroupService {

	/**
	 * 新建标签
	 * 
	 * @param token
	 * @param group
	 * @return
	 */
	public static TagResponse addTag(String token, Group group) {

		CreateTagRequest ctr = new CreateTagRequest();
		ctr.setToken(token);

		Tag tag = new Tag();
		tag.setName(group.getName());
		tag.setShared(true);
		ctr.setTag(tag);

		String rs = InhandHttpClient.getInstance().httpPost(ctr);
		if (StringUtils.isBlank(rs))
			return null;

		JSONObject jsObject = JSONObject.fromObject(rs);
		TagResponse response = new TagResponse();
		response.fromJsonObject(jsObject);

		// //如果已经存在，就查询得到ID
		if ("20007".equals(response.getError_code())) {
			String inhandId = queryTag(token, group.getName());
			if (StringUtils.isNotBlank(inhandId)) {
				tag = new Tag();
				tag.set_id(inhandId);
				response.setTag(tag);
			}
		}
		return response;
	}

	/**
	 * 查询标签
	 * 
	 * @param token
	 * @param name
	 * @return
	 */
	public static String queryTag(String token, String name) {

		try {
			QueryTagRequest qtr = new QueryTagRequest();
			qtr.setToken(token);
			qtr.setName(name);

			String rs = InhandHttpClient.getInstance().httpGet(qtr);
			if (StringUtils.isNotBlank(rs)) {
				JSONObject jsObject = JSONObject.fromObject(rs);
				if (jsObject.containsKey("result") && null != jsObject.getJSONArray("result")) {
					JSONArray tagList = jsObject.getJSONArray("result");
					if (tagList.size() == 0)
						return null;
					if (tagList.getJSONObject(0).containsKey("_id")) {
						return tagList.getJSONObject(0).getString("_id");
					}
				}
			}
			return null;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * 修改标签
	 * 
	 * @param token
	 * @param group
	 * @return
	 */
	public static TagResponse updateTag(String token, Group group) {

		EditTagRequest ctr = new EditTagRequest();
		ctr.setToken(token);
		ctr.setTagId(group.getInhandId());

		Tag tag = new Tag();
		tag.setName(group.getName());
		tag.setShared(true);
		ctr.setTag(tag);

		String rs = InhandHttpClient.getInstance().httpPut(ctr);
		if (StringUtils.isBlank(rs))
			return null;

		JSONObject jsObject = JSONObject.fromObject(rs);
		TagResponse response = new TagResponse();
		response.fromJsonObject(jsObject);
		return response;
	}

	/**
	 * 删除标签
	 * 
	 * @param token
	 * @param group
	 * @return
	 */
	public static TagResponse deleteTag(String token, Group group) {

		if (group == null)
			return null;

		String inhandId = group.getInhandId();
		if (StringUtils.isBlank(inhandId))
			inhandId = queryTag(token, group.getName());

		if (StringUtils.isBlank(inhandId))
			return null;

		DeleteTagRequest dtr = new DeleteTagRequest();
		dtr.setToken(token);
		dtr.setTagId(inhandId);

		String rs = InhandHttpClient.getInstance().httpDelete(dtr);
		if (StringUtils.isBlank(rs))
			return null;

		JSONObject jsObject = JSONObject.fromObject(rs);
		TagResponse response = new TagResponse();
		response.fromJsonObject(jsObject);
		return response;
	}
}
