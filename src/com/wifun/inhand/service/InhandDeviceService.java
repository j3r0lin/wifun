package com.wifun.inhand.service;

import java.util.List;

import org.apache.commons.lang.StringUtils;

import net.sf.json.JSONObject;

import com.wifun.admin.domain.model.Device;
import com.wifun.inhand.model.Model;
import com.wifun.inhand.model.Router;
import com.wifun.inhand.request.device.CreateRouterRequest;
import com.wifun.inhand.request.device.DeleteRouterRequest;
import com.wifun.inhand.request.device.EditRouterRequest;
import com.wifun.inhand.request.device.PutTagRequest;
import com.wifun.inhand.request.device.QueryRouterListRequest;
import com.wifun.inhand.request.device.RemoveTagRequest;
import com.wifun.inhand.request.model.ModelRequest;
import com.wifun.inhand.response.device.RouterListResponse;
import com.wifun.inhand.response.device.RouterResponse;
import com.wifun.inhand.response.model.ModelResponse;
import com.wifun.inhand.util.InhandHttpClient;

/**
 * 路由器设备接口服务类
 * 
 * @File wifun-admin/com.wifun.inhand.service.InhandDeviceService.java
 * @Desc
 */
public class InhandDeviceService {

	/**
	 * 添加设备
	 * 
	 * @param token
	 * @param device
	 * @return
	 */
	public static RouterResponse addDevice(String token, Device device) {

		if (StringUtils.isBlank(token))
			return null;

		CreateRouterRequest cdr = new CreateRouterRequest();
		cdr.setToken(token);
		Router router = new Router();
		router.setSerialNumber(device.getSn());
		router.setName(device.getName());
		router.setModel(device.getModel());
		router.setModelId(queryModelId(token, device));

		cdr.setRouter(router);
		String rs = InhandHttpClient.getInstance().httpPost(cdr);
		if (StringUtils.isBlank(rs))
			return null;

		JSONObject jsonObject = JSONObject.fromObject(rs);
		RouterResponse rr = new RouterResponse();
		rr.fromJsonObject(jsonObject);
		return rr;
	}

	/**
	 * 修改设备信息
	 * 
	 * @param token
	 * @param device
	 * @return
	 */
	public static RouterResponse updateDevice(String token, Device device) {

		if (StringUtils.isBlank(token) || device == null)
			return null;

		EditRouterRequest edr = new EditRouterRequest();
		edr.setId(device.getInhandId());
		edr.setCancelSite(device.getInhandSiteId());
		edr.setToken(token);

		Router router = new Router();
		router.set_id(device.getInhandId());
		router.setSerialNumber(device.getSn());
		router.setName(device.getName());
		router.setSiteId(device.getInhandSiteId());
		edr.setRouter(router);

		String rs = InhandHttpClient.getInstance().httpPut(edr);
		if (StringUtils.isBlank(rs))
			return null;

		JSONObject jsonObject = JSONObject.fromObject(rs);
		RouterResponse rr = new RouterResponse();
		rr.fromJsonObject(jsonObject);
		return rr;
	}

	/**
	 * 删除设备
	 * 
	 * @param token
	 * @param routerId
	 * @return
	 */
	public static RouterResponse deleteDevice(String token, String routerId) {

		if (StringUtils.isBlank(token) || StringUtils.isBlank(routerId))
			return null;

		DeleteRouterRequest ddr = new DeleteRouterRequest();
		ddr.setToken(token);
		ddr.setId(routerId);

		String rs = InhandHttpClient.getInstance().httpDelete(ddr);
		if (StringUtils.isBlank(rs))
			return null;

		JSONObject jsonObject = JSONObject.fromObject(rs);
		RouterResponse rr = new RouterResponse();
		rr.fromJsonObject(jsonObject);
		return rr;
	}

	/**
	 * 查询设备列表
	 * 
	 * @param token
	 * @param sn
	 * @return
	 */
	public static RouterListResponse queryDevices(String token, String sn) {

		QueryRouterListRequest mr = new QueryRouterListRequest();
		mr.setVerbose("100");
		mr.setToken(token);
		mr.setSn(sn);
		String rs = InhandHttpClient.getInstance().httpGet(mr);
		if (StringUtils.isBlank(rs))
			return null;

		JSONObject jsonObject = JSONObject.fromObject(rs);
		RouterListResponse rr = new RouterListResponse();
		rr.fromJsonObject(jsonObject);
		return rr;
	}

	/**
	 * 查询设备类型
	 * 
	 * @param token
	 * @param device
	 * @return
	 */
	public static String queryModelId(String token, Device device) {

		ModelRequest mr = new ModelRequest();
		mr.setToken(token);
		mr.setName(device.getModel());

		String rs = InhandHttpClient.getInstance().httpGet(mr);
		if (StringUtils.isNotBlank(rs)) {
			JSONObject jsObject = JSONObject.fromObject(rs);

			ModelResponse modelRes = new ModelResponse();
			modelRes.fromJsonObject(jsObject);
			List<Model> mlist = modelRes.getModelList();
			if (null != mlist && mlist.size() > 0) {
				return mlist.get(0).get_id();
			}
		}
		return null;
	}

	/**
	 * 给设备设置标签
	 * 
	 * @param token
	 * @param inhandSiteId
	 * @param tagId
	 * @return
	 */
	public static boolean putTag(String token, String inhandSiteId, String tagId) {

		PutTagRequest ptr = new PutTagRequest();
		ptr.setToken(token);
		ptr.setResourceType("14");
		ptr.setResourceId(inhandSiteId);
		ptr.setTagId(tagId);

		String rs = InhandHttpClient.getInstance().httpPut(ptr);
		if (StringUtils.isNotBlank(rs)) {
			JSONObject jsObject = JSONObject.fromObject(rs);

			if (jsObject.containsKey("error_code"))
				return false;
		}
		return true;
	}

	/**
	 * 删除设备标签
	 * 
	 * @param token
	 * @param inhandSiteId
	 * @param tagId
	 * @return
	 */
	public static boolean removeTag(String token, String inhandSiteId, String tagId) {

		RemoveTagRequest ptr = new RemoveTagRequest();
		ptr.setToken(token);
		ptr.setResourceId(inhandSiteId);
		ptr.setTagId(tagId);

		String rs = InhandHttpClient.getInstance().httpDelete(ptr);
		if (StringUtils.isNotBlank(rs)) {
			JSONObject jsObject = JSONObject.fromObject(rs);

			if (jsObject.containsKey("error_code"))
				return false;
		}
		return true;
	}
}
