package com.wifun.inhand.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import net.sf.json.JSONObject;

import com.wifun.admin.app.vo.PvuvMovieVo;
import com.wifun.admin.util.TimeUtils;
import com.wifun.inhand.model.Pvuv;
import com.wifun.inhand.request.statistic.PvuvResourceRequest;
import com.wifun.inhand.request.statistic.PvuvTypesRequest;
import com.wifun.inhand.request.statistic.QueryPvuvRequest;
import com.wifun.inhand.response.statistic.PvuvResource;
import com.wifun.inhand.response.statistic.PvuvResponse;
import com.wifun.inhand.response.statistic.ResourcesResponse;
import com.wifun.inhand.util.InhandHttpClient;

/**
 * PVUV接口服务类
 * 
 * @File wifun-admin/com.wifun.inhand.service.InhandPVUVService.java
 * @Desc
 */
public class InhandPVUVService {

	/**
	 * 分类型查询pvuv信息
	 * 
	 * @param token
	 * @param siteId
	 * @return
	 */
	public static List<PvuvResource> queryPvuvTypes(String token, String siteId) {

		ArrayList<PvuvResource> result = null;
		PvuvTypesRequest request = new PvuvTypesRequest();
		request.setToken(token);
		request.setSiteId(siteId);
		String rs = InhandHttpClient.getInstance().httpGet(request);

		if (StringUtils.isBlank(rs))
			return result;

		JSONObject jsObject = JSONObject.fromObject(rs);
		ResourcesResponse resp = new ResourcesResponse();
		resp.fromJsonObject(jsObject);
		result = resp.getRespResult();
		return result;
	}

	/**
	 * 获取指定栏目下的所有页面及概要信息
	 * 
	 * @param tid
	 * @param token
	 * @param siteId
	 * @return
	 */
	public static ArrayList<PvuvResource> queryPvuvResource(String tid, String token, String siteId) {

		ArrayList<PvuvResource> result = null;
		PvuvResourceRequest req = new PvuvResourceRequest();
		try {
			req.setToken(token);
			req.setTid(tid);
			req.setSort("total.pv");
			req.setDirection(0);
			req.setSite_id(siteId);

			String rs = InhandHttpClient.getInstance().httpGet(req);
			JSONObject jsObject = JSONObject.fromObject(rs);
			ResourcesResponse resp = new ResourcesResponse();
			resp.fromJsonObject(jsObject);
			result = resp.getRespResult();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * 根据companyId获取数据
	 * 
	 * @param token
	 * @param type
	 * @return
	 */
	public static ArrayList<PvuvMovieVo> queryPvuvResource(String token, String type) {

		ArrayList<PvuvMovieVo> result = new ArrayList<PvuvMovieVo>();
		try {

			PvuvResourceRequest req = new PvuvResourceRequest();
			req.setToken(token);
			req.setTid(type);
			req.setSort("total.pv");
			req.setDirection(0);

			String rs = InhandHttpClient.getInstance().httpGet(req);
			JSONObject jsObject = JSONObject.fromObject(rs);
			ResourcesResponse resp = new ResourcesResponse();
			resp.fromJsonObject(jsObject);
			ArrayList<PvuvResource> models = resp.getRespResult();
			result = getVosFromModelList(models);
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}

	/**
	 * 整理查询结果
	 * 
	 * @param result
	 * @return
	 */
	public static ArrayList<PvuvMovieVo> getVosFromModelList(List<PvuvResource> result) {

		ArrayList<PvuvMovieVo> vos = new ArrayList<PvuvMovieVo>();

		for (int k = 0; k < result.size(); k++) {

			PvuvResource model = result.get(k);
			PvuvMovieVo vo = new PvuvMovieVo();
			int currentPv = 0;
			int currentUv = 0;
			int lastPv = 0;
			int lastUv = 0;
			int totalPv = 0;
			int totalUv = 0;

			// 判断数据时间是否是当前时间
			String currDate = model.getCurrent().get("date");
			if (null != currDate) {
				if (currDate.equals(TimeUtils.getStringDateShort())) {
					currentPv = (null == model.getCurrent().get("pv")) ? 0 : Integer.parseInt(model
							.getCurrent().get("pv"));
					currentUv = (null == model.getCurrent().get("uv")) ? 0 : Integer.parseInt(model
							.getCurrent().get("uv"));
				} else if (currDate.equals(TimeUtils.dateToStr(TimeUtils.getYesterday()))) {
					// 如果当前时间是昨天，取今天的数据作为昨天的数据，
					lastPv = (null == model.getCurrent().get("pv")) ? 0 : Integer.parseInt(model
							.getCurrent().get("pv"));
					lastUv = (null == model.getCurrent().get("uv")) ? 0 : Integer.parseInt(model
							.getCurrent().get("uv"));
				}
			}

			vo.setCurrentPv(currentPv);
			vo.setCurrentUv(currentUv);

			String lastDate = model.getLast().get("date");
			if (null != lastDate) {
				if (lastDate.equals(TimeUtils.dateToStr(TimeUtils.getYesterday()))) {
					lastPv = (null == model.getLast().get("pv")) ? 0 : Integer.parseInt(model
							.getLast().get("pv"));
					lastUv = (null == model.getLast().get("uv")) ? 0 : Integer.parseInt(model
							.getLast().get("uv"));
				}
			}

			vo.setLastPv(lastPv);
			vo.setLastUv(lastUv);

			if (null != model.getTotal()) {
				totalPv = (null == model.getTotal().get("pv")) ? 0 : Integer.parseInt(model
						.getTotal().get("pv"));
				totalUv = (null == model.getTotal().get("uv")) ? 0 : Integer.parseInt(model
						.getTotal().get("uv"));
			}
			vo.setTotalPv(totalPv);
			vo.setTotalUv(totalUv);
			vo.setRId(model.getRId());
			vos.add(vo);
		}

		return vos;
	}

	public static List<Pvuv> queryPvuvDetail(String token, String tid, String rid, String siteId,
			long startTime, long endTime) {

		List<Pvuv> result = null;
		QueryPvuvRequest req = new QueryPvuvRequest();
		try {
			req.setToken(token);
			req.setTid(tid);
			req.setRid(rid);
			req.setStartTime(startTime);
			req.setEndTime(endTime);
			req.setSite_id(siteId);

			String rs = InhandHttpClient.getInstance().httpGet(req);
			JSONObject jsObject = JSONObject.fromObject(rs);
			PvuvResponse resp = new PvuvResponse();
			resp.fromJsonObject(jsObject);
			result = resp.getPvuvs();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}
}
