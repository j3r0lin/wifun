package com.wifun.inhand.service;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import com.wifun.inhand.request.statistic.QueryWifiStatRequest;
import com.wifun.inhand.response.statistic.WifiStatResponse;
import com.wifun.inhand.util.InhandHttpClient;

/**
 * Wi-Fi统计数据查询接口服务类
 * 
 * @File wifun-admin/com.wifun.inhand.service.InhandStatisticService.java
 * @Desc
 */
public class InhandStatisticService {

	/**
	 * Wi-Fi统计数据查询 统计类型，以','分隔开的数组，70: 每日新用户数，71: 每日新终端数， 50: 每日总用户数，51：每日总终端数，
	 * 307：停留时间超1小时的终端数
	 * 
	 * @param token
	 * @param types
	 * @param startTime
	 * @param endTime
	 * @param objectId
	 * @return
	 */
	public static HashMap<String, Map<String, Integer>> queryWifiStat(String token, String types,
			long startTime, long endTime, String objectId) {

		HashMap<String, Map<String, Integer>> result = new HashMap<String, Map<String, Integer>>();
		try {

			QueryWifiStatRequest request = new QueryWifiStatRequest();
			request.setToken(token);
			request.setTypes(types);
			request.setStartTime(startTime);
			request.setEndTime(endTime);
			request.setObjectId(objectId);

			String rs = InhandHttpClient.getInstance().httpGet(request);
			JSONObject jsObject = JSONObject.fromObject(rs);
			WifiStatResponse resp = new WifiStatResponse();
			resp.fromJsonObject(jsObject);

			result = resp.getResonseMap();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return result;
	}
}
