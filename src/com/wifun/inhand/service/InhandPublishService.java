package com.wifun.inhand.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.wifun.inhand.model.PortalPublish;
import com.wifun.inhand.model.PublishState;
import com.wifun.inhand.request.publish.PortalPublishRequest;
import com.wifun.inhand.request.publish.PublishPointRequest;
import com.wifun.inhand.request.publish.PublishStateRequest;
import com.wifun.inhand.response.publish.PublishStateResponse;
import com.wifun.inhand.util.InhandHttpClient;

/**
 * 资源发布接口服务类
 * 
 * @File wifun-admin/com.wifun.inhand.service.InhandPublishService.java
 * @Desc
 */
public class InhandPublishService {

	/**
	 * 资源发布
	 * 
	 * @param token
	 * @param pointId
	 * @param tagId
	 * @param fileUrl
	 * @param version
	 * @return
	 */
	public static String publish(String token, String pointId, String tagId, String fileUrl,
			String version) {

		PortalPublishRequest fpr = new PortalPublishRequest();
		fpr.setVersion(version);
		fpr.setToken(token);
		PortalPublish ppp = new PortalPublish();
		ppp.setPublishPointId(pointId);
		ppp.setUri(fileUrl);
		List<String> tags = new ArrayList<String>();
		tags.add(tagId);
		ppp.setTags(tags);
		fpr.setPublishPoint(ppp);

		String rs = InhandHttpClient.getInstance().httpPost(fpr);
		JSONObject jsObject = JSONObject.fromObject(rs);

		if (jsObject.containsKey("result") && jsObject.getJSONObject("result").containsKey("_id")) {
			return jsObject.getJSONObject("result").getString("_id");
		}

		return null;
	}

	/**
	 * 获取发布点
	 * 
	 * @param token
	 * @param pointName
	 * @return
	 */
	public static String getPublishPointId(String token, String pointName) {

		PublishPointRequest ppr = new PublishPointRequest();
		ppr.setToken(token);

		String rs = InhandHttpClient.getInstance().httpGet(ppr);
		if (StringUtils.isNotBlank(rs)) {
			JSONObject jsObject = JSONObject.fromObject(rs);
			if (jsObject.containsKey("result") && null != jsObject.getJSONArray("result")) {
				JSONArray pointList = jsObject.getJSONArray("result");
				for (int i = 0; i < pointList.size(); i++) {
					if (pointList.getJSONObject(i).containsKey("name")
							&& pointName.equals(pointList.getJSONObject(i).getString("name"))) {
						return pointList.getJSONObject(i).getString("_id");
					}
				}
			}
		}
		return null;
	}

	/**
	 * 查询资源发布状态
	 * 
	 * @param token
	 * @param publishId
	 * @return
	 */
	public static List<PublishState> queryPublishState(String token, String publishId) {

		if (null == publishId)
			return null;
		PublishStateRequest psr = new PublishStateRequest();
		psr.setToken(token);
		psr.setPublishId(publishId);

		String rs = InhandHttpClient.getInstance().httpGet(psr);
		if (StringUtils.isNotBlank(rs)) {
			JSONObject jsObject = JSONObject.fromObject(rs);

			PublishStateResponse psRes = new PublishStateResponse();
			psRes.fromJsonObject(jsObject);
			return psRes.getModelList();
		}
		return null;
	}
}
