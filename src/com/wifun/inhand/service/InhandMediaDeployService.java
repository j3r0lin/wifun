package com.wifun.inhand.service;

import net.sf.json.JSONObject;

import com.wifun.inhand.request.publish.MediaFileUploadRequest;
import com.wifun.inhand.util.InhandHttpClient;

/**
 * 媒体资源上传接口服务类
 * 
 * @File wifun-admin/com.wifun.inhand.service.InhandMediaDeployService.java
 * @Desc
 */
public class InhandMediaDeployService {

	/**
	 * 上传vlist
	 * 
	 * @param token
	 * @param filePath
	 * @return
	 */
	public static String vlistUpload(String token, String filePath) {

		MediaFileUploadRequest fur = new MediaFileUploadRequest();
		fur.setToken(token);
		fur.setFilePath(filePath);
		fur.setFileName("vlist");
		fur.setSuffix("");

		String rs = InhandHttpClient.getInstance().httpPost(fur);
		JSONObject jsObject = JSONObject.fromObject(rs);
		if (jsObject.containsKey("result"))
			return jsObject.getString("result");

		return rs;
	}
}
