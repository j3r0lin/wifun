package com.wifun.inhand.service;

import java.util.ArrayList;

import net.sf.json.JSONObject;

import org.apache.commons.lang.StringUtils;

import com.wifun.inhand.request.statistic.TrafficDailyRequest;
import com.wifun.inhand.request.statistic.TrafficMonthRequest;
import com.wifun.inhand.response.statistic.TrafficDailyResponse;
import com.wifun.inhand.response.statistic.TrafficMonthRespModel;
import com.wifun.inhand.response.statistic.TrafficMonthResponse;
import com.wifun.inhand.util.InhandHttpClient;

/**
 * 流量统计接口服务类
 * @Author xiafy
 * @Date 2015-9-25
 * @File wifun-admin/com.wifun.inhand.service.InhandTrafficService.java
 * @Desc
 */
public class InhandTrafficService {

	/**
	 * 日报流量（当天流量）统计
	 * 
	 * @param token
	 * @param inhandDeviceId
	 * @param month
	 * @return
	 */
	public static TrafficDailyResponse trafficDaily(String token, String inhandDeviceId,
			String month) {

		if (StringUtils.isBlank(inhandDeviceId))
			return null;

		TrafficDailyRequest req = new TrafficDailyRequest();
		req.setDevice_id(inhandDeviceId.toUpperCase());
		req.setMonth(month);
		req.setToken(token);

		String rs = InhandHttpClient.getInstance().httpGet(req);
		if (StringUtils.isBlank(rs))
			return null;

		JSONObject jsObject = JSONObject.fromObject(rs);
		TrafficDailyResponse resp = new TrafficDailyResponse();
		resp.fromJsonObject(jsObject);

		return resp;
	}

	/**
	 * 月报流量
	 * 
	 * @param token
	 * @param month
	 * @param resourceIds
	 * @return
	 */
	public static ArrayList<TrafficMonthRespModel> trafficMonth(String token, String month,
			String[] resourceIds) {

		ArrayList<TrafficMonthRespModel> result = new ArrayList<TrafficMonthRespModel>();
		TrafficMonthRequest req = new TrafficMonthRequest();
		req.setMonth(month);
		req.setToken(token);
		req.setResourceIds(resourceIds);

		String rs = InhandHttpClient.getInstance().httpPost(req);
		if (StringUtils.isBlank(rs))
			return result;

		JSONObject jsObject = JSONObject.fromObject(rs);
		TrafficMonthResponse resp = new TrafficMonthResponse();
		resp.fromJsonObject(jsObject);
		result = resp.getRespResult();

		return result;
	}
}
