package com.wifun.inhand.util;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

import org.apache.http.entity.mime.content.FileBody;

public class FileUtil {

	public static byte[] toByteArray(FileBody file) {

		if (null == file)
			return null;
		try {
			ByteArrayOutputStream output = new ByteArrayOutputStream();
			byte[] buffer = new byte[4096];
			int n = 0;
			InputStream input = file.getInputStream();
			while (-1 != (n = input.read(buffer))) {
				output.write(buffer, 0, n);
			}
			return output.toByteArray();
		} catch (Exception e) {
			return null;
		}

	}
}
