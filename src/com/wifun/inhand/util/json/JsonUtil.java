package com.wifun.inhand.util.json;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.wifun.inhand.util.EmptyUtil;


/**
 * 
 * @ClassName: JsonUtil
 * @Description: JSON工具�?
 * @author wanghc
 */
@SuppressWarnings( { "unused", "unchecked" })
public final class JsonUtil {
	/**
	 * 用正则格式化Json数据
	 * 
	 * @param json
	 *            Json
	 * @return 格式化后的数�?
	 */
	public static String baseFormatJson(String json) {
		if (EmptyUtil.isEmpty(json))
			return json;
		json = json.replaceAll("\\\\\"", "&quot;");
		Pattern pattern = Pattern.compile(":([.[^\"]]+?)[,\\}\\]]");
		Matcher matcher = pattern.matcher(json);
		while (matcher.find()) {
			String temp = matcher.group(1);
			if (EmptyUtil.isEmpty(temp))
				continue;
			json = json.replace(temp, "\"" + temp.replace("\"", "") + "\"");
		}
		return json;
	}

	/**
	 * 转换成对象时�?查并格式化Json数据
	 * 
	 * @param json
	 * @return
	 */
	public static String objFormatJson(String json) {
		if (EmptyUtil.isEmpty(json))
			return null;
		try {
			JSONObject jsObject = JSONObject.fromObject(json);
			return jsObject.toString();
		} catch (Exception e) {
			return baseFormatJson(json);
		}
	}

	/**
	 * 数据或集合时�?查并格式化Json数据
	 * 
	 * @param json
	 * @return
	 */
	public static String arrayFormatJson(String json) {
		if (EmptyUtil.isEmpty(json))
			return null;
		try {
			JSONArray array = JSONArray.fromObject(json);
			return array.toString();
		} catch (Exception e) {
			return baseFormatJson(json);
		}
	}

	/**
	 * 对Map的空值进行过�?
	 * 
	 * @param map
	 *            Map对象
	 * @return 过滤后的集合
	 */
	public static Map<String, Object> filter(Map<String, Object> map) {
		if (map == null || map.size() == 0)
			return null;
		Iterator<Entry<String, Object>> iterator = map.entrySet().iterator();
		Entry<String, Object> entry;
		Map<String, Object> target = new HashMap<String, Object>();
		while (iterator.hasNext()) {
			entry = iterator.next();
			if (!EmptyUtil.isEmpty(entry.getValue())) {
				if (!"null".equals(entry.getValue().toString().trim()))
					target.put(entry.getKey().trim(), entry.getValue());
			}
		}
		map.clear();
		map = null;
		return target;
	}
	

	/**
	 * 将JSONObject转移对java对象，转换属性仅限Java对象可访问属性 
	 * zhaohc 20150511
	 * @param clazz  类的属性
	 * @param jsonObject 
	 * @return
	 */
	public static <T> T fromJsonObject(Class clazz, JSONObject jsonObject)
	{
		if (clazz == null || jsonObject == null)
		{
			return null;
		}
		
		//得到类对象
		T bean = null;
	    try
	    {
	    	bean = (T)clazz.newInstance();
		    /*
		     * 得到类中的所有属性集合
		     */
		    Field[] fields = clazz.getFields();
		    
		    for(Field f : fields ){
		    	
		        f.setAccessible(true); //设置些属性是可以访问的
		        
		        String type = f.getType().toString();//得到此属性的类型
		        //System.out.println(f.getName()+"\t是" + f.getType());
		        if ("serialVersionUID".equals(f.getName()))
		        {
		        	continue;
		        }
		        
		        if (!jsonObject.containsKey(f.getName()))
		        {
		        	continue;
		        }
		        
		        if (type.endsWith("String")) {
		           f.set(bean, jsonObject.getString(f.getName())) ;        //给属性设值
		        }else if(type.endsWith("int") || type.endsWith("Integer")){
		           f.set(bean, jsonObject.getInt(f.getName())) ;       //给属性设值
		        }
		        else if(type.endsWith("Long") ){
			           f.set(bean, jsonObject.getLong(f.getName())) ;       //给属性设值
			    }
		        else{
		        	f.set(bean, null);
		        }
		    }
	    }
	    catch(Exception e)
	    {
	    	e.printStackTrace();
	    	return null;
	    }
		return bean;
	}
	

}
