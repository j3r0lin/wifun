package com.wifun.inhand.util.json;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

public class JsonResultBuilder {

	public static final String LOGIN = "login";
	public static final String CODE = "code";
	public static final String ERROR = "error";
	public static final int CODE_TOKEN_ERROR = 99;
	public static final int CODE_OK = 200;
	public static final int CODE_PARSE_ERROR = 401;
	public static final int CODE_VALIDATE_ERROR = 402;
	public static final int CODE_LOGIN_ERROR = 405;
	public static final int CODE_SESSION_DATED_ERROR = 406;
	public static final int CODE_SYSTEM_ERROR = 500;

	private JSONObject jsonResult;

	public JsonResultBuilder() {

		jsonResult = new JSONObject();
		jsonResult.put(CODE, CODE_OK);
	}

	public JsonResultBuilder setCode(int code) {

		jsonResult.put(CODE, code);
		return this;
	}

	public JsonResultBuilder setError(String error) {

		jsonResult.put(ERROR, error);
		return this;
	}

	public JsonResultBuilder setLogin(boolean login) {

		jsonResult.put(LOGIN, login);
		return this;
	}

	public JsonResultBuilder putResult(String key, Object result) {

		if (result == null) {
			return this;
		}
		if (result instanceof String || result instanceof Integer || result instanceof Float
				|| result instanceof Short || result instanceof Long || result instanceof Double
				|| result instanceof Boolean || result instanceof Byte
				|| result instanceof Character || result instanceof BigDecimal) {
			jsonResult.put(key, result);
		} else {
			if (result instanceof Collection || result.getClass().isArray()) {
				JSONArray json = JSONArray.fromObject(result, configJson(null, "MM/dd/yyyy"));
				jsonResult.put(key, json);
			} else {
				JSONObject json = JSONObject.fromObject(result, configJson(null, "MM/dd/yyyy"));
				jsonResult.put(key, json);
			}
		}
		// jsonResult.put(key, result);
		return this;
	}

	public JSONObject getResult() {

		return jsonResult;
	}

	/**
	 * 配置json-lib需要的excludes和datePattern.
	 * 
	 * @param excludes
	 *            不需要转换的属性数组
	 * @param datePattern
	 *            日期转换模式
	 * @return JsonConfig 根据excludes和dataPattern生成的jsonConfig，用于write
	 */
	public static JsonConfig configJson(String[] excludes, String datePattern) {

		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(excludes);
		jsonConfig.setIgnoreDefaultExcludes(false);
		jsonConfig.setAllowNonStringKeys(true);  
		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor(datePattern));

		return jsonConfig;
	}

}
