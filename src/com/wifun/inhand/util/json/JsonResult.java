package com.wifun.inhand.util.json;

import net.sf.json.JSONObject;

@SuppressWarnings("serial")
public class JsonResult implements java.io.Serializable {
	private String type = null;
	private Integer errorCode = null;
	private String error = null;
	private Object result = null;
	
	@Override
	public String toString() {
		return JSONObject.fromObject(this,JsonResultBuilder.configJson(null,  "MM/dd/yyyy")).toString();
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Integer getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(Integer errorCode) {
		this.errorCode = errorCode;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	public Object getResult() {
		return result;
	}

	public void setResult(Object result) {
		this.result = result;
	}
	
}
