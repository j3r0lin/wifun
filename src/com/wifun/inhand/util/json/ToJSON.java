package com.wifun.inhand.util.json;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ToJSON {
	String name() default "";
	boolean serialize() default true;
	boolean deserialize() default false;
	String format() default "";
}
