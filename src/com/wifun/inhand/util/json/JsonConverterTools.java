package com.wifun.inhand.util.json;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.sf.json.JSON;
import net.sf.json.JSONArray;
import net.sf.json.JSONNull;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import net.sf.json.JsonConfig;
import net.sf.json.processors.DefaultValueProcessor;
import net.sf.json.processors.PropertyNameProcessor;
import net.sf.json.util.CycleDetectionStrategy;
import net.sf.json.util.PropertyFilter;

import com.opensymphony.xwork2.conversion.impl.DefaultTypeConverter;
import com.wifun.inhand.util.EmptyUtil;

/**
 * 
 * @ClassName: JsonConverterTools
 * @Description: Json数据转换工具�?
 * @author wanghc
 */
@SuppressWarnings("unchecked")
public class JsonConverterTools {

	/**
	 * 对象到Json,对象间用-线分�?
	 * 
	 * @param obj
	 * @param objName
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unused")
	private static String objToJson(Object obj, String objName)
			throws Exception {
		if (obj == null)
			return "";
		Field[] fields = obj.getClass().getDeclaredFields();
		StringBuffer sb = new StringBuffer();
		Map<String, String> map = ReflectionUtil
				.getClassMethods(obj.getClass());
		for (Field field : fields) {
			if (!ReflectionUtil.isMethodExistsByField(map, field.getName())) {
				continue;
			}
			PropertyDescriptor descriptor = new PropertyDescriptor(field
					.getName(), obj.getClass());
			if (ReflectionUtil.isPrimitive(field.getType())
					|| field.getType() == Date.class) {
				sb.append("\"");
				String prefix = (objName != null && objName.length() > 0) ? objName
						+ "-" + field.getName()
						: field.getName();
				sb.append(prefix);
				sb.append("\":\"").append(
						descriptor.getReadMethod().invoke(obj)).append("\",");
			} else if (ReflectionUtil.isCollectionOrInterface(field.getType())) {
				continue;
			} else {
				String prefix = (objName != null && objName.length() > 0) ? objName
						+ "-" + field.getName()
						: field.getName();
				sb.append(objToJson(descriptor.getReadMethod().invoke(obj),
						prefix));

			}
		}
		return sb.toString();
	}

	/**
	 * 将对象转换成Json格式数据
	 * 
	 * @param <POJO>
	 *            对象
	 * @param pojo
	 *            对象
	 * @return Json格式数据
	 */
	public static String toJsonString(Class clazz, Object obj,
			final String... paras) {
		return toJsonString(clazz, obj, true, paras);
	}

	/**
	 * 将对象转换成Json格式数据
	 * 
	 * @param <POJO>
	 *            对象
	 * @param pojo
	 *            对象
	 * @return Json格式数据
	 */
	public static String toJsonString(Object obj, final String... paras) {
		return toJsonString(obj, true, paras);
	}

	/**
	 * Java对象转换成Json格式
	 * 
	 * @param obj
	 * @param isShowNull
	 * @param paras
	 * @return
	 */
	public static String toJsonString(Class clazz, Object obj,
			boolean isShowNull, final String... paras) {
		return toJsonDateString(clazz, obj, isShowNull, null, paras);
	}

	/**
	 * Java对象转换成Json格式
	 * 
	 * @param obj
	 * @param isShowNull
	 * @param paras
	 * @return
	 */
	public static String toJsonString(Object obj, boolean isShowNull,
			final String... paras) {
		return toJsonDateString(null, obj, isShowNull, null, paras);
	}

	/**
	 * 对象转换成Json时是否显示空�?
	 * 
	 * @param obj
	 * @param isShowNull
	 * @param dateFormat
	 * @param paras
	 * @return
	 */
	public static String toJsonDateString(Class clazz, Object obj,
			boolean isShowNull, String dateFormat, final String... paras) {
		if (EmptyUtil.isEmpty(obj))
			return "{}";
		if (obj instanceof String) {
			return JsonUtil.baseFormatJson(obj.toString());
		}
		JSON json = JSONSerializer.toJSON(obj, configJson(clazz, dateFormat,
				isShowNull, paras));
		if (json.isEmpty())
			return "{}";
		return json.toString();
	}
	
	/**
	 * 将字符串转换成集�?
	 * 
	 * @param <POJO>
	 * @param jsonString
	 *            Json格式字符�?
	 * @param clazz
	 *            类型
	 * @return 对象
	 */
	public static <POJO> List<POJO> toList(String jsonString, Class clazz,
			String... dateFOrmat) {
		if (EmptyUtil.isEmpty(jsonString))
			return null;
		setDataFormat2JAVA(dateFOrmat);
		return (List<POJO>) JSONArray.toCollection(JSONArray
				.fromObject(JsonUtil.arrayFormatJson(jsonString)), clazz);
	}

	/**
	 * 将Json格式转换成对�?
	 * 
	 * @param <POJO>
	 *            对象
	 * @param jsonString
	 *            Json
	 * @param clazz
	 *            类型
	 * @return 对象
	 */

	public static <POJO> POJO toPOJO(String jsonString, Class clazz,
			String... dateFOrmat) {
		if (EmptyUtil.isEmpty(jsonString))
			return null;
		setDataFormat2JAVA(dateFOrmat);
		return (POJO) JSONObject.toBean(JSONObject.fromObject(JsonUtil
				.objFormatJson(jsonString)), clazz);
	}

	/**
	 * 将Json转换成对�?(参数没有进行格式化处�?)
	 * 
	 * @param <POJO>
	 *            对象
	 * @param clazz
	 *            类型
	 * @param jsonString
	 *            Json数据
	 * @return 结果
	 */
	public static Map<String, Object> toMap(String jsonString, boolean showNull) {
		if (EmptyUtil.isEmpty(jsonString))
			return null;
		JSONObject jsonObject = JSONObject.fromObject(JsonUtil
				.objFormatJson(jsonString));
		Map map = new HashMap();
		for (Iterator iter = jsonObject.keys(); iter.hasNext();) {
			String key = (String) iter.next();
			map.put(key, jsonObject.get(key));
		}
		return showNull ? map : JsonUtil.filter(map);
	}

	/**
	 * Json转换成Map
	 * @param jsonString json字符�?
	 * @param showNull 显示空�??
	 * @param types 类型
	 * @return 
	 */
	public static Map<String, Object> toMap(String jsonString,
			boolean showNull, Map<String, Object> types) {
		if (EmptyUtil.isEmpty(jsonString))
			return null;
		JSONObject jsonObject = JSONObject.fromObject(JsonUtil
				.objFormatJson(jsonString));
		Map<String, Object> map = new HashMap<String, Object>();
		if (types == null || types.size() == 0) {
			for (Iterator iter = jsonObject.keys(); iter.hasNext();) {
				String key = (String) iter.next();
				map.put(key, jsonObject.get(key));
			}
		} else {
			for (Iterator iter = jsonObject.keys(); iter.hasNext();) {
				String key = (String) iter.next();
				if (types.get(key) != null) {
					JsonDateConverter converter = new JsonDateConverter();
					Object type = types.get(key);
					Class clazz = null;
					if (type instanceof Class)
						clazz = (Class) type;
					else {
						clazz = type.getClass();
					}
					map.put(key, converter.convertValue(jsonObject.get(key)
							.toString().trim(), clazz));
				} else {
					map.put(key, jsonObject.get(key));
				}
			}
		}
		return showNull ? map : JsonUtil.filter(map);
	}

	

	/**
	 * 按日期格式将对象转换成Json格式数据
	 * 
	 * @param obj
	 *            对象
	 * @param dateFormat
	 *            日期格式化字符串
	 * @param paras
	 * @return
	 */
	public static String toJsonDateString(Class clazz, Object obj,
			String dateFormat, final String... paras) {
		return toJsonDateString(clazz, obj, true, dateFormat, paras);
	}


	/**
	 * 配置json-lib�?要的excludes和datePattern.
	 * 
	 * @param excludes
	 *            不需要转换的属�?�数�?
	 * @param isShowNull
	 *            是否显示空�??
	 * @param datePattern
	 *            日期转换模式
	 * @return JsonConfig 根据excludes和dataPattern生成的jsonConfig，用于write
	 */
	private static JsonConfig configJson(Class clazz, String datePattern,
			final boolean isShowNull, String... excludes) {
		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setIgnoreDefaultExcludes(false);
		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		jsonConfig.registerJsonValueProcessor(Date.class,
				new DateJsonValueProcessor(datePattern));
		jsonConfig.registerDefaultValueProcessor(Date.class,
				new DefaultValueProcessor() {
					public Object getDefaultValue(Class type) {
						return JSONNull.getInstance();
					}
				});
		jsonConfig.registerDefaultValueProcessor(Long.class,
				new DefaultValueProcessor() {
					public Object getDefaultValue(Class type) {
						return JSONNull.getInstance();
					}
				});
		jsonConfig.registerDefaultValueProcessor(Integer.class,
				new DefaultValueProcessor() {
					public Object getDefaultValue(Class type) {
						return JSONNull.getInstance();
					}
				});
		jsonConfig.registerDefaultValueProcessor(Float.class,
				new DefaultValueProcessor() {
					public Object getDefaultValue(Class type) {
						return JSONNull.getInstance();
					}
				});
		jsonConfig.registerDefaultValueProcessor(Double.class,
				new DefaultValueProcessor() {
					public Object getDefaultValue(Class type) {
						return JSONNull.getInstance();
					}
				});
		jsonConfig.setExcludes(excludes);
		jsonConfig.setJsonPropertyFilter(new PropertyFilter() {
			public boolean apply(Object source, String name, Object value) {
				if (value == null && !isShowNull) {
					return true;
				}
				if (source instanceof Map<?, ?> || source instanceof List<?>
						|| source instanceof Set<?> || source instanceof String)
					return false;
				ToJSON toJSON = null;
				try {
					toJSON = getFieldName(source.getClass(), name);
				} catch (Exception e) {
					e.printStackTrace();
				}
				return toJSON == null ? false : toJSON.deserialize();
			}
		});
		if (clazz != null) {
			jsonConfig.registerJsonPropertyNameProcessor(clazz,
					new PropertyNameProcessor() {
						public String processPropertyName(Class cla, String name) {
							try {
								ToJSON toJSON = getFieldName(cla, name);
								return toJSON == null ? null : toJSON.name();
							} catch (Exception e) {
								e.printStackTrace();
							}
							return null;
						}

					});
		}
		return jsonConfig;
	}




	/**
	 * 从一个JSON 对象字符格式中得到一个java对象，其中beansList是一类的集合，形如： {"id" : idValue, "name" :
	 * nameValue, "aBean" : {"aBeanId" : aBeanIdValue, ...}, beansList:[{}, {},
	 * ...]}
	 * 
	 * @param jsonString
	 * @param clazz
	 * @param map
	 *            集合属�?�的类型 (key : 集合属�?�名, value : 集合属�?�类型class) eg: ("beansList" :
	 *            Bean.class)
	 * @return
	 */
	public static Object getDTO(String jsonString, Class clazz, Map map) {
		if (EmptyUtil.isEmpty(jsonString))
			return null;
		JSONObject jsonObject = null;
		try {
			setDataFormat2JAVA();
			jsonObject = JSONObject.fromObject(JsonUtil
					.objFormatJson(jsonString));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return JSONObject.toBean(jsonObject, clazz, map);
	}

	/**
	 * 从一个JSON数组得到�?个java对象数组，形如： [{"id" : idValue, "name" : nameValue}, {"id" :
	 * idValue, "name" : nameValue}, ...]
	 * 
	 * @param object
	 * @param clazz
	 * @return
	 */
	public static Object[] getDTOArray(String jsonString, Class clazz) {
		if (EmptyUtil.isEmpty(jsonString))
			return null;
		JSONArray array = JSONArray.fromObject(JsonUtil
				.arrayFormatJson(jsonString));
		Object[] obj = new Object[array.size()];
		for (int i = 0; i < array.size(); i++) {
			JSONObject jsonObject = array.getJSONObject(i);
			obj[i] = JSONObject.toBean(jsonObject, clazz);
		}
		return obj;
	}

	/**
	 * 从一个JSON数组得到�?个java对象数组，形如： [{"id" : idValue, "name" : nameValue}, {"id" :
	 * idValue, "name" : nameValue}, ...]
	 * 
	 * @param object
	 * @param clazz
	 * @param map
	 * @return
	 */
	public static Object[] getDTOArray(String jsonString, Class clazz, Map map) {
		if (EmptyUtil.isEmpty(jsonString))
			return null;
		JSONArray array = JSONArray.fromObject(JsonUtil
				.arrayFormatJson(jsonString));
		Object[] obj = new Object[array.size()];
		for (int i = 0; i < array.size(); i++) {
			JSONObject jsonObject = array.getJSONObject(i);
			obj[i] = JSONObject.toBean(jsonObject, clazz, map);
		}
		return obj;
	}

	/**
	 * 从一个JSON数组得到�?个java对象集合
	 * 
	 * @param object
	 * @param clazz
	 * @return
	 */
	public static <T extends Object> List<T> getDTOList(String jsonString,
			Class<T> clazz) {
		if (EmptyUtil.isEmpty(jsonString))
			return null;
		JSONArray array = JSONArray.fromObject(JsonUtil
				.arrayFormatJson(jsonString));
		List<T> list = new ArrayList<T>();
		for (Iterator iter = array.iterator(); iter.hasNext();) {
			JSONObject jsonObject = (JSONObject) iter.next();
			list.add((T) JSONObject.toBean(jsonObject, clazz));
		}
		return list;
	}

	/**
	 * 根据Json格式得到对象集合，支持对象包含对�?(对象属�?�用-分隔)
	 * 
	 * @param <POJO>
	 * @param jsonString
	 *            Json数据
	 * @param clazz
	 *            类型
	 * @return
	 * @throws Exception
	 */
	public static <POJO extends Object> List<POJO> getPOJOListByJson(
			String jsonString, Class<POJO> clazz) throws Exception {
		if (EmptyUtil.isEmpty(jsonString))
			return null;
		JSONArray array = JSONArray.fromObject(JsonUtil
				.arrayFormatJson(jsonString));
		List<POJO> list = new ArrayList<POJO>();
		Map<String, String> map = ReflectionUtil.getClassMethods(clazz);
		DefaultTypeConverter deConverter = new JsonDateConverter();
		for (Iterator iter = array.iterator(); iter.hasNext();) {
			JSONObject jsonObject = (JSONObject) iter.next();
			Iterator it = jsonObject.keys();
			POJO pojo = (POJO) clazz.newInstance();
			Object objTemp = null;
			while (it.hasNext()) {
				String keyStr = it.next().toString();
				String[] arrStr = keyStr.split("-");
				if (arrStr.length > 1) {
					if (ReflectionUtil.isMethodExistsByField(map, arrStr[0])) {
						PropertyDescriptor subDescriptor = new PropertyDescriptor(
								arrStr[0], pojo.getClass());
						if (!ReflectionUtil.isPrimitive(subDescriptor
								.getReadMethod().getReturnType())
								&& !ReflectionUtil
										.isCollectionOrInterface(subDescriptor
												.getReadMethod()
												.getReturnType())) {
							if (objTemp == null
									|| objTemp.getClass() != subDescriptor
											.getReadMethod().getReturnType()) {
								objTemp = subDescriptor.getReadMethod()
										.getReturnType().newInstance();
							}
							PropertyDescriptor desc = new PropertyDescriptor(
									arrStr[1], objTemp.getClass());
							if (EmptyUtil.isEmpty(jsonObject.get(keyStr))) {
								continue;
							}
							desc.getWriteMethod().invoke(
									objTemp,
									deConverter.convertValue(jsonObject
											.get(keyStr), desc.getReadMethod()
											.getReturnType()));
							subDescriptor.getWriteMethod().invoke(pojo,
									new Object[] { objTemp });
						}
					}
				} else {
					if (ReflectionUtil.isMethodExistsByField(map, keyStr)) {
						PropertyDescriptor desc = new PropertyDescriptor(
								keyStr, pojo.getClass());
						objTemp = jsonObject.get(keyStr);
						if (EmptyUtil.isEmpty(objTemp)) {
							continue;
						}
						desc.getWriteMethod().invoke(
								pojo,
								deConverter.convertValue(
										jsonObject.get(keyStr), desc
												.getReadMethod()
												.getReturnType()));
					}
				}
			}
			list.add(pojo);
		}
		return list;

	}

	/**
	 * 从一个JSON数组得到�?个java对象集合，其中对象中包含有集合属�?
	 * 
	 * @param object
	 * @param clazz
	 * @param map
	 *            集合属�?�的类型
	 * @return
	 */
	public static List getDTOList(String jsonString, Class clazz, Map map) {
		if (EmptyUtil.isEmpty(jsonString))
			return null;
		JSONArray array = JSONArray.fromObject(JsonUtil
				.arrayFormatJson(jsonString));
		List list = new ArrayList();
		for (Iterator iter = array.iterator(); iter.hasNext();) {
			JSONObject jsonObject = (JSONObject) iter.next();
			list.add(JSONObject.toBean(jsonObject, clazz, map));
		}
		return list;
	}

	/**
	 * 从json数组中得到相应java数组 json形如：["123", "456"]
	 * 
	 * @param jsonString
	 * @return
	 */
	public static Object[] getObjectArrayFromJson(String jsonString) {
		if (EmptyUtil.isEmpty(jsonString))
			return null;
		JSONArray jsonArray = JSONArray.fromObject(JsonUtil
				.arrayFormatJson(jsonString));
		return jsonArray.toArray();
	}

	/**
	 * 设置日期转换
	 * 
	 * @param formats
	 *            格式
	 */
	private static void setDataFormat2JAVA(String... formats) {
//		if (formats == null || formats.length == 0) {
//			JSONUtils.getMorpherRegistry().registerMorpher(
//					new DateMorpher(new String[] { "yyyy-MM-dd HH:mm:ss",
//							"yyyy-MM-dd", "yyyy年MM月dd�? HH:mm:ss",
//							"yyyy年MM月dd�? HH时mm分ss�?" }));
//		} else {
//			JSONUtils.getMorpherRegistry().registerMorpher(
//					new DateMorpher(formats));
//		}
	}
	/**
	 * 根据类型和属性名称得到注�?
	 * 
	 * @param clz
	 *            类型
	 * @param name
	 *            属�?�名�?
	 * @return 注解的名�?
	 * @throws Exception
	 */
	private static ToJSON getFieldName(Class clz, String name) throws Exception {
		Method method = ReflectionUtil.methodByFieldName(clz, name, true);
		if (method != null && method.isAnnotationPresent(ToJSON.class)) {
			ToJSON anno = method.getAnnotation(ToJSON.class);
			return anno;
		}
		return null;
	}
}
