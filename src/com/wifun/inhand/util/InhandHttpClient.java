package com.wifun.inhand.util;

import java.io.IOException;
//import java.io.InputStream;
import java.io.UnsupportedEncodingException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

import com.wifun.inhand.request.IInhandRequest;

/**
 * HTTP客户端请求类
 * 
 * @File wifun-admin/com.wifun.inhand.util.InhandHttpClient.java
 * @Desc
 */
public class InhandHttpClient {

	public DefaultHttpClient httpclient = new DefaultHttpClient();

	private static final Logger logger = Logger.getLogger("inhand.api.log");

	public static void main(String[] argus) {

	}

	private InhandHttpClient() {

	}

	public static InhandHttpClient getInstance() {

		return new InhandHttpClient();
	}

	/**
	 * GET请求
	 * 
	 * @param reqObj
	 * @return
	 */
	public String httpGet(IInhandRequest reqObj) {

		HttpGet httpGet = null;
		try {
			String queryStr = reqObj.getQueryStr();
			String url = reqObj.getRequestUri() + "?" + queryStr;

			logger.info("GET Request : " + url);
			System.out.println(url);

			httpGet = new HttpGet(url);

			HttpResponse response = httpclient.execute(httpGet);
			HttpEntity entity = response.getEntity();

			String respStr = EntityUtils.toString(entity);
			logger.info("GET Response : " + respStr);

			EntityUtils.consume(entity);
			return respStr;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			logger.error("GET ERROR!", e);
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("GET ERROR!", e);
			return null;
		} finally {
			if (null != httpGet) {
				httpGet.releaseConnection();
			}
			if (null != httpclient) {
				httpclient.getConnectionManager().shutdown();
			}
		}
	}

	/**
	 * POST请求
	 * 
	 * @param reqObj
	 * @return
	 */
	public String httpPost(IInhandRequest reqObj) {

		HttpPost httpPost = null;
		try {
			String queryStr = reqObj.getQueryStr();
			String url = reqObj.getRequestUri() + "?" + queryStr;

			logger.info("POST Request : " + url);
			System.out.println(url);

			httpPost = new HttpPost(url);
			httpPost.setHeader("Content-Type", reqObj.getContentType());

			// 数据体参数
			httpPost.setEntity(reqObj.getRequestEntity());

			HttpResponse response = httpclient.execute(httpPost);

			HttpEntity entity = response.getEntity();
			String respStr = EntityUtils.toString(entity);
			logger.info("POST Response : " + respStr);

			EntityUtils.consume(entity);
			return respStr;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			logger.error("POST ERROR!", e);
			return null;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			logger.error("POST ERROR!", e);
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("POST ERROR!", e);
			return null;
		} finally {
			if (null != httpPost) {
				httpPost.releaseConnection();
			}
			if (null != httpclient) {
				httpclient.getConnectionManager().shutdown();
			}
		}
	}

	/**
	 * PUT请求
	 * 
	 * @param reqObj
	 * @return
	 */
	public String httpPut(IInhandRequest reqObj) {

		HttpPut httpPut = null;
		try {
			String queryStr = reqObj.getQueryStr();
			String url = reqObj.getRequestUri() + "?" + queryStr;

			logger.info("PUT Request : " + url);
			System.out.println(url);

			httpPut = new HttpPut(url);
			httpPut.setHeader("Content-Type", reqObj.getContentType());

			httpPut.setEntity(reqObj.getRequestEntity());

			HttpResponse response = httpclient.execute(httpPut);

			HttpEntity entity = response.getEntity();

			String respStr = EntityUtils.toString(entity);
			logger.info("PUT Response : " + respStr);

			// 释放entity资源
			EntityUtils.consume(entity);
			return respStr;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			logger.error("PUT ERROR!", e);
			return null;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			logger.error("PUT ERROR!", e);
			return null;
		} catch (IOException e) {
			e.printStackTrace();
			logger.error("PUT ERROR!", e);
			return null;
		} finally {
			if (null != httpPut) {
				httpPut.releaseConnection();
			}
			if (null != httpclient) {
				httpclient.getConnectionManager().shutdown();
			}
		}
	}

	/**
	 * DELETE请求
	 * 
	 * @param reqObj
	 * @return
	 */
	public String httpDelete(IInhandRequest reqObj) {

		HttpDelete httpDelete = null;
		try {
			String queryStr = reqObj.getQueryStr();
			String url = reqObj.getRequestUri() + "?" + queryStr;

			logger.info("DELETE Request : " + url);
			System.out.println(url);

			httpDelete = new HttpDelete(url);
			HttpResponse response = httpclient.execute(httpDelete);

			HttpEntity entity = response.getEntity();
			String respStr = EntityUtils.toString(entity);
			logger.info("DELETE Response : " + respStr);

			EntityUtils.consume(entity);
			return respStr;
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			logger.error("DELETE ERROR!", e);
			return null;
		} catch (ClientProtocolException e) {
			e.printStackTrace();
			logger.error("DELETE ERROR!", e);
			return null;
		} catch (IOException e) {
			logger.error("DELETE ERROR!", e);
			e.printStackTrace();
			return null;
		} finally {
			if (null != httpDelete) {
				httpDelete.releaseConnection();
			}
			if (null != httpclient) {
				httpclient.getConnectionManager().shutdown();
			}
		}
	}
}
