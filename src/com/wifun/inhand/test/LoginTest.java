package com.wifun.inhand.test;

import net.sf.json.JSONObject;

import com.wifun.inhand.request.login.LoginRequest;
import com.wifun.inhand.response.login.LoginResponse;
import com.wifun.inhand.util.InhandHttpClient;

public class LoginTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		LoginRequest lr = new LoginRequest();
		lr.setClientId("000017953450251798098136");
		lr.setClientSecret("08E9EC6793345759456CB8BAE52615F3");
		lr.setUserName("admin");   ///  admin
		lr.setPassword("admin");   //admin
		String rs = InhandHttpClient.getInstance().httpPost(lr);
		System.out.println(rs);
		if (null != rs)
		{
			JSONObject jsObject = JSONObject.fromObject(rs);
		    
			LoginResponse login = new LoginResponse();
			login.fromJsonObject(jsObject);
			if (null != login)
			{
				System.out.println("error=" + login.getError());
			}
		}
	}
 
}
