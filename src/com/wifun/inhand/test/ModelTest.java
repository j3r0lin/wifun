package com.wifun.inhand.test;

import java.util.List;

import net.sf.json.JSONObject;

import com.wifun.inhand.common.Constants;
import com.wifun.inhand.model.Model;
import com.wifun.inhand.request.model.ModelRequest;
import com.wifun.inhand.response.model.ModelResponse;
import com.wifun.inhand.util.InhandHttpClient;

public class ModelTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		queryModelId( "IP3012L");
	}
	
	public static String queryModelId( String model)
	{
		ModelRequest mr = new ModelRequest();
		mr.setToken(Constants.token);
		mr.setName(model);
		
		String rs = InhandHttpClient.getInstance().httpGet( mr);
		System.out.println("responseStr="+rs);
		if (null != rs)
		{
			JSONObject jsObject = JSONObject.fromObject(rs);
		    
			ModelResponse modelRes = new ModelResponse();
			modelRes.fromJsonObject(jsObject);
			List<Model> mlist = modelRes.getModelList();
			if (null != mlist && mlist.size() > 0)
			{
				System.out.println("modelId=" + mlist.get(0).get_id());
				return mlist.get(0).get_id();
			}
		}
		return null;
	}

}
