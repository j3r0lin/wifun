package com.wifun.inhand.test;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.wifun.inhand.model.Tag;
import com.wifun.inhand.request.tag.CreateTagRequest;
import com.wifun.inhand.request.tag.DeleteTagRequest;
import com.wifun.inhand.request.tag.QueryTagRequest;
import com.wifun.inhand.response.tag.TagResponse;
import com.wifun.inhand.util.InhandHttpClient;

public class TagTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		clearTags("185748e3bfba60960b716ea91272dd1a");
	}
	
	public static void clearTags(String token)
	{
		List<String > tagList = queryList(token);
		if (null != tagList && tagList.size() > 0)
		{
			for (int i = 0; i < tagList.size(); i++)
			{
				deleteTag(token, tagList.get(i));
				
			}
		}
	}
	
	public static List<String> queryList(String token)
	{
		QueryTagRequest mr = new QueryTagRequest();
		mr.setToken(token);
		
		String rs = InhandHttpClient.getInstance().httpGet( mr);
		
		if (null != rs)
		{
			JSONObject jsObject = JSONObject.fromObject(rs);

			if (null != jsObject && jsObject.containsKey("result") 
					&& jsObject.getJSONArray("result").size() > 0)
			{
				JSONArray jsl = jsObject.getJSONArray("result");
				List<String> devList = new ArrayList<String>();
				for (int i = 0; i < jsl.size(); i++ )
				{
					if (jsl.getJSONObject(i).containsKey("_id"))
						devList.add(jsl.getJSONObject(i).getString("_id"));
				}
				return devList;
			}
		}
		return null;
	}

	public static void addTag()
	{
		CreateTagRequest ctr = new CreateTagRequest();
		ctr.setToken("03053f8ae9a7e06c206e45941d1e0875");
		
		Tag tag = new Tag();
		tag.setName("ubt_wifun");
		tag.setShared(true);
		ctr.setTag(tag);
		
		String rs = InhandHttpClient.getInstance().httpPost( ctr);
		System.out.println(rs);
		if (null != rs)
		{
			JSONObject jsObject = JSONObject.fromObject(rs);
		    
			TagResponse tagRes = new TagResponse();
			tagRes.fromJsonObject(jsObject);
			if (null != tagRes.getTag())
			{
				System.out.println("tagId=" + tagRes.getTag().get_id());
			}
		}
	}
	
	public static boolean deleteTag(String token, String inhandId)
	{
		try
		{
			DeleteTagRequest dtr = new DeleteTagRequest();
			dtr.setToken(token);
			dtr.setTagId(inhandId);
			
			String rs = InhandHttpClient.getInstance().httpDelete(dtr);
			System.out.println(rs);
			if (null != rs)
			{
				JSONObject jsObject = JSONObject.fromObject(rs);
			    
				if (jsObject.containsKey("error_code"))
					return false;
				System.out.println(inhandId + " Delete Succeed!");
			}
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}
}
