package com.wifun.inhand.test;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.wifun.inhand.model.Organization;
import com.wifun.inhand.request.company.CreateCompanyRequest;
import com.wifun.inhand.request.company.DeleteCompanyRequest;
import com.wifun.inhand.request.company.QueryCompanyListRequest;
import com.wifun.inhand.response.company.OrganizationResponse;
import com.wifun.inhand.util.InhandHttpClient;

public class CompanyTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		AddCompany();
	}
	
	public static void clearCompany(String token)
	{
		List<String > tagList = queryList(token);
		if (null != tagList && tagList.size() > 0)
		{
			for (int i = 0; i < tagList.size(); i++)
			{
				deleteCompany(token, tagList.get(i));
				
			}
		}
	}
	
	public static List<String> queryList(String token)
	{
		QueryCompanyListRequest mr = new QueryCompanyListRequest();
		mr.setToken(token);
		mr.setVerbose("100");
		
		String rs = InhandHttpClient.getInstance().httpGet( mr);
		System.out.println(rs);
		if (null != rs)
		{
			JSONObject jsObject = JSONObject.fromObject(rs);

			if (null != jsObject && jsObject.containsKey("result") 
					&& jsObject.getJSONArray("result").size() > 0)
			{
				JSONArray jsl = jsObject.getJSONArray("result");
				List<String> devList = new ArrayList<String>();
				for (int i = 0; i < jsl.size(); i++ )
				{
					if (jsl.getJSONObject(i).containsKey("_id")
							&& jsl.getJSONObject(i).containsKey("email") 
							&&!"397163671@qq.com".equals(jsl.getJSONObject(i).getString("email")))
						devList.add(jsl.getJSONObject(i).getString("_id"));
				}
				return devList;
			}
		}
		return null;
	}
	
	public static void AddCompany()
	{
		CreateCompanyRequest ccr= new CreateCompanyRequest();
		ccr.setToken("86066a16af07e5731ed7cea52adae5ba");
		Organization company = new Organization();
		company.setName("UBT_TTT");
		company.setUsername("zhaohc");
		company.setEmail("city0532@126.com");
		//company.setPassword("abcb4b77276d28a93210c7a21c83a245");
		ccr.setCompany(company);
		System.out.println(JSONObject.fromObject(company).toString());
		
		String rs = InhandHttpClient.getInstance().httpPost( ccr);
		System.out.println(rs);
		if (null != rs)
		{
			JSONObject jsonObject = JSONObject.fromObject(rs);
			OrganizationResponse or = new OrganizationResponse();
			boolean b = or.fromJsonObject(jsonObject);
			if (null != or.getOrganization())
			{
				System.out.println("orgId=" + or.getOrganization().get_id());
			}
		}
	}
	
	public static boolean deleteCompany(String token, String inhandId)
	{
		try
		{
			DeleteCompanyRequest ccr= new DeleteCompanyRequest();
			ccr.setToken( token );
			ccr.setOid(inhandId);
			
			
			String rs = InhandHttpClient.getInstance().httpDelete( ccr);
			System.out.println(rs);
			if (null != rs)
			{
				JSONObject jsonObject = JSONObject.fromObject(rs);
				if (jsonObject.containsKey("error_code"))
					return false;
			}
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}

}
