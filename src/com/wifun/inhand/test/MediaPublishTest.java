package com.wifun.inhand.test;

import java.util.ArrayList;
import java.util.List;

import com.wifun.inhand.common.Constants;
import com.wifun.inhand.model.PortalPublish;
import com.wifun.inhand.request.publish.PortalPublishRequest;
import com.wifun.inhand.util.InhandHttpClient;

public class MediaPublishTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PortalPublishRequest fpr = new PortalPublishRequest();
		fpr.setVersion("1.2118");
		fpr.setVerbose("100");
		fpr.setToken(Constants.token);
		PortalPublish ppp = new PortalPublish();
		ppp.setPublishPointId("55615ab4893429c280ceaae9");
		ppp.setUri("/var/lib/tomcat7/content_sync/tmp/1432451354/");
		List<String> tags = new ArrayList<String>();
		tags.add("defaultTagId");
		ppp.setTags(tags);
		fpr.setPublishPoint(ppp);
		
		String rs = InhandHttpClient.getInstance().httpPost( fpr);
		System.out.println(rs);
	}
 
}
