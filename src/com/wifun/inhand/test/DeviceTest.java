package com.wifun.inhand.test;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.wifun.inhand.common.Constants;
import com.wifun.inhand.model.Router;
import com.wifun.inhand.request.device.CreateRouterRequest;
import com.wifun.inhand.request.device.DeleteRouterRequest;
import com.wifun.inhand.request.device.PutTagRequest;
import com.wifun.inhand.request.device.QueryRouterListRequest;
import com.wifun.inhand.request.device.RemoveTagRequest;
import com.wifun.inhand.response.device.RouterResponse;
import com.wifun.inhand.util.InhandHttpClient;

public class DeviceTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//addDevice();
		addDevice();
	}
	
	public static void clearDevice(String token)
	{
		List<String > devList = queryList(token);
		if (null != devList && devList.size() > 0)
		{
			for (int i = 0; i < devList.size(); i++)
			{
				deleteDevice(token, devList.get(i));
				
			}
		}
	}
	
	public static List<String> queryList(String token)
	{
		QueryRouterListRequest mr = new QueryRouterListRequest();
		mr.setToken(token);
		
		String rs = InhandHttpClient.getInstance().httpGet( mr);
		
		if (null != rs)
		{
			JSONObject jsObject = JSONObject.fromObject(rs);

			if (null != jsObject && jsObject.containsKey("result") 
					&& jsObject.getJSONArray("result").size() > 0)
			{
				JSONArray jsl = jsObject.getJSONArray("result");
				List<String> devList = new ArrayList<String>();
				for (int i = 0; i < jsl.size(); i++ )
				{
					if (jsl.getJSONObject(i).containsKey("_id"))
						devList.add(jsl.getJSONObject(i).getString("_id"));
				}
				return devList;
			}
		}
		return null;
	}

	public static void addDevice()
	{
		CreateRouterRequest cdr = new CreateRouterRequest();
		cdr.setToken(Constants.token);
		
		Router router = new Router();
		router.setSerialNumber("PP3121504259273");
		router.setName("PP3121504259273");
		router.setModel("IP3012L");
		router.setModelId(ModelTest.queryModelId("IP3012L"));
		cdr.setRouter(router);

		String rs = InhandHttpClient.getInstance().httpPost( cdr);
		System.out.println(rs);
		if (null != rs)
		{
			JSONObject jsonObject = JSONObject.fromObject(rs);
			RouterResponse rr = new RouterResponse();
			boolean b = rr.fromJsonObject(jsonObject);
			if (null != rr.getRouter())
			{
				System.out.println("siteId=" + rr.getRouter().getSiteId());
			}
		}
	}
	
	public static boolean putSiteTag(String siteId, String tagId)
	{
		PutTagRequest ptr = new PutTagRequest();
		ptr.setToken(Constants.token);
		ptr.setResourceType("14");
		ptr.setResourceId(siteId);
		ptr.setTagId(tagId);


		String rs = InhandHttpClient.getInstance().httpPut( ptr);
		System.out.println(rs);
		if (null != rs)
		{
			JSONObject jsObject = JSONObject.fromObject(rs);
		    
			
		}
		return false;
	}
	
	public static boolean removeSiteTag(String siteId, String tagId)
	{
		RemoveTagRequest ptr = new RemoveTagRequest();
		ptr.setToken(Constants.token);
		ptr.setResourceId(siteId);
		ptr.setTagId(tagId);

		String rs = InhandHttpClient.getInstance().httpDelete( ptr);
		System.out.println(rs);
		if (null != rs)
		{
			JSONObject jsObject = JSONObject.fromObject(rs);
		    
			
		}
		return false;
	}
	
	public static boolean deleteDevice(String token, String routerId)
	{
		try
		{
			if (null == token || null == routerId)
				return false;
			DeleteRouterRequest ddr = new DeleteRouterRequest();
			ddr.setToken(token);
			ddr.setId(routerId);

			String rs = InhandHttpClient.getInstance().httpDelete( ddr);
			System.out.println(rs);
			if (null != rs)
			{
				JSONObject jsonObject = JSONObject.fromObject(rs);
				if (jsonObject.containsKey("error_code"))
					return false;
				System.out.println(routerId + " Delete Succeed!");
			}
			return true;
		}
		catch(Exception e)
		{
			return false;
		}
	}
}
