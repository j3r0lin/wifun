package com.wifun.inhand.test;

import java.util.ArrayList;
import java.util.List;

import com.wifun.inhand.common.Constants;
import com.wifun.inhand.model.PortalPublish;
import com.wifun.inhand.request.publish.PortalPublishRequest;
import com.wifun.inhand.util.InhandHttpClient;

public class PortalPublishTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		PortalPublishRequest fpr = new PortalPublishRequest();
		fpr.setVersion("1.1118");
		fpr.setVerbose("100");
		fpr.setToken(Constants.token);
		PortalPublish ppp = new PortalPublish();
		ppp.setPublishPointId("554b0475893429c280cea9e8");
		ppp.setUri("/var/lib/tomcat7/content_sync/tmp/1432019384/");
		List<String> tags = new ArrayList<String>();
		tags.add("5551B29D6D7EB52D00000048");
		ppp.setTags(tags);
		fpr.setPublishPoint(ppp);
		
		String rs = InhandHttpClient.getInstance().httpPost( fpr);
		System.out.println(rs);
	}
 
}
