package com.wifun.inhand.request.login;

import org.apache.http.HttpEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

public class CompanyManageRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/access_token/org_mgr";
	private static final String request_type = "POST";

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		return super.baseQueryParam();
	}

	@Override
	public HttpEntity getRequestEntity() {

		return null;
	}
}
