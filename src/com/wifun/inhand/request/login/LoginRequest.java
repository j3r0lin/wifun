package com.wifun.inhand.request.login;

import org.apache.http.HttpEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 登录请求
 * @File wifun-admin/com.wifun.inhand.request.login.LoginRequest.java
 * @Desc
 */
public class LoginRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/oauth2/access_token";
	private static final String request_type = "POST";
	
	private String clientId;
	private String clientSecret;
	private String grantType = "password";
	private String refreshToken;
	private String userName;
	private String password;
	private String language = "2"; 
	private String passwordType = "1";

	public String getClientId() {

		return clientId;
	}

	public void setClientId(String clientId) {

		this.clientId = clientId;
	}

	public String getClientSecret() {

		return clientSecret;
	}

	public void setClientSecret(String clientSecret) {

		this.clientSecret = clientSecret;
	}

	public String getGrantType() {

		return grantType;
	}

	public void setGrantType(String grantType) {

		this.grantType = grantType;
	}

	public String getRefreshToken() {

		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {

		this.refreshToken = refreshToken;
	}

	public String getUserName() {

		return userName;
	}

	public void setUserName(String userName) {

		this.userName = userName;
	}

	public String getPassword() {

		return password;
	}

	public void setPassword(String password) {

		this.password = password;
	}

	public String getLanguage() {

		return language;
	}

	public void setLanguage(String language) {

		this.language = language;
	}

	public String getPasswordType() {

		return passwordType;
	}

	public void setPasswordType(String passwordType) {

		this.passwordType = passwordType;
	}

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		String queryStr = "";
		if (null != this.clientId) {
			queryStr += "&client_id=" + this.clientId;
		}
		if (null != this.clientSecret) {
			queryStr += "&client_secret=" + this.clientSecret;
		}

		if (null != this.grantType) {
			queryStr += "&grant_type=" + this.grantType;
		}
		if (null != this.refreshToken) {
			queryStr += "&refresh_token=" + this.refreshToken;
		}
		if (null != this.userName) {
			queryStr += "&username=" + this.userName;
		}
		if (null != this.password) {
			queryStr += "&password=" + this.password;
		}
		if (null != this.language) {
			queryStr += "&language=" + this.language;
		}
		if (null != this.passwordType) {
			queryStr += "&password_type=" + this.passwordType;
		}
		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		return null;
	}
}
