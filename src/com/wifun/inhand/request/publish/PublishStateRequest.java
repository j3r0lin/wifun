package com.wifun.inhand.request.publish;

import org.apache.http.HttpEntity;

import com.wifun.inhand.common.Constants;
import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 查询发布状态
 * 
 * @File wifun-admin/com.wifun.inhand.request.publish.PublishStateRequest.java
 * @Desc
 */
public class PublishStateRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/content_sync/log";
	private static final String request_type = "GET";

	private String publishPointId;
	private String publishId;
	private String limit = "100";
	private String cursor = "0";

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		if (null == this.publishPointId && null == publishId)
			return null;

		String queryStr = super.baseQueryParam();
		if (null != publishPointId)
			queryStr += "&publishPointId=" + this.publishPointId;

		if (null != publishId)
			queryStr += "&publishId=" + this.publishId;

		if (null != limit)
			queryStr += "&limit=" + this.limit;

		if (null != cursor)
			queryStr += "&cursor=" + this.cursor;
		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		return null;
	}

	public String getPublishPointId() {

		return publishPointId;
	}

	public void setPublishPointId(String publishPointId) {

		this.publishPointId = publishPointId;
	}

	public String getPublishId() {

		return publishId;
	}

	public void setPublishId(String publishId) {

		this.publishId = publishId;
	}

	public String getLimit() {

		return limit;
	}

	public void setLimit(String limit) {

		this.limit = limit;
	}

	public String getCursor() {

		return cursor;
	}

	public void setCursor(String cursor) {

		this.cursor = cursor;
	}
}
