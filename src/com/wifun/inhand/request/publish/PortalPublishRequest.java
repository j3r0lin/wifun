package com.wifun.inhand.request.publish;

import java.io.UnsupportedEncodingException;

import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import com.wifun.inhand.model.PortalPublish;
import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * portal发布
 * 
 * @File wifun-admin/com.wifun.inhand.request.publish.PortalPublishRequest.java
 * @Desc
 */
public class PortalPublishRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/content_sync/publish";
	private static final String request_type = "POST";

	private PortalPublish publishPoint;
	private String version = "V1.1";

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		if (null == this.version)
			return null;

		String queryStr = super.baseQueryParam();

		queryStr += "&version=" + this.version;
		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		if (null == publishPoint)
			return null;

		try {
			System.out.println(JSONObject.fromObject(this.publishPoint).toString());
			return new StringEntity(JSONObject.fromObject(this.publishPoint).toString());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	public PortalPublish getPublishPoint() {

		return publishPoint;
	}

	public void setPublishPoint(PortalPublish publishPoint) {

		this.publishPoint = publishPoint;
	}

	public String getVersion() {

		return version;
	}

	public void setVersion(String version) {

		this.version = version;
	}

	@Override
	public String getContentType() {

		return "application/json";
	}
}
