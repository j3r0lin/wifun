package com.wifun.inhand.request.publish;

import org.apache.http.HttpEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 请求资源发布点
 * 
 * @File wifun-admin/com.wifun.inhand.request.publish.PublishPointRequest.java
 * @Desc
 */
public class PublishPointRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/publish_point";
	private static final String request_type = "GET";

	private String cursor = "0";
	private String limit = "10";
	private String status = "1";

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		if (null == this.status)
			return null;

		String queryStr = super.baseQueryParam();

		if (null != limit) {
			queryStr += "&limit=" + this.limit;
		}

		if (null != cursor) {
			queryStr += "&cursor=" + this.cursor;
		}

		queryStr += "&status=" + this.status;
		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		return null;
	}

	public String getCursor() {

		return cursor;
	}

	public void setCursor(String cursor) {

		this.cursor = cursor;
	}

	public String getLimit() {

		return limit;
	}

	public void setLimit(String limit) {

		this.limit = limit;
	}

	public String getStatus() {

		return status;
	}

	public void setStatus(String status) {

		this.status = status;
	}
}
