package com.wifun.inhand.request.publish;

import java.io.File;

import org.apache.http.HttpEntity;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.mime.content.FileBody;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;
import com.wifun.inhand.util.FileUtil;

/**
 * 媒体资源zip文件上传
 * 
 * @File 
 *       wifun-admin/com.wifun.inhand.request.publish.MediaFileUploadRequest.java
 * @Desc
 */
public class MediaFileUploadRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/content_sync/upload";
	private static final String request_type = "POST";

	private String fileName;
	private String filePath;
	private String suffix;

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		String queryStr = super.baseQueryParam();

		if (null != fileName) {
			queryStr += "&filename=" + this.fileName;
		}

		if (null != suffix) {
			queryStr += "&suffix=" + this.suffix;
		}

		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		FileBody file = new FileBody(new File(this.filePath));

		return new ByteArrayEntity(FileUtil.toByteArray(file));
	}

	@Override
	public String getContentType() {

		return "";
	}

	public String getFileName() {

		return fileName;
	}

	public void setFileName(String fileName) {

		this.fileName = fileName;
	}

	public String getFilePath() {

		return filePath;
	}

	public void setFilePath(String filePath) {

		this.filePath = filePath;
	}

	public String getSuffix() {

		return suffix;
	}

	public void setSuffix(String suffix) {

		this.suffix = suffix;
	}
}
