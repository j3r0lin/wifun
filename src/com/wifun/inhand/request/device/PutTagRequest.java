package com.wifun.inhand.request.device;

import org.apache.http.HttpEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 修改设备标签
 * @File wifun-admin/com.wifun.inhand.request.device.PutTagRequest.java
 * @Desc
 */
public class PutTagRequest  extends BaseRequest  implements IInhandRequest {

	private static final String request = "/api/resource_tags";
	private static final String request_type = "PUT";
	
	////需要对车辆（SITE）添加 
	private String resourceType = "14";
	private String resourceId;
	private String tagId;
	
	@Override
	public String getQueryStr() {
		if (null == this.resourceId || null == this.tagId)
			return null;
		
		String queryStr = super.baseQueryParam();
		
		if (null != resourceType)
		{
			queryStr += "&resource_type=" + this.resourceType;
		}
		
		queryStr += "&resource_id=" + this.resourceId;
		queryStr += "&tag_id=" + this.tagId;
		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {
		// TODO Auto-generated method stub
		return null;
	}

	public String getResourceType() {
		return resourceType;
	}

	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public String getTagId() {
		return tagId;
	}

	public void setTagId(String tagId) {
		this.tagId = tagId;
	}

	@Override
	public String getRequestUri() {
		return getBaseUrl() + request;
	}

}
