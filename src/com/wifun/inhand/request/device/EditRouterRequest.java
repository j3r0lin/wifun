package com.wifun.inhand.request.device;

import java.io.UnsupportedEncodingException;

import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import com.wifun.inhand.model.Router;
import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 修改路由器
 * 
 * @File wifun-admin/com.wifun.inhand.request.device.EditRouterRequest.java
 * @Desc
 */
public class EditRouterRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/devices";
	private static final String request_type = "PUT";

	private String id;
	private String cancelSite;
	private Router router;

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request + "/" + this.id;
	}

	@Override
	public String getQueryStr() {

		String queryStr = super.baseQueryParam();
		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		if (null == router)
			return null;

		try {
			return new StringEntity(JSONObject.fromObject(this.router).toString());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public String getContentType() {

		return "application/json";
	}

	public String getId() {

		return id;
	}

	public void setId(String id) {

		this.id = id;
	}

	public Router getRouter() {

		return router;
	}

	public void setRouter(Router router) {

		this.router = router;
	}

	public String getCancelSite() {

		return cancelSite;
	}

	public void setCancelSite(String cancelSite) {

		this.cancelSite = cancelSite;
	}
}
