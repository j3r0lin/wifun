package com.wifun.inhand.request.device;

import org.apache.http.HttpEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 删除设备的标签
 * 
 * @File wifun-admin/com.wifun.inhand.request.device.RemoveTagRequest.java
 * @Desc
 */
public class RemoveTagRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/resource_tags";
	private static final String request_type = "DELETE";

	private String resourceId;
	private String tagId;

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		if (null == this.resourceId || null == this.tagId)
			return null;

		String queryStr = super.baseQueryParam();

		queryStr += "&resource_id=" + this.resourceId;
		queryStr += "&tag_id=" + this.tagId;
		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		return null;
	}

	public String getResourceId() {

		return resourceId;
	}

	public void setResourceId(String resourceId) {

		this.resourceId = resourceId;
	}

	public String getTagId() {

		return tagId;
	}

	public void setTagId(String tagId) {

		this.tagId = tagId;
	}
}
