package com.wifun.inhand.request.device;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 查询路由器列表
 * 
 * @File wifun-admin/com.wifun.inhand.request.device.QueryRouterListRequest.java
 * @Desc
 */
public class QueryRouterListRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/devices";
	private static final String request_type = "GET";

	private String cousor = "0";
	private String limit = "100";

	private String name;
	private int online;
	private String site_id;
	private String sn;
	private String model;
	private String businessState;

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		String queryStr = super.baseQueryParam();

		if (null != this.cousor) {
			queryStr += "&cousor=" + this.cousor;
		}

		if (null != this.limit) {
			queryStr += "&limit=" + this.limit;
		}

		if (StringUtils.isNotBlank(sn))
			queryStr += "&serial_number=" + this.sn;

		if (queryStr.length() > 2)
			return queryStr.substring(1);

		return null;
	}

	@Override
	public HttpEntity getRequestEntity() {

		return null;
	}

	@Override
	public String getContentType() {

		return "application/json";
	}

	public String getCousor() {

		return cousor;
	}

	public void setCousor(String cousor) {

		this.cousor = cousor;
	}

	public String getLimit() {

		return limit;
	}

	public void setLimit(String limit) {

		this.limit = limit;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public int getOnline() {

		return online;
	}

	public void setOnline(int online) {

		this.online = online;
	}

	public String getSite_id() {

		return site_id;
	}

	public void setSite_id(String site_id) {

		this.site_id = site_id;
	}

	public String getSn() {

		return sn;
	}

	public void setSn(String sn) {

		this.sn = sn;
	}

	public String getModel() {

		return model;
	}

	public void setModel(String model) {

		this.model = model;
	}

	public String getBusinessState() {

		return businessState;
	}

	public void setBusinessState(String businessState) {

		this.businessState = businessState;
	}
}
