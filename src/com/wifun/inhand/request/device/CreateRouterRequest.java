package com.wifun.inhand.request.device;

import java.io.UnsupportedEncodingException;

import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import com.wifun.inhand.model.Router;
import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 添加路由器
 * @File wifun-admin/com.wifun.inhand.request.device.CreateRouterRequest.java
 * @Desc
 */
public class CreateRouterRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/devices";
	private static final String request_type = "POST";

	private String createSite = "1";
	private Router router;

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		String queryStr = super.baseQueryParam();

		if (null != this.createSite) {
			queryStr += "&create_site=" + this.createSite;
		}

		if (queryStr.length() > 2)
			return queryStr.substring(1);

		return null;
	}

	@Override
	public HttpEntity getRequestEntity() {

		if (null == router)
			return null;

		try {
			return new StringEntity(JSONObject.fromObject(this.router, router.getJsonConfig())
					.toString());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	public String getCreateSite() {

		return createSite;
	}

	public void setCreateSite(String createSite) {

		this.createSite = createSite;
	}

	public Router getRouter() {

		return router;
	}

	public void setRouter(Router router) {

		this.router = router;
	}

	@Override
	public String getContentType() {

		return "application/json";
	}
}
