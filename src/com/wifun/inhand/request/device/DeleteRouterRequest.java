package com.wifun.inhand.request.device;

import org.apache.http.HttpEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 删除路由器
 * 
 * @File wifun-admin/com.wifun.inhand.request.device.DeleteRouterRequest.java
 * @Desc
 */
public class DeleteRouterRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/devices";
	private static final String request_type = "DELETE";

	private String id;
	private String deleteSite = "1";

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request + "/" + this.id;
	}

	@Override
	public String getQueryStr() {

		if (null == this.id)
			return null;

		String queryStr = super.baseQueryParam();

		queryStr += "&ID=" + this.id;
		queryStr += "&delete_site=" + this.deleteSite;
		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		return null;
	}

	public String getId() {

		return id;
	}

	public void setId(String id) {

		this.id = id;
	}

	public String getDeleteSite() {

		return deleteSite;
	}

	public void setDeleteSite(String deleteSite) {

		this.deleteSite = deleteSite;
	}
}
