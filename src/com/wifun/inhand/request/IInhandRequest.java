package com.wifun.inhand.request;

import org.apache.http.HttpEntity;

public interface IInhandRequest {

	/**
	 * 获取请求地址
	 * 
	 * @return
	 */
	public String getRequestUri();

	/**
	 * 获取通过Url传递的参数键值
	 * 
	 * @return
	 */
	public String getQueryStr();

	/**
	 * 获取请求的内容类型
	 * 
	 * @return
	 */
	public String getContentType();

	/**
	 * 获取请求的请求参数实体，包括字符串、文件等
	 * 
	 * @return
	 */
	public HttpEntity getRequestEntity();
}
