package com.wifun.inhand.request;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import com.wifun.admin.util.SysConfig;

public class BaseRequest {

	private String token;
	private String verbose = "1";
	private String oid;

	public String getToken() {

		return token;
	}

	public void setToken(String token) {

		this.token = token;
	}

	public String getVerbose() {

		return verbose;
	}

	public void setVerbose(String verbose) {

		this.verbose = verbose;
	}

	public String getOid() {

		return oid;
	}

	public void setOid(String oid) {

		this.oid = oid;
	}

	public String baseQueryParam() {

		String queryStr = "";
		if (null != this.getToken()) {
			queryStr += "&access_token=" + this.getToken();
		}
		if (null != this.getVerbose()) {
			queryStr += "&verbose=" + this.getVerbose();
		}

		return queryStr;
	}

	public List<NameValuePair> basePostParam() {

		List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		if (null != this.getToken()) {
			nvps.add(new BasicNameValuePair("access_token", this.getToken()));
		}
		if (null != this.getVerbose()) {
			nvps.add(new BasicNameValuePair("verbose", this.getVerbose()));
		}

		return nvps;
	}

	public String getContentType() {

		return "application/x-www-form-urlencoded";
	}

	public String getBaseUrl() {

		return SysConfig.getSetting("inhand.api.url");
	}
}
