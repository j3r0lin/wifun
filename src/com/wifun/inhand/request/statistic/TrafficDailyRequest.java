package com.wifun.inhand.request.statistic;

import org.apache.http.HttpEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 查询每日流量
 * 
 * @File wifun-admin/com.wifun.inhand.request.statistic.TrafficDailyRequest.java
 * @Desc
 */
public class TrafficDailyRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/traffic_day";
	private static final String request_type = "GET";

	private String device_id;
	private String month;

	public String getDevice_id() {

		return device_id;
	}

	public void setDevice_id(String device_id) {

		this.device_id = device_id;
	}

	public String getMonth() {

		return month;
	}

	public void setMonth(String month) {

		this.month = month;
	}

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		String queryStr = super.baseQueryParam();
		queryStr += "&device_id=" + this.device_id;
		queryStr += "&month=" + this.month;

		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		return null;
	}
}
