package com.wifun.inhand.request.statistic;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 查询pvuv
 * @File wifun-admin/com.wifun.inhand.request.statistic.QueryPvuvRequest.java
 * @Desc
 */
public class QueryPvuvRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/pvuv/resource/stat";
	private static final String request_type = "GET";
	
	private String tid;
	private String rid;
	private long startTime;
	private long endTime;
	private String site_id;
	
	@Override
	public String getRequestUri() {
		return getBaseUrl() + request;
	}
	
	@Override
	public String getQueryStr() {
		
		String queryStr =  super.baseQueryParam();
		queryStr += "&tid=" + this.tid;
		queryStr += "&rid=" + this.rid;

		queryStr += "&start_time=" + this.startTime;
		queryStr += "&end_time=" + this.endTime;

		if (StringUtils.isNotBlank(site_id))
			queryStr += "&site_id=" + this.site_id;
		return queryStr.substring(1);
	}

	public String getTid() {
		return tid;
	}

	public void setTid(String tid) {
		this.tid = tid;
	}

	public String getRid() {
		return rid;
	}

	public void setRid(String rid) {
		this.rid = rid;
	}

	public long getStartTime() {
		return startTime;
	}

	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}

	public long getEndTime() {
		return endTime;
	}

	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}

	public String getSite_id() {
		return site_id;
	}

	public void setSite_id(String site_id) {
		this.site_id = site_id;
	}

	@Override
	public HttpEntity getRequestEntity() {
		return null;
	}
}
