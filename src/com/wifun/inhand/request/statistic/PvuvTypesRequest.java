package com.wifun.inhand.request.statistic;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 获取所有栏目及概要信息 请求
 * 
 * @File wifun-admin/com.wifun.inhand.request.statistic.PvuvTypesRequest.java
 * @Desc
 */
public class PvuvTypesRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/pvuv/types";
	private static final String request_type = "GET";

	private String siteId;

	public String getSiteId() {

		return siteId;
	}

	public void setSiteId(String siteId) {

		this.siteId = siteId;
	}

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		String queryStr = super.baseQueryParam();
		if (StringUtils.isNotBlank(siteId)) {
			queryStr += "&site_id=" + this.siteId;
		}

		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		return null;
	}
}
