package com.wifun.inhand.request.statistic;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 获取指定栏目下的所有页面及概要信息
 * 
 * @File wifun-admin/com.wifun.inhand.request.statistic.PvuvResourceRequest.java
 * @Desc
 */

public class PvuvResourceRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/pvuv/resources";
	private static final String request_type = "GET";

	private String tid;
	private String sort;
	private int direction = 0;// direction 默认0==倒序
	private String site_id;

	public String getTid() {

		return tid;
	}

	public void setTid(String tid) {

		this.tid = tid;
	}

	public String getSort() {

		return sort;
	}

	public void setSort(String sort) {

		this.sort = sort;
	}

	public int getDirection() {

		return direction;
	}

	public void setDirection(int direction) {

		this.direction = direction;
	}

	public String getSite_id() {

		return site_id;
	}

	public void setSite_id(String site_id) {

		this.site_id = site_id;
	}

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		String queryStr = super.baseQueryParam();
		queryStr += "&tid=" + this.tid;
		queryStr += "&direction=" + this.direction;

		if (StringUtils.isNotBlank(site_id)) {
			queryStr += "&site_id=" + this.site_id;
		}
		if (StringUtils.isNotBlank(sort)) {
			queryStr += "&sort=" + this.sort;
		}

		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		return null;
	}
}
