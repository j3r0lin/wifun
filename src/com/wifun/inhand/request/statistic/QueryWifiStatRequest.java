package com.wifun.inhand.request.statistic;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 查询访问情况
 * 
 * @File 
 *       wifun-admin/com.wifun.inhand.request.statistic.QueryWifiStatRequest.java
 * @Desc
 */
public class QueryWifiStatRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/wifi/stat";
	private static final String request_type = "GET";

	private String types; // 统计类型，以','分隔开的数组，71: 每日新终端数， 51：每日总终端数，
							// 307：停留时间超1小时的终端数
	private long startTime;
	private long endTime;
	private String objectId;// siteId

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		String queryStr = super.baseQueryParam();

		queryStr += "&types=" + this.types;

		queryStr += "&start_time=" + this.startTime;
		queryStr += "&end_time=" + this.endTime;

		if (StringUtils.isNotBlank(objectId))
			queryStr += "&object_id=" + this.objectId;

		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		return null;
	}

	public String getTypes() {

		return types;
	}

	public void setTypes(String types) {

		this.types = types;
	}

	public long getStartTime() {

		return startTime;
	}

	public void setStartTime(long startTime) {

		this.startTime = startTime;
	}

	public long getEndTime() {

		return endTime;
	}

	public void setEndTime(long endTime) {

		this.endTime = endTime;
	}

	public String getObjectId() {

		return objectId;
	}

	public void setObjectId(String objectId) {

		this.objectId = objectId;
	}
}
