package com.wifun.inhand.request.statistic;

import java.io.UnsupportedEncodingException;

import net.sf.json.JSONArray;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 流量按月查询
 * 
 * @File wifun-admin/com.wifun.inhand.request.statistic.TrafficMonthRequest.java
 * @Desc
 */
public class TrafficMonthRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/traffic_month/list";
	private static final String request_type = "GET";

	private String[] resourceIds;

	private String month;

	public String getMonth() {

		return month;
	}

	public void setMonth(String month) {

		this.month = month;
	}

	public String[] getResourceIds() {

		return resourceIds;
	}

	public void setResourceIds(String[] resourceIds) {

		this.resourceIds = resourceIds;
	}

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		String queryStr = super.baseQueryParam();
		queryStr += "&month=" + this.month;

		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		try {

			String resourceStr = JSONArray.fromObject(resourceIds).toString();
			System.out.println(resourceStr);
			return new StringEntity("{\"resourceIds\":" + resourceStr + "}");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}
}
