package com.wifun.inhand.request.tag;

import org.apache.http.HttpEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 删除标签
 * 
 * @File wifun-admin/com.wifun.inhand.request.tag.DeleteTagRequest.java
 * @Desc
 */
public class DeleteTagRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/tags";
	private static final String request_type = "DELETE";
	
	private String tagId;
	
	@Override
	public String getRequestUri() {
		return getBaseUrl() + request + "/" + tagId;
	}
	
	@Override
	public String getQueryStr() {
		if (null == tagId)
			return null;
		String queryStr =  super.baseQueryParam();
		queryStr += "&ID=" + this.tagId;
		
		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {
		return null;
	}

	public String getTagId() {
		return tagId;
	}

	public void setTagId(String tagId) {
		this.tagId = tagId;
	}
}
