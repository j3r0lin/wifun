package com.wifun.inhand.request.tag;

import java.io.UnsupportedEncodingException;

import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import com.wifun.inhand.model.Tag;
import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 修改标签
 * 
 * @File wifun-admin/com.wifun.inhand.request.tag.EditTagRequest.java
 * @Desc
 */
public class EditTagRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/tags";
	private static final String request_type = "PUT";

	private Tag tag;
	private String tagId;

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request + "/" + getTagId();
	}

	@Override
	public String getQueryStr() {

		if (null == tagId)
			return null;
		String queryStr = super.baseQueryParam();
		queryStr = queryStr + "&ID=" + this.getTagId();

		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		if (null == tag)
			return null;

		try {
			return new StringEntity(JSONObject.fromObject(this.tag).toString());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Tag getTag() {

		return tag;
	}

	public void setTag(Tag tag) {

		this.tag = tag;
	}

	public String getTagId() {

		return tagId;
	}

	public void setTagId(String tagId) {

		this.tagId = tagId;
	}
}
