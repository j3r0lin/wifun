package com.wifun.inhand.request.tag;

import org.apache.http.HttpEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 查询标签
 * 
 * @File wifun-admin/com.wifun.inhand.request.tag.QueryTagRequest.java
 * @Desc
 */
public class QueryTagRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/tags";
	private static final String request_type = "GET";

	private String cursor = "0";
	private String name;
	private String limit = "100";

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		String queryStr = super.baseQueryParam();
		if (null != name)
			queryStr += "&name=" + this.name;

		queryStr += "&cursor=" + this.cursor;
		queryStr += "&limit=" + this.limit;

		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		return null;
	}

	public String getCursor() {

		return cursor;
	}

	public void setCursor(String cursor) {

		this.cursor = cursor;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}
}
