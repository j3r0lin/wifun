package com.wifun.inhand.request.tag;

import java.io.UnsupportedEncodingException;

import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import com.wifun.inhand.model.Tag;
import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 创建标签
 * 
 * @File wifun-admin/com.wifun.inhand.request.tag.CreateTagRequest.java
 * @Desc
 */
public class CreateTagRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/tags";
	private static final String request_type = "POST";

	private Tag tag;

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		String queryStr = super.baseQueryParam();

		if (queryStr.length() > 2)
			return queryStr.substring(1);

		return null;
	}

	@Override
	public HttpEntity getRequestEntity() {

		if (null == tag)
			return null;

		try {
			return new StringEntity(JSONObject.fromObject(this.tag).toString());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}

	public Tag getTag() {

		return tag;
	}

	public void setTag(Tag tag) {

		this.tag = tag;
	}
}
