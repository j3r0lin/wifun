package com.wifun.inhand.request.company;

import java.io.UnsupportedEncodingException;

import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;

import com.wifun.inhand.model.Organization;
import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 查询公司
 * 
 * @File wifun-admin/com.wifun.inhand.request.company.GetCompanyRequest.java
 * @Desc
 */
public class GetCompanyRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api2/organizations";
	private static final String request_type = "GET";

	private Organization company;

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request + "/" + this.getOid();
	}

	@Override
	public String getQueryStr() {

		return "access_token=" + this.getToken() + "&ID=" + this.getOid();
	}

	public Organization getCompany() {

		return company;
	}

	public void setCompany(Organization company) {

		this.company = company;
	}

	@Override
	public HttpEntity getRequestEntity() {

		if (null == company)
			return null;

		try {
			return new StringEntity(JSONObject.fromObject(this.company).toString());
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
			return null;
		}
	}
}
