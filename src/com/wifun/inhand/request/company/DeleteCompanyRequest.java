package com.wifun.inhand.request.company;

import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;

import com.wifun.inhand.model.Organization;
import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 刪除公司
 * 
 * @File wifun-admin/com.wifun.inhand.request.company.DeleteCompanyRequest.java
 * @Desc
 */
public class DeleteCompanyRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api2/organizations";
	private static final String request_type = "DELETE";

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request + "/" + this.getOid();
	}

	@Override
	public String getQueryStr() {

		return "access_token=" + this.getToken();
	}

	@Override
	public HttpEntity getRequestEntity() {

		return null;
	}
}
