package com.wifun.inhand.request.company;

import net.sf.json.JSONObject;

import org.apache.http.HttpEntity;

import com.wifun.inhand.model.Organization;
import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 查询公司列表
 * 
 * @File 
 *       wifun-admin/com.wifun.inhand.request.company.QueryCompanyListRequest.java
 * @Desc
 */
public class QueryCompanyListRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api2/organizations";
	private static final String request_type = "GET";

	private String cursor = "0";
	private String name;
	private String limit = "100";

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		String queryStr = super.baseQueryParam();
		if (null != name)
			queryStr += "&name=" + this.name;

		queryStr += "&cursor=" + this.cursor;
		queryStr += "&limit=" + this.limit;
		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		return null;
	}
}
