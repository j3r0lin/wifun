package com.wifun.inhand.request.model;

import java.util.ArrayList;

/**
 * @author chw 流量月报表post请求
 */
public class TrafficMonReqModel {

	private ArrayList<String> resourceIds = new ArrayList<String>();

	public ArrayList<String> getResourceIds() {

		return resourceIds;
	}
}
