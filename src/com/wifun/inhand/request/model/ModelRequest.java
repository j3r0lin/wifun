package com.wifun.inhand.request.model;

import org.apache.http.HttpEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 查询设备型号
 * 
 * @File wifun-admin/com.wifun.inhand.request.model.ModelRequest.java
 * @Desc
 */
public class ModelRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/models";
	private static final String request_type = "GET";

	private String cursor;
	private String limit = "1";
	private String name;

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		if (null == this.name)
			return null;

		String queryStr = super.baseQueryParam();

		if (null != limit) {
			queryStr += "&limit=" + this.limit;
		}

		if (null != cursor) {
			queryStr += "&cursor=" + this.cursor;
		}

		queryStr += "&name=" + this.name;
		return queryStr.substring(1);
	}

	@Override
	public HttpEntity getRequestEntity() {

		return null;
	}

	public String getCursor() {

		return cursor;
	}

	public void setCursor(String cursor) {

		this.cursor = cursor;
	}

	public String getLimit() {

		return limit;
	}

	public void setLimit(String limit) {

		this.limit = limit;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}
}
