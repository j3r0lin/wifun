package com.wifun.inhand.request.site;

import org.apache.http.HttpEntity;

import com.wifun.inhand.request.BaseRequest;
import com.wifun.inhand.request.IInhandRequest;

/**
 * 查询车辆列表
 * 
 * @File wifun-admin/com.wifun.inhand.request.site.QuerySiteListRequest.java
 * @Desc
 */
public class QuerySiteListRequest extends BaseRequest implements IInhandRequest {

	private static final String request = "/api/sites";
	private static final String request_type = "GET";

	private String cousor = "0";
	private String limit = "100";

	@Override
	public String getRequestUri() {

		return getBaseUrl() + request;
	}

	@Override
	public String getQueryStr() {

		String queryStr = super.baseQueryParam();

		if (null != this.cousor) {
			queryStr += "&cousor=" + this.cousor;
		}

		if (null != this.limit) {
			queryStr += "&limit=" + this.limit;
		}

		if (queryStr.length() > 2)
			return queryStr.substring(1);

		return null;
	}

	@Override
	public HttpEntity getRequestEntity() {

		return null;
	}

	@Override
	public String getContentType() {

		return "application/json";
	}
}
