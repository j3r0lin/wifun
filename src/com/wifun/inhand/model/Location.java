package com.wifun.inhand.model;

import net.sf.json.JSONObject;

/**
 * 设备位置信息
 * 
 * @File wifun-admin/com.wifun.inhand.model.Location.java
 * @Desc
 */
public class Location {

	private double longitude; // 精度
	private double latitude; // 纬度
	private double speed; // 速度
	private double course; // 方向

	public Location(JSONObject jsonObject) {

		if (jsonObject.containsKey("longitude")) {
			this.longitude = jsonObject.optDouble("longitude");
		}
		if (jsonObject.containsKey("latitude")) {
			this.latitude = jsonObject.optDouble("latitude");
		}
		if (jsonObject.containsKey("speed")) {
			this.speed = jsonObject.optDouble("speed");
		}
		if (jsonObject.containsKey("course")) {
			this.course = jsonObject.optDouble("course");
		}
	}

	public double getLongitude() {

		return longitude;
	}

	public void setLongitude(double longitude) {

		this.longitude = longitude;
	}

	public double getLatitude() {

		return latitude;
	}

	public void setLatitude(double latitude) {

		this.latitude = latitude;
	}

	public double getSpeed() {

		return speed;
	}

	public void setSpeed(double speed) {

		this.speed = speed;
	}

	public double getCourse() {

		return course;
	}

	public void setCourse(double course) {

		this.course = course;
	}
}
