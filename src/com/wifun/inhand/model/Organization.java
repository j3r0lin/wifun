package com.wifun.inhand.model;

import java.lang.reflect.Field;

import net.sf.json.JSONObject;

/**
 * 公司
 * 
 * @File wifun-admin/com.wifun.inhand.model.Organization.java
 * @Desc
 */
public class Organization extends BaseBean {

	private String _id;
	private String name;
	private String email;
	private String username;
	private String password = "E10ADC3949BA59ABBE56E057F20F883E";

	private String phone = "";
	private String website = "www.website.com";
	private String fax = "";
	private String address = "";
	private String cellPhone = "";
	private int questionId = 1;
	private String question = "";
	private String answer = "";

	public Organization() {

	}

	public Organization(JSONObject jsonObject) {

		try {
			Field[] fields = Organization.class.getDeclaredFields();
			;

			for (Field f : fields) {

				f.setAccessible(true); // 设置些属性是可以访问的

				String type = f.getType().toString();// 得到此属性的类型

				if (!jsonObject.containsKey(f.getName()))
					continue;

				if (type.endsWith("String")) {

					f.set(this, jsonObject.getString(f.getName())); // 给属性设值
				} else if (type.endsWith("int") || type.endsWith("Integer")) {

					f.set(this, jsonObject.getInt(f.getName())); // 给属性设值
				} else if (type.endsWith("Long")) {

					f.set(this, jsonObject.getLong(f.getName())); // 给属性设值
				} else if (type.endsWith("List")) {

					f.set(this, jsonObject.getJSONArray(f.getName())); // 给属性设值
				}

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String get_id() {

		return _id;
	}

	public void set_id(String _id) {

		this._id = _id;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public String getUsername() {

		return username;
	}

	public void setUsername(String username) {

		this.username = username;
	}

	public String getPassword() {

		return password;
	}

	public void setPassword(String password) {

		this.password = password;
	}

	public String getPhone() {

		return phone;
	}

	public void setPhone(String phone) {

		this.phone = phone;
	}

	public String getWebsite() {

		return website;
	}

	public void setWebsite(String website) {

		this.website = website;
	}

	public String getFax() {

		return fax;
	}

	public void setFax(String fax) {

		this.fax = fax;
	}

	public String getAddress() {

		return address;
	}

	public void setAddress(String address) {

		this.address = address;
	}

	public String getCellPhone() {

		return cellPhone;
	}

	public void setCellPhone(String cellPhone) {

		this.cellPhone = cellPhone;
	}

	public int getQuestionId() {

		return questionId;
	}

	public void setQuestionId(int questionId) {

		this.questionId = questionId;
	}

	public String getQuestion() {

		return question;
	}

	public void setQuestion(String question) {

		this.question = question;
	}

	public String getAnswer() {

		return answer;
	}

	public void setAnswer(String answer) {

		this.answer = answer;
	}
}
