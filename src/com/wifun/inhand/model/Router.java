package com.wifun.inhand.model;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

/**
 * 路由器
 * 
 * @File wifun-admin/com.wifun.inhand.model.Router.java
 * @Desc
 */
public class Router extends BaseBean {

	private String oid;
	private String _id;
	private String name;
	private String model;
	private String modelId;
	private String serialNumber;
	private String siteId;

	private RouterInfo info;

	public Router() {

	}

	public Router(JSONObject jsonObject) {

		if (jsonObject.containsKey("oid")) {
			this.oid = jsonObject.getString("oid");
		}
		if (jsonObject.containsKey("_id")) {
			this._id = jsonObject.getString("_id");
		}
		if (jsonObject.containsKey("name")) {
			this.name = jsonObject.getString("name");
		}
		if (jsonObject.containsKey("model")) {
			this.model = jsonObject.getString("model");
		}
		if (jsonObject.containsKey("modelId")) {
			this.modelId = jsonObject.getString("modelId");
		}
		if (jsonObject.containsKey("serialNumber")) {
			this.serialNumber = jsonObject.getString("serialNumber");
		}
		if (jsonObject.containsKey("siteId")) {
			this.siteId = jsonObject.getString("siteId");
		}
		if (jsonObject.containsKey("info")) {
			this.info = new RouterInfo(jsonObject.getJSONObject("info"));
		}
	}

	public String getOid() {

		return oid;
	}

	public void setOid(String oid) {

		this.oid = oid;
	}

	public String get_id() {

		return _id;
	}

	public void set_id(String _id) {

		this._id = _id;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String getModel() {

		return model;
	}

	public void setModel(String model) {

		this.model = model;
	}

	public String getModelId() {

		return modelId;
	}

	public void setModelId(String modelId) {

		this.modelId = modelId;
	}

	public String getSerialNumber() {

		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {

		this.serialNumber = serialNumber;
	}

	public String getSiteId() {

		return siteId;
	}

	public void setSiteId(String siteId) {

		this.siteId = siteId;
	}

	public RouterInfo getInfo() {

		return info;
	}

	public void setInfo(RouterInfo info) {

		this.info = info;
	}

	@Override
	public JsonConfig getJsonConfig() {

		JsonConfig jc = super.getJsonConfig();
		jc.setExcludes(new String[] { "siteId" });
		return jc;
	}
}
