package com.wifun.inhand.model;

import net.sf.json.JSONObject;

/**
 * 资源同步状态
 * 
 * @File wifun-admin/com.wifun.inhand.model.PublishState.java
 * @Desc
 */
public class PublishState extends BaseBean {

	private String _id;
	private String deviceId;
	private String currentVersion;
	private int state;
	private int createTime;
	private int syncStartTime;

	public PublishState() {

	}

	public PublishState(JSONObject jsonObject) {

		if (jsonObject.containsKey("_id")) {
			this._id = jsonObject.getString("_id");
		}
		if (jsonObject.containsKey("deviceId")) {
			this.deviceId = jsonObject.getString("deviceId");
		}
		if (jsonObject.containsKey("currentVersion")) {
			this.currentVersion = jsonObject.getString("currentVersion");
		}
		if (jsonObject.containsKey("state")) {
			this.state = jsonObject.getInt("state");
		}
		if (jsonObject.containsKey("createTime")) {
			this.createTime = jsonObject.getInt("createTime");
		}
		if (jsonObject.containsKey("syncStartTime")) {
			this.syncStartTime = jsonObject.getInt("syncStartTime");
		}
	}

	public String get_id() {

		return _id;
	}

	public void set_id(String _id) {

		this._id = _id;
	}

	public String getDeviceId() {

		return deviceId;
	}

	public void setDeviceId(String deviceId) {

		this.deviceId = deviceId;
	}

	public String getCurrentVersion() {

		return currentVersion;
	}

	public void setCurrentVersion(String currentVersion) {

		this.currentVersion = currentVersion;
	}

	public int getState() {

		return state;
	}

	public void setState(int state) {

		this.state = state;
	}

	public int getCreateTime() {

		return createTime;
	}

	public void setCreateTime(int createTime) {

		this.createTime = createTime;
	}

	public int getSyncStartTime() {

		return syncStartTime;
	}

	public void setSyncStartTime(int syncStartTime) {

		this.syncStartTime = syncStartTime;
	}
}
