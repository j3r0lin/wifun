package com.wifun.inhand.model;

import net.sf.json.JSONObject;

/**
 * 车辆，业务平台没有车辆概念，与设备对应
 * 
 * @File wifun-admin/com.wifun.inhand.model.Site.java
 * @Desc
 */
public class Site extends BaseBean {

	private String oid;
	private String _id;
	private String name;
	private int online;// 联网状态 0:离线 1在线（由设备主动上报）

	private Location location;

	public Site() {

	}

	public Site(JSONObject jsonObject) {

		if (jsonObject.containsKey("oid")) {
			this.oid = jsonObject.getString("oid");
		}
		if (jsonObject.containsKey("_id")) {
			this._id = jsonObject.getString("_id");
		}
		if (jsonObject.containsKey("name")) {
			this.name = jsonObject.getString("name");
		}
		if (jsonObject.containsKey("online")) {
			this.online = jsonObject.getInt("online");
		}
		if (jsonObject.containsKey("location")) {
			this.location = new Location(jsonObject.getJSONObject("location"));
		}
	}

	public String getOid() {

		return oid;
	}

	public void setOid(String oid) {

		this.oid = oid;
	}

	public String get_id() {

		return _id;
	}

	public void set_id(String _id) {

		this._id = _id;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public int getOnline() {

		return online;
	}

	public void setOnline(int online) {

		this.online = online;
	}

	public Location getLocation() {

		return location;
	}

	public void setLocation(Location location) {

		this.location = location;
	}
}
