package com.wifun.inhand.model;

import com.wifun.admin.util.TimeUtils;

import net.sf.json.JSONObject;

/**
 * pvuv
 * 
 * @File wifun-admin/com.wifun.inhand.model.Pvuv.java
 * @Desc
 */
public class Pvuv {

	private int pv;
	private int uv;
	private String date;

	public Pvuv(JSONObject jsonObject) {

		if (jsonObject.containsKey("pv")) {
			this.pv = jsonObject.getInt("pv");
		}
		if (jsonObject.containsKey("uv")) {
			this.uv = jsonObject.getInt("uv");
		}
		if (jsonObject.containsKey("date")) {
			long time = jsonObject.getLong("date");
			this.date = TimeUtils.getShortDateStrFromLong(time);
		}
	}

	public int getPv() {

		return pv;
	}

	public void setPv(int pv) {

		this.pv = pv;
	}

	public int getUv() {

		return uv;
	}

	public void setUv(int uv) {

		this.uv = uv;
	}

	public String getDate() {

		return date;
	}

	public void setDate(String date) {

		this.date = date;
	}
}
