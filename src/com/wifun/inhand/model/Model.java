package com.wifun.inhand.model;

/**
 * 设备型号
 * 
 * @File wifun-admin/com.wifun.inhand.model.Model.java
 * @Desc
 */
public class Model {

	private String _id;
	private String name;

	public String get_id() {

		return _id;
	}

	public void set_id(String _id) {

		this._id = _id;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}
}
