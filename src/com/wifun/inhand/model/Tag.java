package com.wifun.inhand.model;

import net.sf.json.JSONObject;

/**
 * 标签,与业务平台的分组对应
 * 
 * @File wifun-admin/com.wifun.inhand.model.Tag.java
 * @Desc
 */
public class Tag extends BaseBean {

	private String _id;
	private String name;
	private boolean shared;

	public Tag() {

	}

	public Tag(JSONObject jsonObject) {

		if (jsonObject.containsKey("_id")) {
			this._id = jsonObject.getString("_id");
		}
		if (jsonObject.containsKey("name")) {
			this.name = jsonObject.getString("name");
		}
		if (jsonObject.containsKey("shared")) {
			this.shared = jsonObject.getBoolean("shared");
		}
	}

	public String get_id() {

		return _id;
	}

	public void set_id(String _id) {

		this._id = _id;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public boolean isShared() {

		return shared;
	}

	public void setShared(boolean shared) {

		this.shared = shared;
	}
}
