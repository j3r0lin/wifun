package com.wifun.inhand.model;

import net.sf.json.JSONObject;

/**
 * 发布点
 * 
 * @File wifun-admin/com.wifun.inhand.model.PublishPoint.java
 * @Desc
 */
public class PublishPoint extends BaseBean {

	private String _id;
	private String status;
	private String name;
	private String devicePath;

	public PublishPoint() {

	}

	public PublishPoint(JSONObject jsonObject) {

		if (jsonObject.containsKey("_id")) {
			this._id = jsonObject.getString("_id");
		}
		if (jsonObject.containsKey("name")) {
			this.name = jsonObject.getString("name");
		}
		if (jsonObject.containsKey("status")) {
			this.status = jsonObject.getString("status");
		}
		if (jsonObject.containsKey("devicePath")) {
			this.devicePath = jsonObject.getString("devicePath");
		}
	}

	public String get_id() {

		return _id;
	}

	public void set_id(String _id) {

		this._id = _id;
	}

	public String getStatus() {

		return status;
	}

	public void setStatus(String status) {

		this.status = status;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String getDevicePath() {

		return devicePath;
	}

	public void setDevicePath(String devicePath) {

		this.devicePath = devicePath;
	}
}
