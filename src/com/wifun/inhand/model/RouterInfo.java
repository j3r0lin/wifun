package com.wifun.inhand.model;

import net.sf.json.JSONObject;

/**
 * 路由器信息
 * 
 * @File wifun-admin/com.wifun.inhand.model.RouterInfo.java
 * @Desc
 */
public class RouterInfo {

	private String swVersion;
	private String imsi;

	public RouterInfo() {

	}

	public RouterInfo(JSONObject jsonObject) {

		if (jsonObject.containsKey("swVersion")) {
			this.swVersion = jsonObject.getString("swVersion");
		}
		if (jsonObject.containsKey("imsi")) {
			this.imsi = jsonObject.getString("imsi");
		}
	}

	public String getSwVersion() {

		return swVersion;
	}

	public void setSwVersion(String swVersion) {

		this.swVersion = swVersion;
	}

	public String getImsi() {

		return imsi;
	}

	public void setImsi(String imsi) {

		this.imsi = imsi;
	}
}
