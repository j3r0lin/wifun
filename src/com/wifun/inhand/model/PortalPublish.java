package com.wifun.inhand.model;

import java.util.List;

/**
 * portal发布
 * 
 * @File wifun-admin/com.wifun.inhand.model.PortalPublish.java
 * @Desc
 */
public class PortalPublish {

	private String publishPointId;
	private String uri;
	private List<String> tags;

	public String getPublishPointId() {

		return publishPointId;
	}

	public void setPublishPointId(String publishPointId) {

		this.publishPointId = publishPointId;
	}

	public String getUri() {

		return uri;
	}

	public void setUri(String uri) {

		this.uri = uri;
	}

	public List<String> getTags() {

		return tags;
	}

	public void setTags(List<String> tags) {

		this.tags = tags;
	}
}
