package com.wifun.inhand.response;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

public class BaseResponse implements IFieldList {

	public String request;
	public String error_code;
	public String error;

	public String getRequest() {

		return request;
	}

	public void setRequest(String request) {

		this.request = request;
	}

	public String getError_code() {

		return error_code;
	}

	public void setError_code(String error_code) {

		this.error_code = error_code;
	}

	public String getError() {

		return error;
	}

	public void setError(String error) {

		this.error = error;
	}

	@Override
	public List<Field> getFieldList() {

		List<Field> fieldList = new ArrayList<Field>();
		Field[] fs = BaseResponse.class.getDeclaredFields();
		for (Field f : fs) {
			fieldList.add(f);
		}

		return fieldList;
	}

	public boolean fromJsonObject(JSONObject jsonObject) {

		try {
			List<Field> fields = getFieldList();

			for (Field f : fields) {

				f.setAccessible(true); // 设置些属性是可以访问的

				String type = f.getType().toString();// 得到此属性的类型
				if ("serialVersionUID".equals(f.getName())) {
					continue;
				}

				if (!jsonObject.containsKey(f.getName())) {
					continue;
				}

				if (type.endsWith("String")) {
					f.set(this, jsonObject.getString(f.getName())); // 给属性设值
				} else if (type.endsWith("int") || type.endsWith("Integer")) {
					f.set(this, jsonObject.getInt(f.getName())); // 给属性设值
				} else if (type.endsWith("Long")) {
					f.set(this, jsonObject.getLong(f.getName())); // 给属性设值
				} else if (type.endsWith("List")) {
					f.set(this, jsonObject.getJSONArray(f.getName())); // 给属性设值
				}

			}
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
