package com.wifun.inhand.response;

import java.lang.reflect.Field;
import java.util.List;

public interface IFieldList{

	public List<Field> getFieldList();
}
