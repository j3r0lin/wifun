package com.wifun.inhand.response.statistic;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.wifun.inhand.model.Pvuv;
import com.wifun.inhand.response.BaseResponse;

public class PvuvResponse extends BaseResponse {

	List<Pvuv> pvuvs = new ArrayList<Pvuv>();

	@Override
	public List<Field> getFieldList() {

		List<Field> fieldList = super.getFieldList();
		Field[] fs = PvuvResponse.class.getDeclaredFields();

		for (Field f : fs) {
			fieldList.add(f);
		}

		return fieldList;
	}

	@Override
	public boolean fromJsonObject(JSONObject jsonObject) {

		boolean b = super.fromJsonObject(jsonObject);
		if (false == b)
			return false;

		if (jsonObject.containsKey("result") && jsonObject.getJSONArray("result").size() > 0) {

			JSONArray jsonArr = jsonObject.getJSONArray("result");
			for (int i = 0; i < jsonArr.size(); i++) {

				Pvuv pvuv = new Pvuv(jsonArr.getJSONObject(i));
				pvuvs.add(pvuv);
			}
		}
		return true;
	}

	public List<Pvuv> getPvuvs() {

		return pvuvs;
	}
}
