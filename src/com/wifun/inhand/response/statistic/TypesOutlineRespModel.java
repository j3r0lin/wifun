/**
 * 
 */
package com.wifun.inhand.response.statistic;

import java.util.HashMap;

/**
 * @author chenwei 获取所有栏目及概要信息 时返回结果对应的业务bean
 * 
 *         {"tId":"main", "updateTime":1437541760757,
 *         "current":{"pv":1,"uv":1,"date":1437537600000},
 *         "last":{"pv":87,"uv":40,"date":1437451200000},
 *         "total":{"pv":2443,"uv":839}},
 */
public class TypesOutlineRespModel {

	private String tid;
	private String updateTime;
	private HashMap<String, String> current = new HashMap<String, String>();
	private HashMap<String, String> last = new HashMap<String, String>();
	private HashMap<String, String> total = new HashMap<String, String>();

	public String getTid() {

		return tid;
	}

	public void setTid(String tid) {

		this.tid = tid;
	}

	public String getUpdateTime() {

		return updateTime;
	}

	public void setUpdateTime(String updateTime) {

		this.updateTime = updateTime;
	}

	public HashMap<String, String> getCurrent() {

		return current;
	}

	public HashMap<String, String> getLast() {

		return last;
	}

	public HashMap<String, String> getTotal() {

		return total;
	}
}