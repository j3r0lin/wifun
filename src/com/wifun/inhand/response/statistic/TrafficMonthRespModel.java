/**
 * 
 */
package com.wifun.inhand.response.statistic;

/**
 * @author chw
 * 
 *         流量月报报返回的response对应的业务bean "result": [ { "max": 276133720, "total":
 *         791199843, "receive": 533569074, "send": 257630769, "deviceId":
 *         "55822AD2E4B0E85F6CE22BCB" }, { "max": 427117505, "total": 941045598,
 *         "receive": 767739242,
 */
public class TrafficMonthRespModel {

	private String deviceId;
	private long total;
	private long receive;
	private long send;
	private long max;

	public String getDeviceId() {

		return deviceId;
	}

	public void setDeviceId(String deviceId) {

		this.deviceId = deviceId;
	}

	public long getTotal() {

		return total;
	}

	public void setTotal(long total) {

		this.total = total;
	}

	public long getReceive() {

		return receive;
	}

	public void setReceive(long receive) {

		this.receive = receive;
	}

	public long getSend() {

		return send;
	}

	public void setSend(long send) {

		this.send = send;
	}

	public long getMax() {

		return max;
	}

	public void setMax(long max) {

		this.max = max;
	}
}
