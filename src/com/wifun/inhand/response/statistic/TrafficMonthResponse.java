package com.wifun.inhand.response.statistic;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.wifun.inhand.response.BaseResponse;

public class TrafficMonthResponse extends BaseResponse {

	private ArrayList<TrafficMonthRespModel> respResult = new ArrayList<TrafficMonthRespModel>();

	public ArrayList<TrafficMonthRespModel> getRespResult() {

		return respResult;
	}

	@Override
	public List<Field> getFieldList() {

		List<Field> fieldList = super.getFieldList();
		Field[] fs = PvuvResponse.class.getDeclaredFields();

		for (Field f : fs) {
			fieldList.add(f);
		}

		return fieldList;
	}

	@Override
	public boolean fromJsonObject(JSONObject jsonObject) {

		boolean b = super.fromJsonObject(jsonObject);
		if (false == b)
			return false;

		if (null != jsonObject && jsonObject.containsKey("result")
				&& jsonObject.getJSONArray("result").size() > 0) {
			JSONArray jsl = jsonObject.getJSONArray("result");
			for (int i = 0; i < jsl.size(); i++) {
				
				TrafficMonthRespModel model = new TrafficMonthRespModel();
				JSONObject tmp = (JSONObject) jsl.get(i);

				if (tmp.containsKey("deviceId")) {
					model.setDeviceId(tmp.getString("deviceId"));
				}
				if (tmp.containsKey("total")) {
					model.setTotal(tmp.optLong("total"));
				}
				if (tmp.containsKey("receive")) {
					model.setReceive(tmp.optLong("receive"));
				}
				if (tmp.containsKey("send")) {
					model.setSend(tmp.optLong("send"));
				}
				if (tmp.containsKey("max")) {
					model.setMax(tmp.optLong("max"));
				}

				respResult.add(model);
			}
		}
		return true;
	}
}
