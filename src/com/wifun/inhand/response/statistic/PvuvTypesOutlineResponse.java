package com.wifun.inhand.response.statistic;

import java.util.ArrayList;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.wifun.admin.util.TimeUtils;
import com.wifun.inhand.response.BaseResponse;

public class PvuvTypesOutlineResponse extends BaseResponse {

	private ArrayList<TypesOutlineRespModel> respResult = new ArrayList<TypesOutlineRespModel>();

	public ArrayList<TypesOutlineRespModel> getRespResult() {

		return respResult;
	}

	@Override
	public boolean fromJsonObject(JSONObject jsonObject) {

		boolean b = super.fromJsonObject(jsonObject);
		if (false == b)
			return false;

		if (null != jsonObject && jsonObject.containsKey("result")
				&& jsonObject.getJSONArray("result").size() > 0) {
			JSONArray jsl = jsonObject.getJSONArray("result");
			for (int i = 0; i < jsl.size(); i++) {
				// System.out.println(jsl.get(i));
				TypesOutlineRespModel model = new TypesOutlineRespModel();

				JSONObject tmp = (JSONObject) jsl.get(i);
				String tId = tmp.getString("tId");
				model.setTid(tId);
				if (tmp.containsKey("updateTime")) {
					String updateTime = tmp.getString("updateTime");
					model.setUpdateTime(updateTime);
				}

				if (tmp.containsKey("current")) {
					JSONObject current = tmp.getJSONObject("current");
					model.getCurrent().put("pv", current.getString("pv"));
					model.getCurrent().put("uv", current.getString("uv"));
					model.getCurrent().put("date",
							TimeUtils.getShortDateStrFromLong(current.getLong("date")));
				}

				if (tmp.containsKey("last")) {
					JSONObject last = tmp.getJSONObject("last");
					model.getLast().put("pv", last.getString("pv"));
					model.getLast().put("uv", last.getString("uv"));
					model.getLast().put("date",
							TimeUtils.getShortDateStrFromLong(last.getLong("date")));
				}

				if (tmp.containsKey("total")) {
					JSONObject total = tmp.getJSONObject("total");

					model.getTotal().put("pv", total.getString("pv"));
					model.getTotal().put("uv", total.getString("uv"));
				}

				respResult.add(model);
			}
		}

		return true;
	}
}
