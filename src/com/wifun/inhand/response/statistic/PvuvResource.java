/**
 * 
 */
package com.wifun.inhand.response.statistic;

import java.util.HashMap;

/**
 * @author chenwei 获取指定栏目下的所有页面及概要信息对应的业务bean GET /api/pvuv/resources
 * 
 *         "result": [ { "rId": "19%3AAmerican%20Sniper", "tId": "movie",
 *         "updateTime": 1437541760768, "current": { "pv": 1, "uv": 1, "date":
 *         1437537600000 }, "last": { "pv": 6, "uv": 1, "date": 1437451200000 },
 *         "total": { "pv": 109, "uv": 69 } }, { "rId": "2%3ALucy",
 */

public class PvuvResource {

	private String rId;
	private String tId;
	private String updateTime;
	private HashMap<String, String> current = new HashMap<String, String>();
	private HashMap<String, String> last = new HashMap<String, String>();
	private HashMap<String, String> total = new HashMap<String, String>();

	public String getRId() {

		return rId;
	}

	public void setRId(String rId) {

		this.rId = rId;
	}

	public String getTId() {

		return tId;
	}

	public void setTId(String tId) {

		this.tId = tId;
	}

	public String getUpdateTime() {

		return updateTime;
	}

	public void setUpdateTime(String updateTime) {

		this.updateTime = updateTime;
	}

	public HashMap<String, String> getCurrent() {

		return current;
	}

	public HashMap<String, String> getLast() {

		return last;
	}

	public HashMap<String, String> getTotal() {

		return total;
	}
}