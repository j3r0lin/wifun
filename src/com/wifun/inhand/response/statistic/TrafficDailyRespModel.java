/**
 * 
 */
package com.wifun.inhand.response.statistic;

/**
 * @author chw
 * 
 *         流量日报返回的response对应的业务bean "result": [ { "total": 19490859, "receive":
 *         17278849, "send": 2212010, "deviceId": "55822AEDE4B0E85F6CE22BCD",
 *         "date": 20150713, "_id": "55A49995A9CF48E1C5E6605E" }, { "total":
 *         34132891,
 */
public class TrafficDailyRespModel {

	private long total;
	private long receive;
	private long send;
	private String deviceId;
	private String date;
	private String _id;

	public long getTotal() {

		return total;
	}

	public void setTotal(long total) {

		this.total = total;
	}

	public long getReceive() {

		return receive;
	}

	public void setReceive(long receive) {

		this.receive = receive;
	}

	public long getSend() {

		return send;
	}

	public void setSend(long send) {

		this.send = send;
	}

	public String getDeviceId() {

		return deviceId;
	}

	public void setDeviceId(String deviceId) {

		this.deviceId = deviceId;
	}

	public String getDate() {

		return date;
	}

	public void setDate(String date) {

		this.date = date;
	}

	public String get_id() {

		return _id;
	}

	public void set_id(String _id) {

		this._id = _id;
	}
}
