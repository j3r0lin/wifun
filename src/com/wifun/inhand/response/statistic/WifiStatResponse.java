package com.wifun.inhand.response.statistic;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.wifun.admin.util.TimeUtils;
import com.wifun.inhand.response.BaseResponse;

public class WifiStatResponse extends BaseResponse {

	private HashMap<String, Map<String, Integer>> resonseMap = new HashMap<String, Map<String, Integer>>();

	@Override
	public List<Field> getFieldList() {

		List<Field> fieldList = super.getFieldList();
		Field[] fs = WifiStatResponse.class.getDeclaredFields();

		for (Field f : fs) {
			fieldList.add(f);
		}

		return fieldList;
	}

	public HashMap<String, Map<String, Integer>> getResonseMap() {

		return resonseMap;
	}

	@Override
	public boolean fromJsonObject(JSONObject jsonObject) {

		boolean b = super.fromJsonObject(jsonObject);
		if (false == b)
			return false;

		if (null != jsonObject && jsonObject.containsKey("result")
				&& jsonObject.getJSONArray("result").size() > 0) {
			JSONArray jsl = jsonObject.getJSONArray("result");
			for (int i = 0; i < jsl.size(); i++) {

				JSONObject tmp = (JSONObject) jsl.get(i);
				int type = tmp.getInt("type");
				System.out.println("type ==" + type);

				JSONArray valuesArray = tmp.getJSONArray("values");
				HashMap<String, Integer> timeValMap = new HashMap<String, Integer>();
				for (int j = 0; j < valuesArray.size(); j++) {
					JSONArray valTmp = (JSONArray) valuesArray.get(j);

					System.out.println("valTmp==" + valTmp);
					String dt = TimeUtils.getShortDateStrFromLong(valTmp.getLong(0));

					timeValMap.put(dt, valTmp.getInt(1));
				}
				resonseMap.put("" + type, timeValMap);
			}
		}

		return true;
	}
}
