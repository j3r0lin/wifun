package com.wifun.inhand.response.company;

import java.lang.reflect.Field;
import java.util.List;

import net.sf.json.JSONObject;

import com.wifun.inhand.model.Organization;
import com.wifun.inhand.response.BaseResponse;
import com.wifun.inhand.response.login.LoginResponse;

/**
 * 公司接口返回结果处理类
 * 
 * @File wifun-admin/com.wifun.inhand.response.company.OrganizationResponse.java
 * @Desc
 */
public class OrganizationResponse extends BaseResponse {

	private Organization organization;

	@Override
	public List<Field> getFieldList() {

		List<Field> fieldList = super.getFieldList();
		Field[] fs = LoginResponse.class.getDeclaredFields();

		for (Field f : fs) {
			fieldList.add(f);
		}

		return fieldList;
	}

	@Override
	public boolean fromJsonObject(JSONObject jsonObject) {

		boolean b = super.fromJsonObject(jsonObject);
		if (false == b)
			return false;

		if (jsonObject.containsKey("result")) {
			this.organization = new Organization(jsonObject.getJSONObject("result"));
		}
		return true;
	}

	public Organization getOrganization() {

		return organization;
	}

	public void setOrganization(Organization organization) {

		this.organization = organization;
	}
}
