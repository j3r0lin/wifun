package com.wifun.inhand.response.login;

import java.lang.reflect.Field;
import java.util.List;

import com.wifun.inhand.response.BaseResponse;

/**
 * 登录接口返回结果处理类
 * 
 * @File wifun-admin/com.wifun.inhand.response.login.LoginResponse.java
 * @Desc
 */
public class LoginResponse extends BaseResponse {

	private int expires_in;
	private String refresh_token;
	private String access_token;

	private long expires_end;

	public int getExpires_in() {

		return expires_in;
	}

	public void setExpires_in(int expires_in) {

		this.expires_in = expires_in;
	}

	public String getRefresh_token() {

		return refresh_token;
	}

	public void setRefresh_token(String refresh_token) {

		this.refresh_token = refresh_token;
	}

	public String getAccess_token() {

		return access_token;
	}

	public void setAccess_token(String access_token) {

		this.access_token = access_token;
	}

	public long getExpires_end() {

		return expires_end;
	}

	public void setExpires_end(long expires_end) {

		this.expires_end = expires_end;
	}

	@Override
	public List<Field> getFieldList() {

		List<Field> fieldList = super.getFieldList();
		Field[] fs = LoginResponse.class.getDeclaredFields();

		for (Field f : fs) {
			fieldList.add(f);
		}

		return fieldList;
	}
}
