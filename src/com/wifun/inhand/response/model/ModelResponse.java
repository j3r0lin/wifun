package com.wifun.inhand.response.model;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import com.wifun.inhand.model.Model;
import com.wifun.inhand.response.BaseResponse;

/**
 * 设备类型接口返回结果处理类
 * 
 * @File wifun-admin/com.wifun.inhand.response.model.ModelResponse.java
 * @Desc
 */
public class ModelResponse extends BaseResponse {

	private int total;
	private int cursor;
	private int limit;
	private List<JSONObject> result;

	public int getTotal() {

		return total;
	}

	public void setTotal(int total) {

		this.total = total;
	}

	public int getCursor() {

		return cursor;
	}

	public void setCursor(int cursor) {

		this.cursor = cursor;
	}

	public int getLimit() {

		return limit;
	}

	public void setLimit(int limit) {

		this.limit = limit;
	}

	public List<JSONObject> getResult() {

		return result;
	}

	public void setResult(List<JSONObject> result) {

		this.result = result;
	}

	public List<Model> getModelList() {

		if (null == this.result || this.result.size() == 0)
			return null;

		List<Model> modelList = new ArrayList<Model>();
		Model m = null;
		for (JSONObject jo : this.result) {
			m = new Model();
			m.set_id(jo.getString("_id"));
			m.setName(jo.getString("name"));
			modelList.add(m);
		}

		return modelList;
	}

	@Override
	public List<Field> getFieldList() {

		List<Field> fieldList = super.getFieldList();
		Field[] fs = ModelResponse.class.getDeclaredFields();

		for (Field f : fs) {
			fieldList.add(f);
		}

		return fieldList;
	}
}
