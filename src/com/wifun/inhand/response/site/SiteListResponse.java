package com.wifun.inhand.response.site;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.wifun.inhand.model.Site;
import com.wifun.inhand.response.BaseResponse;

/**
 * 车辆接口返回结果处理类
 * 
 * @File wifun-admin/com.wifun.inhand.response.site.SiteListResponse.java
 * @Desc
 */
public class SiteListResponse extends BaseResponse {

	private List<Site> sites = new ArrayList<Site>();

	@Override
	public List<Field> getFieldList() {

		List<Field> fieldList = super.getFieldList();
		Field[] fs = SiteListResponse.class.getDeclaredFields();

		for (Field f : fs) {
			fieldList.add(f);
		}

		return fieldList;
	}

	@Override
	public boolean fromJsonObject(JSONObject jsonObject) {

		boolean b = super.fromJsonObject(jsonObject);
		if (false == b)
			return false;

		if (jsonObject.containsKey("result") && jsonObject.getJSONArray("result").size() > 0) {

			JSONArray jsonArr = jsonObject.getJSONArray("result");
			for (int i = 0; i < jsonArr.size(); i++) {

				Site site = new Site(jsonArr.getJSONObject(i));
				sites.add(site);
			}

		}
		return true;
	}

	public List<Site> getSites() {
	
		return sites;
	}
}
