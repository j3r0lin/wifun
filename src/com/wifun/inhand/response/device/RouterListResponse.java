package com.wifun.inhand.response.device;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import com.wifun.inhand.model.Router;
import com.wifun.inhand.response.BaseResponse;

/**
 * 路由器设备列表接口返回结果处理类
 * 
 * @File wifun-admin/com.wifun.inhand.response.device.RouterListResponse.java
 * @Desc
 */
public class RouterListResponse extends BaseResponse {

	private List<Router> routers = new ArrayList<Router>();

	@Override
	public List<Field> getFieldList() {

		List<Field> fieldList = super.getFieldList();
		Field[] fs = RouterListResponse.class.getDeclaredFields();

		for (Field f : fs) {
			fieldList.add(f);
		}

		return fieldList;
	}

	@Override
	public boolean fromJsonObject(JSONObject jsonObject) {

		boolean b = super.fromJsonObject(jsonObject);
		if (false == b)
			return false;

		if (jsonObject.containsKey("result") && jsonObject.getJSONArray("result").size() > 0) {

			JSONArray jsonArr = jsonObject.getJSONArray("result");
			for (int i = 0; i < jsonArr.size(); i++) {

				Router router = new Router(jsonArr.getJSONObject(i));
				routers.add(router);
			}
		}
		return true;
	}

	public List<Router> getRouters() {

		return routers;
	}
}
