package com.wifun.inhand.response.device;

import java.lang.reflect.Field;
import java.util.List;

import net.sf.json.JSONObject;

import com.wifun.inhand.model.Router;
import com.wifun.inhand.response.BaseResponse;

/**
 * 路由器设备接口返回结果处理类
 * 
 * @File wifun-admin/com.wifun.inhand.response.device.RouterResponse.java
 * @Desc
 */
public class RouterResponse extends BaseResponse {

	private Router router;

	@Override
	public List<Field> getFieldList() {

		List<Field> fieldList = super.getFieldList();
		Field[] fs = RouterResponse.class.getDeclaredFields();

		for (Field f : fs) {
			fieldList.add(f);
		}

		return fieldList;
	}

	@Override
	public boolean fromJsonObject(JSONObject jsonObject) {

		boolean b = super.fromJsonObject(jsonObject);
		if (false == b)
			return false;

		if (jsonObject.containsKey("result")) {
			this.router = new Router(jsonObject.getJSONObject("result"));
		}
		return true;
	}

	public Router getRouter() {

		return router;
	}

	public void setRouter(Router router) {

		this.router = router;
	}
}
