package com.wifun.inhand.response.tag;

import java.lang.reflect.Field;
import java.util.List;

import net.sf.json.JSONObject;

import com.wifun.inhand.model.Tag;
import com.wifun.inhand.response.BaseResponse;

public class TagResponse extends BaseResponse {

	private Tag tag;

	@Override
	public List<Field> getFieldList() {

		List<Field> fieldList = super.getFieldList();
		Field[] fs = TagResponse.class.getDeclaredFields();

		for (Field f : fs) {
			fieldList.add(f);
		}

		return fieldList;
	}

	@Override
	public boolean fromJsonObject(JSONObject jsonObject) {

		boolean b = super.fromJsonObject(jsonObject);
		if (false == b)
			return false;

		if (jsonObject.containsKey("result")) {
			this.tag = new Tag(jsonObject.getJSONObject("result"));
		}
		return true;
	}

	public Tag getTag() {

		return tag;
	}

	public void setTag(Tag tag) {

		this.tag = tag;
	}
}
