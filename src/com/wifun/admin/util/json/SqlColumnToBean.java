package com.wifun.admin.util.json;

import java.lang.reflect.Field;
import java.util.List;

import org.apache.commons.beanutils.BeanUtils;
import org.hibernate.HibernateException;
import org.hibernate.transform.ResultTransformer;

import com.wifun.admin.util.EmptyUtil;

/**
 * sql结果集转换类
 * 
 * @author wanghc
 * 
 */
public class SqlColumnToBean<T extends Object> implements ResultTransformer {

	private static final long serialVersionUID = 1L;
	private final Class<? extends Object> resultClass;// 类型
	@SuppressWarnings("unused")
	private boolean isSimpleType = false;// 是否是简单类型

	public SqlColumnToBean(Class<T> resultClass) {

		if (resultClass == null)
			throw new IllegalArgumentException("类型为空");
		this.resultClass = resultClass;
		isSimpleType = ReflectionUtil.isPrimitive(resultClass);
	}

	@Override
	public Object transformTuple(Object[] tuple, String[] aliases) {

		Object result = null;
		try {
			result = resultClass.newInstance();
			for (int i = 0; i < aliases.length; i++) {
				String alias = aliases[i];
				if (alias != null) {
					String fieldName = getFieldName(alias);
					if (EmptyUtil.isEmpty(fieldName)) {
						continue;
					}
					BeanUtils.setProperty(result, fieldName, tuple[i]);
				}
			}
		} catch (Exception e) {
			throw new HibernateException("结果集不能转换成对象: " + resultClass.getName());
		}
		return result;

	}

	@SuppressWarnings("rawtypes")
	@Override
	public List transformList(List list) {

		return list;
	}

	/**
	 * 获取对象字段名称
	 * 
	 * @param alias
	 *            别名
	 * @return
	 */
	private String getFieldName(String alias) {

		Field[] fields = resultClass.getDeclaredFields();
		if (fields == null || fields.length == 0) {
			return null;
		}
		String proName = alias.replaceAll("_", "").toLowerCase();
		for (Field field : fields) {
			if (field.getName().toLowerCase().equals(proName)) {
				return field.getName();
			}
		}
		return null;
	}
}
