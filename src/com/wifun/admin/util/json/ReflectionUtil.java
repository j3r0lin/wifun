package com.wifun.admin.util.json;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import com.wifun.admin.util.EmptyUtil;

/**
 * 
 * @ClassName: ReflectionUtil
 * @Description: 反射工具类
 * @author wanghc
 */
public class ReflectionUtil {

	/**
	 * 根据类型得到所有的方法
	 * 
	 * @param clazz
	 *            类型
	 * @return 所有的方法
	 */
	public static Map<String, String> getClassMethods(Class<?> clazz) {

		Method[] methods = clazz.getMethods();
		Map<String, String> map = new HashMap<String, String>();
		for (Method method : methods) {
			map.put(method.getName(), method.getName());
		}
		return map;
	}

	public static boolean isMethodExistsByField(Map<String, String> methods, String fieldName) {

		if (methods != null
				&& (methods.get(methodNameByFieldName(fieldName, "get")) != null || methods
						.get(methodNameByFieldName(fieldName, "is")) != null)
				&& methods.get(methodNameByFieldName(fieldName, "set")) != null) {
			return true;
		}
		return false;
	}

	/**
	 * 判断是否是简单类型
	 * 
	 * @param clazz
	 *            类
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isPrimitive(Class clazz) {

		if (clazz.isPrimitive() || clazz == Long.class || clazz == Long.TYPE
				|| clazz == Integer.class || clazz == Integer.TYPE || clazz == Float.class
				|| clazz == Float.TYPE || clazz == Double.class || clazz == Double.TYPE
				|| clazz == Boolean.class || clazz == Boolean.TYPE || clazz == Character.class
				|| clazz == Character.TYPE || clazz == Byte.class || clazz == Byte.TYPE
				|| clazz == Short.class || clazz == Short.TYPE || clazz == String.class) {
			return true;
		}
		return false;
	}

	/**
	 * 根据类型判断是否是集合或接口及注解
	 * 
	 * @param clazz
	 *            类型
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isCollectionOrInterface(Class clazz) {

		if (clazz.isArray() || clazz.isEnum() || clazz == Map.class || clazz == Collection.class
				|| clazz == Set.class || clazz.isInterface() || clazz.isAnnotation()) {
			return true;
		}
		return false;
	}

	/**
	 * 根据属性名称和前缀得到方法名称
	 * 
	 * @param fieldName
	 *            属性名称
	 * @param prefix
	 *            方法前缀
	 * @return
	 */
	public static String methodNameByFieldName(String fieldName, String prefix) {

		if (EmptyUtil.isEmpty(fieldName))
			return "";
		return prefix + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
	}

	/**
	 * 根据属性名称得到方法
	 * 
	 * @param fieldName
	 *            属性名称
	 * @param prefix
	 *            前缀
	 * @param clazz
	 *            类型
	 * @return 方法
	 * @throws NoSuchMethodException
	 * @throws SecurityException
	 */
	@SuppressWarnings("rawtypes")
	public static Method methodByFieldName(Class clazz, String fieldName, boolean red)
			throws Exception {

		if (isMethodExistsByField(getClassMethods(clazz), fieldName)) {
			PropertyDescriptor descriptor = new PropertyDescriptor(fieldName, clazz);
			if (red) {
				return descriptor.getReadMethod();
			} else {
				return descriptor.getWriteMethod();
			}
		}
		return null;
	}
}
