package com.wifun.admin.util.json;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.opensymphony.xwork2.conversion.impl.DefaultTypeConverter;
import com.wifun.admin.util.EmptyUtil;

/**
 * 
 * @ClassName: JsonDateConverter
 * @Description: 自定义日期类型转换器
 * @author wanghc
 */
public class JsonDateConverter extends DefaultTypeConverter {

	private static final DateFormat[] ACCEPT_DATE_FORMATS = {
			new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"), new SimpleDateFormat("yyyy-MM-dd"),
			new SimpleDateFormat("dd/MM/yyyy"), new SimpleDateFormat("HH:mm:ss"),
			new SimpleDateFormat("yyyy/MM/dd") }; // 支持转换的日期格式

	@SuppressWarnings("rawtypes")
	@Override
	public Object convertValue(Object value, Class toType) {

		if (toType == Date.class) {
			if (EmptyUtil.isEmpty(value)) {
				return null;
			}
			String dateString = value.toString();
			for (DateFormat format : ACCEPT_DATE_FORMATS) {
				try {
					return format.parse(dateString);// 遍历日期支持格式，进行转换
				} catch (Exception e) {
					continue;
				}
			}
			return null;
		}
		return super.convertValue(value, toType);
	}
}
