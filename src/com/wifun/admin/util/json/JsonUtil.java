package com.wifun.admin.util.json;

import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import com.wifun.admin.util.EmptyUtil;

/**
 * 
 * @ClassName: JsonUtil
 * @Description: JSON工具类
 * @author wanghc
 */
public final class JsonUtil {

	/**
	 * 用正则格式化Json数据
	 * 
	 * @param json
	 *            Json
	 * @return 格式化后的数
	 */
	public static String baseFormatJson(String json) {

		if (EmptyUtil.isEmpty(json))
			return json;
		json = json.replaceAll("\\\\\"", "&quot;");
		Pattern pattern = Pattern.compile(":([.[^\"]]+?)[,\\}\\]]");
		Matcher matcher = pattern.matcher(json);
		while (matcher.find()) {
			String temp = matcher.group(1);
			if (EmptyUtil.isEmpty(temp))
				continue;
			json = json.replace(temp, "\"" + temp.replace("\"", "") + "\"");
		}
		return json;
	}

	/**
	 * 转换成对象时检查并格式化Json数据
	 * 
	 * @param json
	 * @return
	 */
	public static String objFormatJson(String json) {

		if (EmptyUtil.isEmpty(json))
			return null;
		try {
			JSONObject jsObject = JSONObject.fromObject(json);
			return jsObject.toString();
		} catch (Exception e) {
			return baseFormatJson(json);
		}
	}

	/**
	 * 数据或集合时检查并格式化Json数据
	 * 
	 * @param json
	 * @return
	 */
	public static String arrayFormatJson(String json) {

		if (EmptyUtil.isEmpty(json))
			return null;
		try {
			JSONArray array = JSONArray.fromObject(json);
			return array.toString();
		} catch (Exception e) {
			return baseFormatJson(json);
		}
	}

	/**
	 * 对Map的空值进行过虑
	 * 
	 * @param map
	 *            Map对象
	 * @return 过滤后的集合
	 */
	public static Map<String, Object> filter(Map<String, Object> map) {

		if (map == null || map.size() == 0)
			return null;
		Iterator<Entry<String, Object>> iterator = map.entrySet().iterator();
		Entry<String, Object> entry;
		Map<String, Object> target = new HashMap<String, Object>();
		while (iterator.hasNext()) {
			entry = iterator.next();
			if (!EmptyUtil.isEmpty(entry.getValue())) {
				if (!"null".equals(entry.getValue().toString().trim()))
					target.put(entry.getKey().trim(), entry.getValue());
			}
		}
		map.clear();
		map = null;
		return target;
	}

	/**
	 * 配置json-lib需要的excludes和datePattern.
	 * 
	 * @param excludes
	 *            不需要转换的属性数组
	 * @param datePattern
	 *            日期转换模式
	 * @return JsonConfig 根据excludes和dataPattern生成的jsonConfig，用于write
	 */
	public static JsonConfig configJson(String[] excludes, String datePattern) {

		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(excludes);
		jsonConfig.setIgnoreDefaultExcludes(false);
		jsonConfig.setAllowNonStringKeys(true);
		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor(datePattern));

		return jsonConfig;
	}
}
