package com.wifun.admin.util;

import org.apache.commons.lang.StringUtils;

public class HourUtils {

	public HourUtils() {

	}

	public static Integer strToMinute(String time, String timeType) {

		if (StringUtils.isBlank(time))
			return null;

		String[] times = time.trim().split(":");
		int h = Integer.valueOf(times[0]);// 小时
		int m = Integer.valueOf(times[1]);// 分钟

		if (h >= 13 || h < 0 || m < 0 || m > 59)
			return null;

		if ("AM".equalsIgnoreCase(timeType)) {
			if (h == 12)
				h = h - 12;
		}

		if ("PM".equalsIgnoreCase(timeType)) {
			if (h != 12)
				h = h + 12;
		}

		int hM = h * 60;// 小时转换成分钟

		int count = hM + m;// 总的时间

		// 如果总的时间大于了24小时，说明格式错误，返回null
		if (count >= 1440)
			return null;

		return count;
	}

	public static String[] minuteToStr(String timeStr) {

		if (StringUtils.isBlank(timeStr))
			return null;
		int time = Integer.valueOf(timeStr);
		return minuteToStr(time);
	}

	/**
	 * 根据分钟，返回属于AM PM 以及拼接的时刻 数组0保存的是AM PM 数组1保存的是拼接的时刻
	 * 
	 * @param time
	 * @return
	 */
	public static String[] minuteToStr(int time) {

		String[] result = new String[2];

		int hM = time / 60;// 小时
		int m = time % 60;// 分钟

		// 如果总的时间大于了24小时，说明数据错误，返回null
		if (time >= 1440)
			return null;

		if (hM >= 12) {
			result[0] = "PM";
			if (hM != 12)
				hM = hM - 12;
		} else {
			result[0] = "AM";
		}

		String hour = "";
		if (hM == 0)
			hour = "12";
		else if (hM < 10)
			hour = "0" + String.valueOf(hM);
		else
			hour = String.valueOf(hM);

		String minute = "";
		if (m < 10)
			minute = "0" + String.valueOf(m);
		else
			minute = String.valueOf(m);

		result[1] = hour + ":" + minute;

		return result;
	}

	/**
	 * 获取某年某月的最后一天
	 * 
	 * @param year
	 *            年
	 * @param month
	 *            月
	 * @return 最后一天
	 */
	public static int getLastDayOfMonth(int year, int month) {

		if (month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10
				|| month == 12) {
			return 31;
		}
		if (month == 4 || month == 6 || month == 9 || month == 11) {
			return 30;
		}
		if (month == 2) {
			if (isLeapYear(year)) {
				return 29;
			} else {
				return 28;
			}
		}
		return 0;
	}

	/**
	 * 是否闰年
	 * 
	 * @param year
	 * @return
	 */
	public static boolean isLeapYear(int year) {

		return (year % 4 == 0 && year % 100 != 0) || (year % 400 == 0);
	}

	public static String minuteToStr2(int time) {

		String[] result = HourUtils.minuteToStr(time);

		return result[1] + " " + result[0];
	}

	public static void main(String[] args) {

		try {

			int t = strToMinute("12:50", "AM");
			System.out.println(t);

			String[] test = minuteToStr(t);
			System.out.println(test[1] + " " + test[0]);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
