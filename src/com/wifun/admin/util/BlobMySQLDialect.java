package com.wifun.admin.util;

import java.sql.Types;

import org.hibernate.Hibernate;
import org.hibernate.dialect.MySQLDialect;

public class BlobMySQLDialect extends MySQLDialect {

	public BlobMySQLDialect() {

		super();
		registerHibernateType(Types.LONGNVARCHAR, Hibernate.TEXT.getName());
		registerHibernateType(Types.LONGVARCHAR, Hibernate.TEXT.getName());
		registerHibernateType(Types.DECIMAL, Hibernate.BIG_DECIMAL.getName());
		registerHibernateType(Types.REAL, Hibernate.FLOAT.getName());
	}
}
