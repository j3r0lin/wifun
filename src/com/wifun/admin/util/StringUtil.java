package com.wifun.admin.util;

import org.apache.commons.lang.StringUtils;

public class StringUtil {

	public static String getOneCharRandom() {

		String[] str = new String[] { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "a", "b",
				"c", "d", "e", "f", "g", "h", "i", "0", "1", "2", "3", "4", "5", "6", "7", "8",
				"9", "j", "k", "l", "m", "n", "o", "p", "q", "r", "0", "1", "2", "3", "4", "5",
				"6", "7", "8", "9", "s", "t", "u", "v", "w", "x", "y", "z" };

		int index = (int) Math.round(Math.random() * (str.length - 1) + 1);
		return str[index - 1];
	}

	public static int getOneSmallNumberRandom() {

		int[] arr = new int[] { 1, 2, 3 };
		int index = (int) Math.round(Math.random() * (arr.length - 1) + 1);

		return arr[index - 1];
	}

	public static int getOneNumberRandom() {

		int[] arr = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		int index = (int) Math.round(Math.random() * (arr.length - 1) + 1);

		return arr[index - 1];
	}

	public static String dealCreditCardNum(String cardNum) {

		String newCardNum = "";
		if (StringUtils.isBlank(cardNum)) {
			newCardNum = "";
		} else if (cardNum.length() <= 8) {
			newCardNum = cardNum;
		} else {
			newCardNum = cardNum.substring(0, 4) + "********"
					+ cardNum.substring(cardNum.length() - 4, cardNum.length());
		}
		return newCardNum;
	}

	public static String dealEmail(String email) {

		try {
			String e1 = email.substring(0, email.lastIndexOf("@") + 1);
			String e2 = email.substring(email.lastIndexOf("@") + 1);
			String e3 = e2.substring(e2.indexOf("."));
			String str = "";
			for (int i = 0; i < e2.length() - e3.length(); i++) {
				str = str + "*";
			}
			return e1 + str + e3;
		} catch (Exception e) {
			return email;
		}
	}

	public static String replaceBlank(String str) {

		str = StringUtils.trimToEmpty(str);
		return str.replaceAll(" +", " ");
	}
}
