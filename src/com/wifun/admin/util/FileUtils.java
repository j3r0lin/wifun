package com.wifun.admin.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.struts2.ServletActionContext;

public class FileUtils {

	public static int FILEBYTE = (1024 * 200);

	/**
	 * 复制单个文件
	 * 
	 * @param oldPathFile
	 *            准备复制的文件源
	 * @param newPathFile
	 *            拷贝到新绝对路径带文件名
	 * @return
	 * @throws IOException
	 */
	public static boolean copyFile(File oldPathFile, String newPathFile) throws IOException {

		@SuppressWarnings("unused")
		int bytesum = 0;
		int byteread = 0;

		if (oldPathFile.exists()) { // 文件存在时
			InputStream inStream = new FileInputStream(oldPathFile); // 读入原文件

			FileOutputStream fs = new FileOutputStream(newPathFile);
			byte[] buffer = new byte[FILEBYTE];
			while ((byteread = inStream.read(buffer)) != -1) {
				bytesum += byteread; // 字节数 文件大小
				fs.write(buffer, 0, byteread);
			}
			fs.flush();
			fs.close();
			inStream.close();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 
	 * Function:获取后缀
	 * 
	 * @author wanghc DateTime Oct 25, 2012 3:37:24 PM
	 * @param pictype
	 * @return
	 */
	public static String getPictype(String pictype) {

		return pictype.substring(pictype.lastIndexOf("."));
	}

	/**
	 * 单文件上传
	 * 
	 */
	public static String SingleFileUpload(File file, String fileFileName, String path) {

		if (file == null) {

		} else {
			String filename = fileFileName;
			String absolutepath = getFilePath(path);
			if (createDirectory(absolutepath)) {
				try {
					copyFile(file, absolutepath + "/" + filename);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return path + filename;
		}
		return null;
	}

	/**
	 * 拷贝到磁盘 指定路径
	 * 
	 * @param file
	 * @param fileFileName
	 * @param path
	 * @return
	 */
	public static String SingleFileUploadToDisk(File file, String fileFileName, String path) {

		if (file == null) {

		} else {
			String filename = fileFileName;
			String absolutepath = getFilePath(path);
			if (createDirectory(absolutepath)) {
				try {
					copyFile(file, absolutepath + "/" + filename);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return path + filename;
		}
		return null;
	}

	public static boolean createDirectory(String Directory) {

		File directory = new File(Directory);
		if (!directory.exists()) {
			if (directory.mkdirs())
				return true;
		}
		return true;

	}

	private static boolean flag = false;

	/**
	 * 删除单个文件
	 * 
	 * @param sPath
	 *            被删除文件的文件名
	 * @return 单个文件删除成功返回true，否则返回false
	 */
	public static boolean deleteFile(String sPath) {

		flag = false;
		File file = new File(sPath);
		// 路径为文件且不为空则进行删除
		if (file.isFile() && file.exists()) {
			file.delete();
			flag = true;
		}
		return flag;
	}

	/**
	 * 删除目录（文件夹）以及目录下的文件
	 * 
	 * @param sPath
	 *            被删除目录的文件路径
	 * @return 目录删除成功返回true，否则返回false
	 */
	public static boolean deleteDirectory(String sPath) {

		// 如果sPath不以文件分隔符结尾，自动添加文件分隔符
		if (!sPath.endsWith(File.separator)) {
			sPath = sPath + File.separator;
		}
		File dirFile = new File(sPath);
		// 如果dir对应的文件不存在，或者不是一个目录，则退出
		if (!dirFile.exists() || !dirFile.isDirectory()) {
			return false;
		}
		flag = true;
		// 删除文件夹下的所有文件(包括子目录)
		File[] files = dirFile.listFiles();
		for (int i = 0; i < files.length; i++) {
			// 删除子文件
			if (files[i].isFile()) {
				flag = deleteFile(files[i].getAbsolutePath());
				if (!flag)
					break;
			} // 删除子目录
			else {
				flag = deleteDirectory(files[i].getAbsolutePath());
				if (!flag)
					break;
			}
		}
		if (!flag)
			return false;
		// 删除当前目录
		if (dirFile.delete()) {
			return true;
		} else {
			return false;
		}
	}

	// 本地项目路径
	public static String getFilePath(String FILE_DEFAULT_PATH) {

		File file = new File(FILE_DEFAULT_PATH);
		if (!file.exists()) {
			file.mkdirs();
		}
		return ServletActionContext.getRequest().getRealPath(FILE_DEFAULT_PATH).replace("\\", "/");
	}

}
