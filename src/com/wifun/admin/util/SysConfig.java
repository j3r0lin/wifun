package com.wifun.admin.util;

import java.io.InputStream;
import java.util.Properties;

/**
 * 配置
 */
public class SysConfig {

	private static SysConfig instance = new SysConfig();

	private Properties settings = new Properties();

	public static SysConfig getInstance() {

		return instance;
	}

	protected SysConfig() {

		loadConfig();
	}

	private void loadConfig() {

		String filePath = "/config.conf";//
		InputStream is = getClass().getResourceAsStream(filePath);
		try {
			settings.load(is);
			is.close();
		} catch (Exception e) {
			System.err.println("不能读取属性文件. " + filePath + "请确保配置文件在CLASSPATH指定的路径中");
			e.printStackTrace();
			return;
		}
	}

	public static String getSetting(String key) {

		return getInstance().settings.getProperty(key);
	}

	private static final String MODEL_TEST = "test";

	public static boolean isTest() {

		if (MODEL_TEST.equalsIgnoreCase(getInstance().settings.getProperty(MODEL))) {
			return true;
		}
		return false;
	}

	private static final String MODEL = "environment.model";

	public static String getModel() {

		return getInstance().settings.getProperty(MODEL);
	}
}