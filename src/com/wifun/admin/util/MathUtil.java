package com.wifun.admin.util;

import java.math.BigDecimal;

public class MathUtil {

	/**
	 * byte(字节)根据长度转成和mb(兆字节)
	 * 
	 * @param bytes
	 *            scale 小数位数
	 * @return
	 */
	public static float bytes2MB(long bytes, int scale) {

		BigDecimal filesize = new BigDecimal(bytes);
		BigDecimal megabyte = new BigDecimal(1024 * 1024);
		float returnValue = filesize.divide(megabyte, scale, 4).floatValue();
		return returnValue;
	}
}
