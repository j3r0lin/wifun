package com.wifun.admin.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import com.wifun.admin.app.vo.DeviceLocation;

public class LocationFileUtil {

	public static String SAVE_PATH = SysConfig.getSetting("wifun.location.file.directory");

	public static void main(String[] args) throws IOException {

	}

	public static boolean writeLogsFile(String sn, List<DeviceLocation> list) throws IOException {

		try {

			String dir = SAVE_PATH + "/" + sn;
			File fileDir = new File(dir);
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}

			File file = new File(dir, TimeUtils.getStringDateShort() + ".log");
			FileWriter fileWriter = new FileWriter(file, true);
			BufferedWriter writer = new BufferedWriter(fileWriter);
			if (list != null) {
				for (int i = 0; i < list.size(); i++) {
					DeviceLocation params = list.get(i);
					// deviceId|sn|longitude|latitude|time
					String content = params.getDeviceId() + "|" + params.getSn() + "|"
							+ params.getLongitude() + "|" + params.getLatitude() + "|"
							+ params.getTime();
					writer.write(content + "\r\n");
				}
			}
			writer.flush();
			writer.close();
			fileWriter.close();
			return true;
		} catch (IOException e1) {
			e1.printStackTrace();
			return false;
		}
	}

	public static boolean writeLogFile(DeviceLocation location) throws IOException {

		try {

			String dir = SAVE_PATH + "/" + location.getSn();
			File fileDir = new File(dir);
			if (!fileDir.exists()) {
				fileDir.mkdirs();
			}

			File file = new File(dir, TimeUtils.getStringDateShort() + ".log");
			FileWriter fileWriter = new FileWriter(file, true);
			BufferedWriter writer = new BufferedWriter(fileWriter);
			if (location != null) {
				// deviceId|sn|longitude|latitude|speed|course|time
				String content = location.getDeviceId() + "|" + location.getSn() + "|"
						+ location.getLongitude() + "|" + location.getLatitude() + "|"
						+ location.getSpeed() + "|" + location.getCourse() + "|"
						+ location.getTime();
				writer.write(content + "\r\n");
			}
			writer.flush();
			writer.close();
			fileWriter.close();
			return true;
		} catch (IOException e1) {
			e1.printStackTrace();
			return false;
		}
	}

	/**
	 * 拷贝文件
	 * 
	 * @param logPath
	 * @param logCachePath
	 *            源文件
	 */
	public static void appendLogTxtFile(String logPath, String logCachePath) {

		try {
			File sourceFile = new File(logCachePath);
			if (sourceFile.exists()) {
				FileReader fileReader = new FileReader(sourceFile);
				BufferedReader bufread = new BufferedReader(fileReader);

				FileWriter fileWriter = new FileWriter(logPath, true);
				BufferedWriter writer = new BufferedWriter(fileWriter);

				char cache[] = new char[1024];
				int d = bufread.read(cache);
				while (d != -1) {
					String str = new String(cache, 0, d);
					writer.write(str); // 写一行
					writer.flush();
					d = bufread.read(cache);
				}
				writer.close();
				fileWriter.close();
				bufread.close();
				fileReader.close();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 删除文件
	 * 
	 * @param path
	 */
	public static void deldeteTxtFile(String path) {

		try {
			File file = new File(path);
			if (file.exists())
				file.delete();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<DeviceLocation> readDeviceLocations(String sn, String data) {

		String dir = SAVE_PATH + "/" + sn;
		String filePath = dir + "/" + data + ".log";

		List<DeviceLocation> deviceLocations = new ArrayList<DeviceLocation>();
		String[] datas = LocationFileUtil.readFile(filePath);

		for (int i = 0; i < datas.length; i++) {

			// deviceId|sn|longitude|latitude|time
			String[] dataArray = datas[i].split("\\|");
			DeviceLocation deviceLocation = new DeviceLocation();
			deviceLocation.setDeviceId(Integer.parseInt(dataArray[0]));
			deviceLocation.setSn(dataArray[1]);
			deviceLocation.setLongitude(Double.parseDouble(dataArray[2]));
			deviceLocation.setLatitude(Double.parseDouble(dataArray[3]));
			deviceLocation.setSpeed(Double.parseDouble(dataArray[4]));
			deviceLocation.setCourse(Double.parseDouble(dataArray[5]));
			deviceLocation.setTime(dataArray[6]);
			deviceLocations.add(deviceLocation);
		}

		return deviceLocations;
	}

	public static String[] readFile(String filePath) {

		List<String> list = new ArrayList<String>();
		File file = new File(filePath);
		if (!file.exists()) {
			return new String[] {};
		}
		BufferedReader bf = null;
		FileInputStream fileInpustream = null;
		InputStreamReader inputStreamReader = null;
		try {
			fileInpustream = new FileInputStream(file);
			inputStreamReader = new InputStreamReader(fileInpustream);
			bf = new BufferedReader(inputStreamReader);
			String line = null;
			line = bf.readLine();
			while (line != null) {
				list.add(line);
				line = bf.readLine();
			}
		} catch (Exception e4) {
			e4.printStackTrace();
		} finally {
			try {
				if (bf != null) {
					bf.close();
				}
				if (inputStreamReader != null) {
					inputStreamReader.close();
				}
				if (fileInpustream != null) {
					fileInpustream.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		String[] result = new String[list.size()];
		for (int i = 0; i < result.length; i++) {
			result[i] = list.get(i);
		}
		return result;
	}
}
