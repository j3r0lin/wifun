package com.wifun.admin.util;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BaseDb {

	// 数据库连接
	Connection conn = null;

	private final static String DRIVER = SysConfig.getInstance().getSetting(
			"datasource.driverClassName");
	private final static String URL = SysConfig.getInstance().getSetting("datasource.url");
	private final static String DUSER = SysConfig.getInstance().getSetting("datasource.username");
	private final static String DPASS = SysConfig.getInstance().getSetting("datasource.password");

	// 获取连接
	public Connection getConn() {

		try {
			Class.forName(DRIVER);
			conn = DriverManager.getConnection(URL, DUSER, DPASS);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return conn;
	}

	/**
	 * 返回一个不自动提交的数据库连接
	 * 
	 * @param autoCommit
	 *            : 是否自动提交
	 * @return
	 * @throws SQLException
	 */
	public Connection getConnection(boolean autoCommit) throws SQLException {

		Connection conn = null;
		try {
			conn = DriverManager.getConnection(URL, DUSER, DPASS);
			conn.setAutoCommit(autoCommit);
		} catch (SQLException e) {
			e.printStackTrace();
			if (null != conn) {
				try {
					if (!conn.isClosed()) {
						conn.close();
					}
					conn = null;
				} catch (SQLException sqlEx) {
					sqlEx.printStackTrace();
					throw new SQLException(sqlEx);
				}
			}
			throw new SQLException(e);
		}
		return conn;
	}

	public void closeConnection(Connection connection) throws SQLException {

		if (null == connection || connection.isClosed()) {
			return;
		}
		connection.close();
	}

	public void commit(Connection connection) throws SQLException {

		if (null == connection) {
			throw new SQLException("commit exception : The connection is null.");
		}
		connection.commit();
	}

	public void rollback(Connection connection) throws SQLException {

		if (null == connection) {
			throw new SQLException("rollback exception : The connection is null.");
		}
		connection.rollback();
	}

	/**
	 * 数据更新
	 * 
	 * @param sql
	 * @param list
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public int updateSQL(String sql, List list) {

		Connection conn = this.getConn();
		PreparedStatement pstmt = null;
		int flag = 0;
		try {
			pstmt = conn.prepareStatement(sql);
			if (list != null) {
				for (int i = 0; i < list.size(); i++) {
					pstmt.setObject(1 + i, list.get(i));
				}
			}
			flag = pstmt.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
				conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return flag;
	}

	/**
	 * 数据更新（抛出异常）
	 * 
	 * @param conn
	 * @param sql
	 * @param list
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public int updateSQLNonCloseConnection(Connection conn, String sql, List list) throws Exception {

		if (null == conn) {
			throw new SQLException(
					"updateSQLNonCloseConnection() exception : The connection is null.");
		}
		PreparedStatement pstmt = null;
		int flag = 0;
		try {
			pstmt = conn.prepareStatement(sql);
			if (list != null) {
				for (int i = 0; i < list.size(); i++) {
					pstmt.setObject(1 + i, list.get(i));
				}
			}
			flag = pstmt.executeUpdate();
		} catch (Exception e) {
			throw new Exception("updateSQLNonCloseConnection() exception.", e);
		} finally {
			try {
				pstmt.close();
			} catch (Exception e) {
				throw new Exception("updateSQLNonCloseConnection() exception.", e);
			}
		}
		return flag;
	}

	/**
	 * 统计总数
	 * 
	 * @param sql
	 * @param list
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public int countSQL(String sql, ArrayList list) {

		PreparedStatement pstmt = null;
		ResultSet resultset = null;
		Connection conn = this.getConn();
		try {
			pstmt = conn.prepareStatement(sql);
			if (list != null) {
				for (int i = 0; i < list.size(); i++) {
					pstmt.setObject(i + 1, list.get(i));
				}
			}
			resultset = pstmt.executeQuery();
			if (resultset.next()) {
				return resultset.getInt(1);
			}
			return 0;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
				resultset.close();
				conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;
	}

	/**
	 * 查询内容
	 * 
	 * @param sql
	 * @param list
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public Object querySQL(String sql, ArrayList list) {

		PreparedStatement pstmt = null;
		ResultSet resultset = null;
		Connection conn = this.getConn();
		try {
			pstmt = conn.prepareStatement(sql);
			if (list != null) {
				for (int i = 0; i < list.size(); i++) {
					pstmt.setObject(i + 1, list.get(i));
				}
			}
			resultset = pstmt.executeQuery();
			if (resultset.next()) {
				return resultset.getObject(1);
			}
			return 0;
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				pstmt.close();
				resultset.close();
				conn.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
}
