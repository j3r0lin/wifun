package com.wifun.admin.util;

import java.util.Date;

import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;
import net.sf.json.util.CycleDetectionStrategy;

import com.wifun.admin.util.json.DateJsonValueProcessor;

public class JSONBean {

	JSONObject json = null;

	private JSONBean() {

		json = new JSONObject();
	}

	public static JSONBean createJSONBean(String key, Object value) {

		JSONBean result = new JSONBean();
		return result.add(key, value);
	}

	public static JSONBean createJSONBean() {

		return new JSONBean();
	}

	public JSONBean add(String key, Object value) {

		json.put(key, value);
		return this;
	}

	/**
	 * 配置json-lib需要的excludes和datePattern.
	 * 
	 * @param excludes
	 *            不需要转换的属性数组
	 * @param datePattern
	 *            日期转换模式
	 * @return JsonConfig 根据excludes和dataPattern生成的jsonConfig，用于write
	 */
	public static JsonConfig configJson(String[] excludes, String datePattern) {

		JsonConfig jsonConfig = new JsonConfig();
		jsonConfig.setExcludes(excludes);
		jsonConfig.setIgnoreDefaultExcludes(false);
		jsonConfig.setAllowNonStringKeys(true);
		jsonConfig.setCycleDetectionStrategy(CycleDetectionStrategy.LENIENT);
		jsonConfig.registerJsonValueProcessor(Date.class, new DateJsonValueProcessor(datePattern));

		return jsonConfig;
	}

	public String toString() {

		return json.toString();
	}

	public static void main(String[] args) {

		System.out.println(JSONBean.createJSONBean("result", 1).add("type", 1).toString());
	}
}
