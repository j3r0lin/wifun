package com.wifun.admin.util;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class ZipUtils {

	static final int BUFFER = 8192;
	private static ZipUtils zipUtil = null;

	public static ZipUtils getInstance() {

		if (null == zipUtil)
			zipUtil = new ZipUtils();
		return zipUtil;
	}

	public void compress(String srcPathName, String destFile, String rootFileName) throws Exception {

		File file = new File(srcPathName);
		if (!file.exists())
			throw new Exception(srcPathName + "不存在！");
		try {
			File zipFile = new File(destFile);
			FileOutputStream fileOutputStream = new FileOutputStream(zipFile);
			ZipOutputStream out = new ZipOutputStream(fileOutputStream);
			String basedir = rootFileName;
			compress(file, out, basedir);
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	private void compress(File file, ZipOutputStream out, String basedir) {

		/* 判断是目录还是文件 */
		if (file.isDirectory()) {
			System.out.println("压缩：" + basedir + file.getName());
			this.compressDirectory(file, out, basedir);
		} else {
			System.out.println("压缩：" + basedir + file.getName());
			this.compressFile(file, out, basedir);
		}
	}

	/** 压缩一个目录 */
	private void compressDirectory(File dir, ZipOutputStream out, String basedir) {

		if (!dir.exists())
			return;

		File[] files = dir.listFiles();
		for (int i = 0; i < files.length; i++) {
			/* 递归 */
			compress(files[i], out, basedir + "/");
		}
	}

	/** 压缩一个文件 */
	private void compressFile(File file, ZipOutputStream out, String basedir) {

		if (!file.exists()) {
			return;
		}
		try {
			BufferedInputStream bis = new BufferedInputStream(new FileInputStream(file));
			ZipEntry entry = new ZipEntry(basedir + file.getName());
			out.putNextEntry(entry);
			int count;
			byte data[] = new byte[BUFFER];
			while ((count = bis.read(data, 0, BUFFER)) != -1) {
				out.write(data, 0, count);
			}
			bis.close();
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
