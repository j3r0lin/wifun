package com.wifun.admin.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.wifun.admin.app.vo.FileVo;

public class FileUtil {

	public static List<FileVo> getFiles(File root, String suffixList) {

		List<FileVo> fileList = new ArrayList<FileVo>();
		File[] files = root.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {

				// 递归调用
				List<FileVo> subList = getFiles(file, suffixList);
				fileList.addAll(subList);
			} else {
				if (suffixList.indexOf(getExtensionName(file.getName())) < 0)
					continue;

				FileVo fv = new FileVo();
				fv.setName(file.getName());
				fv.setPath(file.getAbsolutePath().replaceAll("\\\\", "/"));
				fv.setDirectory(file.isDirectory());
				fileList.add(fv);
			}
		}
		return fileList;
	}

	public static List<FileVo> loadFileList(File file, String suffixList, boolean forward) {

		try {
			List<FileVo> directoryList = new ArrayList<FileVo>();
			List<FileVo> fileList = new ArrayList<FileVo>();
			FileVo fv = null;
			if (forward) {
				fv = new FileVo();
				fv.setName("向上一级");
				fv.setPath(file.getParentFile().getAbsolutePath().replaceAll("\\\\", "/"));
				directoryList.add(fv);
			}

			File[] files = file.listFiles();
			for (int i = 0; i < files.length; i++) {
				// //后缀名过滤
				if (suffixList.indexOf("," + getExtensionName(files[i].getName()) + ",") < 0
						&& !files[i].isDirectory())
					continue;

				fv = new FileVo();
				fv.setName(files[i].getName());
				fv.setPath(files[i].getAbsolutePath().replaceAll("\\\\", "/"));
				fv.setDirectory(files[i].isDirectory());
				if (fv.isDirectory()) {
					directoryList.add(fv);
				} else
					fileList.add(fv);
			}

			directoryList.addAll(fileList);
			return directoryList;
		} catch (Exception e) {
			System.out.println("readfile()   Exception:" + e.getMessage());
			return null;
		}
	}

	public static String getExtensionName(String filename) {

		if ((filename != null) && (filename.length() > 0)) {
			int dot = filename.lastIndexOf('.');
			if ((dot > -1) && (dot < (filename.length() - 1))) {
				return filename.substring(dot + 1);
			}
		}
		return filename;
	}
}
