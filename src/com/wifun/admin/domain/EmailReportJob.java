package com.wifun.admin.domain;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.wifun.admin.app.vo.DailyReportCompanyVo;
import com.wifun.admin.app.vo.DailyReportVo;
import com.wifun.admin.domain.dao.ICompanyDao;
import com.wifun.admin.domain.dao.IMediaListDao;
import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.model.MediaList;
import com.wifun.admin.domain.model.TaskPvuvMovie;
import com.wifun.admin.domain.service.IReportService;
import com.wifun.admin.domain.util.DailyReportExcelCreater;
import com.wifun.admin.domain.util.MoviePvuvExcelCreater;
import com.wifun.admin.domain.util.SendMail;
import com.wifun.admin.util.SysConfig;
import com.wifun.admin.util.TimeUtils;

/**
 * 日报邮件发送定时任务,每天凌晨5点执行
 * 
 * @File wifun-admin/com.wifun.admin.domain.EmailReportJob.java
 * @Desc
 */
@Component
public class EmailReportJob {

	@Resource(name = "companyDao")
	private ICompanyDao companyDao;

	@Resource(name = "reportService")
	private IReportService reportService;

	@Resource(name = "mediaListDao")
	private IMediaListDao mediaListDao;

	private String yesterday = TimeUtils.getFormatterDate(TimeUtils.getYesterday(), "yyyy-MM-dd");

	// 每天5点执行
	// @Scheduled(cron = "0 0/1 *  * * ?")
	@Scheduled(cron = "0 0 5 ? * *")
	public void schedule() {

		System.out.println("**************email job start***************");
		yesterday = TimeUtils.getFormatterDate(TimeUtils.getYesterday(), "yyyy-MM-dd");
		sendDailyReport();
		sendMoviePvuvReport();
		System.out.println("**************email job end***************");
	}

	/**
	 * 发送电影PVVU报表
	 */
	private void sendMoviePvuvReport() {

		String filePath = SysConfig.getSetting("wifun.report.file.directory");
		String fileName = "movie_" + yesterday + ".xls";
		String subject = "Wifun Movie PVUV Report "
				+ TimeUtils.getFormatterDate(TimeUtils.getYesterday(), "MM/dd/yy");
		if (createMoviePvuvExcel(fileName, filePath))
			try {
				sendEmail(fileName, filePath, subject);
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	/**
	 * 生成电影pvuv报表
	 * 
	 * @param fileName
	 * @param filePath
	 * @return
	 */
	private boolean createMoviePvuvExcel(String fileName, String filePath) {

		List<TaskPvuvMovie> sumReportList = reportService.getMoivePvuvSum(yesterday);
		Map<String, List<TaskPvuvMovie>> reportMap = new HashMap<String, List<TaskPvuvMovie>>();

		List<MediaList> mediaLists = mediaListDao.getActiveMediaList();
		for (int i = 0; i < mediaLists.size(); i++) {

			MediaList mediaList = mediaLists.get(i);
			List<TaskPvuvMovie> reportList = reportService.getMoivePvuvByMediaList(
					mediaList.getId(), yesterday);

			if (reportList != null && reportList.size() > 0)
				reportMap.put(mediaList.getTitle(), reportList);
		}

		return MoviePvuvExcelCreater.create(filePath, fileName, sumReportList, reportMap);
	}

	/**
	 * 发送每日运营报表
	 */
	private void sendDailyReport() {

		String filePath = SysConfig.getSetting("wifun.report.file.directory");
		String fileName = "report_" + yesterday + ".xls";
		String subject = " Wifun Daily Report "
				+ TimeUtils.getFormatterDate(TimeUtils.getYesterday(), "MM/dd/yy");
		if (createDailyReportExcel(fileName, filePath))
			try {
				sendEmail(fileName, filePath, subject);
			} catch (Exception e) {
				e.printStackTrace();
			}
	}

	/**
	 * 生成每日运营报表excel文件
	 * 
	 * @param fileName
	 * @param filePath
	 * @return
	 */
	private boolean createDailyReportExcel(String fileName, String filePath) {

		String date = TimeUtils.getFormatterDate(TimeUtils.getYesterday(), "MM/dd/yyyy");
		List<DailyReportCompanyVo> sumReportList = reportService.getCompanyDailyReport(date);
		Map<String, List<DailyReportVo>> reportMap = new HashMap<String, List<DailyReportVo>>();
		List<Company> companys = companyDao.getCompanyList(true, null);

		for (int i = 0; i < companys.size(); i++) {

			Company company = companys.get(i);
			String companyName = company.getName();
			int companyId = company.getId();

			if (companyId == 56) // 测试账号的数据不做统计
				continue;

			List<DailyReportVo> reportList = reportService.getReportDailyVo(companyId, -1, date);
			if (reportList != null && reportList.size() > 0)
				reportMap.put(companyName, reportList);
		}

		return DailyReportExcelCreater.create(filePath, fileName, sumReportList, reportMap);
	}

	/**
	 * 发送邮件
	 * 
	 * @param fileName
	 * @param savePath
	 * @param subject
	 * @return
	 * @throws Exception
	 */
	private boolean sendEmail(String fileName, String savePath, String subject) throws Exception {

		String excelPath = savePath + "/" + fileName;
		try {
			SendMail cn = new SendMail();

			String sendEmailStmp = SysConfig.getSetting("send.email.stmp");
			String sendEmail = SysConfig.getSetting("send.email.address");
			String sendEmailPwd = SysConfig.getSetting("send.email.pwd");
			String receiver = SysConfig.getSetting("receiver.email");

			// 接收人
			List<String> toList = new ArrayList<String>();
			String[] array = receiver.split(",");
			for (String string : array) {
				if (string != null && StringUtils.isNotBlank(string.trim())) {
					toList.add(string);
				}
			}

			// 设置发件人地址、收件人地址和邮件标题
			cn.setAddress(sendEmail, toList, subject, "", "Wifun Daily Report");
			// 设置要发送附件的位置和标题
			cn.setAffix(excelPath, fileName);

			// 设置smtp服务器以及邮箱的帐号和密码
			cn.send(sendEmailStmp, sendEmail, sendEmailPwd);
			return true;

		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}