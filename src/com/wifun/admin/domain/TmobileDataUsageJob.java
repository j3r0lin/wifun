package com.wifun.admin.domain;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Date;

import javax.annotation.Resource;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.wifun.admin.domain.dao.ITaskTmobileDataUsageDao;
import com.wifun.admin.domain.model.TaskTmobileDataUse;
import com.wifun.admin.util.SysConfig;
import com.wifun.admin.util.TimeUtils;

/*
 * Tmobile data usage 每日定时任务
 * 读取每日采集的数据文件，分析，入库。
 */
@Component
public class TmobileDataUsageJob {

	@Resource(name = "taskTmobileDataUsageDao")
	private ITaskTmobileDataUsageDao taskTmobileDataUsageDao;

	private String date = TimeUtils.getFormatterDate(TimeUtils.getYesterday(), "yyyy-MM-dd");

	// 每天2点执行
	// @Scheduled(cron = "0 0/1 *  * * ?")
	@Scheduled(cron = "0 0 2 ? * *")
	public void schedule() {

		System.out.println("**************Tmobile datausage job start***************");
		date = TimeUtils.getFormatterDate(TimeUtils.getYesterday(), "yyyy-MM-dd");
		fetchTmobileData();
		System.out.println("**************Tmobile datausage job end***************");
	}

	/*
	 * 从ftp服务器上download，当前日期的Tmobile 手机卡 流量usage统计文件，文件名为tmobileyyyymmdd.txt
	 * 读文件，将结果保存到数据库中。
	 */
	private void fetchTmobileData() {

		String filePath = SysConfig.getSetting("wifun.tmobile.file.directory");// /ubt/www/tmobile
		String fileName = "tmobile" + date + ".txt";

		StringBuilder sb = new StringBuilder();
		// 读文件获取内容
		try {
			String encoding = "utf-8";
			File file = new File(filePath + "/" + fileName);
			if (file.isFile() && file.exists()) { // 判断文件是否存在
				InputStreamReader read = new InputStreamReader(new FileInputStream(file), encoding);
				BufferedReader bufferedReader = new BufferedReader(read);
				String lineTxt = null;
				while ((lineTxt = bufferedReader.readLine()) != null) {

					System.out.println(lineTxt);
					sb.append(lineTxt);
				}
				read.close();
			} else {
				System.out.println("找不到指定的文件");
			}
		} catch (Exception e) {
			System.out.println("读取文件内容出错");
			e.printStackTrace();
		}

		// String rs =
		// "[{\"billCycle\":\"07/21/15-08/20/15\",\"chargesInclude\":\"$0.00\",\"chargesReduced\":\"$0.00\",\"currentUsageTime\":\"08/04/15 09:04:43 PM\",\"dataUsageAll\":\"1.6G\",\"includedInclude\":\"Unlimited\",\"includedReduced\":\"Unlimited\",\"phoneNum\":\"202-763-1427\",\"remainingInclude\":\"Unlimited\",\"remainingReduced\":\"Unlimited\",\"usedInclude\":\"1 GB\",\"usedReduced\":\"541.1 MB\"},{\"billCycle\":\"07/21/15-08/20/15\",\"chargesInclude\":\"$0.00\",\"chargesReduced\":\"$0.00\",\"currentUsageTime\":\"08/04/15 09:04:43 PM\",\"dataUsageAll\":\"1.6G\",\"includedInclude\":\"Unlimited\",\"includedReduced\":\"Unlimited\",\"phoneNum\":\"202-763-8888\",\"remainingInclude\":\"Unlimited\",\"remainingReduced\":\"Unlimited\",\"usedInclude\":\"1 GB\",\"usedReduced\":\"541.1 MB\"}]";
		String rs = sb.toString();

		if ((null != rs) && (rs.length() > 0)) {
			JSONArray jsl = JSONArray.fromObject(rs);

			for (int i = 0; i < jsl.size(); i++) {

				JSONObject jsobj = (JSONObject) jsl.get(i);
				TaskTmobileDataUse smVo = (TaskTmobileDataUse) JSONObject.toBean(jsobj,
						TaskTmobileDataUse.class);
				smVo.setCreateTime(TimeUtils.getNowDateShort());
				// 插入data-time
				String dtTime = smVo.getCurrentUsageTime();
				if (null != dtTime) {

					String tmpStr = dtTime.substring(0, dtTime.indexOf(" "));
					Date dt = TimeUtils.strUsTwoDate(tmpStr);
					smVo.setDataTime(dt);
				}

				taskTmobileDataUsageDao.save(smVo);
			}
		}
	}
}
