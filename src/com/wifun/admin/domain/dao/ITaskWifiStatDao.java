package com.wifun.admin.domain.dao;

import com.wifun.admin.domain.model.TaskWifiStat;

public interface ITaskWifiStatDao extends IBaseDao<TaskWifiStat, Integer> {

	TaskWifiStat getTaskWifiStat(String date, String type, int deviceId);
}
