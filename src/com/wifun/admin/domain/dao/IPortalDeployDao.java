package com.wifun.admin.domain.dao;

import java.util.List;

import com.wifun.admin.domain.model.PortalDeploy;

public interface IPortalDeployDao extends IBaseDao<PortalDeploy, Integer> {

	/**
	 * 更新发布信息
	 * 
	 * @param id
	 * @param publishPointId
	 * @param publishId
	 * @return
	 */
	public boolean updatePublishInfo(Integer id, String publishPointId, String publishId);

	/**
	 * 查询发布信息
	 * 
	 * @param companyId
	 * @param description
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PortalDeploy> searchPortal(int companyId, String description, String startDate,
			String endDate);

	/**
	 * 更新错误信息
	 * 
	 * @param id
	 * @return
	 */
	boolean updatePublishError(Integer id);
}
