package com.wifun.admin.domain.dao;

import java.util.List;

import com.wifun.admin.domain.model.Company;

public interface ICompanyDao extends IBaseDao<Company, Integer> {

	/**
	 * 根据account 查询comapny
	 * 
	 * @param account
	 * @return
	 */
	public Company getCompanyByAccount(String account);

	/**
	 * 根据name 模糊查询comapny
	 * 
	 * @param active
	 * @param name
	 * @return
	 */
	public List<Company> getCompanyList(boolean active, String name);

	/**
	 * 根据inhandId 得到company
	 * 
	 * @param companyId
	 * @param inhandId
	 * @return
	 */
	public boolean updateInhandId(Integer companyId, String inhandId);

	/**
	 * 更新company 某个字段
	 * 
	 * @param columnName
	 * @param columnValue
	 * @param id
	 * @return
	 */
	public boolean updateColumn(String columnName, Object columnValue, Integer id);

	/**
	 * 根据名字 得到company
	 * 
	 * @param name
	 * @return
	 */
	public Company getCompanyByName(String name);

	/**
	 * 根据email 得到company
	 * 
	 * @param email
	 * @return
	 */
	public Company getCompanyByEmail(String email);

}
