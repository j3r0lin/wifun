package com.wifun.admin.domain.dao;

import java.util.List;

import com.wifun.admin.domain.model.MediaDeploy;

public interface IMediaDeployDao extends IBaseDao<MediaDeploy, Integer> {

	/**
	 * 更新 发布信息
	 * 
	 * @param id
	 * @param publishPointId
	 * @param publishId
	 * @param version
	 * @return
	 */
	boolean updatePublishInfo(Integer id, String publishPointId, String publishId, String version);

	/**
	 * 查询媒体资源发布列表
	 * 
	 * @param companyId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<MediaDeploy> searchNetDeployList(int companyId, String startDate, String endDate);

	/**
	 * 更新发布错误信息
	 * 
	 * @param id
	 * @return
	 */
	boolean updatePublishError(Integer id);

	/**
	 * 查询sd卡发布信息
	 * 
	 * @param companyId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<MediaDeploy> searchSDDeployList(int companyId, String startDate, String endDate);

	/**
	 * 通过版本号查询 媒体资源发布信息
	 * 
	 * @param version
	 * @return
	 */
	MediaDeploy getMediaDeployByVersion(String version);
}
