package com.wifun.admin.domain.dao;

import com.wifun.admin.domain.model.TaskTmobileDataUse;

public interface ITaskTmobileDataUsageDao extends IBaseDao<TaskTmobileDataUse, Integer> {

}
