package com.wifun.admin.domain.dao;

import java.util.List;

import com.wifun.admin.domain.model.MediaList;

public interface IMediaListDao extends IBaseDao<MediaList, Integer> {

	/**
	 * 查询所有的 媒体
	 * 
	 * @param title
	 * @return
	 */
	public List<MediaList> findAllMedias(String title);

	/**
	 * 通过公司查询媒体
	 * 
	 * @param title
	 * @param companyId
	 * @return
	 */
	public List<MediaList> findMediasByCompany(String title, int companyId);

	/**
	 * 查询所有媒体
	 * 
	 * @return
	 */
	List<MediaList> getActiveMediaList();
}
