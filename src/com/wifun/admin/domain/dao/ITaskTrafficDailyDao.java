package com.wifun.admin.domain.dao;

import java.util.List;

import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.model.TaskTrafficDaily;

public interface ITaskTrafficDailyDao extends IBaseDao<TaskTrafficDaily, Integer> {

	public List<TaskTrafficDaily> getTaskTrafficDailyList();
}
