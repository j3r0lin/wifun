package com.wifun.admin.domain.dao;

import java.util.List;

import com.wifun.admin.domain.model.TaskPvuvMovie;

public interface ITaskPVUVMovieDao extends IBaseDao<TaskPvuvMovie, Integer> {

	/**
	 * 根据 公司和 日期 得到puuv 统计
	 * 
	 * @param companyId
	 * @param date
	 * @return
	 */
	List<TaskPvuvMovie> getMoivePvuvList(int companyId, String date);

	/**
	 * 根据媒体资源列表 日期得到 pvuv 统计
	 * 
	 * @param mediaListId
	 * @param date
	 * @return
	 */
	List<TaskPvuvMovie> getMoivePvuvByMediaList(int mediaListId, String date);

	/**
	 * 得到电影 pvuv 统计
	 * 
	 * @param date
	 * @return
	 */
	List<TaskPvuvMovie> getMoivePvuvSum(String date);
}
