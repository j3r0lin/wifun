package com.wifun.admin.domain.dao;

import java.util.List;

import com.wifun.admin.domain.model.MediaCategory;

public interface IMediaCategoryDao extends IBaseDao<MediaCategory, Integer> {

	public List<MediaCategory> getMediaCategoryByMediaId(int mediaId);
}
