package com.wifun.admin.domain.dao;

import java.util.List;
import java.util.Map;

import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.Group;

public interface IDeviceDao extends IBaseDao<Device, Integer> {

	/**
	 * 根据巴士公司查询设备
	 * 
	 * @param companyId
	 * @return
	 */
	public List<Device> loadByCompany(Integer companyId);

	/**
	 * 根据GroupId 查询设备信息
	 * 
	 * @param groupId
	 * @return
	 */
	public List<Device> loadByGroup(Integer groupId);

	/**
	 * 根据Group 查询设备
	 * 
	 * @param group
	 * @return
	 */
	public List<Device> loadByGroup(Group group);

	/**
	 * 更新设备 信息 通过字段
	 * 
	 * @param columnName
	 * @param columnValue
	 * @param id
	 * @return
	 */
	public boolean updateColumn(String columnName, Object columnValue, Integer id);

	/**
	 * 根据条件查询设备信息
	 * 
	 * @param active
	 * @param companyId
	 * @param groupId
	 * @param sn
	 * @param name
	 * @return
	 */
	public List<Device> searchDevices(boolean active, Integer companyId, int groupId, String sn,
			String name);

	/**
	 * 通过companyId 获取设备数量
	 * 
	 * @param companyId
	 * @return
	 */
	public Map<String, Object> getDeviceNumByCompany(int companyId);

	/**
	 * 根据 comapnyId ,groupId 获取设备数量
	 * 
	 * @param companyId
	 * @param groupId
	 * @return
	 */
	public Map<String, Object> getDeviceNumByGroup(int companyId, int groupId);

	/**
	 * 得到设备列表
	 * 
	 * @param companyId
	 * @param groupId
	 * @return
	 */
	public List<Device> getDeviceList(int companyId, int groupId);

	/**
	 * 根据sn 得到设备信息
	 * 
	 * @param sn
	 * @return
	 */
	public Device getDeviceBySn(String sn);
}
