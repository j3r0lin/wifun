package com.wifun.admin.domain.dao;

import java.util.List;

import com.wifun.admin.domain.model.Portal;

public interface IPortalDao extends IBaseDao<Portal, Integer> {

	/**
	 * 查询Portal 信息
	 * 
	 * @param companyId
	 * @param portalName
	 * @return
	 */
	List<Portal> searchPortal(Integer companyId, String portalName);

	/**
	 * 得到Portal 信息
	 * 
	 * @param companyId
	 * @return
	 */
	List<Portal> getPortalByCompanyId(Integer companyId);

}
