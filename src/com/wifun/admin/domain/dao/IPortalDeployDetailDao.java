package com.wifun.admin.domain.dao;

import com.wifun.admin.domain.model.PortalDeployDetail;

public interface IPortalDeployDetailDao extends IBaseDao<PortalDeployDetail, Integer> {

}
