package com.wifun.admin.domain.dao;

import java.util.List;

import com.wifun.admin.domain.model.Media;

public interface IMediaDao extends IBaseDao<Media, Integer> {

	/**
	 * 通过mediaListId 得到电影
	 * 
	 * @param mediaListId
	 * @return
	 */
	List<Media> findMediaByMediaListId(Integer mediaListId);

	List<Media> findMediaExceptMediaListId(Integer mediaListId);

	/**
	 * 更新MovieUrl
	 * 
	 * @param mediaId
	 * @param movieUrl
	 * @return
	 */
	boolean updateMovieUrl(Integer mediaId, String movieUrl);

	/**
	 * 更新 HPosterUrl
	 * 
	 * @param mediaId
	 * @param posterUrl
	 * @return
	 */
	boolean updateHPosterUrl(Integer mediaId, String posterUrl);

	/**
	 * 更新 VPosterUrl
	 * 
	 * @param mediaId
	 * @param posterUrl
	 * @return
	 */
	boolean updateVPosterUrl(Integer mediaId, String posterUrl);

	List<Media> search(Integer categoryId, String title);

	/**
	 * 按照语言查询电影
	 * 
	 * @param language
	 * @return
	 */
	List<Media> getMediasByLanguage(int language);
}
