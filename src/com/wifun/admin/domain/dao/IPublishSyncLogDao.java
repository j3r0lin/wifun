package com.wifun.admin.domain.dao;

import com.wifun.admin.domain.model.PublishSyncLog;

public interface IPublishSyncLogDao extends IBaseDao<PublishSyncLog, Integer> {

	/**
	 * 更新发布状态
	 * 
	 * @param deployId
	 * @param flag
	 * @return
	 */
	public boolean updateMediaPublishState(Integer deployId, String flag);

	/**
	 * 更新对应状态
	 * 
	 * @param deployId
	 * @param flag
	 * @return
	 */
	public boolean updatePortalPublishState(Integer deployId, String flag);
}
