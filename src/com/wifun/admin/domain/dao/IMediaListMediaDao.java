package com.wifun.admin.domain.dao;

import java.util.List;

import com.wifun.admin.domain.model.MediaListMedia;

public interface IMediaListMediaDao extends IBaseDao<MediaListMedia, Integer> {

	/**
	 * 根据 mediaListId 获取媒体资源列表
	 * 
	 * @param mediaListId
	 * @return
	 */
	public List<MediaListMedia> getMediaListMediaByMediaListId(int mediaListId);
}
