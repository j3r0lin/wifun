package com.wifun.admin.domain.dao;

import java.util.List;
import java.util.Map;

import com.wifun.admin.domain.model.Group;

public interface IGroupDao extends IBaseDao<Group, Integer> {

	/**
	 * 根据companyId 获取分组信息
	 * 
	 * @param companyId
	 * @return
	 */
	public List<Group> getGroupsByCompanyId(Integer companyId);

	/**
	 * 根据条件 获取group 列表
	 * 
	 * @param companyId
	 * @param groupName
	 * @return
	 */
	public List<Group> searchGroups(Integer companyId, String groupName);

	/**
	 * 更新 Group 信息，通过单个字段
	 * 
	 * @param columnName
	 * @param columnValue
	 * @param id
	 * @return
	 */
	public boolean updateColumn(String columnName, Object columnValue, Integer id);

	/**
	 * 根据companyId，name 获取单个 Group 信息
	 * 
	 * @param companyId
	 * @param name
	 * @return
	 */
	public Group getGroupByName(int companyId, String name);
}
