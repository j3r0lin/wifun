package com.wifun.admin.domain.dao;

import java.util.List;

import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.model.TaskDeviceSumDaily;
import com.wifun.admin.domain.model.TaskTrafficDaily;

public interface ITaskDeviceSumDailyDao extends IBaseDao<TaskDeviceSumDaily, Integer> {

	public List<TaskDeviceSumDaily> getTaskDeviceSumDailyList();
}
