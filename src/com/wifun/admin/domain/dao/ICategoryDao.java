package com.wifun.admin.domain.dao;

import java.util.List;

import com.wifun.admin.domain.model.Category;

public interface ICategoryDao extends IBaseDao<Category, Integer> {

	/**
	 * 获取电影分类列表
	 * 
	 * @param active
	 * @return
	 */
	List<Category> getCategoryList(boolean active);

}
