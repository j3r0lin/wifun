package com.wifun.admin.domain.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.hibernate.LockMode;
import org.hibernate.criterion.DetachedCriteria;

public interface IBaseDao<T, PK extends Serializable> {

	// -------------------- 基本检索、增加、修改、删除操作 --------------------

	// 根据主键获取实体。如果没有相应的实体，返回 null。
	public T get(PK id);

	// 根据主键获取实体。如果没有相应的实体，抛出异常。
	public T load(PK id);

	// 根据主键获取实体并加锁。如果没有相应的实体，抛出异常。
	public T loadWithLock(PK id, LockMode lock);

	// 获取全部实体。
	public List<T> loadAll();

	// 更新实体
	public T update(T entity);

	// 更新实体并加锁
	public void updateWithLock(T entity, LockMode lock);

	// 存储实体到数据库
	public T save(T entity);

	// 增加或更新实体
	public void saveOrUpdate(T entity);

	// 增加或更新集合中的全部实体
	public void saveOrUpdateAll(Collection<T> entities);

	// 删除指定的实体
	public void delete(T entity);

	// 加锁并删除指定的实体
	public void deleteWithLock(T entity, LockMode lock);

	// 根据主键删除指定实体
	public void deleteByKey(PK id);

	// 根据主键加锁并删除指定的实体
	public void deleteByKeyWithLock(PK id, LockMode lock);

	// 删除集合中的全部实体
	public void deleteAll(Collection<T> entities);

	// -------------------- HSQL ----------------------------------------------

	// 使用HSQL语句检索数据
	public List find(String queryString);

	// 使用带参数的HSQL语句检索数据
	public List find(String queryString, Object[] values);

	// 使用带命名的参数的HSQL语句检索数据
	public List findByNamedParam(String queryString, String[] paramNames, Object[] values);

	// 使用命名的HSQL语句检索数据
	public List findByNamedQuery(String queryName);

	// 使用带参数的命名HSQL语句检索数据
	public List findByNamedQuery(String queryName, Object value);

	// 使用带参数数组的命名HSQL语句检索数据
	public List findByNamedQuery(String queryName, Object[] values);

	// 使用带命名参数的命名HSQL语句检索数据
	public List findByNamedQueryAndNamedParam(String queryName, String[] paramNames, Object[] values);

	// 使用带参数的HSQL语句检索数据 分页
	public List find(final String queryString, Object[] values, int firstResult, int maxResult);

	// 使用HSQL语句检索数据，返回 Iterator
	public Iterator iterate(String queryString);

	// 使用带参数HSQL语句检索数据，返回 Iterator
	public Iterator iterate(String queryString, Object[] values);

	// 关闭检索返回的 Iterator
	public void closeIterator(Iterator it);

	// -------------------------------- Criteria ------------------------------

	// 检索满足标准的数据
	public List findByCriteria(DetachedCriteria criteria);

	// 检索满足标准的数据，返回指定范围的记录
	public List findByCriteria(DetachedCriteria criteria, int firstResult, int maxResults);

	// 按照实体中不为空的属性，与数据库中的数据进行匹配查找
	public List<T> findByExample(T entity);

	// 使用指定的检索标准获取满足标准的记录数
	public Integer getRowCount(DetachedCriteria criteria);

	// 使用指定的检索标准检索数据，返回指定统计值(max,min,avg,sum)
	public Object getStatValue(DetachedCriteria criteria, String propertyName, String StatName);

	// -------------------------------- Others --------------------------------

	// 加锁指定的实体
	public void lock(T entity, LockMode lock);

	// 强制初始化指定的实体
	public void initialize(Object proxy);

	// 强制立即更新缓冲数据到数据库（否则仅在事务提交时才更新）
	public void flush();

	/**
	 * 检索满足标准的数据，如果存在返回第一条数据，不存在返回null
	 * 
	 * @param criteria
	 * @return T
	 */
	public T findFirstByCriteria(DetachedCriteria criteria);

}
