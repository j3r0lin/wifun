package com.wifun.admin.domain.dao;

import com.wifun.admin.domain.model.NotifyLog;

public interface INotifyLogDao extends IBaseDao<NotifyLog, Integer> {

}
