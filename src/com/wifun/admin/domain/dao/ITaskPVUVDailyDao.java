package com.wifun.admin.domain.dao;

import java.util.List;

import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.model.TaskPVUVDaily;
import com.wifun.admin.domain.model.TaskTrafficDaily;

public interface ITaskPVUVDailyDao extends IBaseDao<TaskPVUVDaily, Integer> {

	public List<TaskPVUVDaily> getTaskPVUVDailyList();
}
