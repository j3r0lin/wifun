package com.wifun.admin.domain.dao;

import java.util.List;

import com.wifun.admin.app.vo.DailyReportCompanyVo;
import com.wifun.admin.domain.model.TaskReportDaily;

//TaskReportDaily Inteface
public interface ITaskReportDailyDao extends IBaseDao<TaskReportDaily, Integer> {

	/**
	 * 获取报表列表
	 * 
	 * @param companyId
	 * @param groupId
	 * @param date
	 * @return
	 */
	List<TaskReportDaily> getListByCompanyIdAndDate(int companyId, int groupId, String date);

	/**
	 * 获取巴士公司 报表
	 * 
	 * @param date
	 * @return
	 */
	List<DailyReportCompanyVo> getCompanyDailyReport(String date);
}
