package com.wifun.admin.domain.dao;

import java.util.List;
import java.util.Map;

import com.wifun.admin.app.vo.SDDeployDetailVo;
import com.wifun.admin.domain.model.MediaDeployDetail;

public interface IMediaDeployDetailDao extends IBaseDao<MediaDeployDetail, Integer> {

	/**
	 * 通过设备Id，媒体Id，状态等 更新状态
	 * 
	 * @param version
	 * @param deviceId
	 * @param mediaId
	 * @param status
	 * @return
	 */
	boolean updateStatusByNotifyLog(String version, int deviceId, int mediaId, int status);

	/**
	 * 根据Id 获取电影发布详细信息列表
	 * 
	 * @param deployId
	 * @return
	 */
	List<MediaDeployDetail> getMediaDeployDetailsByDeployId(Integer deployId);

	/**
	 * 根据发布Id 获取Sd发布详情
	 * 
	 * @param deployId
	 * @return
	 */
	List<SDDeployDetailVo> getSDDeployDetail(int deployId);

	/**
	 * 根据Id 获取电影发布详细信息
	 * 
	 * @param deployId
	 * @return
	 */
	List<MediaDeployDetail> getSDDeployDeviceDetail(int deployId, int mediaId);

	/**
	 * 根据 条件 获取电影发布详情 Map
	 * 
	 * @param devId
	 * @param mediaId
	 * @param mediaVersion
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	Map getLastMediaDeployDetail(int devId, int mediaId, String mediaVersion);

	/**
	 * 获取等待下载 电影列表
	 * 
	 * @param deployId
	 * @return
	 */
	List<MediaDeployDetail> getMediaDeployDetailWaiting(int deployId);
}
