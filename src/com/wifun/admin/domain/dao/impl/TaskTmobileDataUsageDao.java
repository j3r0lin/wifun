package com.wifun.admin.domain.dao.impl;

import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.ITaskTmobileDataUsageDao;
import com.wifun.admin.domain.model.TaskTmobileDataUse;

@Repository("taskTmobileDataUsageDao")
public class TaskTmobileDataUsageDao extends BaseDao<TaskTmobileDataUse, Integer> implements
		ITaskTmobileDataUsageDao {

}
