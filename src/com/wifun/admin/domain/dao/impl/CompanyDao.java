package com.wifun.admin.domain.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.ICompanyDao;
import com.wifun.admin.domain.model.Company;

@Repository("companyDao")
public class CompanyDao extends BaseDao<Company, Integer> implements ICompanyDao {

	/**
	 * 根据account 查询comapny
	 * 
	 * @param account
	 * @return
	 */
	@Override
	public Company getCompanyByAccount(String account) {

		DetachedCriteria criteria = DetachedCriteria.forClass(Company.class);
		criteria.add(Restrictions.eq(Company.FN_ACCOUNT, account));

		return super.findFirstByCriteria(criteria);
	}

	/**
	 * 根据name 模糊查询comapny
	 * 
	 * @param active
	 * @param name
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<Company> getCompanyList(boolean acive, String name) {

		DetachedCriteria criteria = DetachedCriteria.forClass(Company.class);
		criteria.add(Restrictions.gt("id", 1));
		if (acive)
			criteria.add(Restrictions.eq(Company.FN_IS_ACTIVE, true));

		if (StringUtils.isNotBlank(name))
			criteria.add(Restrictions.like(Company.FN_NAME, name, MatchMode.ANYWHERE));

		return super.findByCriteria(criteria);
	}

	/**
	 * 根据inhandId 得到company
	 * 
	 * @param companyId
	 * @param inhandId
	 * @return
	 */
	@Override
	public boolean updateInhandId(Integer companyId, String inhandId) {

		String strHql = "update Company set inhand_id=? where id=?";
		try {
			super.getHibernateTemplate().bulkUpdate(strHql, new Object[] { inhandId, companyId });
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * 更新company 某个字段
	 * 
	 * @param columnName
	 * @param columnValue
	 * @param id
	 * @return
	 */
	@Override
	public boolean updateColumn(String columnName, Object columnValue, Integer id) {

		String strHql = "update Company set " + columnName + "=? where id=?";
		try {
			super.getHibernateTemplate().bulkUpdate(strHql, new Object[] { columnValue, id });
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	/**
	 * 根据名字 得到company
	 * 
	 * @param name
	 * @return
	 */
	@Override
	public Company getCompanyByName(String name) {

		DetachedCriteria criteria = DetachedCriteria.forClass(Company.class);
		criteria.add(Restrictions.eq(Company.FN_NAME, name));

		return super.findFirstByCriteria(criteria);
	}

	/**
	 * 根据email 得到company
	 * 
	 * @param email
	 * @return
	 */
	@Override
	public Company getCompanyByEmail(String email) {

		DetachedCriteria criteria = DetachedCriteria.forClass(Company.class);
		criteria.add(Restrictions.eq(Company.FN_EMAIL, email));

		return super.findFirstByCriteria(criteria);
	}

}
