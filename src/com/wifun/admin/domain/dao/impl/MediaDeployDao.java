package com.wifun.admin.domain.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.dao.IMediaDeployDao;
import com.wifun.admin.domain.model.MediaDeploy;
import com.wifun.admin.domain.model.PortalDeploy;
import com.wifun.admin.util.TimeUtils;

@Repository("mediaDeployDao")
public class MediaDeployDao extends BaseDao<MediaDeploy, Integer> implements IMediaDeployDao {

	@Override
	public boolean updatePublishInfo(Integer id, String publishPointId, String publishId,
			String version) {

		String strHql = "update MediaDeploy set inhand_status = 1, version = ? ,publish_point_id=?, inhand_publish_id=? where id=?";
		try {
			super.getHibernateTemplate().bulkUpdate(strHql,
					new Object[] { version, publishPointId, publishId, id });
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean updatePublishError(Integer id) {

		String strHql = "update MediaDeploy set inhand_status = 3 where id=?";
		try {
			super.getHibernateTemplate().bulkUpdate(strHql, new Object[] { id });
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MediaDeploy> searchNetDeployList(int companyId, String startDate, String endDate) {

		DetachedCriteria criteria = DetachedCriteria.forClass(MediaDeploy.class);

		criteria.add(Restrictions.eq(MediaDeploy.FN_TYPE, MediaDeploy.DEPLOY_TYPE_NET));

		if (companyId > 0) {
			criteria.add(Restrictions.eq(MediaDeploy.FN_COMPANY_ID, companyId));
		}

		if (isDate(startDate) && isDate(endDate)) {
			criteria.add(Restrictions.between(MediaDeploy.FN_DEPLOY_TIME,
					TimeUtils.strUsToDate(startDate), TimeUtils.dateUsStrAdd(endDate, 1)));
		} else if (isDate(startDate)) {
			criteria.add(Restrictions.ge(MediaDeploy.FN_DEPLOY_TIME,
					TimeUtils.strUsToDate(startDate)));
		} else if (isDate(endDate)) {
			criteria.add(Restrictions.le(MediaDeploy.FN_DEPLOY_TIME,
					TimeUtils.dateUsStrAdd(endDate, 1)));
		}
		criteria.addOrder(Order.desc(MediaDeploy.FN_DEPLOY_TIME));
		return super.findByCriteria(criteria);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MediaDeploy> searchSDDeployList(int companyId, String startDate, String endDate) {

		DetachedCriteria criteria = DetachedCriteria.forClass(MediaDeploy.class);

		criteria.add(Restrictions.eq(MediaDeploy.FN_TYPE, MediaDeploy.DEPLOY_TYPE_SD));

		if (companyId > 0) {
			criteria.add(Restrictions.eq(MediaDeploy.FN_COMPANY_ID, companyId));
		}

		if (isDate(startDate) && isDate(endDate)) {
			criteria.add(Restrictions.between(MediaDeploy.FN_DEPLOY_TIME,
					TimeUtils.strUsToDate(startDate), TimeUtils.dateUsStrAdd(endDate, 1)));
		} else if (isDate(startDate)) {
			criteria.add(Restrictions.ge(MediaDeploy.FN_DEPLOY_TIME,
					TimeUtils.strUsToDate(startDate)));
		} else if (isDate(endDate)) {
			criteria.add(Restrictions.le(MediaDeploy.FN_DEPLOY_TIME,
					TimeUtils.dateUsStrAdd(endDate, 1)));
		}
		criteria.addOrder(Order.desc(MediaDeploy.FN_DEPLOY_TIME));
		return super.findByCriteria(criteria);
	}

	private boolean isDate(String str) {

		if (StringUtils.isNotBlank(str)) {
			try {
				Date date = TimeUtils.strUsToDate(str);
				if (date != null) {
					return true;
				}
			} catch (Exception e) {

			}
		}
		return false;
	}

	@Override
	public MediaDeploy getMediaDeployByVersion(String version) {

		DetachedCriteria criteria = DetachedCriteria.forClass(MediaDeploy.class);

		criteria.add(Restrictions.eq(MediaDeploy.FN_VERSION, version));

		return super.findFirstByCriteria(criteria);
	}
}
