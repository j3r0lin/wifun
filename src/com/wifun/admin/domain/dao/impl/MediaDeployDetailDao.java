package com.wifun.admin.domain.dao.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.hibernate.SQLQuery;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.wifun.admin.app.vo.SDDeployDetailVo;
import com.wifun.admin.domain.dao.IMediaDeployDetailDao;
import com.wifun.admin.domain.model.MediaDeployDetail;
import com.wifun.admin.util.json.SqlColumnToBean;

@Repository("mediaDeployDetailDao")
public class MediaDeployDetailDao extends BaseDao<MediaDeployDetail, Integer> implements
		IMediaDeployDetailDao {

	public boolean updateStatusByNotifyLog(String version, int deviceId, int mediaId, int status) {

		StringBuffer sqlStr = new StringBuffer();
		sqlStr.append(" UPDATE media_deploy_detail mdd ");
		sqlStr.append(" SET mdd.device_status = ? ");
		sqlStr.append(" WHERE ");
		sqlStr.append(" 	deploy_id = ( ");
		sqlStr.append(" 		SELECT ");
		sqlStr.append(" 			id ");
		sqlStr.append(" 		FROM ");
		sqlStr.append(" 			media_deploy md ");
		sqlStr.append(" 		WHERE ");
		sqlStr.append(" 			md.version = ? ");
		sqlStr.append(" 	) ");
		sqlStr.append(" AND mdd.device_id = ? ");
		sqlStr.append(" AND mdd.media_id = ? ");
		try {
			SQLQuery sqlQuery = super.getSqlQuery(sqlStr.toString());
			sqlQuery.setInteger(0, status);
			sqlQuery.setString(1, version);
			sqlQuery.setInteger(2, deviceId);
			sqlQuery.setInteger(3, mediaId);
			sqlQuery.executeUpdate();
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MediaDeployDetail> getMediaDeployDetailsByDeployId(Integer deployId) {

		DetachedCriteria criteria = DetachedCriteria.forClass(MediaDeployDetail.class);
		criteria.add(Restrictions.eq(MediaDeployDetail.FN_DEPLOY_ID, deployId));

		return super.findByCriteria(criteria);
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public Map getLastMediaDeployDetail(int devId, int mediaId, String mediaVersion) {

		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT ");
		sb.append(" 	* ");
		sb.append(" FROM ");
		sb.append(" 	media_deploy_detail ");
		sb.append(" WHERE ");
		sb.append(" 	deploy_id = ( ");
		sb.append(" 		SELECT ");
		sb.append(" 			max(deploy_id) ");
		sb.append(" 		FROM ");
		sb.append(" 			`media_deploy_detail` mdd ");
		sb.append(" 		WHERE ");
		sb.append(" 			mdd.device_id = ? ");
		sb.append(" 	) ");
		sb.append(" AND media_id = ? ");
		sb.append(" AND media_version = ? ");
		sb.append(" AND ( ");
		sb.append(" 	device_status =- 1 ");
		sb.append(" 	OR device_status = 1 ");
		sb.append(" )");

		SQLQuery requisitionDetailQuery = this.getSqlQuery(sb.toString());
		requisitionDetailQuery.setInteger(0, devId);
		requisitionDetailQuery.setInteger(1, mediaId);
		requisitionDetailQuery.setString(2, mediaVersion);
		requisitionDetailQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);

		List<Map> list = requisitionDetailQuery.list();
		if (list == null || list.size() == 0)
			return null;

		return list.get(0);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<SDDeployDetailVo> getSDDeployDetail(int deployId) {

		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT DISTINCT");
		sb.append(" 	mdd.deploy_id deployId, ");
		sb.append(" 	mdd.media_id mediaId, ");
		sb.append(" 	mdd.media_title mediaTitle, ");
		sb.append(" 	mdd.media_version mediaVersion, ");
		sb.append(" 	mdd.deploy_time deployTime ");
		sb.append(" FROM ");
		sb.append(" 	media_deploy_detail mdd ");
		sb.append(" WHERE ");
		sb.append(" 	mdd.deploy_id = " + deployId);
		sb.append(" AND mdd.device_status <> - 1 ");
		SQLQuery requisitionDetailQuery = this.getSqlQuery(sb.toString());
		requisitionDetailQuery.setResultTransformer(new SqlColumnToBean<SDDeployDetailVo>(
				SDDeployDetailVo.class));
		List<SDDeployDetailVo> list = requisitionDetailQuery.list();
		if (null == list)
			return new ArrayList<SDDeployDetailVo>();

		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MediaDeployDetail> getSDDeployDeviceDetail(int deployId, int mediaId) {

		DetachedCriteria criteria = DetachedCriteria.forClass(MediaDeployDetail.class);
		criteria.add(Restrictions.eq(MediaDeployDetail.FN_DEPLOY_ID, deployId));
		criteria.add(Restrictions.eq(MediaDeployDetail.FN_MEDIA_ID, mediaId));

		return super.findByCriteria(criteria);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MediaDeployDetail> getMediaDeployDetailWaiting(int deployId) {

		DetachedCriteria criteria = DetachedCriteria.forClass(MediaDeployDetail.class);
		criteria.add(Restrictions.eq(MediaDeployDetail.FN_DEPLOY_ID, deployId));
		criteria.add(Restrictions.eq(MediaDeployDetail.FN_DEVICE_STATUS,
				MediaDeployDetail.FN_DEVICE_STATUS_WAIT));

		return super.findByCriteria(criteria);
	}
}
