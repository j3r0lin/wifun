package com.wifun.admin.domain.dao.impl;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.Transformers;
import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.IDeviceDao;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.Group;

@Repository("deviceDao")
public class DeviceDao extends BaseDao<Device, Integer> implements IDeviceDao {

	@Override
	public List<Device> loadByCompany(Integer companyId) {

		if (null == companyId)
			return null;

		DetachedCriteria criteria = DetachedCriteria.forClass(Device.class);
		criteria.add(Restrictions.eq(Device.FN_COMPANY_ID, companyId));
		criteria.add(Restrictions.eq(Device.FN_IS_ACTIVE, true));

		return super.findByCriteria(criteria);
	}

	@Override
	public List<Device> loadByGroup(Integer groupId) {

		if (null == groupId)
			return null;
		DetachedCriteria criteria = DetachedCriteria.forClass(Device.class);
		criteria.add(Restrictions.eq(Device.FN_GROUP_ID, groupId));
		criteria.add(Restrictions.eq(Device.FN_IS_ACTIVE, true));

		return super.findByCriteria(criteria);
	}

	@Override
	public List<Device> loadByGroup(Group group) {

		if (null == group)
			return null;
		DetachedCriteria criteria = DetachedCriteria.forClass(Device.class);
		criteria.add(Restrictions.eq(Device.FN_GROUP_ID, group.getId()));
		criteria.add(Restrictions.eq(Device.FN_IS_ACTIVE, true));

		return super.findByCriteria(criteria);
	}

	@Override
	public Device getDeviceBySn(String sn) {

		DetachedCriteria criteria = DetachedCriteria.forClass(Device.class);
		criteria.add(Restrictions.eq(Device.FN_SN, sn));
		criteria.add(Restrictions.eq(Device.FN_IS_ACTIVE, true));

		return super.findFirstByCriteria(criteria);
	}

	@Override
	public boolean updateColumn(String columnName, Object columnValue, Integer id) {

		String strHql = "update Device set " + columnName + "=? where id=?";
		try {
			super.getHibernateTemplate().bulkUpdate(strHql, new Object[] { columnValue, id });
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Device> searchDevices(boolean active, Integer companyId, int groupId, String sn,
			String name) {

		DetachedCriteria criteria = DetachedCriteria.forClass(Device.class);
		if (active)
			criteria.add(Restrictions.eq(Device.FN_WORK_STATUS, "1"));

		if (companyId > 1)
			criteria.add(Restrictions.eq(Device.FN_COMPANY_ID, companyId));

		if (groupId != -1)
			criteria.add(Restrictions.eq(Device.FN_GROUP_ID, groupId));

		if (StringUtils.isNotBlank(sn))
			criteria.add(Restrictions.like(Device.FN_SN, sn, MatchMode.ANYWHERE));

		if (StringUtils.isNotBlank(name))
			criteria.add(Restrictions.like(Device.FN_NAME, name, MatchMode.ANYWHERE));

		criteria.add(Restrictions.eq(Device.FN_IS_ACTIVE, true));

		return super.findByCriteria(criteria);
	}

	@Override
	public List<Device> getDeviceList(int companyId, int groupId) {

		DetachedCriteria criteria = DetachedCriteria.forClass(Device.class);

		if (companyId > 1)
			criteria.add(Restrictions.eq(Device.FN_COMPANY_ID, companyId));

		if (groupId != -1)
			criteria.add(Restrictions.eq(Device.FN_GROUP_ID, groupId));

		criteria.add(Restrictions.eq(Device.FN_IS_ACTIVE, true));

		return super.findByCriteria(criteria);
	}

	@Override
	public Map<String, Object> getDeviceNumByCompany(int companyId) {

		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT ");
		sb.append(" 	count( ");
		sb.append(" 		( ");
		sb.append(" 			CASE ");
		sb.append(" 			WHEN (`d`.`work_status` = '1') THEN ");
		sb.append(" 				1 ");
		sb.append(" 			ELSE ");
		sb.append(" 				NULL ");
		sb.append(" 			END ");
		sb.append(" 		) ");
		sb.append(" 	) AS `normal_num`, ");
		sb.append(" 	count(1) AS `total_num` ");
		sb.append(" FROM ");
		sb.append(" 	`device` `d` ");
		sb.append(" where is_active = true and company_id = " + companyId);

		SQLQuery requisitionDetailQuery = this.getSqlQuery(sb.toString());
		requisitionDetailQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List<Map> list = requisitionDetailQuery.list();
		if (list == null || list.size() == 0)
			return null;

		return list.get(0);
	}

	@Override
	public Map<String, Object> getDeviceNumByGroup(int companyId, int groupId) {

		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT ");
		sb.append(" 	count( ");
		sb.append(" 		( ");
		sb.append(" 			CASE ");
		sb.append(" 			WHEN (`d`.`work_status` = '1') THEN ");
		sb.append(" 				1 ");
		sb.append(" 			ELSE ");
		sb.append(" 				NULL ");
		sb.append(" 			END ");
		sb.append(" 		) ");
		sb.append(" 	) AS `normal_num`, ");
		sb.append(" 	count(1) AS `total_num` ");
		sb.append(" FROM ");
		sb.append(" 	`device` `d` ");
		sb.append(" where is_active = true and company_id = " + companyId);
		sb.append(" and group_id = " + groupId);

		SQLQuery requisitionDetailQuery = this.getSqlQuery(sb.toString());
		requisitionDetailQuery.setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
		List<Map> list = requisitionDetailQuery.list();
		if (list == null || list.size() == 0)
			return null;

		return list.get(0);
	}
}
