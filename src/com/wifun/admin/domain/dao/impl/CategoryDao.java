package com.wifun.admin.domain.dao.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.ICategoryDao;
import com.wifun.admin.domain.model.Category;

@Repository("categoryDao")
public class CategoryDao extends BaseDao<Category, Integer> implements ICategoryDao {

	/**
	 * 获取电影分类列表
	 * 
	 * @param active
	 * @return
	 */
	public List<Category> getCategoryList(boolean active) {

		DetachedCriteria criteria = DetachedCriteria.forClass(Category.class);
		if (active)
			criteria.add(Restrictions.eq(Category.FN_STATUS, "1"));

		return super.findByCriteria(criteria);
	}
}
