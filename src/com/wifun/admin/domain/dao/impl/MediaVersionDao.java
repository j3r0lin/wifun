package com.wifun.admin.domain.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.IMediaVersionDao;
import com.wifun.admin.domain.model.MediaVersion;

@Repository("mediaVersionDao")
public class MediaVersionDao extends BaseDao<MediaVersion, Integer> implements IMediaVersionDao {

	@Override
	public MediaVersion getLastMediaVersion(int mediaId) {

		DetachedCriteria criteria = DetachedCriteria.forClass(MediaVersion.class);
		criteria.add(Restrictions.eq(MediaVersion.FN_MEDIA_ID, mediaId));
		criteria.add(Restrictions.eq(MediaVersion.FN_STATUS, MediaVersion.MEDIA_ZIP_STATUS_SUCCESS));
		criteria.addOrder(Order.desc(MediaVersion.FN_ID));

		return super.findFirstByCriteria(criteria);
	}

	@Override
	public MediaVersion getCurrentMediaVersion(int mediaId) {

		DetachedCriteria criteria = DetachedCriteria.forClass(MediaVersion.class);
		criteria.add(Restrictions.eq(MediaVersion.FN_MEDIA_ID, mediaId));
		criteria.addOrder(Order.desc(MediaVersion.FN_ID));

		return super.findFirstByCriteria(criteria);
	}
}
