package com.wifun.admin.domain.dao.impl;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.wifun.admin.domain.dao.IBaseDao;
import com.wifun.admin.util.json.SqlColumnToBean;

public abstract class BaseDao<T, PK extends Serializable> implements IBaseDao<T, PK> {

	private HibernateTemplate hibernateTemplate;
	private Class<T> entityClass = null;

	public BaseDao() {

		Type t = getClass().getGenericSuperclass();
		if (t instanceof ParameterizedType) {
			Type[] p = ((ParameterizedType) t).getActualTypeArguments();
			this.entityClass = (Class<T>) p[0];
		}
	}

	@SuppressWarnings("unused")
	@Autowired
	private void setSessionFactory(SessionFactory sessionFactory) {

		hibernateTemplate = new HibernateTemplate(sessionFactory);
	}

	protected HibernateTemplate getHibernateTemplate() {

		return this.hibernateTemplate;
	}

	// -------------------- 基本检索、增加、修改、删除操作 --------------------

	// 根据主键获取实体。如果没有相应的实体，返回 null。
	public T get(PK id) {

		if (id == null)
			return null;
		return (T) getHibernateTemplate().get(entityClass, id);
	}

	// 根据主键获取实体。如果没有相应的实体，抛出异常。
	public T load(PK id) {

		return (T) getHibernateTemplate().load(entityClass, id);
	}

	// 根据主键获取实体并加锁。如果没有相应的实体，抛出异常。
	public T loadWithLock(PK id, LockMode lock) {

		T t = (T) getHibernateTemplate().load(entityClass, id, lock);
		if (t != null) {
			this.flush(); // 立即刷新，否则锁不会生效。
		}
		return t;
	}

	// 获取全部实体。
	public List<T> loadAll() {

		return getHibernateTemplate().loadAll(entityClass);
	}

	// 更新实体
	public T update(T entity) {

		getHibernateTemplate().update(entity);
		return entity;
	}

	// 更新实体并加锁
	public void updateWithLock(T entity, LockMode lock) {

		getHibernateTemplate().update(entity, lock);
		this.flush(); // 立即刷新，否则锁不会生效。
	}

	// 存储实体到数据库
	public T save(T entity) {

		getHibernateTemplate().save(entity);
		return entity;
	}

	// 增加或更新实体
	public void saveOrUpdate(T entity) {

		getHibernateTemplate().saveOrUpdate(entity);
	}

	// 增加或更新集合中的全部实体
	public void saveOrUpdateAll(Collection<T> entities) {

		getHibernateTemplate().saveOrUpdateAll(entities);
	}

	// 删除指定的实体
	public void delete(T entity) {

		getHibernateTemplate().delete(entity);
	}

	// 加锁并删除指定的实体
	public void deleteWithLock(T entity, LockMode lock) {

		getHibernateTemplate().delete(entity, lock);
		this.flush(); // 立即刷新，否则锁不会生效。
	}

	// 根据主键删除指定实体
	public void deleteByKey(PK id) {

		this.delete(this.load(id));
	}

	// 根据主键加锁并删除指定的实体
	public void deleteByKeyWithLock(PK id, LockMode lock) {

		this.deleteWithLock(this.load(id), lock);
	}

	// 删除集合中的全部实体
	public void deleteAll(Collection<T> entities) {

		getHibernateTemplate().deleteAll(entities);
	}

	// -------------------- HSQL ----------------------------------------------

	// 使用HSQL语句检索数据
	public List find(String queryString) {

		return getHibernateTemplate().find(queryString);
	}

	// 使用带参数的HSQL语句检索数据
	public List find(String queryString, Object[] values) {

		return getHibernateTemplate().find(queryString, values);
	}

	/**
	 * 获取查询对象
	 * 
	 * @param hql
	 *            HQL语句
	 * @return 查询对象
	 */
	protected Query getQuery(final String hql) {

		return this.hibernateTemplate.execute(new HibernateCallback<Query>() {

			@Override
			public Query doInHibernate(Session session) throws HibernateException, SQLException {

				return session.createQuery(hql);
			}
		});
	}

	// 使用带命名的参数的HSQL语句检索数据
	public List findByNamedParam(String queryString, String[] paramNames, Object[] values) {

		return getHibernateTemplate().findByNamedParam(queryString, paramNames, values);
	}

	// 使用命名的HSQL语句检索数据
	public List findByNamedQuery(String queryName) {

		return getHibernateTemplate().findByNamedQuery(queryName);
	}

	// 使用带参数的命名HSQL语句检索数据
	public List findByNamedQuery(String queryName, Object value) {

		return getHibernateTemplate().findByNamedQuery(queryName, value);
	}

	// 使用带参数数组的命名HSQL语句检索数据
	public List findByNamedQuery(String queryName, Object[] values) {

		return getHibernateTemplate().findByNamedQuery(queryName, values);
	}

	// 使用带命名参数的命名HSQL语句检索数据
	public List findByNamedQueryAndNamedParam(String queryName, String[] paramNames, Object[] values) {

		return getHibernateTemplate().findByNamedQueryAndNamedParam(queryName, paramNames, values);
	}

	// 使用HSQL语句检索数据，返回 Iterator
	public Iterator iterate(String queryString) {

		return getHibernateTemplate().iterate(queryString);
	}

	// 使用带参数HSQL语句检索数据，返回 Iterator
	public Iterator iterate(String queryString, Object[] values) {

		return getHibernateTemplate().iterate(queryString, values);
	}

	// 关闭检索返回的 Iterator
	public void closeIterator(Iterator it) {

		getHibernateTemplate().closeIterator(it);
	}

	// 使用带参数的HSQL语句检索数据 分页
	public List find(final String queryString, Object[] list, int firstResult, int maxResult) {

		Query query = this.hibernateTemplate.execute(new HibernateCallback<Query>() {

			@Override
			public Query doInHibernate(Session session) throws HibernateException, SQLException {

				return session.createQuery(queryString);
			}
		});

		if (list != null) {

			for (int i = 1; i <= list.length; i++) {

				query.setParameter(i, list[i]);
			}
		}

		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);
		return query.list();

	}

	// -------------------------------- Criteria ------------------------------

	// 检索满足标准的数据
	public List findByCriteria(DetachedCriteria criteria) {

		return getHibernateTemplate().findByCriteria(criteria);
	}

	// 检索满足标准的数据，返回指定范围的记录
	public List findByCriteria(DetachedCriteria criteria, int firstResult, int maxResults) {

		return getHibernateTemplate().findByCriteria(criteria, firstResult, maxResults);
	}

	// 按照实体中不为空的属性，与数据库中的数据进行匹配查找
	public List<T> findByExample(T entity) {

		return getHibernateTemplate().findByExample(entity);
	}

	// 使用指定的检索标准获取满足标准的记录数
	public Integer getRowCount(DetachedCriteria criteria) {

		criteria.setProjection(Projections.rowCount());
		List list = this.findByCriteria(criteria, 0, 1);
		return (Integer) list.get(0);
	}

	// 使用指定的检索标准检索数据，返回指定统计值(max,min,avg,sum)
	public Object getStatValue(DetachedCriteria criteria, String propertyName, String StatName) {

		if (StatName.toLowerCase().equals("max"))
			criteria.setProjection(Projections.max(propertyName));
		else if (StatName.toLowerCase().equals("min"))
			criteria.setProjection(Projections.min(propertyName));
		else if (StatName.toLowerCase().equals("avg"))
			criteria.setProjection(Projections.avg(propertyName));
		else if (StatName.toLowerCase().equals("sum"))
			criteria.setProjection(Projections.sum(propertyName));
		else
			return null;
		List list = this.findByCriteria(criteria, 0, 1);
		return list.get(0);
	}

	// -------------------------------- Others --------------------------------

	// 加锁指定的实体
	public void lock(T entity, LockMode lock) {

		getHibernateTemplate().lock(entity, lock);
	}

	// 强制初始化指定的实体
	public void initialize(Object proxy) {

		getHibernateTemplate().initialize(proxy);
	}

	// 强制立即更新缓冲数据到数据库（否则仅在事务提交时才更新）
	public void flush() {

		getHibernateTemplate().flush();
	}

	/**
	 * 检索满足标准的数据，如果存在返回第一条数据，不存在返回null
	 * 
	 * @param criteria
	 * @return T
	 */
	public T findFirstByCriteria(DetachedCriteria criteria) {

		List list = this.getHibernateTemplate().findByCriteria(criteria);

		if (list != null && list.size() >= 1) {
			return (T) list.get(0);
		}
		return null;
	}

	/**
	 * 获取Sql查询对象
	 * 
	 * @param sql
	 *            sql语句
	 * @return 查询对象
	 */
	protected SQLQuery getSqlQuery(final String sql) {

		return this.hibernateTemplate.execute(new HibernateCallback<SQLQuery>() {

			@Override
			public SQLQuery doInHibernate(Session session) throws HibernateException, SQLException {

				return session.createSQLQuery(sql);
			}

		});
	}

	/**
	 * 获取Sql查询对象
	 * 
	 * @param sql
	 *            sql语句
	 * @return 查询对象
	 */
	protected List getSqlQueryList(final String sql) {

		return this.hibernateTemplate.execute(new HibernateCallback<SQLQuery>() {

			@Override
			public SQLQuery doInHibernate(Session session) throws HibernateException, SQLException {

				return session.createSQLQuery(sql);
			}

		}).list();
	}

	protected Query getSqlQueryObj(final String sql) {

		return this.hibernateTemplate.execute(new HibernateCallback<SQLQuery>() {

			@Override
			public SQLQuery doInHibernate(Session session) throws HibernateException, SQLException {

				return session.createSQLQuery(sql);
			}

		}).addEntity(entityClass);
	}

	/**
	 * 获取Sql 分页查询对象并转换成自定义对象
	 * 
	 * @param sql语句
	 * 
	 * @return 查询对象
	 * 
	 */
	protected <T> List<T> getSqlPageQuery(final String sql, Object[] parameterList,
			int firstResult, int maxResult, Class<T> classs) {

		SQLQuery query = this.hibernateTemplate.execute(new HibernateCallback<SQLQuery>() {

			@Override
			public SQLQuery doInHibernate(Session session) throws HibernateException, SQLException {

				return session.createSQLQuery(sql);
			}
		});
		//
		if (parameterList != null) {
			for (int i = 0; i < parameterList.length; i++) {

				query.setParameter(i, parameterList[i]);
			}
		}
		query.setFirstResult(firstResult);
		query.setMaxResults(maxResult);

		// 转换对象
		query.setResultTransformer(new SqlColumnToBean(classs));

		return (List<T>) query.list();

	}

	protected int getSqlQueryCounts(final String sql, Object[] parameterList) {

		SQLQuery query = this.hibernateTemplate.execute(new HibernateCallback<SQLQuery>() {

			@Override
			public SQLQuery doInHibernate(Session session) throws HibernateException, SQLException {

				return session.createSQLQuery(sql);
			}
		});
		//
		if (parameterList != null) {
			for (int i = 0; i < parameterList.length; i++) {

				query.setParameter(i, parameterList[i]);
			}
		}

		Object obj = query.uniqueResult();

		if (obj != null) {
			return Integer.valueOf(obj.toString());
		} else {
			return 0;
		}

	}
}
