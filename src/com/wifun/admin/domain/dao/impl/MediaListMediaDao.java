package com.wifun.admin.domain.dao.impl;

import java.util.List;

import org.hibernate.FetchMode;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.IMediaListMediaDao;
import com.wifun.admin.domain.model.MediaListMedia;

@Repository("mediaListMediaDao")
public class MediaListMediaDao extends BaseDao<MediaListMedia, Integer> implements
		IMediaListMediaDao {

	@SuppressWarnings({ "unchecked", "deprecation" })
	public List<MediaListMedia> getMediaListMediaByMediaListId(int mediaListId) {

		DetachedCriteria criteria = DetachedCriteria.forClass(MediaListMedia.class);
		criteria.setFetchMode("media", FetchMode.LAZY);
		criteria.add(Restrictions.eq(MediaListMedia.FN_MEDIA_LIST_ID, mediaListId));
		return super.findByCriteria(criteria);
	}
}
