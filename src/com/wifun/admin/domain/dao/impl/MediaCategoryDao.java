package com.wifun.admin.domain.dao.impl;

import java.util.List;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.IMediaCategoryDao;
import com.wifun.admin.domain.model.MediaCategory;

@Repository("mediaCategoryDao")
public class MediaCategoryDao extends BaseDao<MediaCategory, Integer> implements IMediaCategoryDao {

	public List<MediaCategory> getMediaCategoryByMediaId(int mediaId) {

		DetachedCriteria criteria = DetachedCriteria.forClass(MediaCategory.class);
		criteria.add(Restrictions.eq(MediaCategory.FN_MEDIA_ID, mediaId));
		return super.findByCriteria(criteria);
	}
}
