package com.wifun.admin.domain.dao.impl;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.ITaskWifiStatDao;
import com.wifun.admin.domain.model.TaskWifiStat;

@Repository("taskWifiStatDao")
public class TaskWifiStatDao extends BaseDao<TaskWifiStat, Integer> implements ITaskWifiStatDao {

	@Override
	public TaskWifiStat getTaskWifiStat(String date, String type, int deviceId) {

		DetachedCriteria criteria = DetachedCriteria.forClass(TaskWifiStat.class);

		criteria.add(Restrictions.eq(TaskWifiStat.FN_DATE, date));
		criteria.add(Restrictions.eq(TaskWifiStat.FN_TYPE, type));
		criteria.add(Restrictions.eq(TaskWifiStat.FN_DEVICE_ID, deviceId));

		return super.findFirstByCriteria(criteria);
	}
}
