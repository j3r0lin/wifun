package com.wifun.admin.domain.dao.impl;

import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.IPortalDeployDetailDao;
import com.wifun.admin.domain.model.PortalDeployDetail;

@Repository("portalDeployDetailDao")
public class PortalDeployDetailDao extends BaseDao<PortalDeployDetail, Integer> implements
		IPortalDeployDetailDao {

}
