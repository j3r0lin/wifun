package com.wifun.admin.domain.dao.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wifun.admin.app.vo.DailyReportCompanyVo;
import com.wifun.admin.domain.dao.ITaskReportDailyDao;
import com.wifun.admin.domain.model.TaskReportDaily;
import com.wifun.admin.util.json.SqlColumnToBean;

//TaskReport业务层
@Repository("taskReportDailyDao")
public class TaskReportDailyDao extends BaseDao<TaskReportDaily, Integer> implements
		ITaskReportDailyDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<TaskReportDaily> getListByCompanyIdAndDate(int companyId, int groupId, String date) {

		if (companyId == 0)
			return null;

		DetachedCriteria criteria = DetachedCriteria.forClass(TaskReportDaily.class);
		criteria.add(Restrictions.eq(TaskReportDaily.FN_COMPANY_ID, companyId));
		criteria.add(Restrictions.eq(TaskReportDaily.FN_DATE, date));

		if (groupId != -1)
			criteria.add(Restrictions.eq(TaskReportDaily.FN_GROUP_ID, groupId));

		criteria.addOrder(Order.asc(TaskReportDaily.FN_GROUP_ID));
		criteria.addOrder(Order.asc(TaskReportDaily.FN_DEVICE_ID));
		return super.findByCriteria(criteria);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DailyReportCompanyVo> getCompanyDailyReport(String date) {

		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT ");
		sb.append(" 	c.id companyId, ");
		sb.append(" 	c. NAME companyName, ");
		sb.append(" 	sum(trd.traffic_recv) trafficRecv, ");
		sb.append(" 	sum(trd.traffic_send) trafficSend, ");
		sb.append(" 	sum(trd.traffic_total) trafficTotal, ");
		sb.append(" 	sum(trd.device_daily_add) deviceDailyAdd, ");
		sb.append(" 	sum(trd.device_total) deviceTotal, ");
		sb.append(" 	sum(trd.device_one_hour_online) deviceOneHourOnline, ");
		sb.append(" 	sum(trd.user_daily_add) userDailyAdd, ");
		sb.append(" 	sum(trd.user_total) userTotal, ");
		sb.append(" 	sum(trd.pv_main) pvMain, ");
		sb.append(" 	sum(trd.pv_movie) pvMovie, ");
		sb.append(" 	sum(trd.uv_main) uvMain, ");
		sb.append(" 	sum(trd.uv_movie) uvMovie, ");
		sb.append(" 	sum(trd.pv_home) pvHome, ");
		sb.append(" 	sum(trd.uv_home) uvHome ");
		sb.append(" FROM ");
		sb.append(" 	task_report_daily trd ");
		sb.append(" JOIN company c ON trd.company_id = c.id ");
		sb.append(" WHERE ");
		sb.append(" 	trd.date = '" + date + "' ");
		sb.append(" GROUP BY ");
		sb.append(" 	c.id, ");
		sb.append(" 	c.`name` ");
		SQLQuery requisitionDetailQuery = this.getSqlQuery(sb.toString());
		requisitionDetailQuery.setResultTransformer(new SqlColumnToBean<DailyReportCompanyVo>(
				DailyReportCompanyVo.class));
		List<DailyReportCompanyVo> list = requisitionDetailQuery.list();

		return list;
	}
}
