package com.wifun.admin.domain.dao.impl;

import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.IPortalDeployDao;
import com.wifun.admin.domain.model.PortalDeploy;
import com.wifun.admin.util.TimeUtils;

@Repository("portalDeployDao")
public class PortalDeployDao extends BaseDao<PortalDeploy, Integer> implements IPortalDeployDao {

	@Override
	public boolean updatePublishInfo(Integer id, String publishPointId, String publishId) {

		String strHql = "update PortalDeploy set inhand_status=1, publish_point_id=?, inhand_publish_id=? where id=?";
		try {
			super.getHibernateTemplate().bulkUpdate(strHql,
					new Object[] { publishPointId, publishId, id });
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean updatePublishError(Integer id) {

		String strHql = "update PortalDeploy set inhand_status=3 where id=?";
		try {
			super.getHibernateTemplate().bulkUpdate(strHql, new Object[] { id });
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public List<PortalDeploy> searchPortal(int companyId, String description, String startDate,
			String endDate) {

		DetachedCriteria criteria = DetachedCriteria.forClass(PortalDeploy.class);

		if (companyId > 0) {
			criteria.add(Restrictions.eq(PortalDeploy.FN_COMPANY_ID, companyId));
		}
		if (StringUtils.isNotBlank(description)) {
			criteria.add(Restrictions.like(PortalDeploy.FN_DESCRIPTION, description,
					MatchMode.ANYWHERE));
		}

		if (isDate(startDate) && isDate(endDate)) {
			criteria.add(Restrictions.between(PortalDeploy.FN_DEPLOYTIME,
					TimeUtils.strUsToDate(startDate), TimeUtils.dateUsStrAdd(endDate, 1)));
		} else if (isDate(startDate)) {
			criteria.add(Restrictions.ge(PortalDeploy.FN_DEPLOYTIME,
					TimeUtils.strUsToDate(startDate)));
		} else if (isDate(endDate)) {
			criteria.add(Restrictions.le(PortalDeploy.FN_DEPLOYTIME,
					TimeUtils.dateUsStrAdd(endDate, 1)));
		}

		return super.findByCriteria(criteria);
	}

	private boolean isDate(String str) {

		if (StringUtils.isNotBlank(str)) {
			try {
				Date date = TimeUtils.strUsToDate(str);
				if (date != null) {
					return true;
				}
			} catch (Exception e) {

			}
		}
		return false;
	}
}
