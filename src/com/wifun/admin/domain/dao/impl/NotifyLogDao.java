package com.wifun.admin.domain.dao.impl;

import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.INotifyLogDao;
import com.wifun.admin.domain.model.NotifyLog;

@Repository("notifyLogDao")
public class NotifyLogDao extends BaseDao<NotifyLog, Integer> implements INotifyLogDao {

}
