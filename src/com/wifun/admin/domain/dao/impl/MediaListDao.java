package com.wifun.admin.domain.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.IMediaListDao;
import com.wifun.admin.domain.model.MediaList;

@Repository("mediaListDao")
public class MediaListDao extends BaseDao<MediaList, Integer> implements IMediaListDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<MediaList> findAllMedias(String title) {

		DetachedCriteria criteria = DetachedCriteria.forClass(MediaList.class);
		criteria.add(Restrictions.eq(MediaList.FN_STATUS, MediaList.STATUS_VALID));
		if (StringUtils.isNotBlank(title)) {
			criteria.add(Restrictions.like(MediaList.FN_TITLE, title, MatchMode.ANYWHERE));
		}

		return super.findByCriteria(criteria);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MediaList> findMediasByCompany(String title, int companyId) {

		DetachedCriteria criteria = DetachedCriteria.forClass(MediaList.class);
		criteria.add(Restrictions.eq(MediaList.FN_STATUS, MediaList.STATUS_VALID));
		criteria.add(Restrictions.or(Restrictions.eq(MediaList.FN_COMPANY_ID, companyId),
				Restrictions.eq(MediaList.FN_COMPANY_ID, 1)));
		if (StringUtils.isNotBlank(title)) {
			criteria.add(Restrictions.like(MediaList.FN_TITLE, title, MatchMode.ANYWHERE));
		}

		return super.findByCriteria(criteria);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MediaList> getActiveMediaList() {

		DetachedCriteria criteria = DetachedCriteria.forClass(MediaList.class);
		criteria.add(Restrictions.eq(MediaList.FN_STATUS, MediaList.STATUS_VALID));

		return super.findByCriteria(criteria);
	}
}