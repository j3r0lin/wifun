package com.wifun.admin.domain.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.IMediaDao;
import com.wifun.admin.domain.model.Media;
import com.wifun.admin.util.json.SqlColumnToBean;

@Repository("mediaDao")
public class MediaDao extends BaseDao<Media, Integer> implements IMediaDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<Media> search(Integer categoryId, String title) {

		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT ");
		sb.append("	* ");
		sb.append(" FROM ");
		sb.append("	media ");
		sb.append(" WHERE ");
		sb.append(" 1 = 1 ");
		if (categoryId != null && categoryId != 0) {
			sb.append(" AND id IN ( ");
			sb.append("	SELECT ");
			sb.append("		media_id ");
			sb.append("	FROM ");
			sb.append("		media_category ");
			sb.append("	WHERE ");
			sb.append("		category_id = " + categoryId);
			sb.append(" ) ");
		}
		if (StringUtils.isNotBlank(title))
			sb.append(" AND title LIKE '%" + title + "%'");

		SQLQuery requisitionDetailQuery = this.getSqlQuery(sb.toString());
		requisitionDetailQuery.setResultTransformer(new SqlColumnToBean<Media>(Media.class));
		List<Media> list = requisitionDetailQuery.list();
		if (null == list)
			return new ArrayList<Media>();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Media> findMediaByMediaListId(Integer mediaListId) {

		String strHql = " from Media where id in (select mediaId from MediaListMedia where medialistId=?)";
		return super.getHibernateTemplate().find(strHql, new Object[] { mediaListId });
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Media> findMediaExceptMediaListId(Integer mediaListId) {

		String strHql = " from Media where id not in (select mediaId from MediaListMedia where medialistId=?) and isActive = 1";
		return super.getHibernateTemplate().find(strHql, new Object[] { mediaListId });
	}

	@Override
	public boolean updateMovieUrl(Integer mediaId, String movieUrl) {

		String strHql = "update Media set media_url=? where id=?";
		try {
			super.getHibernateTemplate().bulkUpdate(strHql, new Object[] { movieUrl, mediaId });
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean updateHPosterUrl(Integer mediaId, String posterUrl) {

		String strHql = "update Media set hposter_url=? where id=?";
		try {
			super.getHibernateTemplate().bulkUpdate(strHql, new Object[] { posterUrl, mediaId });
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public boolean updateVPosterUrl(Integer mediaId, String posterUrl) {

		String strHql = "update Media set vposter_url=? where id=?";
		try {
			super.getHibernateTemplate().bulkUpdate(strHql, new Object[] { posterUrl, mediaId });
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Media> getMediasByLanguage(int language) {

		DetachedCriteria criteria = DetachedCriteria.forClass(Media.class);
		criteria.add(Restrictions.eq(Media.FN_LANGUAGE, language));
		criteria.add(Restrictions.eq(Media.FN_IS_ACTIVE, true));
		return super.findByCriteria(criteria);
	}
}
