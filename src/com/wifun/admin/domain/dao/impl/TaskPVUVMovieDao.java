package com.wifun.admin.domain.dao.impl;

import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.ITaskPVUVMovieDao;
import com.wifun.admin.domain.model.TaskPvuvMovie;
import com.wifun.admin.util.json.SqlColumnToBean;

@Repository("taskPVUVMovieDao")
public class TaskPVUVMovieDao extends BaseDao<TaskPvuvMovie, Integer> implements ITaskPVUVMovieDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<TaskPvuvMovie> getMoivePvuvList(int companyId, String date) {

		DetachedCriteria criteria = DetachedCriteria.forClass(TaskPvuvMovie.class);
		criteria.add(Restrictions.eq(TaskPvuvMovie.FN_COMPANY_ID, companyId));
		criteria.add(Restrictions.eq(TaskPvuvMovie.FN_DATE, date));

		criteria.addOrder(Order.desc(TaskPvuvMovie.FN_TOTAL_PV));

		return super.findByCriteria(criteria);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TaskPvuvMovie> getMoivePvuvSum(String date) {

		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT ");
		sb.append(" 	tpm.movie_title movieTitle, ");
		sb.append(" 	SUM(tpm.total_pv) totalPv, ");
		sb.append(" 	SUM(tpm.total_uv) totalUv, ");
		sb.append(" 	SUM(tpm.current_pv) currentPv, ");
		sb.append(" 	SUM(tpm.current_uv) currentUv ");
		sb.append(" FROM ");
		sb.append(" 	task_pvuv_movie tpm ");
		sb.append(" WHERE ");
		sb.append(" 	tpm.date = '" + date + "' ");
		sb.append(" GROUP BY ");
		sb.append(" 	tpm.movie_title ");
		sb.append(" ORDER BY totalPv DESC ");
		SQLQuery requisitionDetailQuery = this.getSqlQuery(sb.toString());
		requisitionDetailQuery.setResultTransformer(new SqlColumnToBean<TaskPvuvMovie>(
				TaskPvuvMovie.class));
		List<TaskPvuvMovie> list = requisitionDetailQuery.list();

		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TaskPvuvMovie> getMoivePvuvByMediaList(int mediaListId, String date) {

		StringBuffer sb = new StringBuffer();
		sb.append(" SELECT ");
		sb.append(" 	tpm.movie_title movieTitle, ");
		sb.append(" 	SUM(tpm.total_pv) totalPv, ");
		sb.append(" 	SUM(tpm.total_uv) totalUv, ");
		sb.append(" 	SUM(tpm.current_pv) currentPv, ");
		sb.append(" 	SUM(tpm.current_uv) currentUv ");
		sb.append(" FROM ");
		sb.append(" 	media_list_media mlm ");
		sb.append(" LEFT JOIN task_pvuv_movie tpm ON mlm.media_id = tpm.movie_id ");
		sb.append(" WHERE ");
		sb.append(" 	tpm.date = '" + date + "' ");
		sb.append(" AND mlm.media_list_id = " + mediaListId);
		sb.append(" GROUP BY ");
		sb.append(" 	tpm.movie_title ");
		sb.append(" ORDER BY ");
		sb.append(" 	totalPv DESC ");
		SQLQuery requisitionDetailQuery = this.getSqlQuery(sb.toString());
		requisitionDetailQuery.setResultTransformer(new SqlColumnToBean<TaskPvuvMovie>(
				TaskPvuvMovie.class));
		List<TaskPvuvMovie> list = requisitionDetailQuery.list();

		return list;
	}
}
