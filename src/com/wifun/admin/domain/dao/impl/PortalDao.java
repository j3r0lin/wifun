package com.wifun.admin.domain.dao.impl;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.IPortalDao;
import com.wifun.admin.domain.model.Portal;

@Repository("portalDao")
public class PortalDao extends BaseDao<Portal, Integer> implements IPortalDao {

	@Override
	public List<Portal> searchPortal(Integer companyId, String portalName) {

		DetachedCriteria criteria = DetachedCriteria.forClass(Portal.class);

		if (companyId > 1) {
			criteria.add(Restrictions.eq(Portal.FN_COMPANY_ID, companyId));
		}
		if (StringUtils.isNotBlank(portalName)) {
			criteria.add(Restrictions.like(Portal.FN_NAME, portalName, MatchMode.ANYWHERE));
		}

		return super.findByCriteria(criteria);
	}

	@Override
	public List<Portal> getPortalByCompanyId(Integer companyId) {

		DetachedCriteria criteria = DetachedCriteria.forClass(Portal.class);

		criteria.add(Restrictions.eq(Portal.FN_IS_ACTIVE, true));
		criteria.add(Restrictions.eq(Portal.FN_COMPANY_ID, companyId));

		return super.findByCriteria(criteria);
	}
}
