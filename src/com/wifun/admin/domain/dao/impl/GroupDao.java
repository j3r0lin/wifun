package com.wifun.admin.domain.dao.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.hibernate.SQLQuery;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.IGroupDao;
import com.wifun.admin.domain.model.Group;
import com.wifun.admin.util.json.SqlColumnToBean;

@Repository("groupDao")
public class GroupDao extends BaseDao<Group, Integer> implements IGroupDao {

	@Override
	public List<Group> getGroupsByCompanyId(Integer companyId) {

		DetachedCriteria criteria = DetachedCriteria.forClass(Group.class);
		criteria.add(Restrictions.eq(Group.FN_COMPANY_ID, companyId));
		return super.findByCriteria(criteria);
	}

	@Override
	public List<Group> searchGroups(Integer companyId, String groupName) {

		DetachedCriteria criteria = DetachedCriteria.forClass(Group.class);

		criteria.add(Restrictions.eq(Group.FN_COMPANY_ID, companyId));
		if (StringUtils.isNotBlank(groupName)) {
			criteria.add(Restrictions.like(Group.FN_NAME, groupName, MatchMode.ANYWHERE));
		}
		return super.findByCriteria(criteria);
	}

	@Override
	public boolean updateColumn(String columnName, Object columnValue, Integer id) {

		String strHql = "update Group set " + columnName + "=? where id=?";
		try {
			super.getHibernateTemplate().bulkUpdate(strHql, new Object[] { columnValue, id });
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public Group getGroupByName(int companyId, String name) {

		DetachedCriteria criteria = DetachedCriteria.forClass(Group.class);
		criteria.add(Restrictions.eq(Group.FN_COMPANY_ID, companyId));
		criteria.add(Restrictions.eq(Group.FN_NAME, name));
		return super.findFirstByCriteria(criteria);
	}
}
