package com.wifun.admin.domain.dao.impl;

import org.springframework.stereotype.Repository;

import com.wifun.admin.domain.dao.IPublishSyncLogDao;
import com.wifun.admin.domain.model.PublishSyncLog;

@Repository("publishSyncLogDao")
public class PublishSyncLogDao extends BaseDao<PublishSyncLog, Integer> implements
		IPublishSyncLogDao {

	@Override
	public boolean updateMediaPublishState(Integer deployId, String flag) {

		try {
			String sql = "update media_deploy_detail d set d.`status`=(select state from publish_sync_log l where l.flag='"
					+ flag
					+ "'"
					+ " and exists (select id from device dv where dv.id=d.device_id and dv.inhand_id=l.device_inhand_id)) "
					+ "where deploy_id=" + deployId;
			// return super.getHibernateTemplate().bulkUpdate(sql, new
			// Object[]{flag, deployId});
			super.getSqlQuery(sql).executeUpdate();

			// sql = "update media_deploy m set inhand_status=2 where id=" +
			// deployId
			// +
			// " and not exists (select id from media_deploy_detail d where d.deploy_id="
			// + deployId
			// + " and d.`status` < 2)";
			// super.getSqlQuery(sql).executeUpdate();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}

	}

	@Override
	public boolean updatePortalPublishState(Integer deployId, String flag) {

		try {
			String sql = "update portal_deploy_detail d set d.`status`=(select state from publish_sync_log l where l.flag='"
					+ flag
					+ "'"
					+ " and exists (select id from device dv where dv.id=d.device_id and dv.inhand_id=l.device_inhand_id)) "
					+ "where deploy_id=" + deployId;
			super.getSqlQuery(sql).executeUpdate();

			sql = "update portal_deploy m set inhand_status=2 where id=" + deployId
					+ " and not exists (select id from portal_deploy_detail d where d.deploy_id="
					+ deployId + " and d.`status` < 2)";
			super.getSqlQuery(sql).executeUpdate();

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}
}
