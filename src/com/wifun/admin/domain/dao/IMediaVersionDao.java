package com.wifun.admin.domain.dao;

import com.wifun.admin.domain.model.MediaVersion;

public interface IMediaVersionDao extends IBaseDao<MediaVersion, Integer> {

	MediaVersion getLastMediaVersion(int mediaId);

	MediaVersion getCurrentMediaVersion(int mediaId);

}
