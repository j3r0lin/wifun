package com.wifun.admin.domain.service;

import java.util.List;

import com.wifun.admin.domain.model.TaskTmobileDataUse;

public interface ITmobilePageReportService {

	/**
	 * 根据日期查询
	 * 
	 * @param date
	 * @return
	 */
	List<TaskTmobileDataUse> getDataUsageByDate(String date);
}
