package com.wifun.admin.domain.service;

import java.util.List;

import com.wifun.admin.domain.model.PortalDeployDetail;

public interface IPortalDeployDetailService extends IBaseService<PortalDeployDetail, Integer> {

	/**
	 * 根据deployId 获取 Portal发布详情
	 * 
	 * @param deployId
	 * @return
	 */
	List<PortalDeployDetail> loadByDeployId(Integer deployId);
}
