package com.wifun.admin.domain.service;

import java.util.List;

import com.wifun.admin.domain.model.MediaDeploy;

public interface IMediaDeployService extends IBaseService<MediaDeploy, Integer> {

	/**
	 * 新建媒体资源发布信息
	 * 
	 * @param md
	 * @return
	 */
	boolean deploy(MediaDeploy md);

	/**
	 * 通过公司 查询媒体资源发布信息
	 * 
	 * @param companyId
	 * @return
	 */
	List<MediaDeploy> loadByCompany(Integer companyId);

	/**
	 * 查询媒体资源发布列表
	 * 
	 * @param companyId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<MediaDeploy> searchMediaDeployList(int companyId, String startDate, String endDate);

	/**
	 * 保存 SD卡媒体资源发布
	 * 
	 * @param mediaDeploy
	 * @param devices
	 * @return
	 */
	MediaDeploy deploySD(MediaDeploy mediaDeploy, String[] devices);

	/**
	 * 更新发布结果状态
	 * 
	 * @param version
	 */
	void updateDeployStatusByDeviceStatus(String version);
}
