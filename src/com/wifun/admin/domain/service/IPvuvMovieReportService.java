package com.wifun.admin.domain.service;

import java.util.ArrayList;

import com.wifun.admin.app.vo.PvuvMovieVo;

import com.wifun.inhand.response.statistic.PvuvResource;

public interface IPvuvMovieReportService {

	/**
	 * 查询 pvuv 信息列表
	 * 
	 * @param token
	 * @param mediaListId
	 * @return
	 */
	ArrayList<PvuvMovieVo> queryMoviePvuv(String token, int mediaListId);

	/**
	 * 查询 pvuv 信息列表
	 * 
	 * @param token
	 * @return
	 */
	ArrayList<PvuvMovieVo> queryMoviePvuv(String token);

	/**
	 * 查询 pvuv 信息列表
	 * 
	 * @param tid
	 * @param token
	 * @param siteID
	 * @return
	 */
	ArrayList<PvuvResource> resourceStat(String tid, String token, String siteID);
}
