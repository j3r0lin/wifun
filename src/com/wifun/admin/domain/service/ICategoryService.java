package com.wifun.admin.domain.service;

import java.util.List;

import com.wifun.admin.domain.model.Category;

public interface ICategoryService extends IBaseService<Category, Integer> {

	/**
	 * 获取电影分类列表
	 * 
	 * @param active
	 * @return
	 */
	List<Category> getCategoryList(boolean active);

}
