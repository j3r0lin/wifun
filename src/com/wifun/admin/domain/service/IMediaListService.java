package com.wifun.admin.domain.service;

import java.util.List;

import com.wifun.admin.domain.model.MediaList;

public interface IMediaListService extends IBaseService<MediaList, Integer> {

	/**
	 * 增加媒体列表中选择的媒体资源 只增加
	 * 
	 * @param mediaListId
	 * @param mediaIdList
	 * @return
	 */
	public boolean addMedias(Integer mediaListId, List<Integer> mediaIdList);

	/**
	 * 增加媒体列表中选择的媒体资源 增加不存在的
	 * 
	 * @param mediaListId
	 * @param mediaIdList
	 * @return
	 */
	public boolean saveMedias(Integer mediaListId, List<Integer> mediaIdList);

	/**
	 * 移除 媒体资源
	 * 
	 * @param mediaListId
	 * @param mediaId
	 * @return
	 */
	public boolean removeMedia(Integer mediaListId, Integer mediaId);

	/**
	 * 查询所有的媒体资源列表
	 * 
	 * @param title
	 * @return
	 */
	public List<MediaList> getAllMedias(String title);

	/**
	 * 得到某个公司的 妹子资源列表
	 * 
	 * @param title
	 * @param companyId
	 * @return
	 */
	public List<MediaList> getCompanyMedias(String title, int companyId);

	/**
	 * 保存巴士公司的 媒体资源列表
	 * 
	 * @param bo
	 * @param companyId
	 * @param creator
	 * @return
	 */
	public boolean saveMediaList(MediaList bo, int companyId, int creator);

	/**
	 * 更新巴士公司妹子资源列表
	 * 
	 * @param id
	 * @param bo
	 * @param companyId
	 * @return
	 */
	public boolean updateMediaList(Integer id, MediaList bo, int companyId);

	/**
	 * 删除媒体资源列表
	 * 
	 * @param id
	 * @return
	 */
	public boolean deleteMediaList(Integer id);

}
