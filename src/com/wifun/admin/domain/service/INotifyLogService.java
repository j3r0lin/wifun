package com.wifun.admin.domain.service;

public interface INotifyLogService {

	/**
	 * 保存VlistLog 信息
	 * 
	 * @param version
	 * @param sn
	 * @param reasion
	 * @param syncMode
	 */
	public void saveVlistLog(String version, String sn, int reasion, String syncMode);

	/**
	 * 保存电影日志信息
	 * 
	 * @param type
	 * @param version
	 * @param sn
	 * @param media
	 * @param reasion
	 * @param syncMode
	 */
	public void saveMediaLog(int type, String version, String sn, String media, int reasion,
			String syncMode);

}
