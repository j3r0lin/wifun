package com.wifun.admin.domain.service;

import java.util.List;

import com.wifun.admin.app.vo.ResultVo;
import com.wifun.admin.domain.model.Company;

public interface ICompanyService {

	/**
	 * 根據公司ID找到公司实体
	 * 
	 * @param id
	 * @return
	 */
	public Company findById(Integer id);

	/**
	 * 根據公司账号找到公司实体
	 * 
	 * @param id
	 * @return
	 */
	public Company findByAccount(String account);

	/**
	 * 根據公司列表
	 * 
	 * @param id
	 * @return
	 */
	public List<Company> getCompanyList(boolean active, String name);

	public List<Company> getCompanyAllList();

	/**
	 * 新增公司信息
	 * 
	 * @param id
	 * @return
	 */
	public ResultVo addCompany(Company c);

	public ResultVo updateCompany(Company c);

	/**
	 * 根據用戶名和密码验证账户
	 * 
	 * @param username
	 * @param pwd
	 * @return
	 */
	public Company validAccount(String username, String pwd);

	/**
	 * 保存用户最后登陆时间
	 * 
	 * @param id
	 */
	public boolean updateLastLoginTime(Company c);

	/***************************************************************************
	 * 
	 * /** 检查用户名是否存在
	 * 
	 * @param userName
	 * @return
	 */
	public boolean checkUserName(String userName);

	/**
	 * 修改密码
	 * 
	 * @param id
	 * @param newPwd
	 * @return
	 */
	public boolean updatePwd(Integer id, String newPwd);

	/**
	 * 更新InhandId方法
	 * 
	 * @param companyId
	 * @param inhandId
	 * @return
	 */
	public boolean updateInhandId(Integer companyId, String inhandId);

	/**
	 * 验证公司名称唯一性
	 * 
	 * @param name
	 * @return
	 */
	public boolean checkCompanyName(String name);

	/**
	 * 验证公司邮件唯一性
	 * 
	 * @param email
	 * @return
	 */
	public boolean checkCompanyEmail(String email);

	/**
	 * 获取公司列表
	 * 
	 * @param active
	 * @return
	 */
	public List<Company> getCompanyList(boolean active);

}
