package com.wifun.admin.domain.service;

import java.util.List;

import com.wifun.admin.domain.model.Media;
import com.wifun.admin.domain.model.MediaCategory;

public interface IMediaService {

	/**
	 * 根据mediaId 获取影片分类
	 * 
	 * @param mediaId
	 * @return
	 */
	List<MediaCategory> loadCategoryList(Integer mediaId);

	/**
	 * 删除影片分类列表
	 * 
	 * @param mediaId
	 */
	void deleteCategoryList(int mediaId);

	/**
	 * 保存列表
	 * 
	 * @param mediaId
	 * @param categoryList
	 * @return
	 */
	boolean saveCategoryList(Integer mediaId, List<Integer> categoryList);

	/**
	 * 得到影片列表详情
	 * 
	 * @param mediaListId
	 * @return
	 */
	List<Media> loadMediaListDetail(Integer mediaListId);

	/**
	 * 得到可用的影片
	 * 
	 * @param mediaListId
	 * @return
	 */
	List<Media> loadAvailableMedia(Integer mediaListId);

	/**
	 * 更新电影Url
	 * 
	 * @param mediaId
	 * @param movieUrl
	 * @return
	 */
	boolean updateMovieUrl(int mediaId, String movieUrl);

	/**
	 * 更新HPosterUrl
	 * 
	 * @param mediaId
	 * @param posterUrl
	 * @return
	 */
	boolean updateHPosterUrl(int mediaId, String posterUrl);

	/**
	 * 更新VPosterUrl
	 * 
	 * @param mediaId
	 * @param posterUrl
	 * @return
	 */
	boolean updateVPosterUrl(int mediaId, String posterUrl);

	/**
	 * 影片查询
	 * 
	 * @param categoryId
	 * @param title
	 * @return
	 */
	List<Media> search(Integer categoryId, String title);

	Media findById(Integer id);

	void deleteById(Integer id);

	boolean update(Media vo);

	boolean create(Media vo);

	/**
	 * 加载所有的影片
	 * 
	 * @return
	 */
	List<Media> loadAll();

}
