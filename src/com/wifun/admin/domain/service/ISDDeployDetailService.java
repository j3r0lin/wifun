package com.wifun.admin.domain.service;

import java.util.List;

import com.wifun.admin.app.vo.SDDeployDetailVo;
import com.wifun.admin.domain.model.MediaDeployDetail;

public interface ISDDeployDetailService {

	/**
	 * 得到sd 发布详细信息
	 * 
	 * @param deployId
	 * @return
	 */
	List<SDDeployDetailVo> getSDDeployDetailVo(int deployId);

	/**
	 * 得到单个电影的发布详情
	 * 
	 * @param deployId
	 * @param mediaId
	 * @return
	 */
	List<MediaDeployDetail> getSDDeployDeviceDetail(int deployId, int mediaId);
}
