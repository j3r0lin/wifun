package com.wifun.admin.domain.service;

import java.util.List;

import com.wifun.admin.domain.model.PortalDeploy;

public interface IPortalDeployService extends IBaseService<PortalDeploy, Integer> {

	/**
	 * PortalDeploy 信息保存
	 * 
	 * @param pd
	 * @return
	 */
	public boolean deploy(PortalDeploy pd);

	/**
	 * 根据公司 查询PortalDeploy列表
	 * 
	 * @param companyId
	 * @return
	 */
	public List<PortalDeploy> loadByCompany(Integer companyId);

	/**
	 * 查询PortalDeploy
	 * 
	 * @param companyId
	 * @param description
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	public List<PortalDeploy> searchPortal(int companyId, String description, String startDate,
			String endDate);
}
