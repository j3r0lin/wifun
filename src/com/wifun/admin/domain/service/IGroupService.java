package com.wifun.admin.domain.service;

import java.util.List;

import com.wifun.admin.domain.model.Group;

public interface IGroupService extends IBaseService<Group, Integer> {

	/**
	 * 根据companyId 获取分组
	 * 
	 * @param companyId
	 * @return
	 */
	public List<Group> getGroupsByCompanyId(Integer companyId);

	/**
	 * 根据conapnyId ，group名称 获取分组List 模糊查询
	 * 
	 * @param companyId
	 * @param groupName
	 * @return
	 */
	public List<Group> searchGroups(Integer companyId, String groupName);

	/**
	 * 根据 companyId,name 获取单个 group 信息
	 * 
	 * @param companyId
	 * @param name
	 * @return
	 */
	public Group getGroupByName(int companyId, String name);
}
