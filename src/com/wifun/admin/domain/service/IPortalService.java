package com.wifun.admin.domain.service;

import java.io.File;
import java.util.List;

import com.wifun.admin.domain.model.Portal;

public interface IPortalService extends IBaseService<Portal, Integer> {

	public List<Portal> loadByCompany(Integer companyId);

	/**
	 * 保存Portal 信息
	 * 
	 * @param companyId
	 * @param portalName
	 * @param portalPackage
	 * @return
	 */
	public boolean savePortalPackage(Integer companyId, String portalName, File portalPackage);

	/**
	 * 查询Portal
	 * 
	 * @param companyId
	 * @param portalName
	 * @return
	 */
	public List<Portal> searchPortal(Integer companyId, String portalName);

	/**
	 * 根据companyId 得到 Portal
	 * 
	 * @param companyId
	 * @return
	 */
	List<Portal> getPortalByCompanyId(Integer companyId);

	/**
	 * 更新状态
	 * 
	 * @param id
	 * @param isActive
	 * @return
	 */
	public boolean updateStatus(Integer id, boolean isActive);
}
