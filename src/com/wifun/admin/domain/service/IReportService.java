package com.wifun.admin.domain.service;

import java.util.List;

import com.wifun.admin.app.vo.DailyReportCompanyVo;
import com.wifun.admin.app.vo.DailyReportVo;
import com.wifun.admin.domain.model.TaskPvuvMovie;

public interface IReportService {

	/**
	 * 根据条件得到 DailyReport
	 * 
	 * @param companyId
	 * @param groupId
	 * @param date
	 * @return
	 */
	List<DailyReportVo> getReportDailyVo(int companyId, int groupId, String date);

	/**
	 * 根据条件得到公司 报表
	 * 
	 * @param date
	 * @return
	 */
	List<DailyReportCompanyVo> getCompanyDailyReport(String date);

	/**
	 * 得到对应日期的 pvuv统计
	 * 
	 * @param date
	 * @return
	 */
	List<TaskPvuvMovie> getMoivePvuvSum(String date);

	/**
	 * 根据媒体资源列表 得到pvuv 统计
	 * 
	 * @param mediaListId
	 * @param date
	 * @return
	 */
	List<TaskPvuvMovie> getMoivePvuvByMediaList(int mediaListId, String date);
}
