package com.wifun.admin.domain.service;

import java.util.List;

import com.wifun.admin.app.vo.PvuvPageVo;

public interface IPvuvPageReportService {

	/**
	 * 根据公司Id得到 pvuv 统计
	 * 
	 * @param companyId
	 * @return
	 */
	List<PvuvPageVo> getTypesPvuvSum(int companyId);
}
