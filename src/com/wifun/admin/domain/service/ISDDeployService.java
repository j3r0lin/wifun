package com.wifun.admin.domain.service;

import java.util.List;

import com.wifun.admin.domain.model.MediaDeploy;

public interface ISDDeployService {

	/**
	 * 查询电影发布信息列表
	 * 
	 * @param companyId
	 * @param startDate
	 * @param endDate
	 * @return
	 */
	List<MediaDeploy> searchDeployList(int companyId, String startDate, String endDate);

	/**
	 * 通过id 查询
	 * 
	 * @param id
	 * @return
	 */
	MediaDeploy findById(Integer id);
}
