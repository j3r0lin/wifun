package com.wifun.admin.domain.service;

import java.util.List;

import com.wifun.admin.domain.model.MediaDeployDetail;

public interface IMediaDeployDetailService extends IBaseService<MediaDeployDetail, Integer> {

	/**
	 * 根据deployId 查询电影发布情况
	 * 
	 * @param deployId
	 * @return
	 */
	List<MediaDeployDetail> loadByDeployId(Integer deployId);
}
