package com.wifun.admin.domain.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.app.vo.SDDeployDetailVo;
import com.wifun.admin.domain.dao.IMediaDeployDetailDao;
import com.wifun.admin.domain.model.MediaDeployDetail;
import com.wifun.admin.domain.service.ISDDeployDetailService;

@Service("sdDeployDetailService")
@Transactional
public class SDDeployDetailService implements ISDDeployDetailService {

	@Resource(name = "mediaDeployDetailDao")
	private IMediaDeployDetailDao mediaDeployDetailDao;

	@Override
	public List<SDDeployDetailVo> getSDDeployDetailVo(int deployId) {

		return mediaDeployDetailDao.getSDDeployDetail(deployId);
	}

	@Override
	public List<MediaDeployDetail> getSDDeployDeviceDetail(int deployId, int mediaId) {

		return mediaDeployDetailDao.getSDDeployDeviceDetail(deployId, mediaId);
	}
}
