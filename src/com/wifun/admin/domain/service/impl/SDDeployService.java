package com.wifun.admin.domain.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.domain.dao.IMediaDeployDao;
import com.wifun.admin.domain.model.MediaDeploy;
import com.wifun.admin.domain.service.ISDDeployService;

@Service("sdDeployService")
@Transactional
public class SDDeployService implements ISDDeployService {

	@Resource(name = "mediaDeployDao")
	private IMediaDeployDao mediaDeployDao;

	@Override
	public List<MediaDeploy> searchDeployList(int companyId, String startDate, String endDate) {

		return mediaDeployDao.searchSDDeployList(companyId, startDate, endDate);
	}

	@Override
	public MediaDeploy findById(Integer id) {

		return mediaDeployDao.get(id);
	}
}
