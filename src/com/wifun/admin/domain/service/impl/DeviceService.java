package com.wifun.admin.domain.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.app.vo.DeviceLocation;
import com.wifun.admin.app.vo.ResultVo;
import com.wifun.admin.domain.dao.ICompanyDao;
import com.wifun.admin.domain.dao.IDeviceDao;
import com.wifun.admin.domain.dao.IGroupDao;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.Group;
import com.wifun.admin.domain.service.IDeviceService;
import com.wifun.admin.util.LocationFileUtil;
import com.wifun.admin.util.TimeUtils;
import com.wifun.inhand.model.Location;
import com.wifun.inhand.model.Router;
import com.wifun.inhand.model.RouterInfo;
import com.wifun.inhand.model.Site;
import com.wifun.inhand.response.device.RouterListResponse;
import com.wifun.inhand.response.device.RouterResponse;
import com.wifun.inhand.response.site.SiteListResponse;
import com.wifun.inhand.response.tag.TagResponse;
import com.wifun.inhand.service.InhandDeviceService;
import com.wifun.inhand.service.InhandGroupService;
import com.wifun.inhand.service.InhandSiteService;

@Service("deviceService")
@Transactional
public class DeviceService implements IDeviceService {

	@Resource(name = "deviceDao")
	private IDeviceDao deviceDao;

	@Resource(name = "companyDao")
	private ICompanyDao companyDao;

	@Resource(name = "groupDao")
	private IGroupDao groupDao;

	@Override
	public Device findById(Integer id) {

		Device d = deviceDao.get(id);
		d.setCompanyName(companyDao.get(d.getCompanyId()).getName());
		if (d.getGroupId() > 0) {
			d.setGroupName(groupDao.get(d.getGroupId()).getName());
		} else {
			d.setGroupName("default group");
		}
		return d;
	}

	@Override
	public List<Device> loadAll() {

		return fillGroupInfo(deviceDao.loadAll());
	}

	@Override
	public List<Device> findByExample(Device t) {

		return fillGroupInfo(deviceDao.findByExample(t));
	}

	@Override
	public List<Device> searchDevices(boolean active, Integer companyId, int groupId, String sn,
			String name) {

		return fillGroupInfo(deviceDao.searchDevices(active, companyId, groupId, sn, name));
	}

	@Override
	public List<Device> getDeviceList(int companyId, int groupId) {

		return fillGroupInfo(deviceDao.getDeviceList(companyId, groupId));
	}

	@Override
	public boolean create(Device entity) {

		return false;
	}

	@Override
	public ResultVo addDevice(Device entity) {

		ResultVo resultVo = new ResultVo();
		RouterResponse response = InhandDeviceService.addDevice(
				SessionHelper.getToken(entity.getCompanyId()), entity);
		if (response == null) {
			resultVo.setCode(ResultVo.CODE_SYSTEM_ERROR);
			resultVo.setMsg(ResultVo.MSG_SYSTEM_ERROR);
			return resultVo;
		}
		Router router = response.getRouter();
		if (router == null) {
			resultVo.setCode(ResultVo.CODE_API_ERROR);
			resultVo.setMsg(response.getError());
			return resultVo;
		}
		entity.setInhandSiteId(router.getSiteId());
		entity.setInhandId(router.get_id());
		entity.setIsActive(true);
		deviceDao.save(entity);
		if (entity.getGroupId() > 0) {
			Group group = groupDao.get(entity.getGroupId());
			// 分组没有备案进行分组添加
			if (null == group.getInhandId()) {
				TagResponse tagRes = InhandGroupService.addTag(
						SessionHelper.getToken(entity.getCompanyId()), group);
				if (tagRes == null) {
					// TODO
					resultVo.setCode(ResultVo.CODE_SYSTEM_ERROR);
					resultVo.setMsg(ResultVo.MSG_SYSTEM_ERROR);
					return resultVo;
				}
				group.setInhandId(tagRes.getTag().get_id());
			}
			InhandDeviceService.putTag(SessionHelper.getToken(entity.getCompanyId()),
					entity.getInhandSiteId(), group.getInhandId());
		}
		return resultVo;
	}

	@Override
	public boolean save(Device entity) {

		// try {
		// return update(entity);
		// } catch (Exception e) {
		// return false;
		// }
		return false;
	}

	@Override
	public ResultVo updateDevice(Device entity) {

		ResultVo resultVo = new ResultVo();
		Device dev = deviceDao.get(entity.getId());
		Integer oldGroupId = dev.getGroupId();
		dev.updateInfo(entity);

		RouterResponse response = InhandDeviceService.updateDevice(
				SessionHelper.getToken(entity.getCompanyId()), dev);

		if (response == null) {
			resultVo.setCode(ResultVo.CODE_SYSTEM_ERROR);
			resultVo.setMsg(ResultVo.MSG_SYSTEM_ERROR);
			return resultVo;
		}
		Router router = response.getRouter();
		if (router == null) {
			resultVo.setCode(ResultVo.CODE_API_ERROR);
			resultVo.setMsg(response.getError());
			return resultVo;
		}

		deviceDao.update(dev);

		// //如果分组有调整，更新到Inhand
		Group group = null;
		// //之前是默认分组
		if (0 == oldGroupId) {
			if (0 != entity.getGroupId()) {
				group = groupDao.get(entity.getGroupId());
				if (null == group.getInhandId()) // /分组没有备案进行分组添加
				{
					TagResponse tagRes = InhandGroupService.addTag(
							SessionHelper.getToken(entity.getCompanyId()), group);
					if (tagRes == null) {
						resultVo.setCode(ResultVo.CODE_SYSTEM_ERROR);
						resultVo.setMsg(ResultVo.MSG_SYSTEM_ERROR);
						return resultVo;
					}
					group.setInhandId(tagRes.getTag().get_id());
				}
				InhandDeviceService.putTag(SessionHelper.getToken(entity.getCompanyId()),
						entity.getInhandSiteId(), group.getInhandId());
			}
		} else if (0 == entity.getGroupId()) {
			group = groupDao.get(oldGroupId);

			if (null != group.getInhandId())
				InhandDeviceService.removeTag(SessionHelper.getToken(entity.getCompanyId()),
						entity.getInhandSiteId(), group.getInhandId());
		} else if (entity.getGroupId() != oldGroupId) {
			// //删除老分组
			group = groupDao.get(oldGroupId);
			if (null != group.getInhandId())
				InhandDeviceService.removeTag(SessionHelper.getToken(entity.getCompanyId()),
						entity.getInhandSiteId(), group.getInhandId());

			group = groupDao.get(entity.getGroupId());
			// /分组没有备案进行分组添加
			if (null == group.getInhandId()) {
				TagResponse tagRes = InhandGroupService.addTag(
						SessionHelper.getToken(entity.getCompanyId()), group);
				if (tagRes == null) {
					resultVo.setCode(ResultVo.CODE_SYSTEM_ERROR);
					resultVo.setMsg(ResultVo.MSG_SYSTEM_ERROR);
					return resultVo;
				}
				group.setInhandId(tagRes.getTag().get_id());
			}
			InhandDeviceService.putTag(SessionHelper.getToken(entity.getCompanyId()),
					entity.getInhandSiteId(), group.getInhandId());
		}

		return resultVo;
	}

	@Override
	public ResultVo deleteDeviceById(Integer id) {

		ResultVo resultVo = new ResultVo();

		Device device = deviceDao.get(id);
		if (device == null) {
			resultVo.setCode(ResultVo.CODE_SYSTEM_ERROR);
			resultVo.setMsg(ResultVo.MSG_SYSTEM_ERROR);
			return resultVo;
		}
		RouterResponse response = InhandDeviceService.deleteDevice(
				SessionHelper.getToken(device.getCompanyId()), device.getInhandId());

		if (response == null) {
			resultVo.setCode(ResultVo.CODE_SYSTEM_ERROR);
			resultVo.setMsg(ResultVo.MSG_SYSTEM_ERROR);
			return resultVo;
		}
		if (StringUtils.isNotBlank(response.getError())) {
			resultVo.setCode(ResultVo.CODE_API_ERROR);
			resultVo.setMsg(response.getError());
			return resultVo;
		}
		device.setIsActive(false);
		deviceDao.update(device);
		return resultVo;
	}

	@Override
	public boolean updateInfoFromInhaned(Device device) {

		RouterListResponse response = InhandDeviceService.queryDevices(
				SessionHelper.getToken(device.getCompanyId()), device.getSn());
		List<Router> routers = response.getRouters();
		if (routers == null || routers.isEmpty())
			return false;

		Router router = routers.get(0);
		RouterInfo info = router.getInfo();
		if (info != null) {
			device.setHardwareVersion(router.getInfo().getSwVersion());
			device.setImsi(router.getInfo().getImsi());
		}
		deviceDao.saveOrUpdate(device);

		return true;
	}

	@Override
	public void deleteById(Integer id) {

	}

	@Override
	public void saveOrUpdate(Device entity) {

		deviceDao.saveOrUpdate(entity);
	}

	@Override
	public void saveOrUpdateAll(Collection<Device> entities) {

		deviceDao.saveOrUpdateAll(entities);
	}

	@Override
	public void delete(Device entity) {

		// deviceDao.delete(entity);
	}

	@Override
	public void deleteAll(Collection<Device> entities) {

		for (Device dev : entities)
			deviceDao.delete(dev);
	}

	@Override
	public List<Device> loadByCompany(Integer companyId) {

		return fillGroupInfo(deviceDao.loadByCompany(companyId));
	}

	@Override
	public List<Device> loadByGroup(Group group) {

		return fillGroupInfo(deviceDao.loadByGroup(group));
	}

	private List<Device> fillGroupInfo(List<Device> devList) {

		if (null == devList)
			return null;

		for (int i = 0; i < devList.size(); i++) {
			Device d = devList.get(i);
			d.setCompanyName(companyDao.get(d.getCompanyId()).getName());
			if (d.getGroupId() > 0) {
				d.setGroupName(groupDao.get(d.getGroupId()).getName());
			} else {
				d.setGroupName("default group");
			}
		}
		return devList;
	}

	@Override
	public boolean update(Device entity) {

		return false;
	}

	@Override
	public List<DeviceLocation> getDeviceLocations(int companyId) {

		List<DeviceLocation> DeviceLocations = new ArrayList<DeviceLocation>();
		Map<String, Device> deviceMap = dealDeviceMap(companyId);

		SiteListResponse response = InhandSiteService.querySites(SessionHelper.getToken(companyId));
		List<Site> sites = response.getSites();
		if (sites == null || sites.size() == 0)
			return null;

		for (int j = 0; j < sites.size(); j++) {

			Site site = sites.get(j);

			String siteId = site.get_id();
			Location location = site.getLocation();
			Device device = deviceMap.get(siteId);
			if (device == null || location == null)
				continue;

			DeviceLocation deviceLocation = new DeviceLocation(device, location, site.getOnline(),
					TimeUtils.getNowDate());

			DeviceLocations.add(deviceLocation);
		}

		return DeviceLocations;
	}

	@Override
	public List<DeviceLocation> getDeviceRoute(int deviceId, String date) {

		Device device = deviceDao.get(deviceId);
		List<DeviceLocation> DeviceLocations = LocationFileUtil.readDeviceLocations(device.getSn(),
				date);
		return DeviceLocations;
	}

	private Map<String, Device> dealDeviceMap(int companyId) {

		Map<String, Device> deviceMap = new HashMap<String, Device>();
		List<Device> devices = deviceDao.searchDevices(false, companyId, -1, null, null);
		devices = fillGroupInfo(devices);

		for (int i = 0; i < devices.size(); i++) {

			Device device = devices.get(i);
			if (StringUtils.isNotBlank(device.getInhandSiteId()))
				deviceMap.put(device.getInhandSiteId(), device);
		}
		return deviceMap;
	}

	@Override
	public Map<String, Device> getDeviceMapByInhandId(int companyId) {

		Map<String, Device> deviceMap = new HashMap<String, Device>();
		List<Device> devices = deviceDao.searchDevices(false, companyId, -1, null, null);
		devices = fillGroupInfo(devices);

		for (int i = 0; i < devices.size(); i++) {

			Device device = devices.get(i);
			if (StringUtils.isNotBlank(device.getInhandId()))
				deviceMap.put(device.getInhandId().toLowerCase(), device);
		}
		return deviceMap;
	}
}
