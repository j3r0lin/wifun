package com.wifun.admin.domain.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.app.vo.PvuvMovieVo;
import com.wifun.admin.domain.dao.IMediaDao;
import com.wifun.admin.domain.dao.IMediaListMediaDao;
import com.wifun.admin.domain.model.Media;
import com.wifun.admin.domain.model.MediaListMedia;
import com.wifun.admin.domain.service.IPvuvMovieReportService;
import com.wifun.inhand.response.statistic.PvuvResource;
import com.wifun.inhand.service.InhandPVUVService;

@Service("pvuvMovieReportService")
@Transactional
public class PvuvMovieReportService implements IPvuvMovieReportService {

	@Resource(name = "mediaDao")
	private IMediaDao mediaDao;

	@Resource(name = "mediaListMediaDao")
	private IMediaListMediaDao mediaListMediaDao;

	/*
	 * 获取指定栏目下的所有页面及概要信息 http://cloud.ilikebus.com/
	 * api/pvuv/resources?tid=movie&
	 * sort=total.pv&direction=0&access_token=d766a9f50b2718e94b218d525e7e2e9e
	 */
	@Override
	public ArrayList<PvuvResource> resourceStat(String tid, String token, String siteID) {

		ArrayList<PvuvResource> result = InhandPVUVService.queryPvuvResource(tid, token, siteID);

		return result;
	}

	// 根据companyId获取数据
	@Override
	public ArrayList<PvuvMovieVo> queryMoviePvuv(String token, int mediaListId) {

		ArrayList<PvuvMovieVo> result = InhandPVUVService.queryPvuvResource(token, "movie");
		ArrayList<PvuvMovieVo> vos = new ArrayList<PvuvMovieVo>();

		// HashMap<Integer, String> movieMap= new HashMap<Integer, String>();
		HashMap<Integer, PvuvMovieVo> movieMap = new HashMap<Integer, PvuvMovieVo>();
		// List<Media> movies= mediaDao.getMediasByLanguage(0);
		List<MediaListMedia> mediaListMedias = mediaListMediaDao
				.getMediaListMediaByMediaListId(mediaListId);

		for (int i = 0; i < mediaListMedias.size(); i++) {

			Media mv = mediaListMedias.get(i).getMedia();
			movieMap.put(mv.getId(), new PvuvMovieVo(mv.getId() + "", mv.getTitle()));
		}

		// 格式化result中的电影名称
		for (int i = 0; i < result.size(); i++) {

			PvuvMovieVo vo = result.get(i);
			String inhandRid = vo.getRId().trim();
			if ("0".equals(inhandRid))
				continue;

			int movId = getMovieId(inhandRid);
			if (movieMap.get(movId) == null)
				continue;

			PvuvMovieVo tmp = movieMap.get(movId);
			tmp.setTotalPv(tmp.getTotalPv() + vo.getTotalPv());
			tmp.setTotalUv(tmp.getTotalUv() + vo.getTotalUv());
			tmp.setCurrentPv(tmp.getCurrentPv() + vo.getCurrentPv());
			tmp.setCurrentUv(tmp.getCurrentUv() + vo.getCurrentUv());
			tmp.setLastPv(tmp.getLastPv() + vo.getLastPv());
			tmp.setLastUv(tmp.getLastUv() + vo.getLastUv());
		}

		Object[] keys = movieMap.keySet().toArray();
		for (int i = 0; i < keys.length; i++)
			vos.add(movieMap.get(keys[i]));

		Collections.sort(vos, new Comparator<PvuvMovieVo>() {

			public int compare(PvuvMovieVo arg0, PvuvMovieVo arg1) {

				int value0 = arg0.getTotalPv();
				int value1 = arg1.getTotalPv();

				return value1 - value0;
			}
		});

		return vos;
	}

	public int getMovieId(String inhandRid) {

		// 三种情况
		String realId = inhandRid;
		if (inhandRid.endsWith(")")) {
			// case 1 St.%20Vincent(14) Nightcrawler(16)
			realId = inhandRid
					.substring(inhandRid.lastIndexOf("(") + 1, inhandRid.lastIndexOf(")"));
		} else if (inhandRid.indexOf("%3A") > 0) {
			// case 2 2%3ALucy
			realId = inhandRid.substring(0, inhandRid.indexOf("%3A"));
		}

		// case 3: 4 6 直接是数字(ID)
		int movId = Integer.parseInt(realId);

		return movId;
	}

	@Override
	public ArrayList<PvuvMovieVo> queryMoviePvuv(String token) {

		ArrayList<PvuvMovieVo> result = InhandPVUVService.queryPvuvResource(token, "movie");
		ArrayList<PvuvMovieVo> vos = new ArrayList<PvuvMovieVo>();

		// HashMap<Integer, String> movieMap= new HashMap<Integer, String>();
		HashMap<Integer, PvuvMovieVo> movieMap = new HashMap<Integer, PvuvMovieVo>();
		List<Media> movies = mediaDao.loadAll();
		// List<MediaListMedia> mediaListMedias =
		// mediaListMediaDao.getMediaListMediaByMediaListId(mediaListId);

		for (int i = 0; i < movies.size(); i++) {

			Media mv = movies.get(i);
			movieMap.put(mv.getId(), new PvuvMovieVo(mv.getId() + "", mv.getTitle()));
		}

		// 格式化result中的电影名称
		for (int i = 0; i < result.size(); i++) {

			PvuvMovieVo vo = result.get(i);
			String inhandRid = vo.getRId().trim();
			if ("0".equals(inhandRid))
				continue;

			int movId = getMovieId(inhandRid);
			if (movieMap.get(movId) == null)
				continue;

			PvuvMovieVo tmp = movieMap.get(movId);
			tmp.setTotalPv(tmp.getTotalPv() + vo.getTotalPv());
			tmp.setTotalUv(tmp.getTotalUv() + vo.getTotalUv());
			tmp.setCurrentPv(tmp.getCurrentPv() + vo.getCurrentPv());
			tmp.setCurrentUv(tmp.getCurrentUv() + vo.getCurrentUv());
			tmp.setLastPv(tmp.getLastPv() + vo.getLastPv());
			tmp.setLastUv(tmp.getLastUv() + vo.getLastUv());
		}

		Object[] keys = movieMap.keySet().toArray();
		for (int i = 0; i < keys.length; i++)
			vos.add(movieMap.get(keys[i]));

		Collections.sort(vos, new Comparator<PvuvMovieVo>() {

			public int compare(PvuvMovieVo arg0, PvuvMovieVo arg1) {

				int value0 = arg0.getTotalPv();
				int value1 = arg1.getTotalPv();

				return value1 - value0;
			}
		});

		return vos;
	}
}
