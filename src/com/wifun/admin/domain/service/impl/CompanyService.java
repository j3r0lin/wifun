package com.wifun.admin.domain.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.app.vo.ResultVo;
import com.wifun.admin.domain.dao.ICompanyDao;
import com.wifun.admin.domain.dao.IDeviceDao;
import com.wifun.admin.domain.dao.IGroupDao;
import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.model.Group;
import com.wifun.admin.domain.service.ICompanyService;
import com.wifun.inhand.model.Organization;
import com.wifun.inhand.response.company.OrganizationResponse;
import com.wifun.inhand.service.InhandCompanyService;

@Service("companyService")
@Transactional
public class CompanyService implements ICompanyService {

	@Resource(name = "companyDao")
	private ICompanyDao companyDao;

	@Resource(name = "groupDao")
	private IGroupDao groupDao;

	@Resource(name = "deviceDao")
	private IDeviceDao deviceDao;

	/**
	 * 根据实体 增加公司
	 */
	@Override
	public ResultVo addCompany(Company c) {

		ResultVo resultVo = new ResultVo();
		OrganizationResponse response = InhandCompanyService.addCompany(c);
		if (response == null) {
			resultVo.setCode(ResultVo.CODE_SYSTEM_ERROR);
			resultVo.setMsg(ResultVo.MSG_SYSTEM_ERROR);
			return resultVo;
		}
		Organization org = response.getOrganization();
		if (org == null) {
			resultVo.setCode(ResultVo.CODE_API_ERROR);
			resultVo.setMsg(response.getError());
			return resultVo;
		}
		try {
			c.setInhandId(org.get_id());
			companyDao.save(c);
		} catch (Exception e) {
			e.printStackTrace();
			resultVo.setCode(ResultVo.CODE_SYSTEM_ERROR);
			resultVo.setMsg(ResultVo.MSG_SYSTEM_ERROR);
		}
		return resultVo;
	}

	/**
	 * 更新comapny
	 */
	@Override
	public ResultVo updateCompany(Company c) {

		ResultVo resultVo = new ResultVo();

		Company company = companyDao.get(c.getId());
		company.setContact(c.getContact());
		company.setPhone(c.getPhone());
		company.setIsActive(c.getIsActive());

		OrganizationResponse response = InhandCompanyService.editCompany(company);

		if (response == null) {
			resultVo.setCode(ResultVo.CODE_SYSTEM_ERROR);
			resultVo.setMsg(ResultVo.MSG_SYSTEM_ERROR);
			return resultVo;
		}
		Organization org = response.getOrganization();
		if (org == null) {
			resultVo.setCode(ResultVo.CODE_API_ERROR);
			resultVo.setMsg(response.getError());
			return resultVo;
		}

		try {
			companyDao.update(company);
		} catch (Exception e) {
			e.printStackTrace();
			resultVo.setCode(ResultVo.CODE_SYSTEM_ERROR);
			resultVo.setMsg(ResultVo.MSG_SYSTEM_ERROR);
		}
		return resultVo;
	}

	/**
	 * 根据 id查询company 方法
	 */
	@Override
	public Company findById(Integer id) {

		return companyDao.get(id);
	}

	/**
	 * 根据公司名称 获取公司
	 */
	@Override
	public List<Company> getCompanyList(boolean active, String name) {

		List<Company> list = companyDao.getCompanyList(active, name);
		for (Company c : list) {
			List<Group> groups = groupDao.getGroupsByCompanyId(c.getId());
			if (groups != null)
				c.setGroupNum(groups.size());

			Map<String, Object> map = deviceDao.getDeviceNumByCompany(c.getId());
			if (map != null) {
				c.setDeviceNormalNum(map.get("normal_num") == null ? 0 : Integer.parseInt(map.get(
						"normal_num").toString()));
				c.setDeviceTotalNum(map.get("total_num") == null ? 0 : Integer.parseInt(map.get(
						"total_num").toString()));
			}
		}
		return list;
	}

	/**
	 * 根据active 获取公司列表
	 */
	@Override
	public List<Company> getCompanyList(boolean active) {

		return companyDao.getCompanyList(active, null);
	}

	/**
	 * 得到所有的账户列表
	 */
	@Override
	public List<Company> getCompanyAllList() {

		return companyDao.getCompanyList(true, null);
	}

	/**
	 * 查询账户
	 */
	@Override
	public Company findByAccount(String account) {

		return companyDao.getCompanyByAccount(account);
	}

	/**
	 * 更新最新登录时间
	 */
	@Override
	public boolean updateLastLoginTime(Company c) {

		try {
			c.setLastLoginTime(new Date());
			companyDao.saveOrUpdate(c);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	/**
	 * 修改密码
	 */
	@Override
	public boolean updatePwd(Integer id, String newPwd) {

		Company account = companyDao.get(id);
		if (account == null)
			return false;

		account.setPassword(newPwd);
		companyDao.update(account);

		return true;
	}

	/**
	 * 验证账户
	 */
	@Override
	public Company validAccount(String username, String pwd) {

		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * 验证userName 唯一性
	 */
	@Override
	public boolean checkUserName(String userName) {

		Company company = companyDao.getCompanyByAccount(userName);
		if (company == null)
			return true;

		return false;
	}

	/**
	 * 更新InhandId方法
	 * 
	 * @param companyId
	 * @param inhandId
	 * @return
	 */
	@Override
	public boolean updateInhandId(Integer companyId, String inhandId) {

		return companyDao.updateInhandId(companyId, inhandId);
	}

	/**
	 * 验证公司名称唯一性
	 * 
	 * @param name
	 * @return
	 */
	@Override
	public boolean checkCompanyName(String name) {

		Company company = companyDao.getCompanyByName(name);
		if (company == null)
			return true;

		return false;
	}

	/**
	 * 验证公司邮件唯一性
	 * 
	 * @param email
	 * @return
	 */
	@Override
	public boolean checkCompanyEmail(String email) {

		Company company = companyDao.getCompanyByEmail(email);
		if (company == null)
			return true;

		return false;
	}
}
