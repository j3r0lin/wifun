package com.wifun.admin.domain.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.domain.dao.IPortalDao;
import com.wifun.admin.domain.model.Portal;
import com.wifun.admin.domain.service.IPortalService;
import com.wifun.admin.util.SysConfig;

@Service("portalService")
@Transactional
public class PortalService implements IPortalService {

	@Resource(name = "portalDao")
	private IPortalDao portalDao;

	public boolean savePortalPackage(Integer companyId, String portalName, File portalPackage) {

		boolean flag = false;
		String portalPath = SysConfig.getSetting("wifun.portal.directory");
		File savefile = null;
		String name = UUID.randomUUID().toString().replaceAll("-", "") + ".zip";
		// 1.upload file
		if (portalPackage != null) {
			try {

				savefile = new File(portalPath, name);
				if (!savefile.getParentFile().exists()) {// 判断上传目录是否存在
					savefile.getParentFile().mkdirs();
				}
				FileUtils.copyFile(portalPackage, savefile);// 把uploadfile复制到savefile
				flag = true;
			} catch (IOException e) {
				flag = false;
				e.printStackTrace();
			}
		}

		// 2.insert info into tables
		if (flag) {
			try {
				// 2.insert table
				Portal portal = new Portal();
				if (null != companyId)
					portal.setCompanyId(companyId);

				portal.setName(portalName);
				portal.setPackageUrl(savefile.getAbsolutePath());
				portal.setCreateTime(new Date());
				portal.setIsActive(true);
				portalDao.save(portal);
				flag = true;
			} catch (Exception e) {
				flag = false;
				e.printStackTrace();
			}
		}

		return flag;
	}

	@Override
	public Portal findById(Integer id) {

		// TODO Auto-generated method stub
		return portalDao.get(id);
	}

	@Override
	public List<Portal> loadAll() {

		// TODO Auto-generated method stub
		return portalDao.loadAll();
	}

	@Override
	public List<Portal> findByExample(Portal t) {

		// TODO Auto-generated method stub
		return portalDao.findByExample(t);
	}

	@Override
	public boolean create(Portal entity) {

		try {
			portalDao.save(entity);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public boolean save(Portal entity) {

		try {
			portalDao.update(entity);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public boolean update(Portal entity) {

		try {
			portalDao.update(entity);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public void saveOrUpdate(Portal entity) {

		// TODO Auto-generated method stub
		portalDao.saveOrUpdate(entity);
	}

	@Override
	public void saveOrUpdateAll(Collection<Portal> entities) {

		// TODO Auto-generated method stub
		portalDao.saveOrUpdateAll(entities);
	}

	@Override
	public void delete(Portal entity) {

		// TODO Auto-generated method stub
		portalDao.delete(entity);
	}

	@Override
	public void deleteById(Integer id) {

		// TODO Auto-generated method stub
		portalDao.deleteByKey(id);
	}

	@Override
	public void deleteAll(Collection<Portal> entities) {

		// TODO Auto-generated method stub
		portalDao.deleteAll(entities);
	}

	@Override
	public List<Portal> loadByCompany(Integer companyId) {

		Portal p = new Portal();
		p.setCompanyId(companyId);
		return portalDao.findByExample(p);
	}

	@Override
	public List<Portal> searchPortal(Integer companyId, String portalName) {

		return portalDao.searchPortal(companyId, portalName);
	}

	@Override
	public List<Portal> getPortalByCompanyId(Integer companyId) {

		return portalDao.getPortalByCompanyId(companyId);
	}

	@Override
	public boolean updateStatus(Integer id, boolean isActive) {

		Portal portal = portalDao.get(id);
		if (portal == null)
			return false;

		portal.setIsActive(isActive);
		portalDao.update(portal);
		return true;
	}
}
