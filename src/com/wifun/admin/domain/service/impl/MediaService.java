package com.wifun.admin.domain.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.io.FileUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.domain.dao.IMediaCategoryDao;
import com.wifun.admin.domain.dao.IMediaDao;
import com.wifun.admin.domain.dao.IMediaVersionDao;
import com.wifun.admin.domain.model.Media;
import com.wifun.admin.domain.model.MediaCategory;
import com.wifun.admin.domain.model.MediaVersion;
import com.wifun.admin.domain.service.IMediaService;
import com.wifun.admin.util.FileUtil;
import com.wifun.admin.util.SysConfig;

@Service("mediaService")
@Transactional
public class MediaService implements IMediaService {

	@Resource(name = "mediaDao")
	private IMediaDao mediaDao;

	@Resource(name = "mediaCategoryDao")
	private IMediaCategoryDao mediaCategoryDao;

	@Resource(name = "mediaVersionDao")
	private IMediaVersionDao mediaVersionDao;

	@Override
	public Media findById(Integer id) {

		return mediaDao.get(id);
	}

	@Override
	public List<Media> loadAll() {

		List<Media> list = mediaDao.loadAll();
		for (Media m : list) {
			MediaVersion mediaVersion = mediaVersionDao.getCurrentMediaVersion(m.getId());
			m.setMediaVersion(mediaVersion);
		}
		return list;
	}

	@Override
	public List<Media> search(Integer categoryId, String title) {

		List<Media> list = mediaDao.search(categoryId, title);
		for (Media m : list) {
			List<MediaCategory> mcList = mediaCategoryDao.getMediaCategoryByMediaId(m.getId());
			m.setCategorys(mcList);

			MediaVersion mediaVersion = mediaVersionDao.getCurrentMediaVersion(m.getId());
			m.setMediaVersion(mediaVersion);
		}
		return list;
	}

	@Override
	public boolean create(Media entity) {

		try {
			Media media = mediaDao.save(entity);

			if (media.getCopyright() == 0)
				media.setCode(media.getId());
			else
				media.setCode(5000 + media.getId());

			mediaDao.update(media);

			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean update(Media entity) {

		try {
			Media media = mediaDao.get(entity.getId());
			media.updateByVo(entity);
			mediaDao.update(media);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public List<MediaCategory> loadCategoryList(Integer mediaId) {

		MediaCategory mc = new MediaCategory();
		mc.setMediaId(mediaId);

		return mediaCategoryDao.findByExample(mc);
	}

	@Override
	public void deleteCategoryList(int mediaId) {

		Media media = mediaDao.get(mediaId);
		List<MediaCategory> mediaCategorys = media.getCategorys();
		for (MediaCategory mediaCategory : mediaCategorys)
			mediaCategoryDao.deleteByKey(mediaCategory.getId());
	}

	@Override
	public boolean saveCategoryList(Integer mediaId, List<Integer> categoryList) {

		if (null == mediaId)
			return false;

		try {
			List<MediaCategory> mclist = loadCategoryList(mediaId);
			for (MediaCategory mediaCategory : mclist) {
				mediaCategoryDao.delete(mediaCategory);
			}
			List<MediaCategory> newList = new ArrayList<MediaCategory>();

			for (Integer catId : categoryList) {
				newList.add(new MediaCategory(mediaId, catId));
			}

			mediaCategoryDao.saveOrUpdateAll(newList);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public List<Media> loadMediaListDetail(Integer mediaListId) {

		return mediaDao.findMediaByMediaListId(mediaListId);
	}

	@Override
	public List<Media> loadAvailableMedia(Integer mediaListId) {

		return mediaDao.findMediaExceptMediaListId(mediaListId);
	}

	@Override
	public boolean updateMovieUrl(int mediaId, String movieUrl) {

		try {
			String path = SysConfig.getSetting("wifun.media.base.directory");
			File srcFile = new File(movieUrl);
			Media media = mediaDao.get(mediaId);
			String fileName = "v_" + media.getCode() + "."
					+ FileUtil.getExtensionName(srcFile.getName());
			path += "/media_" + media.getCode();
			File destFile = new File(path + "/" + fileName);
			FileUtils.copyFile(srcFile, destFile, true);

			return mediaDao.updateMovieUrl(mediaId, path + "/" + destFile.getName());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean updateHPosterUrl(int mediaId, String posterUrl) {

		try {
			String path = SysConfig.getSetting("wifun.media.base.directory");
			File srcFile = new File(posterUrl);
			Media media = mediaDao.get(mediaId);
			String fileName = "i_h_" + media.getCode() + "."
					+ FileUtil.getExtensionName(srcFile.getName());
			path += "/media_" + media.getCode();
			File destFile = new File(path + "/" + fileName);
			FileUtils.copyFile(srcFile, destFile, true);

			return mediaDao.updateHPosterUrl(mediaId, path + "/" + destFile.getName());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public boolean updateVPosterUrl(int mediaId, String posterUrl) {

		try {
			String path = SysConfig.getSetting("wifun.media.base.directory");
			File srcFile = new File(posterUrl);
			Media media = mediaDao.get(mediaId);
			String fileName = "i_v_" + media.getCode() + "."
					+ FileUtil.getExtensionName(srcFile.getName());
			path += "/media_" + media.getCode();
			File destFile = new File(path + "/" + fileName);
			FileUtils.copyFile(srcFile, destFile, true);

			return mediaDao.updateVPosterUrl(mediaId, path + "/" + destFile.getName());
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public void deleteById(Integer id) {

		mediaDao.deleteByKey(id);
	}
}
