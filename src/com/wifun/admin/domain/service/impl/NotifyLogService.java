package com.wifun.admin.domain.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.domain.dao.IDeviceDao;
import com.wifun.admin.domain.dao.IMediaDeployDetailDao;
import com.wifun.admin.domain.dao.INotifyLogDao;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.NotifyLog;
import com.wifun.admin.domain.service.INotifyLogService;

@Service("notifyLogService")
@Transactional
public class NotifyLogService implements INotifyLogService {

	@Resource(name = "mediaDeployDetailDao")
	private IMediaDeployDetailDao mediaDeployDetailDao;

	@Resource(name = "deviceDao")
	private IDeviceDao deviceDao;

	@Resource(name = "notifyLogDao")
	private INotifyLogDao notifyLogDao;

	@Override
	public void saveVlistLog(String version, String sn, int reasion, String syncMode) {

		Device device = deviceDao.getDeviceBySn(sn);
		int deviceId = 0;
		if (device != null)
			deviceId = device.getId();

		version = formateVersion(version);

		NotifyLog notifyLog = new NotifyLog(2, version, sn, deviceId, reasion, syncMode);
		notifyLogDao.save(notifyLog);
	}

	@Override
	public void saveMediaLog(int type, String version, String sn, String media, int reasion,
			String syncMode) {

		Device device = deviceDao.getDeviceBySn(sn);
		int deviceId = 0;
		if (device != null)
			deviceId = device.getId();

		int mediaId = getMediaId(media);
		version = formateVersion(version);

		NotifyLog notifyLog = new NotifyLog(type, version, sn, deviceId, media, mediaId, reasion,
				syncMode);
		notifyLogDao.save(notifyLog);

		int status = 0;
		if (reasion == 0)
			status = 1;

		// 添加视频成功，更新发布明细表
		if (type == 0)
			mediaDeployDetailDao.updateStatusByNotifyLog(version, deviceId, mediaId, status);
	}

	private String formateVersion(String version) {

		if (version.indexOf("<") < 0 || version.indexOf(">") < 0)
			return version;

		version = version.substring(version.indexOf("<") + 1, version.indexOf(">"));

		return version;
	}

	private int getMediaId(String media) {

		int mediaId = 0;
		if (media.indexOf("_") < 1)
			return mediaId;

		String[] mediaArr = media.split("_");
		mediaId = Integer.parseInt(mediaArr[1]);

		// 国内版权的电影code从5000开始累计
		if (mediaId > 5000) {
			mediaId = mediaId - 5000;
		}

		return mediaId;
	}
}
