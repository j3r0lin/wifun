package com.wifun.admin.domain.service.impl;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.dao.IDeviceDao;
import com.wifun.admin.domain.dao.IGroupDao;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.Group;
import com.wifun.admin.domain.service.IGroupService;
import com.wifun.inhand.response.tag.TagResponse;
import com.wifun.inhand.service.InhandDeviceService;
import com.wifun.inhand.service.InhandGroupService;

@Service("groupService")
@Transactional
public class GroupService implements IGroupService {

	@Resource(name = "groupDao")
	private IGroupDao groupDao;
	@Resource(name = "deviceDao")
	private IDeviceDao deviceDao;

	@Override
	public List<Group> getGroupsByCompanyId(Integer companyId) {

		return groupDao.getGroupsByCompanyId(companyId);
	}

	@Override
	public List<Group> searchGroups(Integer companyId, String groupName) {

		List<Group> list = groupDao.searchGroups(companyId, groupName);
		for (Group g : list) {

			Map<String, Object> map = deviceDao.getDeviceNumByGroup(companyId, g.getId());
			if (map != null) {
				g.setDeviceNormalNum(map.get("normal_num") == null ? 0 : Integer.parseInt(map.get(
						"normal_num").toString()));
				g.setDeviceTotalNum(map.get("total_num") == null ? 0 : Integer.parseInt(map.get(
						"total_num").toString()));
			}
		}
		return list;
	}

	@Override
	public Group getGroupByName(int companyId, String name) {

		return groupDao.getGroupByName(companyId, name);
	}

	@Override
	public Group findById(Integer id) {

		return groupDao.get(id);
	}

	@Override
	public List<Group> loadAll() {

		return groupDao.loadAll();
	}

	@Override
	public boolean create(Group entity) {

		try {
			TagResponse tagRes = InhandGroupService.addTag(
					SessionHelper.getToken(entity.getCompanyId()), entity);
			if (tagRes == null) {
				return false;
			}
			entity.setInhandId(tagRes.getTag().get_id());
			groupDao.save(entity);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public boolean save(Group entity) {

		return update(entity);
	}

	@Override
	public void saveOrUpdate(Group entity) {

		update(entity);
	}

	@Override
	public void saveOrUpdateAll(Collection<Group> entities) {

		for (Group group : entities) {
			update(group);
		}
	}

	@Override
	public void delete(Group group) {

		List<Device> list = deviceDao.loadByGroup(group);
		for (int i = 0; i < list.size(); i++) {
			try {
				Device device = list.get(i);
				if (!InhandDeviceService.removeTag(SessionHelper.getToken(device.getCompanyId()),
						device.getInhandSiteId(), group.getInhandId())) {
					device.setGroupId(0);
					deviceDao.save(device);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		groupDao.delete(group);
		InhandGroupService.deleteTag(SessionHelper.getToken(group.getCompanyId()), group);
	}

	@Override
	public void deleteById(Integer id) {

		delete(groupDao.get(id));
	}

	@Override
	public void deleteAll(Collection<Group> entities) {

		for (Group group : entities) {
			delete(group);
		}
	}

	@Override
	public boolean update(Group entity) {

		try {
			Group g = groupDao.get(entity.getId());
			String oldName = g.getName();
			g.setCompanyId(entity.getCompanyId());
			g.setName(entity.getName());
			g.setStatus(entity.getStatus());
			if (null == g.getInhandId()) {

				TagResponse tagRes = InhandGroupService.addTag(
						SessionHelper.getToken(g.getCompanyId()), g);
				if (tagRes == null) {
					return false;
				}
				g.setInhandId(tagRes.getTag().get_id());
			} else if (!g.getName().equals(oldName)) {
				TagResponse tagRes = InhandGroupService.updateTag(
						SessionHelper.getToken(g.getCompanyId()), g);
				if (tagRes.getTag() == null) {
					return false;
				}
			}
			groupDao.update(g);
			return true;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Group> findByExample(Group t) {

		return groupDao.findByExample(t);
	}

}
