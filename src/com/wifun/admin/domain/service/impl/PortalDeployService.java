package com.wifun.admin.domain.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.PortalDeployThread;
import com.wifun.admin.domain.dao.ICompanyDao;
import com.wifun.admin.domain.dao.IDeviceDao;
import com.wifun.admin.domain.dao.IGroupDao;
import com.wifun.admin.domain.dao.IPortalDao;
import com.wifun.admin.domain.dao.IPortalDeployDao;
import com.wifun.admin.domain.dao.IPortalDeployDetailDao;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.Group;
import com.wifun.admin.domain.model.Portal;
import com.wifun.admin.domain.model.PortalDeploy;
import com.wifun.admin.domain.model.PortalDeployDetail;
import com.wifun.admin.domain.service.IPortalDeployService;
import com.wifun.admin.util.SysConfig;
import com.wifun.admin.util.TimeUtils;
import com.wifun.inhand.service.InhandPortalDeployService;
import com.wifun.inhand.service.InhandPublishService;

@Service("portalDeployService")
@Transactional
public class PortalDeployService implements IPortalDeployService {

	@Resource(name = "portalDeployDao")
	private IPortalDeployDao portalDeployDao;

	@Resource(name = "portalDeployDetailDao")
	private IPortalDeployDetailDao portalDeployDetailDao;

	@Resource(name = "groupDao")
	private IGroupDao groupDao;

	@Resource(name = "deviceDao")
	private IDeviceDao deviceDao;

	@Resource(name = "portalDao")
	private IPortalDao portalDao;

	public boolean savePortalPackage(Integer companyId, String portalName, File portalPackage) {

		boolean flag = false;

		return flag;
	}

	@Override
	public PortalDeploy findById(Integer id) {

		// TODO Auto-generated method stub
		return portalDeployDao.get(id);
	}

	@Override
	public List<PortalDeploy> loadAll() {

		// TODO Auto-generated method stub
		return portalDeployDao.loadAll();
	}

	@Override
	public List<PortalDeploy> findByExample(PortalDeploy t) {

		// TODO Auto-generated method stub
		return portalDeployDao.findByExample(t);
	}

	@Override
	public boolean create(PortalDeploy entity) {

		try {
			portalDeployDao.save(entity);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public boolean save(PortalDeploy entity) {

		try {
			portalDeployDao.update(entity);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public boolean update(PortalDeploy entity) {

		try {
			portalDeployDao.update(entity);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public void saveOrUpdate(PortalDeploy entity) {

		// TODO Auto-generated method stub
		portalDeployDao.saveOrUpdate(entity);
	}

	@Override
	public void saveOrUpdateAll(Collection<PortalDeploy> entities) {

		// TODO Auto-generated method stub
		portalDeployDao.saveOrUpdateAll(entities);
	}

	@Override
	public void delete(PortalDeploy entity) {

		// TODO Auto-generated method stub
		portalDeployDao.delete(entity);
	}

	@Override
	public void deleteById(Integer id) {

		// TODO Auto-generated method stub
		portalDeployDao.deleteByKey(id);
	}

	@Override
	public void deleteAll(Collection<PortalDeploy> entities) {

		// TODO Auto-generated method stub
		portalDeployDao.deleteAll(entities);
	}

	@Override
	public List<PortalDeploy> loadByCompany(Integer companyId) {

		// TODO Auto-generated method stub
		PortalDeploy pd = new PortalDeploy();
		pd.setCompanyId(companyId);

		return portalDeployDao.findByExample(pd);
	}

	@Override
	public boolean deploy(PortalDeploy pd) {

		try {

			Portal p = portalDao.get(pd.getPortalId());

			// 保存发布记录
			pd.setPortalName(p.getName());
			pd.setStatus(0);
			pd.setDeployTime(new Date());
			create(pd);
			// 记录明细
			List<PortalDeployDetail> pddList = new ArrayList<PortalDeployDetail>();

			Device d = new Device();
			d.setCompanyId(pd.getCompanyId());
			if (null == pd.getGroupId()) {
				pd.setGroupId(0);
			}

			d.setGroupId(pd.getGroupId());
			d.setIsActive(true);
			List<Device> devList = deviceDao.findByExample(d);
			for (Device dev : devList) {
				PortalDeployDetail pdd = new PortalDeployDetail();
				pdd.setDeployId(pd.getId());
				pdd.setDeviceId(dev.getId());
				pdd.setStatus("0");
				pdd.setDeployTime(new Date());
				pdd.setDeviceName(dev.getName());
				pdd.setSn(dev.getSn());
				pdd.setPortalName(p.getName());
				pddList.add(pdd);
			}

			portalDeployDetailDao.saveOrUpdateAll(pddList);

			// //向Inhand平台上传文件
			String packageUrl = p.getPackageUrl();
			String portalUrl = SysConfig.getSetting("wifun.publish.point.portal");

			PortalDeployThread thread = new PortalDeployThread(pd, packageUrl, portalUrl);
			thread.start();

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	@Override
	public List<PortalDeploy> searchPortal(int companyId, String description, String startDate,
			String endDate) {

		return portalDeployDao.searchPortal(companyId, description, startDate, endDate);
	}

}
