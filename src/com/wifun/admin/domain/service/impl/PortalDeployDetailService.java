package com.wifun.admin.domain.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.dao.IPortalDeployDao;
import com.wifun.admin.domain.dao.IPortalDeployDetailDao;
import com.wifun.admin.domain.dao.IPublishSyncLogDao;
import com.wifun.admin.domain.model.PortalDeploy;
import com.wifun.admin.domain.model.PortalDeployDetail;
import com.wifun.admin.domain.model.PublishSyncLog;
import com.wifun.admin.domain.service.IPortalDeployDetailService;
import com.wifun.inhand.model.PublishState;
import com.wifun.inhand.service.InhandPublishService;

@Service("portalDeployDetailService")
@Transactional
public class PortalDeployDetailService implements IPortalDeployDetailService {

	@Resource(name = "portalDeployDetailDao")
	private IPortalDeployDetailDao portalDeployDetailDao;

	@Resource(name = "portalDeployDao")
	private IPortalDeployDao portalDeployDao;

	@Resource(name = "publishSyncLogDao")
	private IPublishSyncLogDao publishSyncLogDao;

	public boolean savePortalPackage(Integer companyId, String portalName, File portalPackage) {

		boolean flag = false;

		return flag;
	}

	@Override
	public PortalDeployDetail findById(Integer id) {

		// TODO Auto-generated method stub
		return portalDeployDetailDao.get(id);
	}

	@Override
	public List<PortalDeployDetail> loadAll() {

		// TODO Auto-generated method stub
		return portalDeployDetailDao.loadAll();
	}

	@Override
	public List<PortalDeployDetail> findByExample(PortalDeployDetail t) {

		// TODO Auto-generated method stub
		return portalDeployDetailDao.findByExample(t);
	}

	@Override
	public boolean create(PortalDeployDetail entity) {

		try {
			portalDeployDetailDao.save(entity);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public boolean save(PortalDeployDetail entity) {

		try {
			portalDeployDetailDao.update(entity);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public boolean update(PortalDeployDetail entity) {

		try {
			portalDeployDetailDao.update(entity);
			return true;
		} catch (Exception e) {
			return false;
		}

	}

	@Override
	public void saveOrUpdate(PortalDeployDetail entity) {

		// TODO Auto-generated method stub
		portalDeployDetailDao.saveOrUpdate(entity);
	}

	@Override
	public void saveOrUpdateAll(Collection<PortalDeployDetail> entities) {

		// TODO Auto-generated method stub
		portalDeployDetailDao.saveOrUpdateAll(entities);
	}

	@Override
	public void delete(PortalDeployDetail entity) {

		// TODO Auto-generated method stub
		portalDeployDetailDao.delete(entity);
	}

	@Override
	public void deleteById(Integer id) {

		// TODO Auto-generated method stub
		portalDeployDetailDao.deleteByKey(id);
	}

	@Override
	public void deleteAll(Collection<PortalDeployDetail> entities) {

		// TODO Auto-generated method stub
		portalDeployDetailDao.deleteAll(entities);
	}

	@Override
	public List<PortalDeployDetail> loadByDeployId(Integer deployId) {

		PortalDeploy pd = portalDeployDao.get(deployId);
		if (1 == pd.getStatus()) { // 0时进行查询
			List<PublishState> psList = InhandPublishService.queryPublishState(
					SessionHelper.getToken(pd.getCompanyId()), pd.getPublishId());

			String flag = UUID.randomUUID().toString();
			List<PublishSyncLog> pslList = new ArrayList<PublishSyncLog>();
			if (null != psList) {
				for (PublishState ps : psList) {
					pslList.add(new PublishSyncLog(ps, flag));
				}
			}
			publishSyncLogDao.saveOrUpdateAll(pslList);
			// 更新对应状态
			publishSyncLogDao.updatePortalPublishState(pd.getId(), flag);
		}
		PortalDeployDetail pdd = new PortalDeployDetail();
		pdd.setDeployId(deployId);
		return portalDeployDetailDao.findByExample(pdd);
	}
}
