package com.wifun.admin.domain.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.dao.IMediaListDao;
import com.wifun.admin.domain.dao.IMediaListMediaDao;
import com.wifun.admin.domain.model.MediaList;
import com.wifun.admin.domain.model.MediaListMedia;
import com.wifun.admin.domain.service.IMediaListService;

@Service("mediaListService")
@Transactional
public class MediaListService implements IMediaListService {

	@Resource(name = "mediaListDao")
	private IMediaListDao mediaListDao;

	@Resource(name = "mediaListMediaDao")
	IMediaListMediaDao mediaListMediaDao;

	@Override
	public MediaList findById(Integer id) {

		// TODO Auto-generated method stub
		return mediaListDao.get(id);
	}

	@Override
	public List<MediaList> loadAll() {

		// TODO Auto-generated method stub
		return mediaListDao.loadAll();
	}

	@Override
	public List<MediaList> findByExample(MediaList t) {

		// TODO Auto-generated method stub
		return mediaListDao.findByExample(t);
	}

	@Override
	public boolean create(MediaList entity) {

		try {
			entity.setCreator(SessionHelper.getCurrentAccount().getId());
			entity.setCreateDate(new Date());
			mediaListDao.save(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean save(MediaList entity) {

		try {
			mediaListDao.save(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean update(MediaList entity) {

		try {
			mediaListDao.update(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void saveOrUpdate(MediaList entity) {

		mediaListDao.saveOrUpdate(entity);
	}

	@Override
	public void saveOrUpdateAll(Collection<MediaList> entities) {

		mediaListDao.saveOrUpdateAll(entities);
	}

	@Override
	public void delete(MediaList entity) {

		mediaListDao.delete(entity);
	}

	@Override
	public void deleteById(Integer id) {

		mediaListDao.deleteByKey(id);
	}

	@Override
	public void deleteAll(Collection<MediaList> entities) {

		mediaListDao.deleteAll(entities);
	}

	@Override
	public boolean addMedias(Integer mediaListId, List<Integer> mediaIdList) {

		try {
			List<MediaListMedia> mmlist = new ArrayList<MediaListMedia>();
			MediaListMedia mm = null;
			for (Integer mediaId : mediaIdList) {
				mm = new MediaListMedia();
				mm.setMedialistId(mediaListId);
				mm.setMediaId(mediaId);
				mmlist.add(mm);
			}

			mediaListMediaDao.saveOrUpdateAll(mmlist);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean saveMedias(Integer mediaListId, List<Integer> mediaIdList) {

		try {
			MediaListMedia mm = new MediaListMedia();
			mm.setMedialistId(mediaListId);
			List<MediaListMedia> mmlist = mediaListMediaDao.findByExample(mm);
			for (MediaListMedia m : mmlist) {
				if (!mediaIdList.contains(m.getMediaId()))
					mmlist.remove(m);
			}

			for (Integer mediaId : mediaIdList) {
				mm = new MediaListMedia();
				mm.setMedialistId(mediaListId);
				mm.setMediaId(mediaId);
				mmlist.add(mm);
			}

			mediaListMediaDao.saveOrUpdateAll(mmlist);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean removeMedia(Integer mediaListId, Integer mediaId) {

		try {
			MediaListMedia mm = new MediaListMedia();
			mm.setMedialistId(mediaListId);
			mm.setMediaId(mediaId);

			mediaListMediaDao.deleteAll(mediaListMediaDao.findByExample(mm));
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public List<MediaList> getAllMedias(String title) {

		List<MediaList> list = mediaListDao.findAllMedias(title);
		for (MediaList ml : list) {
			List<MediaListMedia> medias = mediaListMediaDao.getMediaListMediaByMediaListId(ml
					.getId());
			ml.setMedialist(medias);
		}
		return list;
	}

	@Override
	public List<MediaList> getCompanyMedias(String title, int companyId) {

		List<MediaList> list = mediaListDao.findMediasByCompany(title, companyId);
		for (MediaList ml : list) {
			List<MediaListMedia> medias = mediaListMediaDao.getMediaListMediaByMediaListId(ml
					.getId());
			ml.setMedialist(medias);
		}
		return list;
	}

	@Override
	public boolean saveMediaList(MediaList bo, int companyId, int creator) {

		if (null == bo || StringUtils.isBlank(bo.getTitle())) {
			return false;
		}

		bo.setCreateDate(new Date());
		bo.setCreator(creator);
		bo.setCompanyId(companyId);
		bo.setStatus(MediaList.STATUS_VALID);
		mediaListDao.save(bo);
		return true;
	}

	@Override
	public boolean deleteMediaList(Integer id) {

		MediaList bo = mediaListDao.get(id);
		if (null == bo) {
			return false;
		}
		bo.setStatus(MediaList.STATUS_INVALID);
		mediaListDao.save(bo);
		return true;
	}

	@Override
	public boolean updateMediaList(Integer id, MediaList bo, int companyId) {

		MediaList mediaList = mediaListDao.get(id);
		if (null == mediaList) {
			return false;
		}
		mediaList.setTitle(bo.getTitle());
		mediaList.setCompanyId(companyId);
		mediaListDao.update(mediaList);
		return true;
	}

}
