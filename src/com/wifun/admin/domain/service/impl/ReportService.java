package com.wifun.admin.domain.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.app.vo.DailyReportCompanyVo;
import com.wifun.admin.app.vo.DailyReportVo;
import com.wifun.admin.domain.dao.ICompanyDao;
import com.wifun.admin.domain.dao.IDeviceDao;
import com.wifun.admin.domain.dao.IGroupDao;
import com.wifun.admin.domain.dao.ITaskPVUVMovieDao;
import com.wifun.admin.domain.dao.ITaskReportDailyDao;
import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.TaskPvuvMovie;
import com.wifun.admin.domain.model.TaskReportDaily;
import com.wifun.admin.domain.service.IReportService;
import com.wifun.admin.util.TimeUtils;

@Service("reportService")
@Transactional
public class ReportService implements IReportService {

	@Resource(name = "taskReportDailyDao")
	private ITaskReportDailyDao taskReportDailyDao;

	@Resource(name = "deviceDao")
	private IDeviceDao deviceDao;

	@Resource(name = "companyDao")
	private ICompanyDao companyDao;

	@Resource(name = "groupDao")
	private IGroupDao groupDao;

	@Resource(name = "taskPVUVMovieDao")
	private ITaskPVUVMovieDao taskPVUVMovieDao;

	@Override
	public List<DailyReportVo> getReportDailyVo(int companyId, int groupId, String date) {

		String chsDate = TimeUtils.dateToStr(TimeUtils.strUsToDate(date));
		Company company = companyDao.get(companyId);
		List<TaskReportDaily> reportList = taskReportDailyDao.getListByCompanyIdAndDate(companyId,
				groupId, chsDate);

		return this.buildReportDeilyVos(company, reportList);
	}

	@Override
	public List<DailyReportCompanyVo> getCompanyDailyReport(String date) {

		String chsDate = TimeUtils.dateToStr(TimeUtils.strUsToDate(date));
		return taskReportDailyDao.getCompanyDailyReport(chsDate);
	}

	private List<DailyReportVo> buildReportDeilyVos(Company company,
			List<TaskReportDaily> reportList) {

		Map<Integer, Device> deviceMap = this.createDeviceMap(company.getId());

		List<DailyReportVo> voList = new ArrayList<DailyReportVo>();
		for (int i = 0; i < reportList.size(); i++) {
			TaskReportDaily reportDeilay = reportList.get(i);
			Device device = deviceMap.get(reportDeilay.getDeviceId());
			DailyReportVo vo = new DailyReportVo(reportDeilay, company, device);
			voList.add(vo);
		}
		return voList;
	}

	private Map<Integer, Device> createDeviceMap(int companyId) {

		List<Device> deviceList = fillGroupInfo(deviceDao.loadByCompany(companyId));
		Map<Integer, Device> deviceMap = new HashMap<Integer, Device>();
		for (int i = 0; i < deviceList.size(); i++) {
			Device d = deviceList.get(i);
			deviceMap.put(d.getId(), d);
		}
		return deviceMap;
	}

	private List<Device> fillGroupInfo(List<Device> devList) {

		if (null == devList)
			return null;

		for (int i = 0; i < devList.size(); i++) {
			Device d = devList.get(i);
			d.setCompanyName(companyDao.get(d.getCompanyId()).getName());
			if (d.getGroupId() > 0) {
				d.setGroupName(groupDao.get(d.getGroupId()).getName());
			} else {
				d.setGroupName("default group");
			}
		}
		return devList;
	}

	@Override
	public List<TaskPvuvMovie> getMoivePvuvSum(String date) {

		return taskPVUVMovieDao.getMoivePvuvSum(date);
	}

	@Override
	public List<TaskPvuvMovie> getMoivePvuvByMediaList(int mediaListId, String date) {

		return taskPVUVMovieDao.getMoivePvuvByMediaList(mediaListId, date);
	}
}
