package com.wifun.admin.domain.service.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.dao.IMediaDeployDao;
import com.wifun.admin.domain.dao.IMediaDeployDetailDao;
import com.wifun.admin.domain.dao.IPublishSyncLogDao;
import com.wifun.admin.domain.model.MediaDeploy;
import com.wifun.admin.domain.model.MediaDeployDetail;
import com.wifun.admin.domain.model.PublishSyncLog;
import com.wifun.admin.domain.service.IMediaDeployDetailService;
import com.wifun.inhand.model.PublishState;
import com.wifun.inhand.service.InhandPublishService;

@Service("mediaDeployDetailService")
@Transactional
public class MediaDeployDetailService implements IMediaDeployDetailService {

	@Resource(name = "mediaDeployDetailDao")
	private IMediaDeployDetailDao mediaDeployDetailDao;

	@Resource(name = "mediaDeployDao")
	private IMediaDeployDao mediaDeployDao;

	@Resource(name = "publishSyncLogDao")
	private IPublishSyncLogDao publishSyncLogDao;

	public boolean savePortalPackage(Integer companyId, String portalName, File portalPackage) {

		return true;
	}

	@Override
	public MediaDeployDetail findById(Integer id) {

		return mediaDeployDetailDao.get(id);
	}

	@Override
	public List<MediaDeployDetail> loadAll() {

		return mediaDeployDetailDao.loadAll();
	}

	@Override
	public List<MediaDeployDetail> findByExample(MediaDeployDetail t) {

		return mediaDeployDetailDao.findByExample(t);
	}

	@Override
	public boolean create(MediaDeployDetail entity) {

		try {
			mediaDeployDetailDao.save(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean save(MediaDeployDetail entity) {

		try {
			mediaDeployDetailDao.update(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean update(MediaDeployDetail entity) {

		try {
			mediaDeployDetailDao.update(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void saveOrUpdate(MediaDeployDetail entity) {

		mediaDeployDetailDao.saveOrUpdate(entity);
	}

	@Override
	public void saveOrUpdateAll(Collection<MediaDeployDetail> entities) {

		mediaDeployDetailDao.saveOrUpdateAll(entities);
	}

	@Override
	public void delete(MediaDeployDetail entity) {

		mediaDeployDetailDao.delete(entity);
	}

	@Override
	public void deleteById(Integer id) {

		mediaDeployDetailDao.deleteByKey(id);
	}

	@Override
	public void deleteAll(Collection<MediaDeployDetail> entities) {

		mediaDeployDetailDao.deleteAll(entities);
	}

	@Override
	public List<MediaDeployDetail> loadByDeployId(Integer deployId) {

		// /查询同步状态
		MediaDeploy md = mediaDeployDao.get(deployId);
		if (md.getStatus() == 1) { // 1时进行查询
			List<PublishState> psList = InhandPublishService.queryPublishState(
					SessionHelper.getToken(md.getCompanyId()), md.getPublishId());

			String flag = UUID.randomUUID().toString();
			List<PublishSyncLog> pslList = new ArrayList<PublishSyncLog>();
			if (null != psList) {
				for (PublishState ps : psList) {

					pslList.add(new PublishSyncLog(ps, flag));
				}
			}
			publishSyncLogDao.saveOrUpdateAll(pslList);
			// ///更新对应状态
			publishSyncLogDao.updateMediaPublishState(md.getId(), flag);
		}
		MediaDeployDetail mdd = new MediaDeployDetail();
		mdd.setDeployId(deployId);
		List<MediaDeployDetail> list = mediaDeployDetailDao
				.getMediaDeployDetailsByDeployId(deployId);

		return list;
	}
}
