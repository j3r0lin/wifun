package com.wifun.admin.domain.service.impl;

import java.util.ArrayList;
import java.util.List;

import net.sf.json.JSONObject;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.app.vo.PvuvPageVo;
import com.wifun.admin.domain.service.IPvuvPageReportService;
import com.wifun.admin.util.TimeUtils;
import com.wifun.inhand.request.statistic.PvuvTypesRequest;
import com.wifun.inhand.response.statistic.PvuvTypesOutlineResponse;
import com.wifun.inhand.response.statistic.TypesOutlineRespModel;
import com.wifun.inhand.util.InhandHttpClient;

@Service("pvuvPageService")
@Transactional
public class PvuvPageService implements IPvuvPageReportService {

	/*
	 * 获取所有类型的pvuv统计信息
	 */
	@Override
	public List<PvuvPageVo> getTypesPvuvSum(int companyId) {

		List<PvuvPageVo> vos = new ArrayList<PvuvPageVo>();
		List<TypesOutlineRespModel> result = new ArrayList<TypesOutlineRespModel>();
		// String[] types = { "list", "history", "play", "search", "home"
		// ,"main"};
		// 每个字符前后加@@,方便查找

		try {

			PvuvTypesRequest req = new PvuvTypesRequest();
			String token = SessionHelper.getToken(companyId);
			// token = "5ded27100714285e211f08a8e7afed04";
			req.setToken(token);
			String rs = InhandHttpClient.getInstance().httpGet(req);
			System.out.println(rs);

			JSONObject jsObject = JSONObject.fromObject(rs);
			PvuvTypesOutlineResponse resp = new PvuvTypesOutlineResponse();
			resp.fromJsonObject(jsObject);

			result = resp.getRespResult();
			vos = getVosFromModelList(result);

		} catch (Exception e) {
			e.printStackTrace();
		}

		return vos;
	}

	public List<PvuvPageVo> getVosFromModelList(List<TypesOutlineRespModel> result) {

		String types = "@@list@@_@@main@@_@@history@@_@@play@@_@@search@@_@@home@@";
		List<PvuvPageVo> vos = new ArrayList<PvuvPageVo>();

		for (int k = 0; k < result.size(); k++) {

			TypesOutlineRespModel model = result.get(k);
			// 如果是要返回的类型
			if (types.indexOf("@@" + model.getTid() + "@@") > 0) {
				PvuvPageVo vo = new PvuvPageVo();
				String currentPv = "0";
				String currentUv = "0";
				String lastPv = "0";
				String lastUv = "0";
				String totalPv = "0";
				String totalUv = "0";

				// 判断数据时间是否是当前时间
				String currDate = model.getCurrent().get("date");
				// String currDate=model.getLast().get("date"); 测试进第二个分支
				if (null != currDate) {
					if (currDate.equals(TimeUtils.getStringDateShort())) {
						currentPv = (null == model.getCurrent().get("pv")) ? "0" : model
								.getCurrent().get("pv");
						currentUv = (null == model.getCurrent().get("uv")) ? "0" : model
								.getCurrent().get("uv");
					} else if (currDate.equals(TimeUtils.dateToStr(TimeUtils.getYesterday()))) {
						// 如果当前时间是昨天，取今天的数据作为昨天的数据，
						lastPv = (null == model.getCurrent().get("pv")) ? "0" : model.getCurrent()
								.get("pv");
						lastUv = (null == model.getCurrent().get("uv")) ? "0" : model.getCurrent()
								.get("uv");
					}
				}

				vo.setCurrentPv(currentPv);
				vo.setCurrentUv(currentUv);

				String lastDate = model.getLast().get("date");
				if (null != lastDate) {
					if (lastDate.equals(TimeUtils.dateToStr(TimeUtils.getYesterday()))) {
						lastPv = (null == model.getLast().get("pv")) ? "0" : model.getLast().get(
								"pv");
						lastUv = (null == model.getLast().get("uv")) ? "0" : model.getLast().get(
								"uv");

					}
				}

				vo.setLastPv(lastPv);
				vo.setLastUv(lastUv);

				if (null != model.getTotal()) {
					totalPv = (null == model.getTotal().get("pv")) ? "0" : model.getTotal().get(
							"pv");
					totalUv = (null == model.getTotal().get("uv")) ? "0" : model.getTotal().get(
							"uv");

				}
				vo.setTotalPv(totalPv);
				vo.setTotalUv(totalUv);

				vo.setType(model.getTid());
				vos.add(vo);
			}
		}

		return vos;
	}
}