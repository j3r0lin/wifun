package com.wifun.admin.domain.service.impl;

import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.dao.*;
import com.wifun.admin.domain.model.*;
import com.wifun.admin.domain.service.IMediaDeployService;
import com.wifun.admin.util.SysConfig;
import com.wifun.inhand.service.InhandMediaDeployService;
import com.wifun.inhand.service.InhandPublishService;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

/**
 * 媒体资源发布Service
 * 
 * @File wifun-admin/com.wifun.admin.domain.service.impl.MediaDeployService.java
 * @Desc
 */
@Service("mediaDeployService")
@Transactional
public class MediaDeployService implements IMediaDeployService {

	private static final Logger logger = Logger.getLogger("deploy.media.log");

	@Resource(name = "mediaDeployDao")
	private IMediaDeployDao mediaDeployDao;

	@Resource(name = "deviceDao")
	private IDeviceDao deviceDao;

	@Resource(name = "mediaDeployDetailDao")
	private IMediaDeployDetailDao mediaDeployDetailDao;

	@Resource(name = "mediaDao")
	private IMediaDao mediaDao;

	@Resource(name = "groupDao")
	private IGroupDao groupDao;

	/**
	 * 查询媒体资源发布列表
	 */
	public List<MediaDeploy> searchMediaDeployList(int companyId, String startDate, String endDate) {

		return mediaDeployDao.searchNetDeployList(companyId, startDate, endDate);
	}

	@Override
	public MediaDeploy findById(Integer id) {

		return mediaDeployDao.get(id);
	}

	@Override
	public List<MediaDeploy> loadAll() {

		return mediaDeployDao.loadAll();
	}

	@Override
	public List<MediaDeploy> findByExample(MediaDeploy t) {

		return mediaDeployDao.findByExample(t);
	}

	@Override
	public boolean create(MediaDeploy entity) {

		try {
			mediaDeployDao.save(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean save(MediaDeploy entity) {

		try {
			mediaDeployDao.update(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public boolean update(MediaDeploy entity) {

		try {
			mediaDeployDao.update(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	@Override
	public void saveOrUpdate(MediaDeploy entity) {

		mediaDeployDao.saveOrUpdate(entity);
	}

	@Override
	public void saveOrUpdateAll(Collection<MediaDeploy> entities) {

		mediaDeployDao.saveOrUpdateAll(entities);
	}

	@Override
	public void delete(MediaDeploy entity) {

		mediaDeployDao.delete(entity);
	}

	@Override
	public void deleteById(Integer id) {

		mediaDeployDao.deleteByKey(id);
	}

	@Override
	public void deleteAll(Collection<MediaDeploy> entities) {

		mediaDeployDao.deleteAll(entities);
	}

	/**
	 * 网络发布媒体资源
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public boolean deploy(MediaDeploy md) {

		try {
			Date now = new Date();

			// 保存发布记录
			md.setDeployTime(now);
			create(md);

			// 记录明细
			List<MediaDeployDetail> mddList = new ArrayList<MediaDeployDetail>();

			Device d = new Device();
			d.setCompanyId(md.getCompanyId());
			d.setGroupId(md.getGroupId());
			d.setIsActive(true);
			List<Device> devList = deviceDao.findByExample(d);
			List<Media> mediaList = mediaDao.findMediaByMediaListId(md.getMediaListId());
			for (Device dev : devList) {
				for (Media media : mediaList) {
					MediaDeployDetail mdd = new MediaDeployDetail();
					mdd.setDeployId(md.getId());
					mdd.setDeviceId(dev.getId());
					mdd.setMediaId(media.getId());
					mdd.setDeployTime(now);

					mdd.setSn(dev.getSn());
					mdd.setDeviceName(dev.getName());
					if (media.getLanguage() == 0 || media.getLanguage() == 2)
						mdd.setMediaTitle(media.getTitle());

					if (media.getLanguage() == 1 || media.getLanguage() == 2)
						mdd.setMediaTitleCn(media.getTitleCn());

					mdd.setMediaVersion(media.getVersion());

					Map old = mediaDeployDetailDao.getLastMediaDeployDetail(dev.getId(),
							media.getId(), media.getVersion());
					if (old != null) {
						mdd.setDeviceStatus(MediaDeployDetail.FN_DEVICE_STATUS_EXIST);
					} else {
						mdd.setDeviceStatus(MediaDeployDetail.FN_DEVICE_STATUS_WAIT);
					}

					mddList.add(mdd);
				}
			}

			mediaDeployDetailDao.saveOrUpdateAll(mddList);

			// 生成VLIST文件
			String downloadPath = "http://video.ubt.io/download/";

			if (Boolean.valueOf(SysConfig.getSetting("run.model"))) {
				downloadPath = "http://033babdaa6badbbafa46-9249f9b70a837f70f108ceb8162d740f.r3.cf5.rackcdn.com/";
			}

			// version 格式：'V' + id + '.' + 时间戳
			String version = "V" + md.getId() + "." + (new Date()).getTime();
			if (!makeVlistFile(md, mediaList, version, downloadPath)) {
				logger.error("DeployId:" + md.getId() + ";publish error!");
				mediaDeployDao.updatePublishError(md.getId());
			}

			String filePath = SysConfig.getSetting("wifun.media.deploy.directory") + File.separator
					+ "vlist_" + md.getId();

			// 向Inhand平台上传文件
			String uri = InhandMediaDeployService.vlistUpload(
					SessionHelper.getToken(md.getCompanyId()), filePath);
			if (StringUtils.isBlank(uri)) {
				logger.error("DeployId:" + md.getId() + ";vlistUpload error!");
				mediaDeployDao.updatePublishError(md.getId());
				return false;
			}

			// 发布
			String pointId = InhandPublishService.getPublishPointId(
					SessionHelper.getToken(md.getCompanyId()),
					SysConfig.getSetting("wifun.publish.point.media"));
			if (StringUtils.isBlank(pointId)) {
				logger.error("DeployId:" + md.getId() + ";getPublishPointId error!");
				mediaDeployDao.updatePublishError(md.getId());
				return false;
			}

			logger.info("DeployId:" + md.getId() + "; pointId:" + pointId);

			String tagId = "defaultTagId";
			if (md.getGroupId() > 0) {
				Group group = groupDao.get(md.getGroupId());
				tagId = group.getInhandId();
			}
			String publishId = InhandPublishService.publish(
					SessionHelper.getToken(md.getCompanyId()), pointId, tagId, uri,
					null == md.getVersion() ? "V1." + md.getId() : md.getVersion());

			if (StringUtils.isBlank(publishId)) {
				logger.error("DeployId:" + md.getId() + ";publish error!");
				mediaDeployDao.updatePublishError(md.getId());
				return false;
			}

			logger.info("DeployId:" + md.getId() + "; pointId:" + pointId + "; publishId:"
					+ publishId + "; uri:" + uri);

			mediaDeployDao.updatePublishInfo(md.getId(), pointId, publishId, version);

			logger.info("DeployId:" + md.getId() + "version:" + version + ";succeed!");

			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * sd卡发布媒体资源
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public MediaDeploy deploySD(MediaDeploy md, String[] devices) {

		try {
			Date now = new Date();

			// 保存发布记录
			md.setDeployTime(now);
			create(md);

			// 记录明细
			List<MediaDeployDetail> mddList = new ArrayList<MediaDeployDetail>();

			Device d = new Device();
			d.setCompanyId(md.getCompanyId());
			d.setGroupId(md.getGroupId());
			d.setIsActive(true);
			List<Device> devList = deviceDao.findByExample(d);
			List<Media> mediaList = mediaDao.findMediaByMediaListId(md.getMediaListId());

			for (int i = 0; i < devList.size(); i++) {
				Device dev = devList.get(i);
				int devId = dev.getId();
				// 判断设备是否被选中
				boolean isSelected = false;
				for (int j = 0; j < devices.length; j++) {
					if (devices[j] != null && devices[j].trim().equals(String.valueOf(devId))) {
						isSelected = true;
						break;
					}
				}
				// 设备未被选中 continue
				if (!isSelected) {
					continue;
				}

				for (int j = 0; j < mediaList.size(); j++) {
					Media media = mediaList.get(j);
					int mediaId = media.getId();
					MediaDeployDetail mdd = new MediaDeployDetail();
					mdd.setDeployId(md.getId());
					mdd.setDeviceId(devId);
					mdd.setMediaId(mediaId);
					mdd.setStatus("0");// 默认状态waiting
					mdd.setDeployTime(now);

					mdd.setSn(dev.getSn());
					mdd.setDeviceName(dev.getName());
					if (media.getLanguage() == 0 || media.getLanguage() == 2)
						mdd.setMediaTitle(media.getTitle());

					if (media.getLanguage() == 1 || media.getLanguage() == 2)
						mdd.setMediaTitleCn(media.getTitleCn());

					mdd.setMediaVersion(media.getVersion());

					Map old = mediaDeployDetailDao.getLastMediaDeployDetail(devId, mediaId,
							media.getVersion());
					if (old != null) {
						mdd.setDeviceStatus(MediaDeployDetail.FN_DEVICE_STATUS_EXIST);
					} else {
						mdd.setDeviceStatus(MediaDeployDetail.FN_DEVICE_STATUS_WAIT);
					}

					mddList.add(mdd);
				}
			}

			mediaDeployDetailDao.saveOrUpdateAll(mddList);

			// 生成VLIST文件
			// version 格式：'V' + id + '.' + 时间戳
			String version = "V" + md.getId() + "." + (new Date()).getTime();
			if (!makeVlistFile(md, mediaList, version, null)) {
				logger.error("DeployId:" + md.getId() + ";publish error!");
				mediaDeployDao.updatePublishError(md.getId());
			}

			md.setVersion(version);
			saveOrUpdate(md);

			return md;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	private boolean makeVlistFile(MediaDeploy md, List<Media> mediaList, String version,
			String downloadPath) {

		FileWriter out = null;

		try {
			String deployPath = SysConfig.getSetting("wifun.media.deploy.directory");
			String filePath = deployPath + File.separator + "vlist_" + md.getId();

			File oFile = new File(filePath);
			out = new FileWriter(oFile, true);

			out.write("#version:<" + version + ">" + "\n");
			for (int i = 0; i < mediaList.size(); i++) {
				Media media = mediaList.get(i);

				String fileName = "media_" + media.getCode() + "_" + media.getVersion();
				String zipFileName = fileName + ".zip";
				String str = "";
				if (StringUtils.isBlank(downloadPath))
					str = fileName + ",;\n";
				else
					str = fileName + "," + downloadPath + "/" + zipFileName + ";\n";

				out.write(str);
			}
			return true;
		} catch (IOException e) {
			logger.error("DeployId:" + md.getId() + ";makeVlistFile error!", e);
			e.printStackTrace();
			return false;
		} finally {
			try {
				if (out != null)
					out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public List<MediaDeploy> loadByCompany(Integer companyId) {

		MediaDeploy md = new MediaDeploy();
		md.setCompanyId(companyId);
		return mediaDeployDao.findByExample(md);
	}

	@Override
	public void updateDeployStatusByDeviceStatus(String version) {

		MediaDeploy mediaDeploy = mediaDeployDao.getMediaDeployByVersion(version);

		if (mediaDeploy == null)
			return;

		int deployId = mediaDeploy.getId();
		List<MediaDeployDetail> list = mediaDeployDetailDao.getMediaDeployDetailWaiting(deployId);
		if (list == null || list.size() == 0) {
			mediaDeploy.setStatus(2);
			mediaDeployDao.update(mediaDeploy);
		}
	}
}
