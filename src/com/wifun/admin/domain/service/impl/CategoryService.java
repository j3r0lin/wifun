package com.wifun.admin.domain.service.impl;

import java.util.Collection;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.domain.dao.ICategoryDao;
import com.wifun.admin.domain.model.Category;
import com.wifun.admin.domain.service.ICategoryService;

@Service("categoryService")
@Transactional
public class CategoryService implements ICategoryService {

	@Resource(name = "categoryDao")
	private ICategoryDao categoryDao;

	/**
	 * 根据 id 查询
	 */
	@Override
	public Category findById(Integer id) {

		// TODO Auto-generated method stub
		return categoryDao.get(id);
	}

	/**
	 * 加载所有电影分类
	 */
	@Override
	public List<Category> loadAll() {

		return categoryDao.loadAll();
	}

	/**
	 * 获取电影分类列表
	 * 
	 * @param active
	 * @return
	 */
	@Override
	public List<Category> getCategoryList(boolean active) {

		return categoryDao.getCategoryList(active);
	}

	/**
	 * 根据实体查询
	 */
	@Override
	public List<Category> findByExample(Category t) {

		// TODO Auto-generated method stub
		return categoryDao.findByExample(t);
	}

	/**
	 * 保存
	 */
	@Override
	public boolean create(Category entity) {

		try {
			categoryDao.save(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * 保存
	 */
	@Override
	public boolean save(Category entity) {

		try {
			categoryDao.save(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * 更新
	 */
	@Override
	public boolean update(Category entity) {

		try {
			categoryDao.update(entity);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	/**
	 * 增加或更新实体
	 */
	@Override
	public void saveOrUpdate(Category entity) {

		categoryDao.saveOrUpdate(entity);
	}

	/**
	 * 增加或更新列表中实体
	 */
	@Override
	public void saveOrUpdateAll(Collection<Category> entities) {

		categoryDao.saveOrUpdateAll(entities);
	}

	/**
	 * 更新实体 删除
	 */
	@Override
	public void delete(Category entity) {

		categoryDao.delete(entity);
	}

	/**
	 * 根据 Id删除
	 */
	@Override
	public void deleteById(Integer id) {

		categoryDao.deleteByKey(id);
	}

	/**
	 * 删除列表中 实体
	 */
	@Override
	public void deleteAll(Collection<Category> entities) {

		categoryDao.deleteAll(entities);
	}

}
