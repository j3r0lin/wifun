package com.wifun.admin.domain.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.wifun.admin.domain.dao.ITaskTmobileDataUsageDao;
import com.wifun.admin.domain.model.TaskTmobileDataUse;
import com.wifun.admin.domain.service.ITmobilePageReportService;
import com.wifun.admin.util.TimeUtils;

@Service("tmobilePageService")
@Transactional
public class TmobilePageService implements ITmobilePageReportService {

	@Resource(name = "taskTmobileDataUsageDao")
	private ITaskTmobileDataUsageDao taskTmobileDataUsageDao;

	@Override
	public List<TaskTmobileDataUse> getDataUsageByDate(String date) {

		List<TaskTmobileDataUse> vos = new ArrayList<TaskTmobileDataUse>();

		TaskTmobileDataUse entity = new TaskTmobileDataUse();
		Date dt = TimeUtils.strUsToDate(date);
		entity.setDataTime(dt);
		List<TaskTmobileDataUse> result = taskTmobileDataUsageDao.findByExample(entity);
		if (null != result) {
			for (int i = 0; i < result.size(); i++) {
				TaskTmobileDataUse data = result.get(i);
				TaskTmobileDataUse vo = new TaskTmobileDataUse();
				vo.setPhoneNum(data.getPhoneNum());
				vo.setBillCycle(data.getBillCycle());
				vo.setCurrentUsageTime(data.getCurrentUsageTime());
				vo.setDataUsageAll(data.getDataUsageAll());
				vo.setDaysRemainingInCycle(data.getDaysRemainingInCycle());

				vo.setChargesInclude(data.getChargesInclude());
				vo.setIncludedInclude(data.getIncludedInclude());
				vo.setRemainingInclude(data.getRemainingInclude());
				vo.setUsedInclude(data.getUsedInclude());

				vo.setChargesReduced(data.getChargesReduced());
				vo.setIncludedReduced(data.getIncludedReduced());
				vo.setRemainingReduced(data.getRemainingReduced());
				vo.setUsedReduced(data.getUsedReduced());

				vos.add(vo);
			}
		}
		return vos;
	}
}