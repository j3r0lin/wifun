package com.wifun.admin.domain.service;

import java.util.List;
import java.util.Map;

import com.wifun.admin.app.vo.DeviceLocation;
import com.wifun.admin.app.vo.ResultVo;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.Group;

public interface IDeviceService extends IBaseService<Device, Integer> {

	/**
	 * 根据 company 查询设备
	 * 
	 * @param companyId
	 * @return
	 */
	List<Device> loadByCompany(Integer companyId);

	/**
	 * 根据设备 分组 加载 设备信息
	 * 
	 * @param group
	 * @return
	 */
	List<Device> loadByGroup(Group group);

	/**
	 * 根据 查询条件 查询设备信息
	 * 
	 * @param active
	 * @param companyId
	 * @param groupId
	 * @param sn
	 * @param name
	 * @return
	 */
	List<Device> searchDevices(boolean active, Integer companyId, int groupId, String sn,
			String name);

	/**
	 * 根据comapnyId groupId 获取设备信息
	 * 
	 * @param companyId
	 * @param groupId
	 * @return
	 */
	List<Device> getDeviceList(int companyId, int groupId);

	/**
	 * 增加设备信息
	 * 
	 * @param entity
	 * @return
	 */
	ResultVo addDevice(Device entity);

	/**
	 * 更新设备信息
	 * 
	 * @param entity
	 * @return
	 */
	ResultVo updateDevice(Device entity);

	/**
	 * 根据id 删除设备信息
	 * 
	 * @param id
	 * @return
	 */
	ResultVo deleteDeviceById(Integer id);

	/**
	 * 根据Inhanend 信息更新设备信息
	 * 
	 * @param device
	 * @return
	 */
	boolean updateInfoFromInhaned(Device device);

	/**
	 * 根据巴士公司 id 得到设备的位置信息
	 * 
	 * @param companyId
	 * @return
	 */
	List<DeviceLocation> getDeviceLocations(int companyId);

	/**
	 * 根据设备id 日期得到 Route信息
	 * 
	 * @param deviceId
	 * @param date
	 * @return
	 */
	List<DeviceLocation> getDeviceRoute(int deviceId, String date);

	/**
	 * 根据 companyId 得到 设备Map<InhandId,device>
	 * 
	 * @param companyId
	 * @return
	 */
	Map<String, Device> getDeviceMapByInhandId(int companyId);
}
