package com.wifun.admin.domain.service;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import com.wifun.admin.app.vo.ResultVo;

public interface IBaseService<T, PK extends Serializable> {

	// 根据主键获取实体。如果没有相应的实体，返回 null。
	public T findById(PK id);

	// 获取全部实体。
	public List<T> loadAll();

	// 实例查询。
	public List<T> findByExample(T t);

	// 更新实体
	public boolean create(T entity);

	// 存储实体到数据库
	public boolean save(T entity);

	public boolean update(T entity);

	// 增加或更新实体
	public void saveOrUpdate(T entity);

	// 增加或更新集合中的全部实体
	public void saveOrUpdateAll(Collection<T> entities);

	// 删除指定的实体
	public void delete(T entity);

	// 根据主键删除指定实体
	public void deleteById(PK id);

	// 删除集合中的全部实体
	public void deleteAll(Collection<T> entities);

}
