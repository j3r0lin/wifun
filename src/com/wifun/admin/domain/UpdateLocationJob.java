package com.wifun.admin.domain;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.app.vo.DeviceLocation;
import com.wifun.admin.domain.dao.ICompanyDao;
import com.wifun.admin.domain.dao.IDeviceDao;
import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.util.LocationFileUtil;
import com.wifun.admin.util.TimeUtils;
import com.wifun.inhand.model.Location;
import com.wifun.inhand.model.Site;
import com.wifun.inhand.response.site.SiteListResponse;
import com.wifun.inhand.service.InhandSiteService;

/**
 * 获取设备的GPS位置信息，每隔30s执行一次
 * 
 * @File wifun-admin/com.wifun.admin.domain.UpdateLocationJob.java
 * @Desc
 */
@Component
public class UpdateLocationJob {

	@Resource(name = "companyDao")
	private ICompanyDao companyDao;

	@Resource(name = "deviceDao")
	private IDeviceDao deviceDao;

	private Map<String, Device> deviceMap = new HashMap<String, Device>();

	// 每30秒执行一次
	@Scheduled(cron = "0/30 * *  * * ? ")
	public void schedule() {

		System.out.println("**************location job start***************");
		updateLocation();
		System.out.println("**************location job end***************");
	}

	/**
	 * 获取GPS位置信息
	 */
	private void updateLocation() {

		dealDeviceMap();

		List<Company> companys = companyDao.getCompanyList(true, null);
		for (int i = 0; i < companys.size(); i++) {

			Company company = companys.get(i);
			SiteListResponse response = InhandSiteService.querySites(SessionHelper.getToken(company
					.getId()));

			if (response == null)
				continue;

			List<Site> sites = response.getSites();
			if (sites == null || sites.size() == 0)
				continue;

			for (int j = 0; j < sites.size(); j++) {

				Site site = sites.get(j);
				if (site.getOnline() == 0) // 设备离线，不记录线路
					continue;

				String siteId = site.get_id();
				Location location = site.getLocation();
				Device device = deviceMap.get(siteId);
				if (device == null || location == null)
					continue;

				DeviceLocation deviceLocation = new DeviceLocation(device, location,
						TimeUtils.getNowDate());
				// 记录日志
				try {
					LocationFileUtil.writeLogFile(deviceLocation);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private void dealDeviceMap() {

		List<Device> devices = deviceDao.searchDevices(false, 0, -1, null, null);
		for (int i = 0; i < devices.size(); i++) {

			Device device = devices.get(i);
			if (StringUtils.isNotBlank(device.getInhandSiteId()))
				deviceMap.put(device.getInhandSiteId(), device);
		}
	}
}
