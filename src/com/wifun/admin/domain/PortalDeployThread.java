package com.wifun.admin.domain;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.dao.IGroupDao;
import com.wifun.admin.domain.dao.IPortalDeployDao;
import com.wifun.admin.domain.model.Group;
import com.wifun.admin.domain.model.PortalDeploy;
import com.wifun.inhand.service.InhandPortalDeployService;
import com.wifun.inhand.service.InhandPublishService;

/**
 * Portal文件发布流程
 * 
 * @File wifun-admin/com.wifun.admin.domain.PortalDeployThread.java
 * @Desc
 */
public class PortalDeployThread extends Thread {

	private static final Logger logger = Logger.getLogger("deploy.portal.log");

	private PortalDeploy pd;
	private String packageUrl;
	private String portalUrl;
	private IGroupDao groupDao;
	private IPortalDeployDao portalDeployDao;

	public PortalDeployThread(PortalDeploy pd, String packageUrl, String portalUrl) {

		this.pd = pd;
		this.packageUrl = packageUrl;
		this.portalUrl = portalUrl;
		groupDao = (IGroupDao) SpringContextUtil.getBean("groupDao");
		portalDeployDao = (IPortalDeployDao) SpringContextUtil.getBean("portalDeployDao");
	}

	@Override
	public void run() {

		// 上传portal压缩包到inhand平台
		String uri = InhandPortalDeployService.portalUpload(
				SessionHelper.getToken(pd.getCompanyId()), packageUrl);
		if (StringUtils.isBlank(uri)) {
			logger.error("DeployId:" + pd.getId() + ";portalUpload error!");
			portalDeployDao.updatePublishError(pd.getId());
			return;
		}

		// 获取发布点id
		String pointId = InhandPublishService.getPublishPointId(
				SessionHelper.getToken(pd.getCompanyId()), portalUrl);
		if (StringUtils.isBlank(pointId)) {
			logger.error("DeployId:" + pd.getId() + ";getPublishPointId error!");
			portalDeployDao.updatePublishError(pd.getId());
			return;
		}

		// 发布portal
		String tagId = "defaultTagId";
		if (0 != pd.getGroupId()) {
			Group group = groupDao.get(pd.getGroupId());
			tagId = group.getInhandId();
		}
		String publishId = InhandPublishService.publish(SessionHelper.getToken(pd.getCompanyId()),
				pointId, tagId, uri, "V1." + pd.getId());

		if (StringUtils.isBlank(publishId)) {
			logger.error("DeployId:" + pd.getId() + ";publish error!");
			portalDeployDao.updatePublishError(pd.getId());
			return;
		}

		logger.info("DeployId:" + pd.getId() + "; pointId:" + pointId + "; publishId:" + publishId
				+ "; uri:" + uri);

		portalDeployDao.updatePublishInfo(pd.getId(), pointId, publishId);

		logger.info("DeployId:" + pd.getId() + ";succeed!");
	}
}
