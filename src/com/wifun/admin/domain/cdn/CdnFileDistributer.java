package com.wifun.admin.domain.cdn;

import java.io.File;
import com.wifun.admin.util.SysConfig;

/**
 * 文件分发器
 * 
 * @author wanghc
 * 
 */
public class CdnFileDistributer {

	public static CdnFileDistributer instance;
	private static final String container = SysConfig.getSetting("cdn.media.container");
	private static final boolean RUN_MODEL = Boolean.valueOf(SysConfig.getSetting("run.model"));

	private CdnFileDistributer() {

	}

	public static CdnFileDistributer getInstance() {

		if (instance == null) {

			instance = new CdnFileDistributer();
		}

		return instance;
	}

	/**
	 * 拷贝文件到服务器列表 （对于已存在的文件，不覆盖）
	 * 
	 * @param localRootPath
	 *            c://res//
	 * @param filePath
	 *            /1/2/abc.jpg
	 */
	public void copyFileToServer(String localRootPath, String filePath) {

		File oriFile = new File(localRootPath + "/" + filePath);

		System.out.println("cdn upload file: " + oriFile);
		if (oriFile.exists()) {

			// 如果在正式环境中，文件才会复制到CDN中
			if (RUN_MODEL) {
				System.out.println("cdn upload begin！");
				String localFilePath = localRootPath + "/" + filePath;
				FilesPublish.uploadFile(localFilePath, filePath, container);
				System.out.println("cdn upload success！");
			} else {
				System.out.println("run mode test, send url is " + localRootPath);
			}
		}
	}
}
