package com.wifun.admin.domain.cdn;

import java.io.Closeable;
import java.io.File;
import java.io.IOException;

import org.jclouds.ContextBuilder;
import org.jclouds.blobstore.BlobStoreContext;
import org.jclouds.openstack.swift.CommonSwiftAsyncClient;
import org.jclouds.openstack.swift.CommonSwiftClient;
import org.jclouds.openstack.swift.domain.SwiftObject;
import org.jclouds.rest.RestContext;

import com.google.common.io.Closeables;

/**
 * 向CDN发布文件
 * 
 * @Author xiafy
 * @Date 2015-9-25
 * @File wifun-admin/com.wifun.admin.domain.cdn.FilesPublish.java
 * @Desc
 */
public class FilesPublish implements Closeable {

	@SuppressWarnings("deprecation")
	private RestContext<CommonSwiftClient, CommonSwiftAsyncClient> swift;
	private BlobStoreContext context;

	public FilesPublish(String username, String apiKey) {

		context = ContextBuilder.newBuilder(Constants.PROVIDER).credentials(username, apiKey)
				.buildView(BlobStoreContext.class);
		swift = context.unwrap();
	}

	/**
	 * 上传文件
	 * 
	 * @return
	 */
	public static boolean uploadFile(String localFilePath, String remoteFileName, String container) {

		final String ID = Constants.ID;
		final String KEY = Constants.KEY;

		FilesPublish filesPublish = new FilesPublish(ID, KEY);

		try {
			filesPublish.publishFile(localFilePath, remoteFileName, container);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				filesPublish.close();
			} catch (Exception e) {
				e.printStackTrace();
				return false;
			}
		}
		return true;
	}

	@SuppressWarnings("deprecation")
	public void publishFile(String localFilePath, String remoteFileName, String container) {

		File tempFile = new File(localFilePath);

		SwiftObject object = swift.getApi().newSwiftObject();
		object.getInfo().setName(remoteFileName);
		object.setPayload(tempFile);

		swift.getApi().putObject(container, object);
	}

	public void close() throws IOException {

		Closeables.close(context, true);
	}
}
