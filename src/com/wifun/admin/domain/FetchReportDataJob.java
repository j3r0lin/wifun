package com.wifun.admin.domain;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.app.vo.PvuvMovieVo;
import com.wifun.admin.domain.dao.ICompanyDao;
import com.wifun.admin.domain.dao.IDeviceDao;
import com.wifun.admin.domain.dao.ITaskDeviceSumDailyDao;
import com.wifun.admin.domain.dao.ITaskPVUVDailyDao;
import com.wifun.admin.domain.dao.ITaskPVUVMovieDao;
import com.wifun.admin.domain.dao.ITaskReportDailyDao;
import com.wifun.admin.domain.dao.ITaskTrafficDailyDao;
import com.wifun.admin.domain.dao.ITaskWifiStatDao;
import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.TaskDeviceSumDaily;
import com.wifun.admin.domain.model.TaskPVUVDaily;
import com.wifun.admin.domain.model.TaskPvuvMovie;
import com.wifun.admin.domain.model.TaskReportDaily;
import com.wifun.admin.domain.model.TaskTrafficDaily;
import com.wifun.admin.domain.model.TaskWifiStat;
import com.wifun.admin.domain.service.IPvuvMovieReportService;
import com.wifun.admin.util.TimeUtils;
import com.wifun.inhand.response.statistic.PvuvResource;
import com.wifun.inhand.response.statistic.TrafficDailyRespModel;
import com.wifun.inhand.response.statistic.TrafficDailyResponse;
import com.wifun.inhand.service.InhandPVUVService;
import com.wifun.inhand.service.InhandStatisticService;
import com.wifun.inhand.service.InhandTrafficService;

/**
 * 
 * @author chw 统计每日新增终端、累计终端、停留时间超过1小时的终端数、 WiFi流量大于200MB的终端数、首页PVUV、电影播放页PVUV
 *         取inhand数据后台定时任务 每天凌晨1点执行
 */

@Component
public class FetchReportDataJob {

	@Resource(name = "companyDao")
	private ICompanyDao companyDao;

	@Resource(name = "deviceDao")
	private IDeviceDao deviceDao;

	@Resource(name = "taskTrafficDailyDao")
	private ITaskTrafficDailyDao taskTrafficDailyDao;

	@Resource(name = "taskPVUVDailyDao")
	private ITaskPVUVDailyDao taskPVUVDailyDao;

	@Resource(name = "taskDeviceSumDailyDao")
	private ITaskDeviceSumDailyDao taskDeviceSumDailyDao;

	@Resource(name = "taskReportDailyDao")
	private ITaskReportDailyDao reportDailyDao;

	@Resource(name = "taskWifiStatDao")
	private ITaskWifiStatDao taskWifiStatDao;

	@Resource(name = "taskPVUVMovieDao")
	private ITaskPVUVMovieDao taskPVUVMovieDao;

	@Resource(name = "pvuvMovieReportService")
	private IPvuvMovieReportService pvuvMovieReportService;

	private String date = TimeUtils.getFormatterDate(TimeUtils.getYesterday(), "yyyy-MM-dd");

	// 每天1点执行
	@Scheduled(cron = "0 0 1 ? * *")
	public void schedule() {

		System.out.println("**************report job start***************");
		fetchAllReportData();
		System.out.println("**************report job end***************");
	}

	/*
	 * 获取一个公司所有device的pvuv数据，每日的数据，及总计
	 */
	public void fetchAllReportData() {

		date = TimeUtils.getFormatterDate(TimeUtils.getYesterday(), "yyyy-MM-dd");

		List<Company> companys = companyDao.getCompanyList(true, null);

		for (int i = 0; i < companys.size(); i++) {

			Company company = companys.get(i);
			int companyId = company.getId();

			if (companyId == 56) // 测试账号的数据不做统计
				continue;

			String token = SessionHelper.getToken(companyId);

			List<Device> devices = deviceDao.getDeviceList(companyId, -1);

			for (int j = 0; j < devices.size(); j++) {

				Device device = devices.get(j);
				TaskTrafficDaily trafficDaily = fetchTrafficDaily(device, token);
				TaskDeviceSumDaily deviceSumDaily = fetchStatisticDaily(device, token);
				Map<String, TaskPVUVDaily> pvuvMap = fetchPVUVDailyByTypes(device, token);
				TaskPVUVDaily mainPvuvDaily = pvuvMap.get("main");
				TaskPVUVDaily moviePvuvDaily = pvuvMap.get("movie");
				TaskPVUVDaily homePvuvDaily = pvuvMap.get("home");
				updateDailyReport(device, trafficDaily, deviceSumDaily, homePvuvDaily,
						mainPvuvDaily, moviePvuvDaily);
			}

			// 每个公司每个电影pvuv统计
			fetchMoviePvuv(companyId, token);
		}
	}

	/**
	 * 获取电影的pvuv
	 * 
	 * @param companyId
	 * @param token
	 */
	private void fetchMoviePvuv(int companyId, String token) {

		ArrayList<PvuvMovieVo> list = pvuvMovieReportService.queryMoviePvuv(token);

		// 将获取到的pvuv数据写入数据库表task_pvuv_movie
		for (int i = 0; i < list.size(); i++) {
			PvuvMovieVo mv = list.get(i);
			TaskPvuvMovie entity = new TaskPvuvMovie();
			// 定时任务获取昨天的数据，接口返回的last数据就是昨天数据。
			entity.setCurrentPv(mv.getLastPv());
			entity.setCurrentUv(mv.getLastUv());
			entity.setTotalPv(mv.getTotalPv());
			entity.setTotalUv(mv.getTotalUv());
			entity.setMovieTitle(mv.getTitle());
			entity.setMovieId(Integer.parseInt(mv.getRId()));
			entity.setCompanyId(companyId);
			entity.setDate(date);
			entity.setCreateTime(new Date());
			taskPVUVMovieDao.save(entity);
		}
	}

	/**
	 * 按照页面类型获取pvuv
	 * 
	 * @param device
	 * @param token
	 * @return
	 */
	private Map<String, TaskPVUVDaily> fetchPVUVDailyByTypes(Device device, String token) {

		Map<String, TaskPVUVDaily> map = new HashMap<String, TaskPVUVDaily>();
		String siteId = device.getInhandSiteId();
		if (StringUtils.isBlank(siteId))
			return map;

		List<PvuvResource> list = InhandPVUVService.queryPvuvTypes(token, siteId);
		if (list == null)
			return map;

		for (int i = 0; i < list.size(); i++) {

			PvuvResource resource = list.get(i);
			String type = resource.getTId();
			if (StringUtils.isBlank(type))
				continue;

			HashMap<String, String> yesterdayMap = null;
			HashMap<String, String> currentMap = resource.getCurrent();
			HashMap<String, String> lastMap = resource.getLast();

			int pvValue = 0;
			int uvValue = 0;

			if (date.equals(currentMap.get("date")))
				yesterdayMap = currentMap;

			if (date.equals(lastMap.get("date")))
				yesterdayMap = lastMap;

			if (yesterdayMap != null) {
				pvValue += StringUtils.isBlank(lastMap.get("pv")) ? 0 : Integer.parseInt(lastMap
						.get("pv"));
				uvValue += StringUtils.isBlank(lastMap.get("uv")) ? 0 : Integer.parseInt(lastMap
						.get("uv"));
			}

			TaskPVUVDaily taskPVUVDaily = savePVUVDaily(device, type, pvValue, uvValue);
			map.put(type, taskPVUVDaily);
		}

		return map;
	}

	/**
	 * 保存pvuv数据
	 * 
	 * @param device
	 * @param type
	 * @param pv
	 * @param uv
	 * @return
	 */
	private TaskPVUVDaily savePVUVDaily(Device device, String type, int pv, int uv) {

		TaskPVUVDaily taskPVUVDaily = new TaskPVUVDaily();
		taskPVUVDaily.setDeviceId(device.getId());
		taskPVUVDaily.setCompanyId(device.getCompanyId());
		taskPVUVDaily.setType(type);
		taskPVUVDaily.setRid("0");
		taskPVUVDaily.setPv(pv);
		taskPVUVDaily.setUv(uv);
		taskPVUVDaily.setDate(date);
		taskPVUVDaily.setCreateTime(new Date());
		return taskPVUVDailyDao.save(taskPVUVDaily);
	}

	/**
	 * 获取访问情况数据
	 * 
	 * @param device
	 * @param token
	 * @return
	 */
	private TaskDeviceSumDaily fetchStatisticDaily(Device device, String token) {

		long startTime = TimeUtils.getYesterday().getTime();
		long endTime = TimeUtils.getYesterdayEnd().getTime();
		String types = "50,51,70,71";
		// 70: 每日新用户数，71: 每日新终端数， 50: 每日总用户数，51：每日总终端数， 307：停留时间超1小时的终端数
		HashMap<String, Map<String, Integer>> wifiStatMap = InhandStatisticService.queryWifiStat(
				token, types, startTime, endTime, device.getInhandSiteId());

		int deviceDailyAdd = 0;
		int deviceTotal = 0;
		int deviceOneHourOnline = 0;
		int userDailyAdd = 0;
		int userTotal = 0;

		if (wifiStatMap == null)
			return saveStatisticDaily(device, deviceDailyAdd, deviceTotal, deviceOneHourOnline,
					userDailyAdd, userTotal);

		// 71: 每日新终端数
		Map<String, Integer> deviceDailyAddMap = wifiStatMap.get("71");

		if (deviceDailyAddMap != null && deviceDailyAddMap.get(date) != null) {
			deviceDailyAdd = deviceDailyAddMap.get(date);
			saveWifiStat(device, "71", deviceDailyAdd);
		}

		// 51：每日总终端数
		Map<String, Integer> deviceTotleMap = wifiStatMap.get("51");

		if (deviceTotleMap != null && deviceTotleMap.get(date) != null) {
			deviceTotal = deviceTotleMap.get(date);
			saveWifiStat(device, "51", deviceTotal);
		}

		// 70: 每日新用户数
		Map<String, Integer> userDailyAddMap = wifiStatMap.get("70");

		if (userDailyAddMap != null && userDailyAddMap.get(date) != null) {
			userDailyAdd = userDailyAddMap.get(date);
			saveWifiStat(device, "70", userDailyAdd);
		}

		// 50: 每日总用户数
		Map<String, Integer> userTotleMap = wifiStatMap.get("50");

		if (userTotleMap != null && userTotleMap.get(date) != null) {
			userTotal = userTotleMap.get(date);
			saveWifiStat(device, "50", userTotal);
		}

		// 307：停留时间超1小时的终端数
		deviceOneHourOnline = getOneHourOnline(device, token);

		return saveStatisticDaily(device, deviceDailyAdd, deviceTotal, deviceOneHourOnline,
				userDailyAdd, userTotal);
	}

	/**
	 * 获取停留超过1小时的用户数量
	 * 
	 * @param device
	 * @param token
	 * @return
	 */
	private int getOneHourOnline(Device device, String token) {

		Calendar cal = Calendar.getInstance();
		cal.setTime(TimeUtils.getYesterday());

		int day = cal.get(Calendar.DATE);

		cal.set(Calendar.DATE, 1);
		long startTime = cal.getTime().getTime();
		long endTime = TimeUtils.getYesterdayEnd().getTime();
		int value = 0;

		HashMap<String, Map<String, Integer>> map = InhandStatisticService.queryWifiStat(token,
				"307", startTime, endTime, device.getInhandSiteId());

		// 307：停留时间超1小时的终端数
		Map<String, Integer> oneHourOnlineMap = map.get("307");

		String monthFirstDay = TimeUtils.getFormatterDate(cal.getTime(), "yyyy-MM-dd");
		if (oneHourOnlineMap != null && oneHourOnlineMap.get(monthFirstDay) != null) {
			value = oneHourOnlineMap.get(monthFirstDay);
			saveWifiStat(device, "307", value);
		}

		if (day != 1) {
			String lastDate = TimeUtils.dateAdd(date, "yyyy-MM-dd", -1);
			int lastValue = 0;
			TaskWifiStat taskWifiStat = taskWifiStatDao.getTaskWifiStat(lastDate, "307",
					device.getId());

			if (taskWifiStat != null)
				lastValue = taskWifiStat.getValue();
			value = value - lastValue;
		}

		return value;
	}

	/**
	 * 保存设备访问数据(全部)
	 * 
	 * @param device
	 * @param type
	 * @param value
	 */
	private void saveWifiStat(Device device, String type, int value) {

		TaskWifiStat taskWifiStat = new TaskWifiStat();
		taskWifiStat.setDeviceId(device.getId());
		taskWifiStat.setCompanyId(device.getCompanyId());
		taskWifiStat.setType(type);
		taskWifiStat.setValue(value);
		taskWifiStat.setDate(date);
		taskWifiStat.setCreateTime(new Date());
		taskWifiStatDao.save(taskWifiStat);
	}

	/**
	 * 保存设备访问数据
	 * 
	 * @param device
	 * @param deviceDailyAdd
	 * @param deviceTotal
	 * @param deviceOneHourOnline
	 * @param userDailyAdd
	 * @param userTotal
	 * @return
	 */
	private TaskDeviceSumDaily saveStatisticDaily(Device device, int deviceDailyAdd,
			int deviceTotal, int deviceOneHourOnline, int userDailyAdd, int userTotal) {

		TaskDeviceSumDaily taskDeviceSumDaily = new TaskDeviceSumDaily();
		taskDeviceSumDaily.setDeviceId(device.getId());
		taskDeviceSumDaily.setCompanyId(device.getCompanyId());
		taskDeviceSumDaily.setDeviceDailyAdd(deviceDailyAdd);
		taskDeviceSumDaily.setDeviceTotal(deviceTotal);
		taskDeviceSumDaily.setDeviceOneHourOnline(deviceOneHourOnline);
		taskDeviceSumDaily.setUserDailyAdd(userDailyAdd);
		taskDeviceSumDaily.setUserTotal(userTotal);
		taskDeviceSumDaily.setDate(date);
		taskDeviceSumDaily.setCreateTime(new Date());
		return taskDeviceSumDailyDao.save(taskDeviceSumDaily);
	}

	/**
	 * 获取流量数据
	 * 
	 * @param device
	 * @param token
	 * @return
	 */
	private TaskTrafficDaily fetchTrafficDaily(Device device, String token) {

		String shortDate = TimeUtils.getFormatterDate(TimeUtils.getYesterday(), "yyyyMMdd");
		String month = TimeUtils.getFormatterDate(TimeUtils.getYesterday(), "yyyyMM");

		TrafficDailyResponse response = InhandTrafficService.trafficDaily(token,
				device.getInhandId(), month);

		if (response == null || response.getRespResult().size() == 0)
			return saveTrafficDaily(device, null);

		List<TrafficDailyRespModel> list = response.getRespResult();

		TrafficDailyRespModel model = null;
		for (int i = 0; i < list.size(); i++) {

			TrafficDailyRespModel tmp = list.get(i);
			if (shortDate.equals(tmp.getDate())) {
				model = tmp;
				break;
			}
		}
		return saveTrafficDaily(device, model);
	}

	/**
	 * 保存流量数据
	 * 
	 * @param device
	 * @param model
	 * @return
	 */
	private TaskTrafficDaily saveTrafficDaily(Device device, TrafficDailyRespModel model) {

		TaskTrafficDaily taskTrafficDaily = new TaskTrafficDaily();
		taskTrafficDaily.setDeviceId(device.getId());
		taskTrafficDaily.setCompanyId(device.getCompanyId());

		taskTrafficDaily.setDate(date);
		taskTrafficDaily.setCreateTime(new Date());

		if (model == null) {
			taskTrafficDaily.setSend(0L);
			taskTrafficDaily.setRecv(0L);
			taskTrafficDaily.setTotal(0L);
		} else {
			taskTrafficDaily.setSend(model.getSend());
			taskTrafficDaily.setRecv(model.getReceive());
			taskTrafficDaily.setTotal(model.getTotal());
		}

		return taskTrafficDailyDao.save(taskTrafficDaily);
	}

	/**
	 * 更新报表数据
	 * 
	 * @param device
	 * @param trafficDaily
	 * @param deviceSumDaily
	 * @param homePvuvDaily
	 * @param mainPvuvDaily
	 * @param moviePvuvDaily
	 */
	private void updateDailyReport(Device device, TaskTrafficDaily trafficDaily,
			TaskDeviceSumDaily deviceSumDaily, TaskPVUVDaily homePvuvDaily,
			TaskPVUVDaily mainPvuvDaily, TaskPVUVDaily moviePvuvDaily) {

		TaskReportDaily taskReportDaily = new TaskReportDaily();
		taskReportDaily.setDeviceId(device.getId());
		taskReportDaily.setCompanyId(device.getCompanyId());
		taskReportDaily.setGroupId(device.getGroupId());

		// 流量
		taskReportDaily.setTrafficRecv(trafficDaily.getRecv());
		taskReportDaily.setTrafficSend(trafficDaily.getSend());
		taskReportDaily.setTrafficTotal(trafficDaily.getTotal());
		// 终端
		taskReportDaily.setDeviceDailyAdd(deviceSumDaily.getDeviceDailyAdd());
		taskReportDaily.setDeviceTotal(deviceSumDaily.getDeviceTotal());
		taskReportDaily.setDeviceOneHourOnline(deviceSumDaily.getDeviceOneHourOnline());
		taskReportDaily.setUserDailyAdd(deviceSumDaily.getUserDailyAdd());
		taskReportDaily.setUserTotal(deviceSumDaily.getUserTotal());

		taskReportDaily.setPvHome(homePvuvDaily == null ? 0 : homePvuvDaily.getPv());
		taskReportDaily.setUvHome(homePvuvDaily == null ? 0 : homePvuvDaily.getUv());
		taskReportDaily.setPvMain(mainPvuvDaily == null ? 0 : mainPvuvDaily.getPv());
		taskReportDaily.setUvMain(mainPvuvDaily == null ? 0 : mainPvuvDaily.getUv());
		taskReportDaily.setPvMovie(moviePvuvDaily == null ? 0 : moviePvuvDaily.getPv());
		taskReportDaily.setUvMovie(moviePvuvDaily == null ? 0 : moviePvuvDaily.getUv());

		taskReportDaily.setDate(date);
		taskReportDaily.setCreateTime(new Date());
		reportDailyDao.save(taskReportDaily);
	}
}
