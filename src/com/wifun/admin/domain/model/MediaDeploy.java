package com.wifun.admin.domain.model;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * CompanyAccount.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "media_deploy")
public class MediaDeploy implements java.io.Serializable {

	public static final int DEPLOY_TYPE_NET = 1;
	public static final int DEPLOY_TYPE_SD = 2;

	public static final String FN_NAME = "name";
	public static final String FN_TYPE = "type";
	public static final String FN_COMPANY_ID = "companyId";
	public static final String FN_PACKAGE = "packageUrl";
	public static final String FN_NOTE = "note";
	public static final String FN_DEPLOY_TIME = "deployTime";
	public static final String FN_VERSION = "version";

	// Fields
	private Integer id;
	private Integer type;
	private String description;
	private Integer companyId;
	private Integer groupId;
	private Integer mediaListId;
	private String version;
	private Date deployTime;

	private String publishPointId;
	private String publishId;
	private int status;

	private Company company;
	private Group group;
	private MediaList mediaList;

	/** default constructor */
	public MediaDeploy() {

	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {

		return this.id;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	@Column(name = "type")
	public Integer getType() {

		return type;
	}

	public void setType(Integer type) {

		this.type = type;
	}

	@Column(name = "company_id")
	public Integer getCompanyId() {

		return companyId;
	}

	public void setCompanyId(Integer companyId) {

		this.companyId = companyId;
	}

	@Column(name = "description", length = 256)
	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	@Column(name = "group_id")
	public Integer getGroupId() {

		return groupId;
	}

	public void setGroupId(Integer groupId) {

		this.groupId = groupId;
	}

	@Column(name = "media_list_id")
	public Integer getMediaListId() {

		return mediaListId;
	}

	public void setMediaListId(Integer mediaListId) {

		this.mediaListId = mediaListId;
	}

	@Column(name = "version", length = 64)
	public String getVersion() {

		return version;
	}

	public void setVersion(String version) {

		this.version = version;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "deploy_time", insertable = true, updatable = false)
	public Date getDeployTime() {

		return deployTime;
	}

	public void setDeployTime(Date deployTime) {

		this.deployTime = deployTime;
	}

	@Column(name = "publish_point_id", length = 64)
	public String getPublishPointId() {

		return publishPointId;
	}

	public void setPublishPointId(String publishPointId) {

		this.publishPointId = publishPointId;
	}

	@Column(name = "inhand_publish_id", length = 64)
	public String getPublishId() {

		return publishId;
	}

	public void setPublishId(String publishId) {

		this.publishId = publishId;
	}

	@Column(name = "inhand_status")
	public int getStatus() {

		return status;
	}

	public void setStatus(int status) {

		this.status = status;
	}

	@OneToOne
	@JoinColumn(name = "company_id", insertable = false, updatable = false)
	public Company getCompany() {

		if (null == company) {
			company = new Company();
			company.setName("ALL");
		}

		return company;
	}

	public void setCompany(Company company) {

		this.company = company;
	}

	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "group_id", insertable = false, updatable = false)
	public Group getGroup() {

		if (null != group && group.getId().equals(groupId)) {
			return group;
		}

		group = new Group();
		group.setId(groupId);
		group.setName("default");
		return group;
	}

	public void setGroup(Group group) {

		this.group = group;
	}

	@OneToOne
	@JoinColumn(name = "media_list_id", insertable = false, updatable = false)
	public MediaList getMediaList() {

		return mediaList;
	}

	public void setMediaList(MediaList mediaList) {

		this.mediaList = mediaList;
	}

}