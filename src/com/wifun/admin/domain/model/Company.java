package com.wifun.admin.domain.model;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * CompanyAccount.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "company")
public class Company implements java.io.Serializable {

	public static int ROLE_TYPE_ADMIN = 1;
	public static int ROLE_TYPE_USER = 2;

	public static final String FN_NAME = "name";
	public static final String FN_ACCOUNT = "account";
	public static final String FN_PASSWORD = "password";
	public static final String FN_CONTACT = "contact";
	public static final String FN_PHONE = "phone";
	public static final String FN_EMAIL = "email";
	public static final String FN_IS_ACTIVE = "isActive";
	public static final String FN_CREATE_DATE = "createDate";
	public static final String FN_LAST_LOGIN_TIME = "lastLoginTime";

	// Fields
	private Integer id;
	private String name;
	private String account;
	private String password;
	private String contact;
	private String phone;
	private String email;
	private boolean isActive;
	private Date createDate;
	private Date lastLoginTime;
	private String inhandId;

	private String token;
	private Date expireTime;

	private int groupNum;
	private int deviceNormalNum;
	private int deviceTotalNum;

	/** default constructor */
	public Company() {

	}

	/** full constructor */
	public Company(String name, String account, String contact, String phone, String email,
			boolean isActive) {

		this.name = name;
		this.account = account;
		this.contact = contact;
		this.phone = phone;
		this.email = email;
		this.isActive = isActive;
		this.password = "123456";
		this.createDate = new Date();
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {

		return this.id;
	}

	public void setId(Integer accountId) {

		this.id = accountId;
	}

	@Column(name = "name", length = 64)
	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	@Column(name = "account", length = 32)
	public String getAccount() {

		return account;
	}

	public void setAccount(String account) {

		this.account = account;
	}

	@Column(name = "password", length = 32)
	public String getPassword() {

		return this.password;
	}

	public void setPassword(String password) {

		this.password = password;
	}

	@Column(name = "contact", length = 32)
	public String getContact() {

		return contact;
	}

	public void setContact(String contact) {

		this.contact = contact;
	}

	@Column(name = "phone", length = 16)
	public String getPhone() {

		return phone;
	}

	public void setPhone(String phone) {

		this.phone = phone;
	}

	@Column(name = "email", length = 32)
	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	@Column(name = "is_active", length = 2)
	public boolean getIsActive() {

		return isActive;
	}

	public void setIsActive(boolean isActive) {

		this.isActive = isActive;
	}

	@Column(name = "inhand_id", length = 32)
	public String getInhandId() {

		return inhandId;
	}

	public void setInhandId(String inhandId) {

		this.inhandId = inhandId;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", updatable = false)
	public Date getCreateDate() {

		return this.createDate;
	}

	public void setCreateDate(Date createTime) {

		this.createDate = createTime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_login_time", length = 0)
	public Date getLastLoginTime() {

		return this.lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {

		this.lastLoginTime = lastLoginTime;
	}

	@Column(name = "token", length = 32)
	public String getToken() {

		return token;
	}

	public void setToken(String token) {

		this.token = token;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "expire_time", length = 0, updatable = false)
	public Date getExpireTime() {

		return expireTime;
	}

	public void setExpireTime(Date expireTime) {

		this.expireTime = expireTime;
	}

	@Transient
	public int getGroupNum() {

		return groupNum;
	}

	public void setGroupNum(int groupNum) {

		this.groupNum = groupNum;
	}

	@Transient
	public int getDeviceNormalNum() {

		return deviceNormalNum;
	}

	public void setDeviceNormalNum(int deviceNormalNum) {

		this.deviceNormalNum = deviceNormalNum;
	}

	@Transient
	public int getDeviceTotalNum() {

		return deviceTotalNum;
	}

	public void setDeviceTotalNum(int deviceTotalNum) {

		this.deviceTotalNum = deviceTotalNum;
	}
}