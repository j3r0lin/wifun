package com.wifun.admin.domain.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author chw
 * 
 *         定时任务，每个device的流量日报 数据
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "task_device_sum_daily")
public class TaskDeviceSumDaily implements java.io.Serializable {

	private int id;
	private int deviceId;
	private int companyId;
	// private int dailyAdd;
	// private int total;
	// private int oneHourOnline;

	// 终端
	private int deviceDailyAdd;
	private int deviceTotal;
	private int deviceOneHourOnline;

	// 用户
	private int userDailyAdd;
	private int userTotal;

	private String date;
	private Date createTime;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {

		return id;
	}

	public void setId(int id) {

		this.id = id;
	}

	@Column(name = "device_id")
	public int getDeviceId() {

		return deviceId;
	}

	public void setDeviceId(int deviceId) {

		this.deviceId = deviceId;
	}

	@Column(name = "company_id")
	public int getCompanyId() {

		return companyId;
	}

	public void setCompanyId(int companyId) {

		this.companyId = companyId;
	}

	@Column(name = "device_daily_add")
	public int getDeviceDailyAdd() {

		return deviceDailyAdd;
	}

	public void setDeviceDailyAdd(int deviceDailyAdd) {

		this.deviceDailyAdd = deviceDailyAdd;
	}

	@Column(name = "device_total")
	public int getDeviceTotal() {

		return deviceTotal;
	}

	public void setDeviceTotal(int deviceTotal) {

		this.deviceTotal = deviceTotal;
	}

	@Column(name = "device_one_hour_online")
	public int getDeviceOneHourOnline() {

		return deviceOneHourOnline;
	}

	public void setDeviceOneHourOnline(int deviceOneHourOnline) {

		this.deviceOneHourOnline = deviceOneHourOnline;
	}

	@Column(name = "user_daily_add")
	public int getUserDailyAdd() {

		return userDailyAdd;
	}

	public void setUserDailyAdd(int userDailyAdd) {

		this.userDailyAdd = userDailyAdd;
	}

	@Column(name = "user_total")
	public int getUserTotal() {

		return userTotal;
	}

	public void setUserTotal(int userTotal) {

		this.userTotal = userTotal;
	}

	@Column(name = "date")
	public String getDate() {

		return date;
	}

	public void setDate(String date) {

		this.date = date;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time", insertable = true, updatable = false)
	public Date getCreateTime() {

		return createTime;
	}

	public void setCreateTime(Date createTime) {

		this.createTime = createTime;
	}
}
