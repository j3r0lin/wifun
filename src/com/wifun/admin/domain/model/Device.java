package com.wifun.admin.domain.model;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * CompanyAccount.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "device")
public class Device implements java.io.Serializable {

	public static final String FN_SN = "sn";
	public static final String FN_NAME = "name";
	public static final String FN_COMPANY_ID = "companyId";
	public static final String FN_GROUP_ID = "groupId";
	public static final String FN_WORK_STATUS = "workStatus";
	public static final String FN_CREATE_DATE = "createDate";
	public static final String FN_IS_ACTIVE = "isActive";

	// Fields
	private Integer id;
	private String sn;
	private String name;
	private String model;
	private String hardwareVersion;
	private Integer companyId;
	private Integer groupId;
	private String workStatus;
	private Date startDate;
	private Date updateDate;
	private String carrier;
	private String imsi;
	private String imei;
	private String phoneNumber;
	private String busInfo;
	private Date createDate;

	private String inhandId;
	private String inhandSiteId;

	private boolean isActive;

	private String companyName;
	private String groupName;

	/** default constructor */
	public Device() {

	}

	public void updateInfo(Device vo) {

		this.name = vo.getName();
		this.model = vo.getModel();
		// this.hardwareVersion = vo.getHardwareVersion();
		// this.companyId = vo.getCompanyId();
		this.groupId = vo.getGroupId();
		this.workStatus = vo.getWorkStatus();
		this.startDate = vo.startDate;
		this.carrier = vo.getCarrier();
		this.imei = vo.getImei();
		this.phoneNumber = vo.getPhoneNumber();
		// this.imsi = vo.getImsi();
		this.busInfo = vo.getBusInfo();
		this.updateDate = new Date();
	}

	public void updateInfoCompany(Device vo) {

		this.name = vo.getName();
		this.groupId = vo.getGroupId();
		this.workStatus = vo.getWorkStatus();
		this.busInfo = vo.getBusInfo();
		this.updateDate = new Date();
	}

	/** full constructor */
	public Device(String name, Integer companyId, String status, Date createDate) {

	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {

		return this.id;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	@Column(name = "company_id")
	public Integer getCompanyId() {

		return companyId;
	}

	public void setCompanyId(Integer companyId) {

		this.companyId = companyId;
	}

	@Column(name = "sn", length = 32, insertable = true, updatable = true)
	public String getSn() {

		return sn;
	}

	public void setSn(String sn) {

		this.sn = sn;
	}

	@Column(name = "name", length = 64)
	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	@Column(name = "model", length = 32, insertable = true, updatable = false)
	public String getModel() {

		return model;
	}

	public void setModel(String model) {

		this.model = model;
	}

	@Column(name = "hardware_version", length = 16)
	public String getHardwareVersion() {

		return hardwareVersion;
	}

	public void setHardwareVersion(String hardwareVersion) {

		this.hardwareVersion = hardwareVersion;
	}

	@Column(name = "group_id")
	public Integer getGroupId() {

		return groupId;
	}

	public void setGroupId(Integer groupId) {

		this.groupId = groupId;
	}

	@Column(name = "work_status", length = 2)
	public String getWorkStatus() {

		return workStatus;
	}

	public void setWorkStatus(String workStatus) {

		this.workStatus = workStatus;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "start_date")
	public Date getStartDate() {

		return startDate;
	}

	public void setStartDate(Date startDate) {

		this.startDate = startDate;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "update_date")
	public Date getUpdateDate() {

		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {

		this.updateDate = updateDate;
	}

	@Column(name = "carrier", length = 32)
	public String getCarrier() {

		return carrier;
	}

	public void setCarrier(String carrier) {

		this.carrier = carrier;
	}

	@Column(name = "imsi", length = 32)
	public String getImsi() {

		return imsi;
	}

	public void setImsi(String imsi) {

		this.imsi = imsi;
	}

	@Column(name = "imei", length = 32)
	public String getImei() {

		return imei;
	}

	public void setImei(String imei) {

		this.imei = imei;
	}

	@Column(name = "phone_number", length = 32)
	public String getPhoneNumber() {

		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {

		this.phoneNumber = phoneNumber;
	}

	@Column(name = "bus_info", length = 256)
	public String getBusInfo() {

		return busInfo;
	}

	public void setBusInfo(String busInfo) {

		this.busInfo = busInfo;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", insertable = true, updatable = false)
	public Date getCreateDate() {

		return this.createDate;
	}

	public void setCreateDate(Date createTime) {

		this.createDate = createTime;
	}

	@Transient
	public String getCompanyName() {

		return companyName;
	}

	public void setCompanyName(String companyName) {

		this.companyName = companyName;
	}

	@Transient
	public String getGroupName() {

		return groupName;
	}

	public void setGroupName(String groupName) {

		this.groupName = groupName;
	}

	@Column(name = "inhand_id", insertable = true, updatable = false)
	public String getInhandId() {

		return inhandId;
	}

	public void setInhandId(String inhandId) {

		this.inhandId = inhandId;
	}

	@Column(name = "inhand_site_id", insertable = true, updatable = false)
	public String getInhandSiteId() {

		return inhandSiteId;
	}

	public void setInhandSiteId(String inhandSiteId) {

		this.inhandSiteId = inhandSiteId;
	}

	@Column(name = "is_active")
	public boolean getIsActive() {

		return isActive;
	}

	public void setIsActive(boolean isActive) {

		this.isActive = isActive;
	}

}