package com.wifun.admin.domain.model;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * CompanyAccount.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "category")
public class Category implements java.io.Serializable {

	public static final String FN_TITLE = "title";
	public static final String FN_STATUS = "status";

	// Fields
	private Integer id;
	private String title;
	private String titleCn;
	private String status;
	private String check;

	/** default constructor */
	public Category() {

	}

	/** full constructor */
	public Category(String title, String titleCn, String status) {

		this.title = title;
		this.titleCn = titleCn;
		this.status = status;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {

		return this.id;
	}

	public void setId(Integer accountId) {

		this.id = accountId;
	}

	@Column(name = "title", length = 32)
	public String getTitle() {

		return title;
	}

	public void setTitle(String title) {

		this.title = title;
	}

	@Column(name = "title_cn", length = 32)
	public String getTitleCn() {

		return titleCn;
	}

	public void setTitleCn(String titleCn) {

		this.titleCn = titleCn;
	}

	@Column(name = "status")
	public String getStatus() {

		return status;
	}

	public void setStatus(String status) {

		this.status = status;
	}

	@Transient
	public String getCheck() {

		return check;
	}

	public void setCheck(String check) {

		this.check = check;
	}
}