package com.wifun.admin.domain.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author chw
 * 
 *         定时任务，所有统计 数据 统计每日新增终端、累计终端、停留时间超过1小时的终端数、
 *         WiFi流量大于200MB的终端数(记明细，出报表时过滤)、首页PVUV、电影播放页PVUV
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "task_report_daily")
public class TaskReportDaily implements java.io.Serializable {

	public static final String FN_COMPANY_ID = "companyId";
	public static final String FN_DATE = "date";
	public static final String FN_GROUP_ID = "groupId";
	public static final String FN_DEVICE_ID = "deviceId";

	private int id;
	private int deviceId;
	private int groupId;
	private int companyId;

	// pvuv
	private int pvMain;
	private int uvMain;
	private int pvMovie;
	private int uvMovie;
	private int pvHome;
	private int uvHome;

	// 终端
	private int deviceDailyAdd;
	private int deviceTotal;
	private int deviceOneHourOnline;

	// 用户
	private int userDailyAdd;
	private int userTotal;

	// 流量
	private long trafficSend;
	private long trafficRecv;
	private long trafficTotal;

	private String date;
	private Date createTime;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {

		return id;
	}

	public void setId(int id) {

		this.id = id;
	}

	@Column(name = "device_id")
	public int getDeviceId() {

		return deviceId;
	}

	public void setDeviceId(int deviceId) {

		this.deviceId = deviceId;
	}

	@Column(name = "group_id")
	public int getGroupId() {

		return groupId;
	}

	public void setGroupId(int groupId) {

		this.groupId = groupId;
	}

	@Column(name = "company_id")
	public int getCompanyId() {

		return companyId;
	}

	public void setCompanyId(int companyId) {

		this.companyId = companyId;
	}

	@Column(name = "pv_main")
	public int getPvMain() {

		return pvMain;
	}

	public void setPvMain(int pvMain) {

		this.pvMain = pvMain;
	}

	@Column(name = "uv_main")
	public int getUvMain() {

		return uvMain;
	}

	public void setUvMain(int uvMain) {

		this.uvMain = uvMain;
	}

	@Column(name = "pv_movie")
	public int getPvMovie() {

		return pvMovie;
	}

	public void setPvMovie(int pvMovie) {

		this.pvMovie = pvMovie;
	}

	@Column(name = "uv_movie")
	public int getUvMovie() {

		return uvMovie;
	}

	public void setUvMovie(int uvMovie) {

		this.uvMovie = uvMovie;
	}

	@Column(name = "pv_home")
	public int getPvHome() {

		return pvHome;
	}

	public void setPvHome(int pvHome) {

		this.pvHome = pvHome;
	}

	@Column(name = "uv_home")
	public int getUvHome() {

		return uvHome;
	}

	public void setUvHome(int uvHome) {

		this.uvHome = uvHome;
	}

	@Column(name = "device_daily_add")
	public int getDeviceDailyAdd() {

		return deviceDailyAdd;
	}

	public void setDeviceDailyAdd(int deviceDailyAdd) {

		this.deviceDailyAdd = deviceDailyAdd;
	}

	@Column(name = "device_total")
	public int getDeviceTotal() {

		return deviceTotal;
	}

	public void setDeviceTotal(int deviceTotal) {

		this.deviceTotal = deviceTotal;
	}

	@Column(name = "device_one_hour_online")
	public int getDeviceOneHourOnline() {

		return deviceOneHourOnline;
	}

	public void setDeviceOneHourOnline(int deviceOneHourOnline) {

		this.deviceOneHourOnline = deviceOneHourOnline;
	}

	@Column(name = "user_daily_add")
	public int getUserDailyAdd() {

		return userDailyAdd;
	}

	public void setUserDailyAdd(int userDailyAdd) {

		this.userDailyAdd = userDailyAdd;
	}

	@Column(name = "user_total")
	public int getUserTotal() {

		return userTotal;
	}

	public void setUserTotal(int userTotal) {

		this.userTotal = userTotal;
	}

	@Column(name = "traffic_send")
	public long getTrafficSend() {

		return trafficSend;
	}

	public void setTrafficSend(long trafficSend) {

		this.trafficSend = trafficSend;
	}

	@Column(name = "traffic_recv")
	public long getTrafficRecv() {

		return trafficRecv;
	}

	public void setTrafficRecv(long trafficRecv) {

		this.trafficRecv = trafficRecv;
	}

	@Column(name = "traffic_total")
	public long getTrafficTotal() {

		return trafficTotal;
	}

	public void setTrafficTotal(long trafficTotal) {

		this.trafficTotal = trafficTotal;
	}

	@Column(name = "date")
	public String getDate() {

		return date;
	}

	public void setDate(String date) {

		this.date = date;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time", insertable = true, updatable = false)
	public Date getCreateTime() {

		return createTime;
	}

	public void setCreateTime(Date createTime) {

		this.createTime = createTime;
	}
}
