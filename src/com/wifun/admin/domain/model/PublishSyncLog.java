package com.wifun.admin.domain.model;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import com.wifun.inhand.model.PublishState;

/**
 * CompanyAccount.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "publish_sync_log")
public class PublishSyncLog implements java.io.Serializable {

	// Fields
	private Integer id;
	private String deviceId;
	private Integer state;
	private Integer createTime;
	private Integer startTime;
	private String flag;

	/** default constructor */
	public PublishSyncLog() {

	}

	public PublishSyncLog(PublishState ps, String flag) {

		this.deviceId = ps.getDeviceId();
		this.state = ps.getState();
		this.createTime = ps.getCreateTime();
		this.startTime = ps.getSyncStartTime();
		this.flag = flag;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {

		return this.id;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	@Column(name = "device_inhand_id")
	public String getDeviceId() {

		return deviceId;
	}

	public void setDeviceId(String deviceId) {

		this.deviceId = deviceId;
	}

	@Column(name = "state")
	public Integer getState() {

		return state;
	}

	public void setState(Integer state) {

		this.state = state;
	}

	@Column(name = "create_time")
	public Integer getCreateTime() {

		return createTime;
	}

	public void setCreateTime(Integer createTime) {

		this.createTime = createTime;
	}

	@Column(name = "start_time")
	public Integer getStartTime() {

		return startTime;
	}

	public void setStartTime(Integer startTime) {

		this.startTime = startTime;
	}

	@Column(name = "flag")
	public String getFlag() {

		return flag;
	}

	public void setFlag(String flag) {

		this.flag = flag;
	}

}