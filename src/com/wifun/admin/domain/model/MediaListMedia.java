package com.wifun.admin.domain.model;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * CompanyAccount.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "media_list_media")
public class MediaListMedia implements java.io.Serializable {

	public static final String FN_MEDIA_LIST_ID = "medialistId";
	public static final String FN_TITLE = "title";
	public static final String FN_STATUS = "status";

	// Fields
	private Integer id;
	private Integer medialistId;
	private Integer mediaId;

	private Media media;

	/** default constructor */
	public MediaListMedia() {

	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {

		return this.id;
	}

	public void setId(Integer accountId) {

		this.id = accountId;
	}

	@Column(name = "media_id")
	public Integer getMediaId() {

		return mediaId;
	}

	public void setMediaId(Integer mediaId) {

		this.mediaId = mediaId;
	}

	@Column(name = "media_list_id")
	public Integer getMedialistId() {

		return medialistId;
	}

	public void setMedialistId(Integer medialistId) {

		this.medialistId = medialistId;
	}

	@OneToOne
	@JoinColumn(name = "media_id", insertable = false, updatable = false)
	public Media getMedia() {

		return media;
	}

	public void setMedia(Media media) {

		this.media = media;
	}
}