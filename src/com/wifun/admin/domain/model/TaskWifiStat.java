package com.wifun.admin.domain.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@SuppressWarnings("serial")
@Entity
@Table(name = "task_wifi_stat")
public class TaskWifiStat implements java.io.Serializable {

	public static final String FN_DATE = "date";
	public static final String FN_TYPE = "type";
	public static final String FN_DEVICE_ID = "deviceId";
	
	private int id;
	private int deviceId;
	private int companyId;
	private String type;
	private int value;
	private String date;
	private Date createTime;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {

		return id;
	}

	public void setId(int id) {

		this.id = id;
	}

	@Column(name = "device_id")
	public int getDeviceId() {

		return deviceId;
	}

	public void setDeviceId(int deviceId) {

		this.deviceId = deviceId;
	}

	@Column(name = "company_id")
	public int getCompanyId() {

		return companyId;
	}

	public void setCompanyId(int companyId) {

		this.companyId = companyId;
	}

	@Column(name = "type")
	public String getType() {

		return type;
	}

	public void setType(String type) {

		this.type = type;
	}

	@Column(name = "value")
	public int getValue() {

		return value;
	}

	public void setValue(int value) {

		this.value = value;
	}

	@Column(name = "date")
	public String getDate() {

		return date;
	}

	public void setDate(String date) {

		this.date = date;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time", insertable = true, updatable = false)
	public Date getCreateTime() {

		return createTime;
	}

	public void setCreateTime(Date createTime) {

		this.createTime = createTime;
	}
}
