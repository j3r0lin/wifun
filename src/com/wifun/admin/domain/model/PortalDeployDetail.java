package com.wifun.admin.domain.model;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * CompanyAccount.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "portal_deploy_detail")
public class PortalDeployDetail implements java.io.Serializable {

	public static final String FN_NAME = "name";

	// Fields
	private Integer id;
	private Integer deployId;
	private Integer deviceId;
	private String status;
	private Date deployTime;

	private String sn;
	private String deviceName;
	private String portalName;

	/** default constructor */
	public PortalDeployDetail() {

	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {

		return this.id;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	@Column(name = "deploy_id")
	public Integer getDeployId() {

		return deployId;
	}

	public void setDeployId(Integer deployId) {

		this.deployId = deployId;
	}

	@Column(name = "device_id")
	public Integer getDeviceId() {

		return deviceId;
	}

	public void setDeviceId(Integer deviceId) {

		this.deviceId = deviceId;
	}

	@Column(name = "status")
	public String getStatus() {

		return status;
	}

	public void setStatus(String status) {

		this.status = status;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "deploy_time", insertable = true, updatable = false)
	public Date getDeployTime() {

		return deployTime;
	}

	public void setDeployTime(Date deployTime) {

		this.deployTime = deployTime;
	}

	@Column(name = "sn")
	public String getSn() {

		return sn;
	}

	public void setSn(String sn) {

		this.sn = sn;
	}

	@Column(name = "device_name")
	public String getDeviceName() {

		return deviceName;
	}

	public void setDeviceName(String deviceName) {

		this.deviceName = deviceName;
	}

	@Column(name = "portal_name")
	public String getPortalName() {

		return portalName;
	}

	public void setPortalName(String portalName) {

		this.portalName = portalName;
	}
}