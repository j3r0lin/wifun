package com.wifun.admin.domain.model;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * CompanyAccount.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "portal_deploy")
public class PortalDeploy implements java.io.Serializable {

	public static final String FN_COMPANY_ID = "companyId";
	public static final String FN_DESCRIPTION = "description";
	public static final String FN_DEPLOYTIME = "deployTime";

	// Fields
	private Integer id;
	private String description;
	private Integer companyId;
	private Integer groupId;
	private Integer portalId;
	private String portalName;
	private Date deployTime;
	private int status;
	private String publishPointId;
	private String publishId;

	private Company company;
	private Group group;

	/** default constructor */
	public PortalDeploy() {

	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {

		return this.id;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	@Column(name = "company_id")
	public Integer getCompanyId() {

		return companyId;
	}

	public void setCompanyId(Integer companyId) {

		this.companyId = companyId;
	}

	@Column(name = "description", length = 128)
	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	@Column(name = "group_id")
	public Integer getGroupId() {

		return groupId;
	}

	public void setGroupId(Integer groupId) {

		this.groupId = groupId;
	}

	@Column(name = "portal_id")
	public Integer getPortalId() {

		return portalId;
	}

	public void setPortalId(Integer portalId) {

		this.portalId = portalId;
	}

	@Column(name = "portal_name")
	public String getPortalName() {

		return portalName;
	}

	public void setPortalName(String portalName) {

		this.portalName = portalName;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "deploy_time", insertable = true, updatable = false)
	public Date getDeployTime() {

		return deployTime;
	}

	public void setDeployTime(Date deployTime) {

		this.deployTime = deployTime;
	}

	@Column(name = "publish_point_id", length = 64)
	public String getPublishPointId() {

		return publishPointId;
	}

	@Column(name = "inhand_status", length = 2)
	public int getStatus() {

		return status;
	}

	public void setStatus(int status) {

		this.status = status;
	}

	public void setPublishPointId(String publishPointId) {

		this.publishPointId = publishPointId;
	}

	@Column(name = "inhand_publish_id", length = 64)
	public String getPublishId() {

		return publishId;
	}

	public void setPublishId(String publishId) {

		this.publishId = publishId;
	}

	@ManyToOne
	@JoinColumn(name = "company_id", insertable = false, updatable = false)
	public Company getCompany() {

		if (null == company) {
			company = new Company();
			company.setName("ALL");
		}

		return company;
	}

	public void setCompany(Company company) {

		this.company = company;
	}

	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "group_id", insertable = false, updatable = false)
	public Group getGroup() {

		if (null != group && group.getId().equals(groupId)) {
			return group;
		}

		group = new Group();
		group.setId(groupId);
		group.setName("default");
		return group;
	}

	public void setGroup(Group group) {

		this.group = group;
	}

}