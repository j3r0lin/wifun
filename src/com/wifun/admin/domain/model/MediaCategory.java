package com.wifun.admin.domain.model;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 * CompanyAccount.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "media_category")
public class MediaCategory implements java.io.Serializable {

	public static final String FN_ID = "id";
	public static final String FN_CATEGORY_ID = "categoryId";
	public static final String FN_MEDIA_ID = "mediaId";

	// Fields
	private Integer id;
	private Integer categoryId;
	private Integer mediaId;

	private Category category;
	private Media media;

	/** default constructor */
	public MediaCategory() {

	}

	public MediaCategory(Integer mediaId, Integer catId) {

		this.mediaId = mediaId;
		this.categoryId = catId;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {

		return this.id;
	}

	public void setId(Integer accountId) {

		this.id = accountId;
	}

	@Column(name = "category_id")
	public Integer getCategoryId() {

		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {

		this.categoryId = categoryId;
	}

	@Column(name = "media_id")
	public Integer getMediaId() {

		return mediaId;
	}

	public void setMediaId(Integer mediaId) {

		this.mediaId = mediaId;
	}

	@OneToOne
	@JoinColumn(name = "category_id", insertable = false, updatable = false)
	public Category getCategory() {

		return category;
	}

	public void setCategory(Category category) {

		this.category = category;
	}

	@ManyToOne
	@JoinColumn(name = "media_id", insertable = false, updatable = false)
	public Media getMedia() {

		return media;
	}

	public void setMedia(Media media) {

		this.media = media;
	}

}