package com.wifun.admin.domain.model;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * CompanyAccount.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "portal")
public class Portal implements java.io.Serializable {

	public static final String FN_NAME = "name";
	public static final String FN_COMPANY_ID = "companyId";
	public static final String FN_PACKAGE = "packageUrl";
	public static final String FN_NOTE = "note";
	public static final String FN_IS_ACTIVE = "isActive";

	// Fields
	private Integer id;
	private String name;
	private String description;
	private Integer companyId;
	private String packageUrl;
	private Date createTime;
	private boolean isActive;

	private Company company;

	/** default constructor */
	public Portal() {

	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {

		return this.id;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	@Column(name = "company_id")
	public Integer getCompanyId() {

		return companyId;
	}

	public void setCompanyId(Integer companyId) {

		this.companyId = companyId;
	}

	@Column(name = "name", length = 64)
	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	@Column(name = "description")
	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	@Column(name = "package", length = 128)
	public String getPackageUrl() {

		return packageUrl;
	}

	public void setPackageUrl(String packageUrl) {

		this.packageUrl = packageUrl;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time", insertable = true, updatable = false)
	public Date getCreateTime() {

		return createTime;
	}

	public void setCreateTime(Date createTime) {

		this.createTime = createTime;
	}

	@Column(name = "is_active")
	public boolean getIsActive() {

		return isActive;
	}

	public void setIsActive(boolean isActive) {

		this.isActive = isActive;
	}

	@OneToOne
	@JoinColumn(name = "company_id", insertable = false, updatable = false)
	public Company getCompany() {

		if (null == company) {
			company = new Company();
			company.setName("ALL");
		}

		return company;
	}

	public void setCompany(Company company) {

		this.company = company;
	}

}