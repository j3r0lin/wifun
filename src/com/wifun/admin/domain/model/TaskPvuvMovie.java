package com.wifun.admin.domain.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author chw
 * 
 *         每个公司，每个电影每天的pvuv总计
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "task_pvuv_movie")
public class TaskPvuvMovie implements java.io.Serializable {

	public static final String FN_COMPANY_ID = "companyId";
	public static final String FN_TOTAL_PV = "totalPv";
	public static final String FN_DATE = "date";

	private int id;
	private int movieId;
	private String movieTitle;
	private int companyId;

	// pvuv

	private int totalPv;
	private int totalUv;
	private int currentPv;
	private int currentUv;

	private String date;
	private Date createTime;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {

		return id;
	}

	public void setId(int id) {

		this.id = id;
	}

	@Column(name = "company_id")
	public int getCompanyId() {

		return companyId;
	}

	public void setCompanyId(int companyId) {

		this.companyId = companyId;
	}

	@Column(name = "movie_id")
	public int getMovieId() {

		return movieId;
	}

	public void setMovieId(int movieId) {

		this.movieId = movieId;
	}

	@Column(name = "movie_title")
	public String getMovieTitle() {

		return movieTitle;
	}

	public void setMovieTitle(String movieTitle) {

		this.movieTitle = movieTitle;
	}

	@Column(name = "total_pv")
	public int getTotalPv() {

		return totalPv;
	}

	public void setTotalPv(int totalPv) {

		this.totalPv = totalPv;
	}

	@Column(name = "total_uv")
	public int getTotalUv() {

		return totalUv;
	}

	public void setTotalUv(int totalUv) {

		this.totalUv = totalUv;
	}

	@Column(name = "current_pv")
	public int getCurrentPv() {

		return currentPv;
	}

	public void setCurrentPv(int currentPv) {

		this.currentPv = currentPv;
	}

	@Column(name = "current_uv")
	public int getCurrentUv() {

		return currentUv;
	}

	public void setCurrentUv(int currentUv) {

		this.currentUv = currentUv;
	}

	@Column(name = "date")
	public String getDate() {

		return date;
	}

	public void setDate(String date) {

		this.date = date;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time", insertable = true, updatable = false)
	public Date getCreateTime() {

		return createTime;
	}

	public void setCreateTime(Date createTime) {

		this.createTime = createTime;
	}
}
