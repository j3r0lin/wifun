package com.wifun.admin.domain.model;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * CompanyAccount.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "media_deploy_detail")
public class MediaDeployDetail implements java.io.Serializable {

	// 设备状态 -1:已存在 0:等待下载 1:下载成功 2:下载失败
	public static final int FN_DEVICE_STATUS_EXIST = -1;
	public static final int FN_DEVICE_STATUS_WAIT = 0;
	public static final int FN_DEVICE_STATUS_SUCCESS = 1;
	public static final int FN_DEVICE_STATUS_FAIL = 2;

	public static final String FN_NAME = "name";
	public static final String FN_DEVICEID = "deviceId";
	public static final String FN_MEDIAID = "mediaId";
	public static final String FN_DEPLOY_ID = "deployId";
	public static final String FN_MEDIA_ID = "mediaId";
	public static final String FN_DEVICE_STATUS = "deviceStatus";

	// Fields
	private Integer id;
	private Integer deployId;
	private Integer deviceId;
	private Integer mediaId;
	private String status;
	private Date deployTime;

	private String sn;
	private String deviceName;
	private String mediaTitle;
	private String mediaTitleCn;
	private String mediaVersion;

	private int deviceStatus;

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {

		return this.id;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	@Column(name = "deploy_id")
	public Integer getDeployId() {

		return deployId;
	}

	public void setDeployId(Integer deployId) {

		this.deployId = deployId;
	}

	@Column(name = "device_id")
	public Integer getDeviceId() {

		return deviceId;
	}

	public void setDeviceId(Integer deviceId) {

		this.deviceId = deviceId;
	}

	@Column(name = "status")
	public String getStatus() {

		return status;
	}

	public void setStatus(String status) {

		this.status = status;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "deploy_time", insertable = true, updatable = false)
	public Date getDeployTime() {

		return deployTime;
	}

	public void setDeployTime(Date deployTime) {

		this.deployTime = deployTime;
	}

	@Column(name = "media_id")
	public Integer getMediaId() {

		return mediaId;
	}

	public void setMediaId(Integer mediaId) {

		this.mediaId = mediaId;
	}

	@Column(name = "sn")
	public String getSn() {

		return sn;
	}

	public void setSn(String sn) {

		this.sn = sn;
	}

	@Column(name = "device_name")
	public String getDeviceName() {

		return deviceName;
	}

	public void setDeviceName(String deviceName) {

		this.deviceName = deviceName;
	}

	@Column(name = "media_title")
	public String getMediaTitle() {

		return mediaTitle;
	}

	public void setMediaTitle(String mediaTitle) {

		this.mediaTitle = mediaTitle;
	}

	@Column(name = "media_title_cn")
	public String getMediaTitleCn() {

		return mediaTitleCn;
	}

	public void setMediaTitleCn(String mediaTitleCn) {

		this.mediaTitleCn = mediaTitleCn;
	}

	@Column(name = "media_version")
	public String getMediaVersion() {

		return mediaVersion;
	}

	public void setMediaVersion(String mediaVersion) {

		this.mediaVersion = mediaVersion;
	}

	@Column(name = "device_status")
	public int getDeviceStatus() {

		return deviceStatus;
	}

	public void setDeviceStatus(int deviceStatus) {

		this.deviceStatus = deviceStatus;
	}
}