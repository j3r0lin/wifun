package com.wifun.admin.domain.model;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.hibernate.annotations.NotFound;
import org.hibernate.annotations.NotFoundAction;

/**
 * CompanyAccount.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "media_list")
public class MediaList implements java.io.Serializable {

	public static final int STATUS_VALID = 1;// 有效
	public static final int STATUS_INVALID = 2;// 无效

	public static final String FN_CODE = "code";
	public static final String FN_TITLE = "title";
	public static final String FN_STATUS = "status";
	public static final String FN_COMPANY_ID = "companyId";

	// Fields
	private Integer id;
	private String title;
	private Integer status;
	private Integer creator;
	private Date createDate;
	private Integer companyId;

	private Company company;

	private List<MediaListMedia> medialist = new ArrayList<MediaListMedia>();
	private int listSize;

	/** default constructor */
	public MediaList() {

	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {

		return this.id;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	@Column(name = "title", length = 64)
	public String getTitle() {

		return title;
	}

	public void setTitle(String title) {

		this.title = title;
	}

	@Column(name = "status", length = 2)
	public Integer getStatus() {

		return status;
	}

	public void setStatus(Integer status) {

		this.status = status;
	}

	@Column(name = "creator", insertable = true, updatable = false)
	public Integer getCreator() {

		return creator;
	}

	public void setCreator(Integer creator) {

		this.creator = creator;
	}

	@Column(name = "company_id")
	public Integer getCompanyId() {

		return companyId;
	}

	public void setCompanyId(Integer companyId) {

		this.companyId = companyId;
	}

	@ManyToOne(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
	@NotFound(action = NotFoundAction.IGNORE)
	@JoinColumn(name = "company_id", nullable = true, insertable = false, updatable = false)
	public Company getCompany() {

		if (company != null && company.getId().equals(companyId))
			return company;

		company = new Company();
		company.setId(companyId);
		return company;
	}

	public void setCompany(Company company) {

		this.company = company;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", insertable = true, updatable = false)
	public Date getCreateDate() {

		return createDate;
	}

	public void setCreateDate(Date createDate) {

		this.createDate = createDate;
	}

	public void setMedialist(List<MediaListMedia> medialist) {

		this.medialist = medialist;
		this.listSize = medialist.size();
	}

	@Transient
	public int getListSize() {

		if (null == medialist)
			return 0;
		return medialist.size();
	}
}