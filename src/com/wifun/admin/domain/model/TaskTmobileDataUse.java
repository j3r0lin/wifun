package com.wifun.admin.domain.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author chw
 * 
 *         每个电话号码，tmobile流量usage情况
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "task_tmobile_data_usage")
public class TaskTmobileDataUse implements java.io.Serializable {

	// public static final String FN_COMPANY_ID = "companyId";
	// public static final String FN_TOTAL_PV = "totalPv";
	// public static final String FN_DATE = "date";

	private int id;
	private String phoneNum;// 电话号码
	private String billCycle;// 账单周期
	private String daysRemainingInCycle;// 当前计费周期剩余天数，也就是还剩多少天到下一个bill 周期

	private String currentUsageTime;// 当前数据统计的日期

	private String dataUsageAll;// 总流量

	private String usedInclude;// 用量 (servicetype=''included data''')
	private String includedInclude;
	private String remainingInclude;
	private String chargesInclude;// 费用

	private String usedReduced;// 用量 (servicetype=''reduced-speed data''')
	private String includedReduced;
	private String remainingReduced;
	private String chargesReduced;// 费用

	private Date createTime;
	private Date dataTime;// tmobile返回的数据时间，取自current_usage_time字段，方便查询

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {

		return id;
	}

	public void setId(int id) {

		this.id = id;
	}

	@Column(name = "phone_num")
	public String getPhoneNum() {

		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {

		this.phoneNum = phoneNum;
	}

	@Column(name = "bill_cycle")
	public String getBillCycle() {

		return billCycle;
	}

	@Column(name = "days_remaining_in_cycle")
	public String getDaysRemainingInCycle() {

		return daysRemainingInCycle;
	}

	public void setDaysRemainingInCycle(String daysRemainingInCycle) {

		this.daysRemainingInCycle = daysRemainingInCycle;
	}

	public void setBillCycle(String billCycle) {

		this.billCycle = billCycle;
	}

	@Column(name = "current_usage_time")
	public String getCurrentUsageTime() {

		return currentUsageTime;
	}

	public void setCurrentUsageTime(String currentUsageTime) {

		this.currentUsageTime = currentUsageTime;
	}

	@Column(name = "data_usage_all")
	public String getDataUsageAll() {

		return dataUsageAll;
	}

	public void setDataUsageAll(String dataUsageAll) {

		this.dataUsageAll = dataUsageAll;
	}

	@Column(name = "used_include")
	public String getUsedInclude() {

		return usedInclude;
	}

	public void setUsedInclude(String usedInclude) {

		this.usedInclude = usedInclude;
	}

	@Column(name = "included_include")
	public String getIncludedInclude() {

		return includedInclude;
	}

	public void setIncludedInclude(String includedInclude) {

		this.includedInclude = includedInclude;
	}

	@Column(name = "remaining_include")
	public String getRemainingInclude() {

		return remainingInclude;
	}

	public void setRemainingInclude(String remainingInclude) {

		this.remainingInclude = remainingInclude;
	}

	@Column(name = "charges_include")
	public String getChargesInclude() {

		return chargesInclude;
	}

	public void setChargesInclude(String chargesInclude) {

		this.chargesInclude = chargesInclude;
	}

	@Column(name = "used_reduced")
	public String getUsedReduced() {

		return usedReduced;
	}

	public void setUsedReduced(String usedReduced) {

		this.usedReduced = usedReduced;
	}

	@Column(name = "included_reduced")
	public String getIncludedReduced() {

		return includedReduced;
	}

	public void setIncludedReduced(String includedReduced) {

		this.includedReduced = includedReduced;
	}

	@Column(name = "remaining_reduced")
	public String getRemainingReduced() {

		return remainingReduced;
	}

	public void setRemainingReduced(String remainingReduced) {

		this.remainingReduced = remainingReduced;
	}

	@Column(name = "charges_reduced")
	public String getChargesReduced() {

		return chargesReduced;
	}

	public void setChargesReduced(String chargesReduced) {

		this.chargesReduced = chargesReduced;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time", insertable = true, updatable = false)
	public Date getCreateTime() {

		return createTime;
	}

	public void setCreateTime(Date createTime) {

		this.createTime = createTime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "data_time", insertable = true, updatable = false)
	public Date getDataTime() {

		return dataTime;
	}

	public void setDataTime(Date dataTime) {

		this.dataTime = dataTime;
	}

}
