package com.wifun.admin.domain.model;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

/**
 * CompanyAccount.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "`group`")
public class Group implements java.io.Serializable {

	public static final String FN_NAME = "name";
	public static final String FN_COMPANY_ID = "companyId";
	public static final String FN_STATUS = "status";
	public static final String FN_CREATE_DATE = "createDate";

	// Fields
	private Integer id;
	private String name;
	private Integer companyId;
	private String status;
	private Date createDate;
	private String inhandId;

	private Company company;

	private int deviceNormalNum;
	private int deviceTotalNum;

	/** default constructor */
	public Group() {

	}

	/** full constructor */
	public Group(String name, Integer companyId, String status, Date createDate) {

		this.name = name;
		this.companyId = companyId;
		this.status = status;
		this.createDate = createDate;
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public Integer getId() {

		return this.id;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	@Column(name = "name", length = 64)
	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	@Column(name = "company_id")
	public Integer getCompanyId() {

		return companyId;
	}

	public void setCompanyId(Integer companyId) {

		this.companyId = companyId;
	}

	@Column(name = "is_active", length = 2)
	public String getStatus() {

		return status;
	}

	public void setStatus(String status) {

		this.status = status;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_date", insertable = true, updatable = false)
	public Date getCreateDate() {

		return this.createDate;
	}

	public void setCreateDate(Date createTime) {

		this.createDate = createTime;
	}

	@OneToOne
	@JoinColumn(name = "company_id", insertable = false, updatable = false)
	public Company getCompany() {

		return company;
	}

	public void setCompany(Company company) {

		this.company = company;
	}

	@Column(name = "inhand_id", insertable = true, updatable = true)
	public String getInhandId() {

		return inhandId;
	}

	public void setInhandId(String inhandId) {

		this.inhandId = inhandId;
	}

	@Transient
	public int getDeviceNormalNum() {

		return deviceNormalNum;
	}

	public void setDeviceNormalNum(int deviceNormalNum) {

		this.deviceNormalNum = deviceNormalNum;
	}

	@Transient
	public int getDeviceTotalNum() {

		return deviceTotalNum;
	}

	public void setDeviceTotalNum(int deviceTotalNum) {

		this.deviceTotalNum = deviceTotalNum;
	}

}