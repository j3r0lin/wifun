package com.wifun.admin.domain.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "media_version")
public class MediaVersion {

	public static final int MEDIA_ZIP_STATUS_ZIPPING = 0;
	public static final int MEDIA_ZIP_STATUS_SUCCESS = 1;
	public static final int MEDIA_ZIP_STATUS_ERROR = 2;

	public static final String FN_MEDIA_ID = "mediaId";
	public static final String FN_STATUS = "status";
	public static final String FN_ID = "id";

	// Fields
	private int id;
	private int mediaId;
	private String fileName;
	private int status;
	private Date createTime;
	private Date lastModify;

	public MediaVersion() {

	}

	public MediaVersion(int mediaId) {

		this.mediaId = mediaId;
		this.status = MediaVersion.MEDIA_ZIP_STATUS_ZIPPING;
		this.createTime = new Date();
		this.lastModify = new Date();
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {

		return id;
	}

	public void setId(int id) {

		this.id = id;
	}

	@Column(name = "media_id")
	public int getMediaId() {

		return mediaId;
	}

	public void setMediaId(int mediaId) {

		this.mediaId = mediaId;
	}

	@Column(name = "file_name")
	public String getFileName() {

		return fileName;
	}

	public void setFileName(String fileName) {

		this.fileName = fileName;
	}

	@Column(name = "status")
	public int getStatus() {

		return status;
	}

	public void setStatus(int status) {

		this.status = status;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time", updatable = false)
	public Date getCreateTime() {

		return createTime;
	}

	public void setCreateTime(Date createTime) {

		this.createTime = createTime;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "last_modify", length = 0)
	public Date getLastModify() {

		return lastModify;
	}

	public void setLastModify(Date lastModify) {

		this.lastModify = lastModify;
	}
}
