package com.wifun.admin.domain.model;

// default package

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 * CompanyAccount.
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "media")
public class Media implements java.io.Serializable {

	public static final int MEDIA_LANGUAGE_EN = 0;
	public static final int MEDIA_LANGUAGE_CN = 1;
	public static final int MEDIA_LANGUAGE_ALL = 2;

	public static final int COPYRIGHT_US = 0;
	public static final int COPYRIGHT_CN = 1;

	public static final String FN_TITLE = "title";
	public static final String FN_IS_ACTIVE = "isActive";
	public static final String FN_LANGUAGE = "language";
	public static final String FN_COPYRIGHT = "copyright";

	// Fields
	private int id;
	private int code;
	private int copyright;
	private String entertainment;
	private String title;
	private String year;
	private String writer;
	private String director;
	private String starring;
	private int length;
	private int showIndex;
	private String mediaUrl;
	private String vposterUrl;
	private String hposterUrl;
	private String introduction;
	private String version;
	private boolean isActive;

	private int language;// 支持语言 0:英文 1:中文 2:中/英
	private String entertainmentCn;
	private String titleCn;
	private String writerCn;
	private String directorCn;
	private String starringCn;
	private String introductionCn;

	private List<MediaCategory> categorys = new ArrayList<MediaCategory>();
	private String categoryList;
	private MediaVersion mediaVersion;

	/** default constructor */
	public Media() {

	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {

		return this.id;
	}

	public void setId(int id) {

		this.id = id;
	}

	@Column(name = "code")
	public int getCode() {

		return code;
	}

	public void setCode(int code) {

		this.code = code;
	}

	@Column(name = "copyright")
	public int getCopyright() {

		return copyright;
	}

	public void setCopyright(int copyright) {

		this.copyright = copyright;
	}

	@Column(name = "language")
	public int getLanguage() {

		return language;
	}

	public void setLanguage(int language) {

		this.language = language;
	}

	@Column(name = "title")
	public String getTitle() {

		return title;
	}

	public void setTitle(String title) {

		this.title = title;
	}

	@Column(name = "entertainment")
	public String getEntertainment() {

		return entertainment;
	}

	public void setEntertainment(String entertainment) {

		this.entertainment = entertainment;
	}

	@Column(name = "year")
	public String getYear() {

		return year;
	}

	public void setYear(String year) {

		this.year = year;
	}

	@Column(name = "writer")
	public String getWriter() {

		return writer;
	}

	public void setWriter(String writer) {

		this.writer = writer;
	}

	@Column(name = "director")
	public String getDirector() {

		return director;
	}

	public void setDirector(String director) {

		this.director = director;
	}

	@Column(name = "starring")
	public String getStarring() {

		return starring;
	}

	public void setStarring(String starring) {

		this.starring = starring;
	}

	@Column(name = "length")
	public int getLength() {

		return length;
	}

	public void setLength(int length) {

		this.length = length;
	}

	@Column(name = "show_index")
	public int getShowIndex() {

		return showIndex;
	}

	public void setShowIndex(int showIndex) {

		this.showIndex = showIndex;
	}

	@Column(name = "media_url", updatable = false)
	public String getMediaUrl() {

		return mediaUrl;
	}

	public void setMediaUrl(String mediaUrl) {

		this.mediaUrl = mediaUrl;
	}

	@Column(name = "vposter_url", updatable = false)
	public String getVposterUrl() {

		return vposterUrl;
	}

	public void setVposterUrl(String vposterUrl) {

		this.vposterUrl = vposterUrl;
	}

	@Column(name = "hposter_url", updatable = false)
	public String getHposterUrl() {

		return hposterUrl;
	}

	public void setHposterUrl(String hposterUrl) {

		this.hposterUrl = hposterUrl;
	}

	@Column(name = "introduction")
	public String getIntroduction() {

		return introduction;
	}

	public void setIntroduction(String introduction) {

		this.introduction = introduction;
	}

	@Column(name = "version")
	public String getVersion() {

		return version;
	}

	public void setVersion(String version) {

		this.version = version;
	}

	@Column(name = "is_active")
	public boolean getIsActive() {

		return isActive;
	}

	public void setIsActive(boolean isActive) {

		this.isActive = isActive;
	}

	@Column(name = "entertainment_cn")
	public String getEntertainmentCn() {

		return entertainmentCn;
	}

	public void setEntertainmentCn(String entertainmentCn) {

		this.entertainmentCn = entertainmentCn;
	}

	@Column(name = "title_cn")
	public String getTitleCn() {

		return titleCn;
	}

	public void setTitleCn(String titleCn) {

		this.titleCn = titleCn;
	}

	@Column(name = "writer_cn")
	public String getWriterCn() {

		return writerCn;
	}

	public void setWriterCn(String writerCn) {

		this.writerCn = writerCn;
	}

	@Column(name = "director_cn")
	public String getDirectorCn() {

		return directorCn;
	}

	public void setDirectorCn(String directorCn) {

		this.directorCn = directorCn;
	}

	@Column(name = "starring_cn")
	public String getStarringCn() {

		return starringCn;
	}

	public void setStarringCn(String starringCn) {

		this.starringCn = starringCn;
	}

	@Column(name = "introduction_cn")
	public String getIntroductionCn() {

		return introductionCn;
	}

	public void setIntroductionCn(String introductionCn) {

		this.introductionCn = introductionCn;
	}

	@OneToMany(mappedBy = "media", fetch = FetchType.EAGER)
	@JoinColumn(name = "media_id", insertable = false, updatable = false)
	@OrderBy(value = "id ASC")
	public List<MediaCategory> getCategorys() {

		return categorys;
	}

	public void setCategorys(List<MediaCategory> categorys) {

		this.categorys = categorys;
	}

	@Transient
	public String getCategoryList() {

		categoryList = "";
		for (MediaCategory mc : categorys) {
			categoryList += "," + mc.getCategory().getTitle();
		}

		if (categoryList.length() > 1)
			return categoryList.substring(1);

		return categoryList;
	}

	public void setCategoryList(String categoryList) {

		this.categoryList = categoryList;
	}

	@Transient
	public MediaVersion getMediaVersion() {

		return mediaVersion;
	}

	public void setMediaVersion(MediaVersion mediaVersion) {

		this.mediaVersion = mediaVersion;
	}

	public void updateByVo(Media entity) {

		this.entertainment = entity.getEntertainment();
		this.title = entity.getTitle();
		this.year = entity.getYear();
		this.writer = entity.getWriter();
		this.director = entity.getDirector();
		this.starring = entity.getStarring();
		this.length = entity.getLength();
		this.showIndex = entity.getShowIndex();
		this.mediaUrl = entity.getMediaUrl();
		this.vposterUrl = entity.getVposterUrl();
		this.hposterUrl = entity.getHposterUrl();
		this.introduction = entity.getIntroduction();
		this.isActive = entity.getIsActive();

		this.language = entity.getLanguage();// 支持语言 0:英文 1:中文 2:中/英
		this.entertainmentCn = entity.getEntertainmentCn();
		this.titleCn = entity.getTitleCn();
		this.writerCn = entity.getWriterCn();
		this.directorCn = entity.getDirectorCn();
		this.starringCn = entity.getStarringCn();
		this.introductionCn = entity.getIntroductionCn();
	}
}