package com.wifun.admin.domain.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author chw
 * 
 *         定时任务，每个device的流量日报 数据
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "task_traffic_daily")
public class TaskTrafficDaily implements java.io.Serializable {

	private int id;
	private int deviceId;
	private int companyId;
	private long send;
	private long recv;
	private long total;
	private String date;
	private Date createTime;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {

		return id;
	}

	public void setId(int id) {

		this.id = id;
	}

	@Column(name = "device_id")
	public int getDeviceId() {

		return deviceId;
	}

	public void setDeviceId(int deviceId) {

		this.deviceId = deviceId;
	}

	@Column(name = "company_id")
	public int getCompanyId() {

		return companyId;
	}

	public void setCompanyId(int companyId) {

		this.companyId = companyId;
	}

	@Column(name = "send")
	public long getSend() {

		return send;
	}

	public void setSend(long send) {

		this.send = send;
	}

	@Column(name = "recv")
	public long getRecv() {

		return recv;
	}

	public void setRecv(long recv) {

		this.recv = recv;
	}

	@Column(name = "total")
	public long getTotal() {

		return total;
	}

	public void setTotal(long total) {

		this.total = total;
	}

	@Column(name = "date")
	public String getDate() {

		return date;
	}

	public void setDate(String date) {

		this.date = date;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time", insertable = true, updatable = false)
	public Date getCreateTime() {

		return createTime;
	}

	public void setCreateTime(Date createTime) {

		this.createTime = createTime;
	}
}
