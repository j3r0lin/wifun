package com.wifun.admin.domain.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * 
 * @author chw
 * 
 *         定时任务，每个device的pvuv 数据
 */
@SuppressWarnings("serial")
@Entity
@Table(name = "task_pvuv_daily")
public class TaskPVUVDaily implements java.io.Serializable {

	private int id;
	private int deviceId;
	private int companyId;

	private String type;
	private String rid;
	private int pv;
	private int uv;

	private String date;
	private Date createTime;

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {

		return id;
	}

	public void setId(int id) {

		this.id = id;
	}

	@Column(name = "device_id")
	public int getDeviceId() {

		return deviceId;
	}

	public void setDeviceId(int deviceId) {

		this.deviceId = deviceId;
	}

	@Column(name = "company_id")
	public int getCompanyId() {

		return companyId;
	}

	public void setCompanyId(int companyId) {

		this.companyId = companyId;
	}

	@Column(name = "type")
	public String getType() {

		return type;
	}

	public void setType(String type) {

		this.type = type;
	}

	@Column(name = "rid")
	public String getRid() {

		return rid;
	}

	public void setRid(String rid) {

		this.rid = rid;
	}

	@Column(name = "pv")
	public int getPv() {

		return pv;
	}

	public void setPv(int pv) {

		this.pv = pv;
	}

	@Column(name = "uv")
	public int getUv() {

		return uv;
	}

	public void setUv(int uv) {

		this.uv = uv;
	}

	@Column(name = "date")
	public String getDate() {

		return date;
	}

	public void setDate(String date) {

		this.date = date;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time", insertable = true, updatable = false)
	public Date getCreateTime() {

		return createTime;
	}

	public void setCreateTime(Date createTime) {

		this.createTime = createTime;
	}
}
