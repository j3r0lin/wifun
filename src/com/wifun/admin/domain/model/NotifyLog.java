package com.wifun.admin.domain.model;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "notify_log")
public class NotifyLog {

	private int id;
	private String syncMode;
	private int type; // 0:add 1:delete 2:syncEnd
	private String version;
	private String sn;
	private int deviceId;
	private String media;
	private int mediaId;
	private int reasion;
	private Date createTime;

	/** default constructor */
	public NotifyLog() {

	}

	public NotifyLog(int type, String version, String sn, int deviceId, String media, int mediaId,
			int reasion, String syncMode) {

		super();
		this.syncMode = syncMode;
		this.type = type;
		this.version = version;
		this.sn = sn;
		this.deviceId = deviceId;
		this.media = media;
		this.mediaId = mediaId;
		this.reasion = reasion;
		this.createTime = new Date();
	}

	public NotifyLog(int type, String version, String sn, int deviceId, int reasion, String syncMode) {

		super();
		this.syncMode = syncMode;
		this.type = type;
		this.version = version;
		this.sn = sn;
		this.deviceId = deviceId;
		this.reasion = reasion;
		this.createTime = new Date();
	}

	// Property accessors
	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	public int getId() {

		return id;
	}

	public void setId(int id) {

		this.id = id;
	}

	@Column(name = "sync_mode")
	public String getSyncMode() {

		return syncMode;
	}

	public void setSyncMode(String syncMode) {

		this.syncMode = syncMode;
	}

	@Column(name = "type")
	public int getType() {

		return type;
	}

	public void setType(int type) {

		this.type = type;
	}

	@Column(name = "version")
	public String getVersion() {

		return version;
	}

	public void setVersion(String version) {

		this.version = version;
	}

	@Column(name = "sn")
	public String getSn() {

		return sn;
	}

	public void setSn(String sn) {

		this.sn = sn;
	}

	@Column(name = "device_id")
	public int getDeviceId() {

		return deviceId;
	}

	public void setDeviceId(int deviceId) {

		this.deviceId = deviceId;
	}

	@Column(name = "media")
	public String getMedia() {

		return media;
	}

	public void setMedia(String media) {

		this.media = media;
	}

	@Column(name = "media_id")
	public int getMediaId() {

		return mediaId;
	}

	public void setMediaId(int mediaId) {

		this.mediaId = mediaId;
	}

	@Column(name = "reasion")
	public int getReasion() {

		return reasion;
	}

	public void setReasion(int reasion) {

		this.reasion = reasion;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "create_time", insertable = true, updatable = false)
	public Date getCreateTime() {

		return createTime;
	}

	public void setCreateTime(Date createTime) {

		this.createTime = createTime;
	}
}
