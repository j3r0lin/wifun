package com.wifun.admin.domain.util;

import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.lang.StringUtils;

public class SendMail {

	private String host = ""; // smtp服务器
	private String from = ""; // 发件人地址
	private List<String> toList = new ArrayList<String>(); // 收件人地址
	private String affix = ""; // 附件地址
	private String affixName = ""; // 附件名称
	private String user = ""; // 用户名
	private String pwd = ""; // 密码
	private String subject = ""; // 邮件标题
	private String content = ""; // 正文
	private String neckName = "";// 发件人昵称

	public void setAddress(String from, List<String> toList, String subject, String content,
			String neckName) {

		this.from = from;
		this.toList = toList;
		this.subject = subject;
		this.content = content;
		this.neckName = neckName;
	}

	public void setAffix(String affix, String affixName) {

		this.affix = affix;
		this.affixName = affixName;
	}

	public void send(String host, String user, String pwd) throws Exception {

		this.host = host;
		this.user = user;
		this.pwd = pwd;

		Properties props = new Properties();

		// 设置发送邮件的邮件服务器的属性（这里使用网易的smtp服务器）
		props.put("mail.smtp.host", host);
		// 需要经过授权，也就是有户名和密码的校验，这样才能通过验证
		props.put("mail.smtp.auth", "true");
		// 用刚刚设置好的props对象构建一个session
		Session session = Session.getDefaultInstance(props);

		// 用session为参数定义消息对象
		MimeMessage message = new MimeMessage(session);
		try {
			// 加载发件人地址
			if (StringUtils.isNotBlank(neckName)) {
				String nick = javax.mail.internet.MimeUtility.encodeText(neckName);
				message.setFrom(new InternetAddress(nick + " <" + from + ">"));
			} else {
				message.setFrom(new InternetAddress(from));
			}

			InternetAddress[] addressToList = new InternetAddress[toList.size()];

			for (int i = 0; i < toList.size(); i++) {
				addressToList[i] = new InternetAddress(toList.get(i));
			}

			// 加载收件人地址
			message.addRecipients(Message.RecipientType.TO, addressToList);

			// 加载标题
			message.setSubject(subject);

			// 向multipart对象中添加邮件的各个部分内容，包括文本内容和附件
			Multipart multipart = new MimeMultipart();

			// 设置邮件的文本内容
			BodyPart contentPart = new MimeBodyPart();
			contentPart.setText(content);
			multipart.addBodyPart(contentPart);

			// 添加附件
			BodyPart messageBodyPart = new MimeBodyPart();
			DataSource source = new FileDataSource(affix);

			// 添加附件的内容
			messageBodyPart.setDataHandler(new DataHandler(source));

			// 添加附件的标题
			// 这里很重要，通过下面的Base64编码的转换可以保证你的中文附件标题名在发送时不会变成乱码
			sun.misc.BASE64Encoder enc = new sun.misc.BASE64Encoder();
			messageBodyPart.setFileName("=?GBK?B?" + enc.encode(affixName.getBytes()) + "?=");
			multipart.addBodyPart(messageBodyPart);

			// 将multipart对象放到message中
			message.setContent(multipart);

			// 保存邮件
			message.saveChanges();

			// 发送邮件
			Transport transport = session.getTransport("smtp");

			// 连接服务器的邮箱
			transport.connect(host, user, pwd);

			// 把邮件发送出去
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw new Exception("发送email 异常", e);
		}
	}

	/**
	 * 发送没有附件的邮件
	 * 
	 * @param host
	 * @param user
	 * @param pwd
	 * @throws Exception
	 */
	public void sendSimple(String host, String user, String pwd) throws Exception {

		this.host = host;
		this.user = user;
		this.pwd = pwd;

		Properties props = new Properties();

		// 设置发送邮件的邮件服务器的属性（这里使用网易的smtp服务器）
		props.put("mail.smtp.host", host);
		// 需要经过授权，也就是有户名和密码的校验，这样才能通过验证
		props.put("mail.smtp.auth", "true");
		// 用刚刚设置好的props对象构建一个session
		Session session = Session.getDefaultInstance(props);

		// 用session为参数定义消息对象
		MimeMessage message = new MimeMessage(session);
		try {
			// 加载发件人地址
			message.setFrom(new InternetAddress(from));

			InternetAddress[] addressToList = new InternetAddress[toList.size()];

			for (int i = 0; i < toList.size(); i++) {
				addressToList[i] = new InternetAddress(toList.get(i));
			}

			// 加载收件人地址
			message.addRecipients(Message.RecipientType.TO, addressToList);

			// 加载标题
			message.setSubject(subject);

			// 向multipart对象中添加邮件的文本内容
			Multipart multipart = new MimeMultipart();

			// 设置邮件的文本内容
			BodyPart contentPart = new MimeBodyPart();
			contentPart.setText(content);
			multipart.addBodyPart(contentPart);

			// 将multipart对象放到message中
			message.setContent(multipart);

			// 保存邮件
			message.saveChanges();

			// 发送邮件
			Transport transport = session.getTransport("smtp");

			// 连接服务器的邮箱
			transport.connect(host, user, pwd);

			// 把邮件发送出去
			transport.sendMessage(message, message.getAllRecipients());

			transport.close();
		} catch (Exception e) {
			throw new Exception("发送email 异常", e);
		}
	}
}