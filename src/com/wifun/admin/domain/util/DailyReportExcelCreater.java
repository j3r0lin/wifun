package com.wifun.admin.domain.util;

import java.io.File;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.format.Colour;
import jxl.write.Label;
import jxl.write.Number;
import jxl.write.WritableCellFeatures;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.log4j.Logger;

import com.wifun.admin.app.vo.DailyReportCompanyVo;
import com.wifun.admin.app.vo.DailyReportVo;
import com.wifun.admin.util.MathUtil;

/**
 * 运营日报excel文件构造器
 * 
 * @File wifun-admin/com.wifun.admin.domain.util.DailyReportExcelCreater.java
 * @Desc
 */
public class DailyReportExcelCreater {

	protected static final Logger logger = Logger.getLogger(DailyReportExcelCreater.class);

	/**
	 * 创建excel报表
	 * 
	 * @param filePath
	 * @param fileName
	 * @param sumReportList
	 * @param reportMap
	 * @return
	 */
	public static boolean create(String filePath, String fileName,
			List<DailyReportCompanyVo> sumReportList, Map<String, List<DailyReportVo>> reportMap) {

		WritableWorkbook workbook = null;
		try {

			String absolutepath = filePath;
			if (createDirectory(absolutepath)) {

				File os = new File(absolutepath + "/" + fileName);

				// 创建工作薄
				workbook = Workbook.createWorkbook(os);
				createNotesSheet(workbook);

				// 生成汇总数据
				if (sumReportList != null && sumReportList.size() > 0)
					createSumSheet(workbook, "Summary", sumReportList);

				// 生成公司明细数据
				if (reportMap != null) {

					Object[] keys = reportMap.keySet().toArray();
					for (int i = keys.length - 1; i >= 0; i--) {

						List<DailyReportVo> list = reportMap.get(keys[i]);
						if (list == null || list.size() == 0)
							continue;
						createDetailSheet(workbook, keys[i].toString(), list);
					}
				}

				// 把创建的内容写入到输出流中，并关闭输出流
				workbook.write();
				workbook.close();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("生成order报表出错" + e);
			return false;
		}
		return false;
	}

	/**
	 * 构造汇总报表
	 * 
	 * @param workbook
	 * @param title
	 * @param list
	 */
	private static void createSumSheet(WritableWorkbook workbook, String title,
			List<DailyReportCompanyVo> list) {

		try {

			// 创建新的一页
			WritableSheet sheet = workbook.createSheet(title, 0);
			// 设置列的宽度
			sheet.setColumnView(0, 25);
			sheet.setColumnView(1, 15);
			sheet.setColumnView(2, 15);
			sheet.setColumnView(3, 15);
			sheet.setColumnView(4, 15);
			sheet.setColumnView(5, 15);
			sheet.setColumnView(6, 15);
			sheet.setColumnView(7, 25);
			sheet.setColumnView(8, 25);
			sheet.setColumnView(9, 25);
			sheet.setColumnView(10, 25);
			sheet.setColumnView(11, 25);
			sheet.setColumnView(12, 15);

			int startRow = 0;

			WritableFont fontTitle = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
			WritableCellFormat formatTitle = new WritableCellFormat(fontTitle);
			formatTitle.setWrap(true);
			formatTitle.setAlignment(jxl.format.Alignment.CENTRE);// 单元格中的内容水平方向居中
			formatTitle.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);// 单元格的内容垂直方向居中

			// 居中 格式
			jxl.write.NumberFormat nf = new jxl.write.NumberFormat("0"); // 设置数字格式
			WritableCellFormat formatCenterNum = new WritableCellFormat(nf);// 生成一个单元格样式控制对象
			formatCenterNum.setAlignment(jxl.format.Alignment.CENTRE);// 单元格中的内容水平方向居中
			formatCenterNum.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);// 单元格的内容垂直方向居中

			WritableCellFormat formatCenter = new WritableCellFormat();// 生成一个单元格样式控制对象
			formatCenter.setAlignment(jxl.format.Alignment.CENTRE);// 单元格中的内容水平方向居中
			formatCenter.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);// 单元格的内容垂直方向居中

			Label formate = new Label(0, startRow, "Company", formatTitle);
			sheet.addCell(formate);

			Label formate1 = new Label(1, startRow, "Home PV", formatTitle);
			setCellComments(formate1, "portal页面的浏览数量");
			sheet.addCell(formate1);
			Label formate2 = new Label(2, startRow, "Home UV", formatTitle);
			setCellComments(formate2, "portal页面的浏览人数");
			sheet.addCell(formate2);
			Label formate3 = new Label(3, startRow, "Main PV", formatTitle);
			setCellComments(formate3, "电影首页的浏览数量");
			sheet.addCell(formate3);
			Label formate4 = new Label(4, startRow, "Main UV", formatTitle);
			setCellComments(formate4, "电影首页的浏览人数");
			sheet.addCell(formate4);
			Label formate5 = new Label(5, startRow, "Movie PV", formatTitle);
			setCellComments(formate5, "播放电影的次数");
			sheet.addCell(formate5);
			Label formate6 = new Label(6, startRow, "Movie UV", formatTitle);
			setCellComments(formate6, "播放电影的人数");
			sheet.addCell(formate6);
			Label formate7 = new Label(7, startRow, "# of new discovered mobile device",
					formatTitle);
			setCellComments(formate7, "每日新增终端");
			sheet.addCell(formate7);
			Label formate8 = new Label(8, startRow, "Total # of mobile device since on-board",
					formatTitle);
			setCellComments(formate8, "累计终端");
			sheet.addCell(formate8);
			Label formate9 = new Label(9, startRow,
					"# of mobile device connected longer than 1 hour ", formatTitle);
			setCellComments(formate9, "停留时间超过1小时的终端数");
			sheet.addCell(formate9);
			Label formate10 = new Label(10, startRow, "# of new mobile device request to surf",
					formatTitle);
			setCellComments(formate10, "每日新增上网用户数");
			sheet.addCell(formate10);
			Label formate11 = new Label(11, startRow, "Total # of device request to surf",
					formatTitle);
			setCellComments(formate11, "累计上网用户数");
			sheet.addCell(formate11);
			Label formate12 = new Label(12, startRow, "Traffic Total", formatTitle);
			setCellComments(formate12, "每日流量");
			sheet.addCell(formate12);

			// 添加内容
			for (int j = 0; j < list.size(); j++) {

				sheet.setRowView(startRow + j + 1, 350); // 设置行高
				DailyReportCompanyVo report = list.get(j);
				sheet.addCell(new Label(0, startRow + j + 1, report.getCompanyName(), formatCenter));

				Number labelNF1 = new Number(1, startRow + j + 1, report.getPvHome(),
						formatCenterNum);
				sheet.addCell(labelNF1);

				Number labelNF2 = new Number(2, startRow + j + 1, report.getUvHome(),
						formatCenterNum);
				sheet.addCell(labelNF2);

				Number labelNF3 = new Number(3, startRow + j + 1, report.getPvMain(),
						formatCenterNum);
				sheet.addCell(labelNF3);

				Number labelNF4 = new Number(4, startRow + j + 1, report.getUvMain(),
						formatCenterNum);
				sheet.addCell(labelNF4);

				Number labelNF5 = new Number(5, startRow + j + 1, report.getPvMovie(),
						formatCenterNum);
				sheet.addCell(labelNF5);

				Number labelNF6 = new Number(6, startRow + j + 1, report.getUvMovie(),
						formatCenterNum);
				sheet.addCell(labelNF6);

				Number labelNF7 = new Number(7, startRow + j + 1, report.getDeviceDailyAdd(),
						formatCenterNum);
				sheet.addCell(labelNF7);

				Number labelNF8 = new Number(8, startRow + j + 1, report.getDeviceTotal(),
						formatCenterNum);
				sheet.addCell(labelNF8);

				Number labelNF9 = new Number(9, startRow + j + 1, report.getDeviceOneHourOnline(),
						formatCenterNum);
				sheet.addCell(labelNF9);

				Number labelNF10 = new Number(10, startRow + j + 1, report.getUserDailyAdd(),
						formatCenterNum);
				sheet.addCell(labelNF10);

				Number labelNF11 = new Number(11, startRow + j + 1, report.getUserTotal(),
						formatCenterNum);
				sheet.addCell(labelNF11);

				sheet.addCell(new Label(12, startRow + j + 1, MathUtil.bytes2MB(
						report.getTrafficTotal(), 3)
						+ "MB", formatCenter));
			}

			createNotes(sheet, list.size() + 5);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("生成报表出错" + e);
		}
	}

	/**
	 * 构造公司明细表
	 * 
	 * @param workbook
	 * @param title
	 * @param list
	 */
	private static void createDetailSheet(WritableWorkbook workbook, String title,
			List<DailyReportVo> list) {

		try {

			// 创建新的一页
			WritableSheet sheet = workbook.createSheet(title, 0);
			// 设置列的宽度
			sheet.setColumnView(0, 25);
			sheet.setColumnView(1, 15);
			sheet.setColumnView(2, 15);
			sheet.setColumnView(3, 15);
			sheet.setColumnView(4, 15);
			sheet.setColumnView(5, 15);
			sheet.setColumnView(6, 25);
			sheet.setColumnView(7, 15);
			sheet.setColumnView(8, 25);
			sheet.setColumnView(9, 25);
			sheet.setColumnView(10, 25);
			sheet.setColumnView(11, 25);
			sheet.setColumnView(12, 25);
			sheet.setColumnView(13, 15);
			sheet.setColumnView(14, 15);

			int startRow = 0;

			WritableFont fontTitle = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
			WritableCellFormat formatTitle = new WritableCellFormat(fontTitle);
			formatTitle.setWrap(true);
			formatTitle.setAlignment(jxl.format.Alignment.CENTRE);// 单元格中的内容水平方向居中
			formatTitle.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);// 单元格的内容垂直方向居中

			// 居中 格式
			jxl.write.NumberFormat nf = new jxl.write.NumberFormat("0"); // 设置数字格式
			WritableCellFormat formatCenterNum = new WritableCellFormat(nf);// 生成一个单元格样式控制对象
			formatCenterNum.setAlignment(jxl.format.Alignment.CENTRE);// 单元格中的内容水平方向居中
			formatCenterNum.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);// 单元格的内容垂直方向居中
			WritableCellFormat formatCenter = new WritableCellFormat();// 生成一个单元格样式控制对象
			formatCenter.setAlignment(jxl.format.Alignment.CENTRE);// 单元格中的内容水平方向居中
			formatCenter.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);// 单元格的内容垂直方向居中

			Label formate = new Label(0, startRow, "Device", formatTitle);
			sheet.addCell(formate);
			Label formate1 = new Label(1, startRow, "Group", formatTitle);
			sheet.addCell(formate1);
			Label formate2 = new Label(2, startRow, "Home PV", formatTitle);
			setCellComments(formate2, "portal页面的浏览数量");
			sheet.addCell(formate2);
			Label formate3 = new Label(3, startRow, "Home UV", formatTitle);
			setCellComments(formate3, "portal页面的浏览人数");
			sheet.addCell(formate3);
			Label formate4 = new Label(4, startRow, "Main PV", formatTitle);
			setCellComments(formate4, "电影首页的浏览数量");
			sheet.addCell(formate4);
			Label formate5 = new Label(5, startRow, "Main UV", formatTitle);
			setCellComments(formate5, "电影首页的浏览人数");
			sheet.addCell(formate5);
			Label formate6 = new Label(6, startRow, "Movie PV", formatTitle);
			setCellComments(formate6, "播放电影的次数");
			sheet.addCell(formate6);
			Label formate7 = new Label(7, startRow, "Movie UV", formatTitle);
			setCellComments(formate7, "播放电影的人数");
			sheet.addCell(formate7);
			Label formate8 = new Label(8, startRow, "# of new discovered mobile device",
					formatTitle);
			setCellComments(formate8, "每日新增终端");
			sheet.addCell(formate8);
			Label formate9 = new Label(9, startRow, "Total # of mobile device since on-board",
					formatTitle);
			setCellComments(formate9, "累计终端");
			sheet.addCell(formate9);
			Label formate10 = new Label(10, startRow,
					"# of mobile device connected longer than 1 hour", formatTitle);
			setCellComments(formate10, "停留时间超过1小时的终端数");
			sheet.addCell(formate10);
			Label formate11 = new Label(11, startRow, "# of new mobile device request to surf",
					formatTitle);
			setCellComments(formate11, "每日新增上网用户数");
			sheet.addCell(formate11);
			Label formate12 = new Label(12, startRow, "Total # of device request to surf",
					formatTitle);
			setCellComments(formate12, "累计上网用户数");
			sheet.addCell(formate12);
			Label formate13 = new Label(13, startRow, "Traffic Total", formatTitle);
			setCellComments(formate13, "每日流量");
			sheet.addCell(formate13);
			Label formate14 = new Label(14, startRow, "Date", formatTitle);
			sheet.addCell(formate14);

			// 添加内容
			for (int j = 0; j < list.size(); j++) {

				// 设置行高
				sheet.setRowView(startRow + j + 1, 350);
				DailyReportVo report = list.get(j);
				sheet.addCell(new Label(0, startRow + j + 1, report.getDeviceName(), formatCenter));
				sheet.addCell(new Label(1, startRow + j + 1, report.getGroupName(), formatCenter));

				Number labelNF2 = new Number(2, startRow + j + 1, report.getPvHome(),
						formatCenterNum);
				sheet.addCell(labelNF2);

				Number labelNF3 = new Number(3, startRow + j + 1, report.getUvHome(),
						formatCenterNum);
				sheet.addCell(labelNF3);

				Number labelNF4 = new Number(4, startRow + j + 1, report.getPvMain(),
						formatCenterNum);
				sheet.addCell(labelNF4);

				Number labelNF5 = new Number(5, startRow + j + 1, report.getUvMain(),
						formatCenterNum);
				sheet.addCell(labelNF5);

				Number labelNF6 = new Number(6, startRow + j + 1, report.getPvMovie(),
						formatCenterNum);
				sheet.addCell(labelNF6);

				Number labelNF7 = new Number(7, startRow + j + 1, report.getUvMovie(),
						formatCenterNum);
				sheet.addCell(labelNF7);

				Number labelNF8 = new Number(8, startRow + j + 1, report.getDeviceDailyAdd(),
						formatCenterNum);
				sheet.addCell(labelNF8);

				Number labelNF9 = new Number(9, startRow + j + 1, report.getDeviceTotal(),
						formatCenterNum);
				sheet.addCell(labelNF9);

				Number labelNF10 = new Number(10, startRow + j + 1,
						report.getDeviceOneHourOnline(), formatCenterNum);
				sheet.addCell(labelNF10);

				Number labelNF11 = new Number(11, startRow + j + 1, report.getUserDailyAdd(),
						formatCenterNum);
				sheet.addCell(labelNF11);

				Number labelNF12 = new Number(12, startRow + j + 1, report.getUserTotal(),
						formatCenterNum);
				sheet.addCell(labelNF12);

				sheet.addCell(new Label(13, startRow + j + 1, report.getTrafficTotal() + "MB",
						formatCenter));
				sheet.addCell(new Label(14, startRow + j + 1, report.getDate(), formatCenter));
			}

			createNotes(sheet, list.size() + 5);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("生成报表出错" + e);
		}
	}

	/**
	 * 构造注释
	 * 
	 * @param sheet
	 * @param startRow
	 */
	private static void createNotes(WritableSheet sheet, int startRow) {

		try {

			// 构造表头
			sheet.mergeCells(0, startRow, 8, startRow);// 添加合并单元格，第一个参数是起始列，第二个参数是起始行，第三个参数是终止列，第四个参数是终止行
			WritableFont bold = new WritableFont(WritableFont.ARIAL, 14, WritableFont.BOLD);// 设置字体种类和黑体显示,字体为Arial,字号大小为10,采用黑体显示
			WritableCellFormat titleFormate = new WritableCellFormat(bold);// 生成一个单元格样式控制对象
			titleFormate.setAlignment(jxl.format.Alignment.CENTRE);// 单元格中的内容水平方向居中
			titleFormate.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);// 单元格的内容垂直方向居中
			titleFormate.setBackground(Colour.ICE_BLUE);

			WritableFont fontTitle = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
			WritableCellFormat formatTitle = new WritableCellFormat(fontTitle);
			formatTitle.setWrap(true);
			formatTitle.setAlignment(jxl.format.Alignment.CENTRE);// 单元格中的内容水平方向居中
			formatTitle.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);// 单元格的内容垂直方向居中

			WritableCellFormat formatCenter = new WritableCellFormat();// 生成一个单元格样式控制对象
			formatCenter.setAlignment(jxl.format.Alignment.CENTRE);// 单元格中的内容水平方向居中
			formatCenter.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);// 单元格的内容垂直方向居中

			String titleLiable = "Notes";

			Label title = new Label(0, startRow, titleLiable, titleFormate);
			sheet.setRowView(startRow, 600, false);// 设置第一行的高度
			sheet.addCell(title);

			startRow++;

			sheet.mergeCells(0, startRow, 3, startRow);
			sheet.mergeCells(4, startRow, 8, startRow);
			sheet.setRowView(startRow, 350);
			Label formate = new Label(0, startRow, "Parameter", formatTitle);
			sheet.addCell(formate);
			Label formate1 = new Label(4, startRow, "Interpretation", formatTitle);
			sheet.addCell(formate1);
			startRow++;

			sheet.mergeCells(0, startRow, 3, startRow);
			sheet.mergeCells(4, startRow, 8, startRow);
			sheet.setRowView(startRow, 350);
			sheet.addCell(new Label(0, startRow, "Home PV", formatCenter));
			sheet.addCell(new Label(4, startRow, "portal页面的浏览数量", formatCenter));
			startRow++;

			sheet.mergeCells(0, startRow, 3, startRow);
			sheet.mergeCells(4, startRow, 8, startRow);
			sheet.setRowView(startRow, 350);
			sheet.addCell(new Label(0, startRow, "Home UV", formatCenter));
			sheet.addCell(new Label(4, startRow, "portal页面的浏览人数", formatCenter));
			startRow++;

			sheet.mergeCells(0, startRow, 3, startRow);
			sheet.mergeCells(4, startRow, 8, startRow);
			sheet.setRowView(startRow, 350);
			sheet.addCell(new Label(0, startRow, "Main PV", formatCenter));
			sheet.addCell(new Label(4, startRow, "电影首页的浏览数量", formatCenter));
			startRow++;

			sheet.mergeCells(0, startRow, 3, startRow);
			sheet.mergeCells(4, startRow, 8, startRow);
			sheet.setRowView(startRow, 350);
			sheet.addCell(new Label(0, startRow, "Main UV", formatCenter));
			sheet.addCell(new Label(4, startRow, "电影首页的浏览人数", formatCenter));
			startRow++;

			sheet.mergeCells(0, startRow, 3, startRow);
			sheet.mergeCells(4, startRow, 8, startRow);
			sheet.setRowView(startRow, 350);
			sheet.addCell(new Label(0, startRow, "Movie PV", formatCenter));
			sheet.addCell(new Label(4, startRow, "播放电影的次数", formatCenter));
			startRow++;

			sheet.mergeCells(0, startRow, 3, startRow);
			sheet.mergeCells(4, startRow, 8, startRow);
			sheet.setRowView(startRow, 350);
			sheet.addCell(new Label(0, startRow, "Movie UV", formatCenter));
			sheet.addCell(new Label(4, startRow, "播放电影的人数", formatCenter));
			startRow++;

			sheet.mergeCells(0, startRow, 3, startRow);
			sheet.mergeCells(4, startRow, 8, startRow);
			sheet.setRowView(startRow, 350);
			sheet.addCell(new Label(0, startRow, "# of new discovered mobile device", formatCenter));
			sheet.addCell(new Label(4, startRow, "每日新增终端", formatCenter));
			startRow++;

			sheet.mergeCells(0, startRow, 3, startRow);
			sheet.mergeCells(4, startRow, 8, startRow);
			sheet.setRowView(startRow, 350);
			sheet.addCell(new Label(0, startRow, "Total # of mobile device since on-board",
					formatCenter));
			sheet.addCell(new Label(4, startRow, "累计终端", formatCenter));
			startRow++;

			sheet.mergeCells(0, startRow, 3, startRow);
			sheet.mergeCells(4, startRow, 8, startRow);
			sheet.setRowView(startRow, 350);
			sheet.addCell(new Label(0, startRow, "# of mobile device connected longer than 1 hour",
					formatCenter));
			sheet.addCell(new Label(4, startRow, "停留时间超过1小时的终端数", formatCenter));
			startRow++;

			sheet.mergeCells(0, startRow, 3, startRow);
			sheet.mergeCells(4, startRow, 8, startRow);
			sheet.setRowView(startRow, 350);
			sheet.addCell(new Label(0, startRow, "# of new mobile device request to surf",
					formatCenter));
			sheet.addCell(new Label(4, startRow, "每日新增上网用户数", formatCenter));
			startRow++;

			sheet.mergeCells(0, startRow, 3, startRow);
			sheet.mergeCells(4, startRow, 8, startRow);
			sheet.setRowView(startRow, 350);
			sheet.addCell(new Label(0, startRow, "Total # of device request to surf", formatCenter));
			sheet.addCell(new Label(4, startRow, "累计上网用户数", formatCenter));
			startRow++;

			sheet.mergeCells(0, startRow, 3, startRow);
			sheet.mergeCells(4, startRow, 8, startRow);
			sheet.setRowView(startRow, 350);
			sheet.addCell(new Label(0, startRow, "Traffic Total", formatCenter));
			sheet.addCell(new Label(4, startRow, "每日流量", formatCenter));
			startRow++;

		} catch (Exception e) {
			e.printStackTrace();
			logger.error("生成报表出错" + e);
		}
	}

	private static void createNotesSheet(WritableWorkbook workbook) {

		try {

			// 创建新的一页
			WritableSheet sheet = workbook.createSheet("Notes", 0);
			// 设置列的宽度
			sheet.setColumnView(0, 25);
			sheet.setColumnView(1, 75);

			int startRow = 0;

			WritableFont fontTitle = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
			WritableCellFormat formatTitle = new WritableCellFormat(fontTitle);
			formatTitle.setWrap(true);
			formatTitle.setAlignment(jxl.format.Alignment.CENTRE);// 单元格中的内容水平方向居中
			formatTitle.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);// 单元格的内容垂直方向居中

			// 居中 格式
			jxl.write.NumberFormat nf = new jxl.write.NumberFormat("0"); // 设置数字格式
			WritableCellFormat formatCenterNum = new WritableCellFormat(nf);// 生成一个单元格样式控制对象
			formatCenterNum.setAlignment(jxl.format.Alignment.CENTRE);// 单元格中的内容水平方向居中
			formatCenterNum.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);// 单元格的内容垂直方向居中

			WritableCellFormat formatCenter = new WritableCellFormat();// 生成一个单元格样式控制对象
			formatCenter.setAlignment(jxl.format.Alignment.CENTRE);// 单元格中的内容水平方向居中
			formatCenter.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);// 单元格的内容垂直方向居中

			Label formate = new Label(0, startRow, "Parameter", formatTitle);
			sheet.addCell(formate);
			Label formate1 = new Label(1, startRow, "Interpretation", formatTitle);
			sheet.addCell(formate1);

			sheet.setRowView(1, 350);
			sheet.addCell(new Label(0, 1, "Home PV", formatCenter));
			sheet.addCell(new Label(1, 1, "portal页面的浏览数量", formatCenter));

			sheet.setRowView(2, 350);
			sheet.addCell(new Label(0, 2, "Home UV", formatCenter));
			sheet.addCell(new Label(1, 2, "portal页面的浏览人数", formatCenter));

			sheet.setRowView(3, 350);
			sheet.addCell(new Label(0, 3, "Main PV", formatCenter));
			sheet.addCell(new Label(1, 3, "电影首页的浏览数量", formatCenter));

			sheet.setRowView(4, 350);
			sheet.addCell(new Label(0, 4, "Main UV", formatCenter));
			sheet.addCell(new Label(1, 4, "电影首页的浏览人数", formatCenter));

			sheet.setRowView(5, 350);
			sheet.addCell(new Label(0, 5, "Movie PV", formatCenter));
			sheet.addCell(new Label(1, 5, "播放电影的次数", formatCenter));

			sheet.setRowView(6, 350);
			sheet.addCell(new Label(0, 6, "Movie UV", formatCenter));
			sheet.addCell(new Label(1, 6, "播放电影的人数", formatCenter));

			sheet.setRowView(7, 350);
			sheet.addCell(new Label(0, 7, "# of new discovered mobile device", formatCenter));
			sheet.addCell(new Label(1, 7, "每日新增终端", formatCenter));

			sheet.setRowView(8, 350);
			sheet.addCell(new Label(0, 8, "Total # of mobile device since on-board", formatCenter));
			sheet.addCell(new Label(1, 8, "累计终端", formatCenter));

			sheet.setRowView(9, 350);
			sheet.addCell(new Label(0, 9, "# of mobile device connected longer than 1 hour",
					formatCenter));
			sheet.addCell(new Label(1, 9, "停留时间超过1小时的终端数", formatCenter));

			sheet.setRowView(10, 350);
			sheet.addCell(new Label(0, 10, "# of new mobile device request to surf", formatCenter));
			sheet.addCell(new Label(1, 10, "每日新增上网用户数", formatCenter));

			sheet.setRowView(11, 350);
			sheet.addCell(new Label(0, 11, "Total # of device request to surf", formatCenter));
			sheet.addCell(new Label(1, 11, "累计上网用户数", formatCenter));

			sheet.setRowView(12, 350);
			sheet.addCell(new Label(0, 12, "Traffic Total", formatCenter));
			sheet.addCell(new Label(1, 12, "每日流量", formatCenter));
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("生成报表出错" + e);
		}
	}

	private static boolean createDirectory(String Directory) {

		File directory = new File(Directory);
		if (!directory.exists()) {
			if (directory.mkdirs())
				return true;
		}
		return true;
	}

	/**
	 * 给单元格加注释
	 * 
	 * @param label
	 * @param comments
	 * @return
	 */
	private static void setCellComments(Object label, String comments) {

		WritableCellFeatures cellFeatures = new WritableCellFeatures();
		cellFeatures.setComment(comments);
		if (label instanceof jxl.write.Number) {
			jxl.write.Number num = (jxl.write.Number) label;
			num.setCellFeatures(cellFeatures);
		} else if (label instanceof jxl.write.Boolean) {
			jxl.write.Boolean bool = (jxl.write.Boolean) label;
			bool.setCellFeatures(cellFeatures);
		} else if (label instanceof jxl.write.DateTime) {
			jxl.write.DateTime dt = (jxl.write.DateTime) label;
			dt.setCellFeatures(cellFeatures);
		} else {
			Label _label = (Label) label;
			_label.setCellFeatures(cellFeatures);
		}
	}
}
