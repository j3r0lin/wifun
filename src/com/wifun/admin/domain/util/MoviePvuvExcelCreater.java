package com.wifun.admin.domain.util;

import java.io.File;
import java.util.List;
import java.util.Map;

import jxl.Workbook;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.Number;

import org.apache.log4j.Logger;

import com.wifun.admin.domain.model.TaskPvuvMovie;

/**
 * 电影pvuv日报excel文件构造器
 * 
 * @File wifun-admin/com.wifun.admin.domain.util.MoviePvuvExcelCreater.java
 * @Desc
 */
public class MoviePvuvExcelCreater {

	protected static final Logger logger = Logger.getLogger(MoviePvuvExcelCreater.class);

	/**
	 * 创建excel报表
	 * 
	 * @param filePath
	 * @param fileName
	 * @param sumReportList
	 * @param reportMap
	 * @return
	 */
	public static boolean create(String filePath, String fileName,
			List<TaskPvuvMovie> sumReportList, Map<String, List<TaskPvuvMovie>> reportMap) {

		WritableWorkbook workbook = null;
		try {

			String absolutepath = filePath;
			if (createDirectory(absolutepath)) {

				File os = new File(absolutepath + "/" + fileName);
				// 创建工作薄
				workbook = Workbook.createWorkbook(os);

				// 生成汇总数据
				if (sumReportList != null && sumReportList.size() > 0)
					createSheet(workbook, "Summary", sumReportList);

				// 生成公司明细数据
				if (reportMap != null) {

					Object[] keys = reportMap.keySet().toArray();
					for (int i = keys.length - 1; i >= 0; i--) {

						List<TaskPvuvMovie> list = reportMap.get(keys[i]);
						if (list == null || list.size() == 0)
							continue;

						createSheet(workbook, keys[i].toString(), list);
					}
				}

				// 把创建的内容写入到输出流中，并关闭输出流
				workbook.write();
				workbook.close();
				return true;
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("生成order报表出错" + e);
			return false;
		}
		return false;
	}

	/**
	 * 创建pvuv统计excel
	 * 
	 * @param workbook
	 * @param title
	 * @param list
	 */
	private static void createSheet(WritableWorkbook workbook, String title,
			List<TaskPvuvMovie> list) {

		try {

			// 创建新的一页
			WritableSheet sheet = workbook.createSheet(title, 0);
			// 设置列的宽度
			sheet.setColumnView(0, 40);
			sheet.setColumnView(1, 15);
			sheet.setColumnView(2, 15);
			sheet.setColumnView(3, 15);
			sheet.setColumnView(4, 15);

			int startRow = 0;

			WritableFont fontTitle = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
			WritableCellFormat formatTitle = new WritableCellFormat(fontTitle);
			formatTitle.setWrap(true);
			formatTitle.setAlignment(jxl.format.Alignment.CENTRE);// 单元格中的内容水平方向居中
			formatTitle.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);// 单元格的内容垂直方向居中

			// 居中 格式
			jxl.write.NumberFormat nf = new jxl.write.NumberFormat("0"); // 设置数字格式
			WritableCellFormat formatCenterNum = new WritableCellFormat(nf);// 生成一个单元格样式控制对象
			formatCenterNum.setAlignment(jxl.format.Alignment.CENTRE);// 单元格中的内容水平方向居中
			formatCenterNum.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);// 单元格的内容垂直方向居中

			WritableCellFormat formatCenter = new WritableCellFormat();// 生成一个单元格样式控制对象
			formatCenter.setAlignment(jxl.format.Alignment.CENTRE);// 单元格中的内容水平方向居中
			formatCenter.setVerticalAlignment(jxl.format.VerticalAlignment.CENTRE);// 单元格的内容垂直方向居中

			Label formate = new Label(0, startRow, "Title", formatTitle);
			sheet.addCell(formate);
			Label formate1 = new Label(1, startRow, "PV Total", formatTitle);
			sheet.addCell(formate1);
			Label formate2 = new Label(2, startRow, "UV Total", formatTitle);
			sheet.addCell(formate2);
			Label formate3 = new Label(3, startRow, "PV Current", formatTitle);
			sheet.addCell(formate3);
			Label formate4 = new Label(4, startRow, "UV Current", formatTitle);
			sheet.addCell(formate4);

			// --- 添加内容
			for (int j = 0; j < list.size(); j++) {
				// 设置行高
				sheet.setRowView(startRow + j + 1, 350);
				TaskPvuvMovie report = list.get(j);
				sheet.addCell(new Label(0, startRow + j + 1, report.getMovieTitle(), formatCenter));

				// 数字格式
				Number labelNF = new Number(1, startRow + j + 1, report.getTotalPv(),
						formatCenterNum);
				sheet.addCell(labelNF);
				Number labelNF2 = new Number(2, startRow + j + 1, report.getTotalUv(),
						formatCenterNum);
				sheet.addCell(labelNF2);
				Number labelNF3 = new Number(3, startRow + j + 1, report.getCurrentPv(),
						formatCenterNum);
				sheet.addCell(labelNF3);
				Number labelNF4 = new Number(4, startRow + j + 1, report.getCurrentUv(),
						formatCenterNum);
				sheet.addCell(labelNF4);
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("生成报表出错" + e);
		}
	}

	/**
	 * 创建目录
	 * 
	 * @param Directory
	 * @return
	 */
	private static boolean createDirectory(String Directory) {

		File directory = new File(Directory);
		if (!directory.exists()) {
			if (directory.mkdirs())
				return true;
		}
		return true;
	}
}
