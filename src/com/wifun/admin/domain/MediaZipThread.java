package com.wifun.admin.domain;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.dom4j.Document;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;

import com.wifun.admin.domain.cdn.CdnFileDistributer;
import com.wifun.admin.domain.dao.impl.MediaDao;
import com.wifun.admin.domain.dao.impl.MediaVersionDao;
import com.wifun.admin.domain.model.Media;
import com.wifun.admin.domain.model.MediaCategory;
import com.wifun.admin.domain.model.MediaVersion;
import com.wifun.admin.util.FileUtils;
import com.wifun.admin.util.SysConfig;
import com.wifun.admin.util.ZipUtils;

/**
 * 媒体资源文件压缩线程
 * 
 * @File wifun-admin/com.wifun.admin.domain.MediaZipThread.java
 * @Desc
 */
public class MediaZipThread extends Thread {

	private static final Logger logger = Logger.getLogger("zip.media.log");

	private String zipPath; // 压缩包存放路径
	private String mediaPath; // 媒体资源文件夹路径
	private Media media;

	private MediaDao mediaDao;
	private MediaVersionDao mediaVersionDao;

	public MediaZipThread(Media media) {

		this.media = media;
		this.zipPath = SysConfig.getSetting("wifun.media.zip.directory");
		this.mediaPath = SysConfig.getSetting("wifun.media.base.directory");

		mediaDao = (MediaDao) SpringContextUtil.getBean("mediaDao");
		mediaVersionDao = (MediaVersionDao) SpringContextUtil.getBean("mediaVersionDao");
	}

	@Override
	public void run() {

		int mediaId = media.getId();
		MediaVersion mediaVersion = new MediaVersion(mediaId);
		mediaVersion = mediaVersionDao.save(mediaVersion);

		String version = String.valueOf(mediaVersion.getId());
		media.setVersion(version);

		int language = media.getLanguage();

		// 删除多余的xml文件
		deleteXmlFile(media);

		// 生成xml文件
		try {
			if (language == 0 || language == 2)
				createMediaXmlEn(media);// 生成英文xml数据文件

			if (language == 1 || language == 2)
				createMediaXmlCn(media);// 生成中文xml数据文件

		} catch (IOException e) {
			updateMediaVersionError(mediaVersion);
			e.printStackTrace();
			logger.error("MediaId:" + media.getId() + ";createMediaXml error!", e);
			return;
		}

		// 压缩视频文件
		try {
			zipMedia(media);
		} catch (Exception e) {
			updateMediaVersionError(mediaVersion);
			e.printStackTrace();
			logger.error("MediaId:" + media.getId() + ";zipMediaList error!", e);
			return;
		}

		String fileName = "media_" + media.getCode() + "_" + media.getVersion();
		String zipFileName = fileName + ".zip";

		logger.info("MediaId:" + media.getId() + ";begin upload file " + zipFileName);

		// 上传压缩文件到CDN服务器
		CdnFileDistributer cdnFileDistributer = CdnFileDistributer.getInstance();
		cdnFileDistributer.copyFileToServer(zipPath, zipFileName);

		mediaDao.update(media);// 压缩成功，更新版本号
		updateMediaVersionSuccess(mediaVersion, zipFileName);
		System.out.println("MediaId:" + media.getId() + ";end upload file " + zipFileName);
		logger.info("MediaId:" + media.getId() + ";end upload file " + zipFileName);
	}

	// 删除多余的xml文件
	private void deleteXmlFile(Media media) {

		String mediaFileName = "/media_" + media.getCode();
		String filePath = mediaPath + mediaFileName;

		String suffix = ".xml";
		if (media.getLanguage() == 0)
			suffix = "_cn" + suffix;
		else if (media.getLanguage() == 1)
			suffix = "_en" + suffix;

		filePath = filePath + "/data_" + media.getCode() + suffix;
		File file = new File(filePath);
		if (file.exists())
			FileUtils.deleteFile(filePath);
	}

	/**
	 * 生成英文xml数据文件
	 * 
	 * @param media
	 * @throws IOException
	 */
	private void createMediaXmlEn(Media media) throws IOException {

		String mediaFileName = "/media_" + media.getCode();
		String deviceFileName = mediaFileName + "_" + media.getVersion();
		String filePath = mediaPath + mediaFileName;
		Document dom = DocumentHelper.createDocument();// 创建xml文件
		Element videoElement = dom.addElement("video");// 添加根元素
		videoElement.addAttribute("language", "EN");
		Element idElement = videoElement.addElement("id");
		idElement.addText(String.valueOf(media.getId()));
		Element codeElement = videoElement.addElement("code");
		codeElement.addText(String.valueOf(media.getCode()));
		Element versionElement = videoElement.addElement("version");
		versionElement.addText(media.getVersion());
		Element categorysElement = videoElement.addElement("categorys");
		for (MediaCategory mc : media.getCategorys()) {
			Element categoryElement = categorysElement.addElement("category");
			categoryElement.addAttribute("categoryId", String.valueOf(mc.getCategoryId()));
			categoryElement.addText(mc.getCategory().getTitle());
		}
		Element titleElement = videoElement.addElement("title");
		titleElement.addText(String.valueOf(media.getTitle()));
		Element mediaUrlElement = videoElement.addElement("videoUrl");
		mediaUrlElement.addText(String.valueOf("/video" + deviceFileName
				+ getFileName(media.getMediaUrl())));
		Element hPosterUrlElement = videoElement.addElement("hPosterUrl");
		hPosterUrlElement.addText(String.valueOf("/video" + deviceFileName
				+ getFileName(media.getHposterUrl())));
		Element vPosterUrlElement = videoElement.addElement("vPosterUrl");
		vPosterUrlElement.addText(String.valueOf("/video" + deviceFileName
				+ getFileName(media.getVposterUrl())));
		Element playLengthElement = videoElement.addElement("playLength");
		playLengthElement.addText(String.valueOf(media.getLength()));
		Element directorElement = videoElement.addElement("director");
		directorElement.addText(String.valueOf(media.getDirector()));
		Element writerElement = videoElement.addElement("writer");
		writerElement.addText(String.valueOf(media.getWriter()));
		Element starringElement = videoElement.addElement("starring");
		starringElement.addText(String.valueOf(media.getStarring()));
		Element introElement = videoElement.addElement("intro");
		introElement.addText(String.valueOf(media.getIntroduction()));
		Element yearElement = videoElement.addElement("year");
		yearElement.addText(String.valueOf(media.getYear()));
		Element entertainmentElement = videoElement.addElement("entertainment");
		entertainmentElement.addText(String.valueOf(media.getEntertainment()));
		Element indexElenment = videoElement.addElement("index");
		indexElenment.addText(String.valueOf(media.getShowIndex()));

		String xml = dom.asXML();
		System.out.println(xml);
		logger.info("MediaId:" + media.getId() + ";mediaFileName:" + mediaFileName + "; xml:" + xml);

		File file = new File(filePath + "/data_" + media.getCode() + "_en.xml");
		File parent = file.getParentFile();
		if (parent != null && !parent.exists()) {
			parent.mkdirs();
		}

		PrintWriter pw = null;
		try {
			pw = new PrintWriter(file, "utf-8");
			pw.write(xml);
		} catch (FileNotFoundException e) {
			logger.error("MediaId:" + media.getId() + ";createMediaXml error!", e);
			e.printStackTrace();
			throw e;
		} finally {
			if (pw != null)
				pw.close();
		}
	}

	/**
	 * 生成中文xml数据文件
	 * 
	 * @param media
	 * @throws IOException
	 */
	private void createMediaXmlCn(Media media) throws IOException {

		String mediaFileName = "/media_" + media.getCode();
		String deviceFileName = mediaFileName + "_" + media.getVersion();
		String filePath = mediaPath + mediaFileName;
		Document dom = DocumentHelper.createDocument();// 创建xml文件
		Element videoElement = dom.addElement("video");// 添加根元素
		videoElement.addAttribute("language", "CN");
		Element idElement = videoElement.addElement("id");
		idElement.addText(String.valueOf(media.getId()));
		Element codeElement = videoElement.addElement("code");
		codeElement.addText(String.valueOf(media.getCode()));
		Element versionElement = videoElement.addElement("version");
		versionElement.addText(media.getVersion());
		Element categorysElement = videoElement.addElement("categorys");
		for (MediaCategory mc : media.getCategorys()) {
			Element categoryElement = categorysElement.addElement("category");
			categoryElement.addAttribute("categoryId", String.valueOf(mc.getCategoryId()));
			categoryElement.addText(mc.getCategory().getTitleCn());
		}
		Element titleElement = videoElement.addElement("title");
		titleElement.addText(String.valueOf(media.getTitleCn()));
		Element mediaUrlElement = videoElement.addElement("videoUrl");
		mediaUrlElement.addText(String.valueOf("/video" + deviceFileName
				+ getFileName(media.getMediaUrl())));
		Element hPosterUrlElement = videoElement.addElement("hPosterUrl");
		hPosterUrlElement.addText(String.valueOf("/video" + deviceFileName
				+ getFileName(media.getHposterUrl())));
		Element vPosterUrlElement = videoElement.addElement("vPosterUrl");
		vPosterUrlElement.addText(String.valueOf("/video" + deviceFileName
				+ getFileName(media.getVposterUrl())));
		Element playLengthElement = videoElement.addElement("playLength");
		playLengthElement.addText(String.valueOf(media.getLength()));
		Element directorElement = videoElement.addElement("director");
		directorElement.addText(String.valueOf(media.getDirectorCn()));
		Element writerElement = videoElement.addElement("writer");
		writerElement.addText(String.valueOf(media.getWriterCn()));
		Element starringElement = videoElement.addElement("starring");
		starringElement.addText(String.valueOf(media.getStarringCn()));
		Element introElement = videoElement.addElement("intro");
		introElement.addText(String.valueOf(media.getIntroductionCn()));
		Element yearElement = videoElement.addElement("year");
		yearElement.addText(String.valueOf(media.getYear()));
		Element entertainmentElement = videoElement.addElement("entertainment");
		entertainmentElement.addText(String.valueOf(media.getEntertainmentCn()));
		Element indexElenment = videoElement.addElement("index");
		indexElenment.addText(String.valueOf(media.getShowIndex()));

		String xml = dom.asXML();
		System.out.println(xml);
		logger.info("MediaId:" + media.getId() + ";mediaFileName:" + mediaFileName + "; xml:" + xml);

		File file = new File(filePath + "/data_" + media.getCode() + "_cn.xml");
		File parent = file.getParentFile();
		if (parent != null && !parent.exists()) {
			parent.mkdirs();
		}

		PrintWriter pw = null;
		try {
			pw = new PrintWriter(file, "utf-8");
			pw.write(xml);
		} catch (FileNotFoundException e) {
			logger.error("MediaId:" + media.getId() + ";createMediaXml error!", e);
			e.printStackTrace();
			throw e;
		} finally {
			if (pw != null)
				pw.close();
		}
	}

	/**
	 * 获取文件名
	 * 
	 * @param url
	 * @return
	 */
	private String getFileName(String url) {

		if (StringUtils.isBlank(url) || url.indexOf("/") < 0)
			return url;

		return url.substring(url.lastIndexOf("/"), url.length());
	}

	/**
	 * 压缩文件夹
	 * 
	 * @param media
	 * @throws Exception
	 */
	private void zipMedia(Media media) throws Exception {

		String fileName = "media_" + media.getCode() + "_" + media.getVersion();
		String zipFileName = fileName + ".zip";
		String zipFilePath = zipPath + "/" + zipFileName;
		File file = new File(zipFilePath);

		logger.info("MediaId:" + media.getId() + "; zipMediaList:" + zipFilePath);

		// 判断是否存在
		if (!file.exists()) {
			// 如果不存在则生成压缩文件
			logger.info("MediaId:" + media.getId() + "; compress file:" + zipFilePath);
			ZipUtils.getInstance().compress(mediaPath + "/media_" + media.getCode(), zipFilePath,
					fileName);
		}
	}

	/**
	 * 当压缩出错时更新数据库信息
	 * 
	 * @param mediaVersion
	 */
	private void updateMediaVersionError(MediaVersion mediaVersion) {

		mediaVersion.setStatus(MediaVersion.MEDIA_ZIP_STATUS_ERROR);
		mediaVersion.setLastModify(new Date());
		mediaVersionDao.saveOrUpdate(mediaVersion);
	}

	/**
	 * 当压缩成功时更新数据库信息
	 * 
	 * @param mediaVersion
	 * @param zipFileName
	 */
	private void updateMediaVersionSuccess(MediaVersion mediaVersion, String zipFileName) {

		mediaVersion.setStatus(MediaVersion.MEDIA_ZIP_STATUS_SUCCESS);
		mediaVersion.setFileName(zipFileName);
		mediaVersion.setLastModify(new Date());
		mediaVersionDao.saveOrUpdate(mediaVersion);
	}
}
