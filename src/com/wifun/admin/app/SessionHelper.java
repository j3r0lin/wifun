package com.wifun.admin.app;

import java.util.HashMap;
import java.util.Map;

import net.sf.json.JSONObject;

import com.opensymphony.xwork2.ActionContext;
import com.wifun.admin.domain.SpringContextUtil;
import com.wifun.admin.domain.dao.ICompanyDao;
import com.wifun.admin.domain.dao.impl.CompanyDao;
import com.wifun.admin.domain.model.Company;
import com.wifun.admin.util.SysConfig;
import com.wifun.inhand.request.login.LoginRequest;
import com.wifun.inhand.response.login.LoginResponse;
import com.wifun.inhand.util.InhandHttpClient;

/**
 * 用户会话状态辅助类
 * 
 */
@SuppressWarnings("unchecked")
public final class SessionHelper {

	/**
	 * 设置当前会话键值
	 */
	public static void setSessionAttribute(String key, Object value) {

		ActionContext.getContext().getSession().put(key, value);
	}

	/**
	 * 获取当前会话里的值
	 */
	public static <T extends Object> T getSessionAttribute(String key) {

		return (T) ActionContext.getContext().getSession().get(key);
	}

	@SuppressWarnings("rawtypes")
	public static Map getSession() {

		return ActionContext.getContext().getSession();
	}

	/**
	 * 移除属性
	 */
	public static void removeSessionAttribute(String key) {

		ActionContext.getContext().getSession().remove(key);
	}

	/**
	 * 清空Session
	 */
	public static void clearSession() {

		getSession().clear();
	}

	/**
	 * 获取当前账户信息
	 */
	public static SessionAccount getCurrentAccount() {

		Object obj = getSessionAttribute(SessionAccount.SESSION_KEY);

		if (obj != null)
			return (SessionAccount) obj;
		else
			return null;
	}

	/**
	 * 获取当前公司的id
	 * 
	 * @return
	 */
	public static int getCompanyId() {

		Object obj = getSessionAttribute(SessionAccount.SESSION_KEY);
		if (obj != null)
			return ((SessionAccount) obj).getId();
		else
			return -1;
	}

	/**
	 * 获取当天用户角色类型
	 * 
	 * @return
	 */
	public static int getUserType() {

		Object obj = getSessionAttribute(SessionAccount.SESSION_KEY);
		if (obj != null)
			return ((SessionAccount) obj).getType();
		else
			return -1;
	}

	// 保存公司id对应的inhand接口token
	private static Map<Integer, LoginResponse> tokenMap = new HashMap<Integer, LoginResponse>();

	/**
	 * 根据公司id获取inhand接口token
	 * 
	 * @param companyId
	 * @return
	 */
	public static String getToken(int companyId) {

		LoginResponse token = null;
		if (tokenMap.containsKey(companyId)) {
			token = tokenMap.get(companyId);
			if (token != null && token.getExpires_end() - System.currentTimeMillis() > 1000) {
				return token.getAccess_token();
			}
		}
		if (companyId <= 1) {
			token = getSuperToken();
		} else {
			token = getCompanyTokenEx(companyId);
		}
		if (token != null) {
			tokenMap.put(companyId, token);
			return token.getAccess_token();
		}
		return null;
	}

	/**
	 * 调用inhand接口获取公司token
	 * 
	 * @param email
	 * @return
	 */
	public static String getToken(String email) {

		// 重启获取token
		LoginRequest lr = new LoginRequest();
		lr.setClientId(SysConfig.getSetting("inhand.api.company.clientId"));
		lr.setClientSecret(SysConfig.getSetting("inhand.api.company.clientSecret"));
		lr.setUserName(email);
		lr.setPassword(SysConfig.getSetting("inhand.api.company.password"));
		String rs = InhandHttpClient.getInstance().httpPost(lr);
		System.out.println(rs);

		LoginResponse loginInfo = null;
		if (null != rs) {
			JSONObject jsObject = JSONObject.fromObject(rs);

			loginInfo = new LoginResponse();
			loginInfo.fromJsonObject(jsObject);
			if (null != loginInfo.getError_code()) {
				return null;
			}
			loginInfo.setExpires_end(System.currentTimeMillis() + loginInfo.getExpires_in() * 1000);
		}

		return loginInfo.getAccess_token();
	}

	/**
	 * 添加账户信息
	 */
	public static void addAccount(SessionAccount account) {

		setSessionAttribute(SessionAccount.SESSION_KEY, account);
	}

	/**
	 * 获取超级管理员token
	 * 
	 * @return
	 */
	private static LoginResponse getSuperToken() {

		// 重启获取token
		LoginRequest lr = new LoginRequest();
		lr.setClientId(SysConfig.getSetting("inhand.api.company.clientId"));
		lr.setClientSecret(SysConfig.getSetting("inhand.api.company.clientSecret"));
		lr.setUserName("admin");
		lr.setPassword(SysConfig.getSetting("inhand.api.admin.password"));
		String rs = InhandHttpClient.getInstance().httpPost(lr);
		System.out.println(rs);

		LoginResponse loginInfo = null;
		if (null != rs) {
			JSONObject jsObject = JSONObject.fromObject(rs);

			loginInfo = new LoginResponse();
			loginInfo.fromJsonObject(jsObject);
			if (null != loginInfo.getError_code()) {
				return null;
			}
			loginInfo.setExpires_end(System.currentTimeMillis() + loginInfo.getExpires_in() * 1000);
		}

		return loginInfo;
	}

	/**
	 * 获取公司越权管理token
	 * 
	 * @param id
	 * @return
	 */
	private static LoginResponse getCompanyTokenEx(int id) {

		ICompanyDao companyDao = (CompanyDao) SpringContextUtil.getBean("companyDao");
		if (null == companyDao) {
			return null;
		}
		Company company = companyDao.get(id);
		// 重启获取token
		LoginRequest lr = new LoginRequest();
		lr.setClientId(SysConfig.getSetting("inhand.api.company.clientId"));
		lr.setClientSecret(SysConfig.getSetting("inhand.api.company.clientSecret"));
		lr.setUserName(company.getEmail());
		lr.setPassword(SysConfig.getSetting("inhand.api.company.password"));
		String rs = InhandHttpClient.getInstance().httpPost(lr);
		System.out.println(rs);

		LoginResponse loginInfo = null;
		if (null != rs) {
			JSONObject jsObject = JSONObject.fromObject(rs);

			loginInfo = new LoginResponse();
			loginInfo.fromJsonObject(jsObject);
			if (null != loginInfo.getError_code()) {
				return null;
			}
			loginInfo.setExpires_end(System.currentTimeMillis() + loginInfo.getExpires_in() * 1000);
		}

		return loginInfo;
	}
}
