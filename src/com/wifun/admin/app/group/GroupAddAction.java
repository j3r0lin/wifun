package com.wifun.admin.app.group;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.model.Group;
import com.wifun.admin.domain.service.IGroupService;

/**
 * 新增设备分组
 * 
 * @File wifun-admin/com.wifun.admin.app.group.GroupAddAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/group")
@ParentPackage(value = "admin-default")
public class GroupAddAction extends BaseAction {

	// Service
	@Resource(name = "groupService")
	private IGroupService groupService;

	// Parameters
	private String name;
	private String status;

	// Results
	private String msg;

	@Action(value = "group-add", results = { @Result(name = "success", location = "/jsp/group/group-add.jsp") })
	public String execute() throws Exception {

		super.putCompanyAllList();
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN == SessionHelper.getUserType()) {
			if (companyList != null && companyList.size() > 0)
				companyId = companyList.get(0).getId();
			else
				companyId = 0;
		} else {
			companyId = SessionHelper.getCompanyId();
		}
		return SUCCESS;
	}

	@Action(value = "group-add-save", results = {
			@Result(name = "error", location = "/jsp/group/group-save-error.jsp"),
			@Result(name = "success", location = "/jsp/group/group-save-ok.jsp") })
	public String save() throws Exception {

		if (SessionHelper.getUserType() != SessionAccount.SYSTEM_ROLE_TYPE_ADMIN) {
			companyId = SessionHelper.getCompanyId();
		}

		Group tmp = groupService.getGroupByName(companyId, name);
		if (tmp != null) {
			msg = "Duplicate group name!";
			return ERROR;
		}

		Group group = new Group(name, companyId, status, new Date());

		boolean flag = groupService.create(group);

		if (flag)
			return SUCCESS;
		else
			return ERROR;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String getStatus() {

		return status;
	}

	public void setStatus(String status) {

		this.status = status;
	}

	public String getMsg() {

		return msg;
	}

	public void setMsg(String msg) {

		this.msg = msg;
	}
}