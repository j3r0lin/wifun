package com.wifun.admin.app.group;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.ResponseHelper;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.model.Group;
import com.wifun.admin.domain.service.IGroupService;
import com.wifun.admin.util.JSONBean;

/**
 * 设备分组列表
 * 
 * @File wifun-admin/com.wifun.admin.app.group.GroupListAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/group")
@ParentPackage(value = "admin-default")
public class GroupListAction extends BaseAction {

	// Service
	@Resource(name = "groupService")
	private IGroupService groupService;

	// Parameters
	private String groupName; // /查询使用

	// Results
	private List<Group> groupList;

	@Action(value = "group-list", results = { @Result(name = "success", location = "/jsp/group/group-list.jsp") })
	public String execute() throws Exception {

		groupName = "";
		super.putCompanyAllList();
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN == SessionHelper.getUserType()) {
			if (companyList != null && companyList.size() > 0)
				companyId = companyList.get(0).getId();
			else
				companyId = 0;
		} else {
			companyId = SessionHelper.getCompanyId();
		}
		groupList = groupService.searchGroups(companyId, groupName);
		return SUCCESS;
	}

	@Action(value = "group-search", results = { @Result(name = "success", location = "/jsp/group/group-list.jsp") })
	public String search() throws Exception {

		super.putCompanyAllList();
		groupList = groupService.searchGroups(companyId, groupName);
		return SUCCESS;
	}

	@Action(value = "company-group-list")
	public void getGroupByCompanyId() throws Exception {

		List<Group> grouplist = new ArrayList<Group>();
		if (companyId != 0)
			grouplist = groupService.getGroupsByCompanyId(companyId);

		// 返回josn结果
		ResponseHelper.writeJsons(JSONBean.createJSONBean("groupList", grouplist).toString());
	}

	public List<Group> getGroupList() {

		return groupList;
	}

	public String getGroupName() {

		return groupName;
	}

	public void setGroupName(String groupName) {

		this.groupName = groupName;
	}
}