package com.wifun.admin.app.group;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.Group;
import com.wifun.admin.domain.service.IDeviceService;
import com.wifun.admin.domain.service.IGroupService;

/**
 * 修改设备分组
 * 
 * @File wifun-admin/com.wifun.admin.app.group.GroupEditAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/group")
@ParentPackage(value = "admin-default")
public class GroupEditAction extends BaseAction {

	// Service
	@Resource(name = "groupService")
	private IGroupService groupService;

	@Resource(name = "deviceService")
	private IDeviceService deviceService;

	// Parameters
	private Integer id;
	// Results
	private Group vo;
	private String msg;

	@Action(value = "group-edit", results = { @Result(name = "input", location = "/jsp/group/group-edit.jsp") })
	public String execute() throws Exception {

		vo = groupService.findById(id);
		return INPUT;
	}

	@Action(value = "group-edit-save", results = {
			@Result(name = "success", location = "/jsp/group/group-save-ok.jsp"),
			@Result(name = "error", location = "/jsp/group/group-save-error.jsp") })
	public String save() throws Exception {

		Group group = groupService.findById(id);
		group.setName(vo.getName());
		group.setStatus(vo.getStatus());

		Group tmp = groupService.getGroupByName(group.getCompanyId(), vo.getName());
		if (tmp != null && !id.equals(tmp.getId())) {
			msg = "Duplicate group name!";
			return ERROR;
		}

		boolean flag = groupService.update(group);

		if (flag)
			return SUCCESS;
		else
			return ERROR;
	}

	@Action(value = "group-delete", results = {
			@Result(name = "success", location = "/jsp/group/group-delete-ok.jsp"),
			@Result(name = "error", location = "/jsp/group/group-delete-error.jsp") })
	public String delete() throws Exception {

		List<Device> devices = deviceService.getDeviceList(companyId, id);
		if (devices != null && devices.size() > 0) {
			msg = "The group is not empty!";
			return ERROR;
		}
		try {
			groupService.deleteById(id);
			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}

	public Integer getId() {

		return id;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	public Group getVo() {

		return vo;
	}

	public void setVo(Group vo) {

		this.vo = vo;
	}

	public String getMsg() {

		return msg;
	}
}