package com.wifun.admin.app.category;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.domain.model.Category;
import com.wifun.admin.domain.service.ICategoryService;

/**
 * 电影分类修改
 * 
 * @File wifun-admin/com.wifun.admin.app.category.CategoryEditAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class CategoryEditAction extends ActionSupport {

	// Service
	@Resource(name = "categoryService")
	private ICategoryService categoryService;
	// Parameters
	private Integer id;
	// Results
	private Category bo;

	@Action(value = "category-edit", results = { @Result(name = "input", location = "/jsp/category/category-edit.jsp") })
	public String execute() throws Exception {

		bo = categoryService.findById(id);
		return INPUT;
	}

	@Action(value = "category-edit-save", results = {
			@Result(name = "success", location = "/jsp/category/category-save-ok.jsp"),
			@Result(name = "error", location = "/jsp/category/category-save-error.jsp") })
	public String save() throws Exception {

		boolean flag = categoryService.update(bo);

		if (flag)
			return SUCCESS;
		else
			return ERROR;
	}

	@Action(value = "category-delete", results = {
			@Result(name = "success", location = "/jsp/category/category-delete-ok.jsp"),
			@Result(name = "error", location = "/jsp/category/category-delete-error.jsp") })
	public String delete() throws Exception {

		if (null == id) {
			return ERROR;
		}

		try {
			categoryService.deleteById(id);
			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}

	public Integer getId() {

		return id;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	public Category getBo() {

		return bo;
	}

	public void setBo(Category bo) {

		this.bo = bo;
	}
}