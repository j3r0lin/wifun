package com.wifun.admin.app.category;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.domain.model.Category;
import com.wifun.admin.domain.service.ICategoryService;

/**
 * 电影分类列表
 * 
 * @File wifun-admin/com.wifun.admin.app.category.CategoryListAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class CategoryListAction extends ActionSupport {

	// Service
	@Resource(name = "categoryService")
	private ICategoryService categoryService;

	// Results
	private List<Category> categoryList;

	@Action(value = "category-list", results = { @Result(name = "success", location = "/jsp/category/category-list.jsp") })
	public String execute() throws Exception {

		categoryList = categoryService.loadAll();

		return SUCCESS;
	}

	public List<Category> getCategoryList() {

		return categoryList;
	}

	public void setCategoryList(List<Category> categoryList) {

		this.categoryList = categoryList;
	}

}