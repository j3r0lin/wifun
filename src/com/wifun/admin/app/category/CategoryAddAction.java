package com.wifun.admin.app.category;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.domain.model.Category;
import com.wifun.admin.domain.service.ICategoryService;

/**
 * 电影分类增加
 * 
 * @File wifun-admin/com.wifun.admin.app.category.CategoryAddAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class CategoryAddAction extends ActionSupport {

	// Service
	@Resource(name = "categoryService")
	private ICategoryService categoryService;

	// Parameters
	private Category bo;

	@Action(value = "category-add", results = { @Result(name = "success", location = "/jsp/category/category-add.jsp") })
	public String execute() throws Exception {

		return SUCCESS;
	}

	@Action(value = "category-add-save", results = {
			@Result(name = "error", location = "/jsp/category/category-save-error.jsp"),
			@Result(name = "success", location = "/jsp/category/category-save-ok.jsp") })
	public String save() throws Exception {

		if (null == bo)
			return ERROR;

		boolean flag = categoryService.create(bo);

		if (flag)
			return SUCCESS;
		else
			return ERROR;
	}

	public Category getBo() {

		return bo;
	}

	public void setBo(Category bo) {

		this.bo = bo;
	}
}