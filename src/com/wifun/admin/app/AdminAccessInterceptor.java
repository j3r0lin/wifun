package com.wifun.admin.app;

import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

/**
 * 后台用户权限拦截
 * 
 * @author ouda
 * 
 */
public class AdminAccessInterceptor implements Interceptor {

	private static final long serialVersionUID = 1L;

	public AdminAccessInterceptor() {

	}

	public void destroy() {

	}

	public void init() {

	}

	public String intercept(ActionInvocation arg0) throws Exception {

		return "adminReLogin";
	}
}
