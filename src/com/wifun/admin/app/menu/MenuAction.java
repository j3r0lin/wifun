package com.wifun.admin.app.menu;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;

/**
 * 系统菜单
 * 
 * @author ouda
 */
@SuppressWarnings("serial")
@ParentPackage(value = "admin-default")
public class MenuAction extends ActionSupport {

	// Parameters
	private int type; // 用户类型 1：系统管理员 2：公司管理员

	@Override
	@Action(value = "menu", results = {
			@Result(name = "company", location = "/jsp/menu/company-menu.jsp"),
			@Result(name = "system", location = "/jsp/menu/super-menu.jsp") })
	public String execute() throws Exception {

		// 根据账户的类型返回某个用户的界面
		int type = SessionHelper.getUserType();
		String result = "company";
		if (type == SessionAccount.SYSTEM_ROLE_TYPE_ADMIN) {
			result = "system";
		}

		return result;
	}

	public int getType() {

		return type;
	}

	public void setType(int type) {

		this.type = type;
	}
}