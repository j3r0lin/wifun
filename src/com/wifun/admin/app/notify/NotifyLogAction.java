package com.wifun.admin.app.notify;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.ResponseHelper;
import com.wifun.admin.domain.service.IMediaDeployService;
import com.wifun.admin.domain.service.INotifyLogService;

/**
 * 发布结果通知接口
 * 
 * @File wifun-admin/com.wifun.admin.app.notify.NotifyLogAction.java
 * @Desc 接收设备上php脚本发来的通知消息，包括媒体资源解压缩成功、媒体资源删除成功、vlist同步完成
 */
@SuppressWarnings("serial")
@Namespace(value = "/notify")
public class NotifyLogAction extends BaseAction {

	// Service
	@Resource(name = "notifyLogService")
	INotifyLogService notifyLogService;

	@Resource(name = "mediaDeployService")
	IMediaDeployService mediaDeployService;

	// Parameters
	private String version; // vlist版本号
	private String sn; // 设备的SN
	private int type; // 类型 0:媒体资源发布成功 1:媒体资源删除成功 2:同步结束
	private String media; // 媒体资源文件夹名称
	private int reasion; // vlist任务结束原因：0表示vlist下载成功； 1表示新vlist任务中止当前任务
	private String syncMode; // 同步模式，网络同步该参数为空，SD卡同步该参数为sd

	@Action(value = "notify-log")
	public void notifyLog() throws Exception {

		if (type == 2) {
			notifyLogService.saveVlistLog(version, sn, reasion, syncMode);
			mediaDeployService.updateDeployStatusByDeviceStatus(version);
		} else {
			notifyLogService.saveMediaLog(type, version, sn, media, reasion, syncMode);
		}

		System.out.println("SN:" + sn + "; type:" + type + "; version:" + version + "; media:"
				+ media + "; reasion:" + reasion);
		ResponseHelper.writeHtmlText("ok");
	}

	public void setVersion(String version) {

		this.version = version;
	}

	public void setSn(String sn) {

		this.sn = sn;
	}

	public void setType(int type) {

		this.type = type;
	}

	public void setMedia(String media) {

		this.media = media;
	}

	public void setReasion(int reasion) {

		this.reasion = reasion;
	}

	public void setSyncMode(String syncMode) {

		this.syncMode = syncMode;
	}
}
