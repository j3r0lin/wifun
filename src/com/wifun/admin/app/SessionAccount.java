package com.wifun.admin.app;

import java.util.Date;

public class SessionAccount {

	public static final int SYSTEM_ROLE_TYPE_ADMIN = 1;// 系统管理员
	public static final int COMPANY_ROLE_TYPE_ADMIN = 2;// 公司账户

	public static final String SESSION_KEY = "account_session";
	public static final int TOKEN_EXPIRE_TIME = 3600000; // token超时时间60分钟

	private Integer id;
	private String account;
	private Integer type;// 用户类型 1系统管理员 2公司管理员
	private String companyName;// 组织结构ID
	private Date lastLoginTime;
	private String inhandId; // 公司在inhand平台对应的id
	private String token;
	private Date expireTime;

	public Integer getId() {

		return id;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	public String getAccount() {

		return account;
	}

	public void setAccount(String account) {

		this.account = account;
	}

	public Integer getType() {

		return type;
	}

	public void setType(Integer type) {

		this.type = type;
	}

	public Date getLastLoginTime() {

		return lastLoginTime;
	}

	public void setLastLoginTime(Date lastLoginTime) {

		this.lastLoginTime = lastLoginTime;
	}

	public String getCompanyName() {

		return companyName;
	}

	public void setCompanyName(String companyName) {

		this.companyName = companyName;
	}

	public String getInhandId() {

		return inhandId;
	}

	public void setInhandId(String inhandId) {

		this.inhandId = inhandId;
	}

	public String getToken() {

		return token;
	}

	public void setToken(String token) {

		this.token = token;
	}

	public Date getExpireTime() {

		return expireTime;
	}

	public void setExpireTime(Date expireTime) {

		this.expireTime = expireTime;
	}
}
