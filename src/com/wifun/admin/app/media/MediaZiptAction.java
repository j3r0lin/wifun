package com.wifun.admin.app.media;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.app.ResponseHelper;
import com.wifun.admin.domain.MediaZipThread;
import com.wifun.admin.domain.model.Media;
import com.wifun.admin.domain.service.IMediaService;
import com.wifun.admin.util.JSONBean;

/**
 * 压缩媒体资源
 * 
 * @File wifun-admin/com.wifun.admin.app.media.MediaZiptAction.java
 * @Desc 首先在媒体资源文件夹中生成该媒体资源的xml数据文件，然后将媒体资源文件夹打成压缩包，打包成功后将压缩包上传至cdn
 *       媒体资源文件夹命名格式："media_"+媒体资源id 媒体资源压缩包命名格式：
 *       "media_"+媒体资源id+"_"+媒体资源版本号+".zip" 媒体资源版本：每次打包都会生成新的版本号
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class MediaZiptAction extends ActionSupport {

	// Service
	@Resource(name = "mediaService")
	private IMediaService mediaService;

	// Parameters
	private Integer id;

	@Action(value = "media-zip")
	public void zipMedia() throws Exception {

		Media media = mediaService.findById(id);

		if (StringUtils.isBlank(media.getMediaUrl())) {
			ResponseHelper.writeJsons(JSONBean.createJSONBean("msg", "Please select media file!")
					.toString());
			return;
		}

		if (StringUtils.isBlank(media.getHposterUrl())) {
			ResponseHelper.writeJsons(JSONBean.createJSONBean("msg", "Please select hposter file!")
					.toString());
			return;
		}

		if (StringUtils.isBlank(media.getVposterUrl())) {
			ResponseHelper.writeJsons(JSONBean.createJSONBean("msg", "Please select vposter file!")
					.toString());
			return;
		}

		// 由于压缩时间较长，每次启动独立线程执行压缩操作
		MediaZipThread thread = new MediaZipThread(media);
		thread.start();

		ResponseHelper.writeJsons(JSONBean.createJSONBean("code", "0").toString());
	}

	public Integer getId() {

		return id;
	}

	public void setId(Integer id) {

		this.id = id;
	}
}