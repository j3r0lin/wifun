package com.wifun.admin.app.media;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.domain.model.Media;
import com.wifun.admin.domain.service.IMediaListService;
import com.wifun.admin.domain.service.IMediaService;

/**
 * 修改媒体列表中选择的媒体资源
 * 
 * @File wifun-admin/com.wifun.admin.app.media.MediaListEditMediaAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class MediaListEditMediaAction extends ActionSupport {

	// Service
	@Resource(name = "mediaService")
	private IMediaService mediaService;

	@Resource(name = "mediaListService")
	private IMediaListService mediaListService;
	// Parameters
	private Integer mlid; // /MediaListId
	private Integer mid; // //mediaId
	private List<Integer> mediaIdList;
	private List<Media> mediaList;

	@Action(value = "media-list-add-media", results = {
			@Result(name = "success", location = "/jsp/media-list/media-list-add-media.jsp"),
			@Result(name = "error", location = "/jsp/media-list/media-list-detail-error.jsp") })
	public String execute() throws Exception {

		if (null == mlid)
			return ERROR;

		mediaList = new ArrayList<Media>();
		List<Media> list = mediaService.loadAvailableMedia(mlid);
		// 删除没有打包的视频
		for (int i = 0; i < list.size(); i++) {
			Media media = list.get(i);
			if (StringUtils.isNotBlank(media.getVersion())) {
				mediaList.add(media);
			}
		}

		return SUCCESS;
	}

	@Action(value = "media-list-add-media-save", results = {
			@Result(name = "success", location = "/jsp/media-list/media-list-detail-success.jsp"),
			@Result(name = "error", location = "/jsp/media-list/media-list-detail-error.jsp") })
	public String save() throws Exception {

		if (null == mlid)
			return ERROR;

		boolean flag = mediaListService.addMedias(mlid, mediaIdList);

		if (flag)
			return SUCCESS;
		else
			return ERROR;
	}

	@Action(value = "media-list-delete-media", results = {
			@Result(name = "success", location = "/jsp/media-list/media-list-detail-success.jsp"),
			@Result(name = "error", location = "/jsp/media-list/media-list-detail-error.jsp") })
	public String delete() throws Exception {

		if (null == mlid || null == mid)
			return ERROR;

		boolean flag = mediaListService.removeMedia(mlid, mid);

		if (flag)
			return SUCCESS;
		else
			return ERROR;
	}

	public List<Media> getMediaList() {

		return mediaList;
	}

	public void setMediaList(List<Media> mediaList) {

		this.mediaList = mediaList;
	}

	public List<Integer> getMediaIdList() {

		return mediaIdList;
	}

	public void setMediaIdList(List<Integer> mediaIdList) {

		this.mediaIdList = mediaIdList;
	}

	public Integer getMlid() {

		return mlid;
	}

	public void setMlid(Integer mlid) {

		this.mlid = mlid;
	}

	public Integer getMid() {

		return mid;
	}

	public void setMid(Integer mid) {

		this.mid = mid;
	}
}