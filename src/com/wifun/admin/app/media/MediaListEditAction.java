package com.wifun.admin.app.media;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.app.ResponseHelper;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.model.MediaList;
import com.wifun.admin.domain.service.ICompanyService;
import com.wifun.admin.domain.service.IMediaListService;
import com.wifun.admin.util.JSONBean;

/**
 * 修改媒体列表
 * 
 * @File wifun-admin/com.wifun.admin.app.media.MediaListEditAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class MediaListEditAction extends ActionSupport {

	// Service
	@Resource(name = "mediaListService")
	private IMediaListService mediaListService;
	@Resource(name = "companyService")
	private ICompanyService companyService;

	// Parameters
	private Integer id;
	private int companyId;
	// Results
	private MediaList bo;
	private List<Company> companyLsit;

	@Action(value = "media-list-edit", results = {
			@Result(name = "noright", location = "/jsp/global/noright.jsp"),
			@Result(name = "input", location = "/jsp/media-list/media-list-edit.jsp") })
	public String execute() throws Exception {

		companyLsit = companyService.getCompanyAllList();
		bo = mediaListService.findById(id);
		return INPUT;
	}

	@Action(value = "media-list-edit-save", results = {
			@Result(name = "success", location = "/jsp/media-list/media-list-save-ok.jsp"),
			@Result(name = "error", location = "/jsp/media-list/media-list-save-error.jsp") })
	public String save() throws Exception {

		if (SessionHelper.getUserType() == SessionAccount.COMPANY_ROLE_TYPE_ADMIN) {
			companyId = SessionHelper.getCompanyId();
		}
		boolean flag = mediaListService.updateMediaList(id, bo, companyId);

		if (flag)
			return SUCCESS;
		else
			return ERROR;
	}

	@Action(value = "media-list-delete")
	public void delete() throws Exception {

		// 0,系统错误,1 成功,3参数错误
		int result = 0;
		try {
			if (null != id) {
				boolean flag = mediaListService.deleteMediaList(id);
				if (flag) {
					result = 1;
				}
			}
		} catch (Exception e) {
		}

		// 返回josn结果
		ResponseHelper.writeJsons(JSONBean.createJSONBean("result", result).toString());

	}

	public Integer getId() {

		return id;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	public MediaList getBo() {

		return bo;
	}

	public void setBo(MediaList bo) {

		this.bo = bo;
	}

	public List<Company> getCompanyLsit() {

		return companyLsit;
	}

	public void setCompanyId(int companyId) {

		this.companyId = companyId;
	}

}