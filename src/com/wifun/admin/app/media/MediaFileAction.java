package com.wifun.admin.app.media;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.app.ResponseHelper;
import com.wifun.admin.domain.service.IMediaService;
import com.wifun.admin.util.JSONBean;

/**
 * 媒体资源文件上传
 * 
 * @File wifun-admin/com.wifun.admin.app.media.MediaFileAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class MediaFileAction extends ActionSupport {

	// Service
	@Resource(name = "mediaService")
	private IMediaService mediaService;

	// Parameters
	private int id; // 媒体资源id
	private int type; // 上传的资源类型 0：视频文件 1：横版海报图片 2：竖版海报图片
	private String path; // 路径

	@Action(value = "media-file-select")
	public void selectFile() throws Exception {

		boolean flag = false;
		if (type == 0) {
			flag = mediaService.updateMovieUrl(id, path);
		} else if (type == 1) {
			flag = mediaService.updateHPosterUrl(id, path);
		} else if (type == 2) {
			flag = mediaService.updateVPosterUrl(id, path);
		}

		if (flag) {
			ResponseHelper.writeJsons(JSONBean.createJSONBean("result", 0).toString());
		} else {
			ResponseHelper.writeJsons(JSONBean.createJSONBean("result", 1).toString());
		}

	}

	public int getId() {

		return id;
	}

	public void setId(int id) {

		this.id = id;
	}

	public int getType() {

		return type;
	}

	public void setType(int type) {

		this.type = type;
	}

	public String getPath() {

		return path;
	}

	public void setPath(String path) {

		this.path = path;
	}
}