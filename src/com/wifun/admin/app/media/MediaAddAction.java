package com.wifun.admin.app.media;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.domain.model.Category;
import com.wifun.admin.domain.model.Media;
import com.wifun.admin.domain.service.ICategoryService;
import com.wifun.admin.domain.service.IMediaService;

/**
 * 新增媒体资源
 * 
 * @File wifun-admin/com.wifun.admin.app.media.MediaAddAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class MediaAddAction extends ActionSupport {

	// Service
	@Resource(name = "mediaService")
	private IMediaService mediaService;

	@Resource(name = "categoryService")
	private ICategoryService categoryService;

	// Parameters
	private Media vo;

	private List<Category> categoryList;
	private List<Integer> mediaCategory;

	@Action(value = "media-add", results = { @Result(name = "success", location = "/jsp/media/media-add.jsp") })
	public String execute() throws Exception {

		categoryList = categoryService.getCategoryList(true);

		return SUCCESS;
	}

	@Action(value = "media-add-save", results = {
			@Result(name = "error", location = "/jsp/media/media-save-error.jsp"),
			@Result(name = "success", location = "/jsp/media/media-save-ok.jsp") })
	public String save() throws Exception {

		if (null == vo)
			return ERROR;

		boolean flag = mediaService.create(vo);

		if (flag) {
			if (null != mediaCategory && mediaCategory.size() > 0) {
				mediaService.saveCategoryList(vo.getId(), mediaCategory);
			}
			return SUCCESS;
		} else
			return ERROR;
	}

	public Media getVo() {

		return vo;
	}

	public void setVo(Media vo) {

		this.vo = vo;
	}

	public List<Category> getCategoryList() {

		return categoryList;
	}

	public void setCategoryList(List<Category> categoryList) {

		this.categoryList = categoryList;
	}

	public List<Integer> getMediaCategory() {

		return mediaCategory;
	}

	public void setMediaCategory(List<Integer> mediaCategory) {

		this.mediaCategory = mediaCategory;
	}
}