package com.wifun.admin.app.media;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.domain.model.Category;
import com.wifun.admin.domain.model.Media;
import com.wifun.admin.domain.service.ICategoryService;
import com.wifun.admin.domain.service.IMediaService;

/**
 * 根据分类和标题查询媒体资源
 * 
 * @File wifun-admin/com.wifun.admin.app.media.MediaListAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class MediaListAction extends ActionSupport {

	// Service
	@Resource(name = "mediaService")
	private IMediaService mediaService;
	@Resource(name = "categoryService")
	private ICategoryService categoryService;
	// Parameters
	private Integer categoryId;
	private String title;
	// Results
	private List<Media> mediaList;
	private List<Category> categoryList;

	@Action(value = "media-list", results = { @Result(name = "success", location = "/jsp/media/media-list.jsp") })
	public String execute() throws Exception {

		categoryList = categoryService.loadAll();
		mediaList = mediaService.loadAll();
		return SUCCESS;
	}

	@Action(value = "media-search", results = { @Result(name = "success", location = "/jsp/media/media-list.jsp") })
	public String search() throws Exception {

		categoryList = categoryService.loadAll();
		if (categoryId == 0 && StringUtils.isBlank(title))
			mediaList = mediaService.loadAll();
		else
			mediaList = mediaService.search(categoryId, title);

		return SUCCESS;
	}

	public List<Media> getMediaList() {

		return mediaList;
	}

	public Integer getCategoryId() {

		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {

		this.categoryId = categoryId;
	}

	public String getTitle() {

		return title;
	}

	public void setTitle(String title) {

		this.title = title;
	}

	public List<Category> getCategoryList() {

		return categoryList;
	}

	public void setMediaList(List<Media> mediaList) {

		this.mediaList = mediaList;
	}

	public void setCategoryList(List<Category> categoryList) {

		this.categoryList = categoryList;
	}
}