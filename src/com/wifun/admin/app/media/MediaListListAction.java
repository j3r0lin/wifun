package com.wifun.admin.app.media;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.ResponseHelper;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.model.MediaList;
import com.wifun.admin.domain.service.IMediaListService;
import com.wifun.admin.util.JSONBean;

/**
 * 查询媒体列表
 * 
 * @File wifun-admin/com.wifun.admin.app.media.MediaListListAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class MediaListListAction extends BaseAction {

	// Service
	@Resource(name = "mediaListService")
	private IMediaListService mediaListService;

	// Parameters
	private String title;

	// Results
	private List<MediaList> list;

	@Action(value = "media-list-list", results = { @Result(name = "success", location = "/jsp/media-list/media-list-list.jsp") })
	public String execute() throws Exception {

		if (SessionHelper.getUserType() == SessionAccount.SYSTEM_ROLE_TYPE_ADMIN) {
			getAdminList();
		} else {
			getCompanyMediaList();
		}

		return SUCCESS;
	}

	@Action(value = "company-media-list")
	public void getMediaListByCompanyId() throws Exception {

		List<MediaList> list = mediaListService.getCompanyMedias("", companyId);

		// 返回josn结果
		ResponseHelper.writeJsons(JSONBean.createJSONBean("list", list).toString());
	}

	private void getAdminList() {

		list = mediaListService.getAllMedias(title);
	}

	private void getCompanyMediaList() {

		companyId = SessionHelper.getCompanyId();
		list = mediaListService.getCompanyMedias(title, companyId);
	}

	public List<MediaList> getList() {

		return list;
	}

	public String getTitle() {

		return title;
	}

	public void setTitle(String title) {

		this.title = title;
	}
}