package com.wifun.admin.app.media;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.domain.model.Media;
import com.wifun.admin.domain.service.IMediaService;

/**
 * 查询媒体列表中包含的媒体资源
 * 
 * @File wifun-admin/com.wifun.admin.app.media.MediaListViewAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class MediaListViewAction extends ActionSupport {

	// Service
	@Resource(name = "mediaService")
	private IMediaService mediaService;
	// Parameters
	private Integer mlid;
	// Results
	private List<Media> mediaList;

	@Action(value = "media-list-detail", results = {
			@Result(name = "success", location = "/jsp/media-list/media-list-detail.jsp"),
			@Result(name = "error", location = "/jsp/media-list/media-list-detail-error.jsp") })
	public String execute() throws Exception {

		if (null == mlid)
			return ERROR;

		mediaList = mediaService.loadMediaListDetail(mlid);
		return SUCCESS;
	}

	public List<Media> getMediaList() {

		return mediaList;
	}

	public void setMediaList(List<Media> mediaList) {

		this.mediaList = mediaList;
	}

	public Integer getMlid() {

		return mlid;
	}

	public void setMlid(Integer mlid) {

		this.mlid = mlid;
	}
}