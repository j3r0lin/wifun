package com.wifun.admin.app.media;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.model.MediaList;
import com.wifun.admin.domain.service.IMediaListService;

/**
 * 新增媒体列表
 * 
 * @File wifun-admin/com.wifun.admin.app.media.MediaListAddAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class MediaListAddAction extends BaseAction {

	// Service
	@Resource(name = "mediaListService")
	private IMediaListService mediaListService;

	// Parameters
	private MediaList bo;

	@Action(value = "media-list-add", results = { @Result(name = "success", location = "/jsp/media-list/media-list-add.jsp") })
	public String execute() throws Exception {

		super.putCompanyAllList();
		companyId = SessionHelper.getCompanyId();
		return SUCCESS;
	}

	@Action(value = "media-list-add-save", results = {
			@Result(name = "error", location = "/jsp/media-list/media-list-save-error.jsp"),
			@Result(name = "success", location = "/jsp/media-list/media-list-save-ok.jsp") })
	public String save() throws Exception {

		int creator = 0;

		if (SessionHelper.getUserType() == SessionAccount.COMPANY_ROLE_TYPE_ADMIN) {

			creator = SessionHelper.getCompanyId();
			companyId = SessionHelper.getCompanyId();
		} else if (SessionHelper.getUserType() == SessionAccount.SYSTEM_ROLE_TYPE_ADMIN) {

			creator = 1;// 系统管理员
		}
		boolean flag = mediaListService.saveMediaList(bo, companyId, creator);

		if (flag)
			return SUCCESS;
		else
			return ERROR;
	}

	public void setBo(MediaList bo) {

		this.bo = bo;
	}
}