package com.wifun.admin.app.media;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.app.ResponseHelper;
import com.wifun.admin.domain.model.Category;
import com.wifun.admin.domain.model.Media;
import com.wifun.admin.domain.model.MediaCategory;
import com.wifun.admin.domain.service.ICategoryService;
import com.wifun.admin.domain.service.IMediaService;

/**
 * 修改媒体资源
 * 
 * @File wifun-admin/com.wifun.admin.app.media.MediaEditAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class MediaEditAction extends ActionSupport {

	// Service
	@Resource(name = "mediaService")
	private IMediaService mediaService;

	@Resource(name = "categoryService")
	private ICategoryService categoryService;

	// Parameters
	private Integer id;
	// Results
	private Media vo;
	private List<Integer> mediaCategory;
	private List<Category> categoryList;
	private String[] years;

	@Action(value = "media-edit", results = {
			@Result(name = "noright", location = "/jsp/global/noright.jsp"),
			@Result(name = "input", location = "/jsp/media/media-edit.jsp") })
	public String execute() throws Exception {

		vo = mediaService.findById(id);
		List<Integer> categorys = new ArrayList<Integer>();

		for (MediaCategory mc : vo.getCategorys()) {
			categorys.add(mc.getCategoryId());
		}

		categoryList = categoryService.getCategoryList(true);
		for (int i = 0; i < categoryList.size(); i++) {
			if (categorys.contains(categoryList.get(i).getId())) {
				categoryList.get(i).setCheck("selected");
			}
		}
		years = new String[16];
		for (int i = 0; i < years.length; i++) {
			years[i] = String.valueOf(2000 + i);
		}
		return INPUT;
	}

	@Action(value = "media-edit-save", results = {
			@Result(name = "success", location = "/jsp/media/media-save-ok.jsp"),
			@Result(name = "error", location = "/jsp/media/media-save-error.jsp") })
	public String save() throws Exception {

		if (null != mediaCategory && mediaCategory.size() > 0) {
			mediaService.saveCategoryList(vo.getId(), mediaCategory);
		}

		boolean flag = mediaService.update(vo);

		if (flag)
			return SUCCESS;
		else
			return ERROR;
	}

	@Action(value = "media-delete", results = {
			@Result(name = "success", location = "/jsp/media/media-delete-ok.jsp"),
			@Result(name = "error", location = "/jsp/media/media-delete-error.jsp") })
	public String delete() throws Exception {

		if (null == id) {
			return ERROR;
		}

		try {
			mediaService.deleteById(id);
			return SUCCESS;
		} catch (Exception e) {
			return ERROR;
		}
	}

	@Action(value = "media-url-confirm")
	public void updateurl() throws Exception {

		if (0 == vo.getId()) {
			ResponseHelper.writeString(ERROR, "text");
			return;
		}

		boolean flag = false;
		if (null != vo.getMediaUrl()) {
			flag = mediaService.updateMovieUrl(vo.getId(), vo.getMediaUrl());
		} else if (null != vo.getHposterUrl()) {
			flag = mediaService.updateHPosterUrl(vo.getId(), vo.getHposterUrl());
		} else if (null != vo.getVposterUrl()) {
			flag = mediaService.updateVPosterUrl(vo.getId(), vo.getVposterUrl());
		}

		if (flag) {
			ResponseHelper.writeString(SUCCESS, "text");
		} else {
			ResponseHelper.writeString(ERROR, "text");
		}
	}

	public Integer getId() {

		return id;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	public Media getVo() {

		return vo;
	}

	public void setVo(Media vo) {

		this.vo = vo;
	}

	public List<Integer> getMediaCategory() {

		return mediaCategory;
	}

	public void setMediaCategory(List<Integer> mediaCategory) {

		this.mediaCategory = mediaCategory;
	}

	public List<Category> getCategoryList() {

		return categoryList;
	}

	public void setCategoryList(List<Category> categoryList) {

		this.categoryList = categoryList;
	}

	public String[] getYears() {

		return years;
	}

	public void setYears(String[] years) {

		this.years = years;
	}
}