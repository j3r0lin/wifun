package com.wifun.admin.app.portal;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.PortalDeploy;
import com.wifun.admin.domain.service.IDeviceService;
import com.wifun.admin.domain.service.IPortalDeployService;

@SuppressWarnings("serial")
@Namespace(value = "/portal")
@ParentPackage(value = "admin-default")
public class PortalDeployAddAction extends BaseAction {

	// Service
	@Resource(name = "portalDeployService")
	private IPortalDeployService portalDeployService;

	@Resource(name = "deviceService")
	private IDeviceService deviceService;

	// Parameters
	private PortalDeploy portalDeploy;

	// Results
	private String msg;

	@Action(value = "portal-deploy-add", results = { @Result(name = "success", location = "/jsp/portal/portal-deploy-add.jsp") })
	public String execute() throws Exception {

		super.putCompanyActiveList();
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN == SessionHelper.getUserType()) {
			if (companyList != null && companyList.size() > 0)
				companyId = companyList.get(0).getId();
			else
				companyId = 0;
		} else {
			companyId = SessionHelper.getCompanyId();
		}
		return SUCCESS;
	}

	@Action(value = "portal-deploy-save", results = {
			@Result(name = "error", location = "/jsp/portal/portal-deploy-error.jsp"),
			@Result(name = "success", location = "/jsp/portal/portal-deploy-ok.jsp") })
	public String save() throws Exception {

		if (null == portalDeploy)
			return ERROR;

		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType()) {
			companyId = SessionHelper.getCompanyId();
			portalDeploy.setCompanyId(companyId);
		}

		List<Device> devices = deviceService.getDeviceList(portalDeploy.getCompanyId(),
				portalDeploy.getGroupId());

		if (portalDeploy.getPortalId() == null || portalDeploy.getPortalId() == 0) {
			msg = "Please select portal.";
			return ERROR;
		}

		if (devices == null || devices.size() == 0) {
			msg = "No device at the group.";
			return ERROR;
		}

		boolean flag = portalDeployService.deploy(portalDeploy);

		if (flag)
			return SUCCESS;
		else
			return ERROR;
	}

	public PortalDeploy getPortalDeploy() {

		return portalDeploy;
	}

	public void setPortalDeploy(PortalDeploy portalDeploy) {

		this.portalDeploy = portalDeploy;
	}

	public String getMsg() {

		return msg;
	}
}