package com.wifun.admin.app.portal;

import java.io.File;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.service.IPortalService;

@SuppressWarnings("serial")
@Namespace(value = "/portal")
@ParentPackage(value = "admin-default")
public class PortalAddAction extends BaseAction {

	// Service
	@Resource(name = "portalService")
	private IPortalService portalService;

	// Parameters
	private File portalPackage;
	private String portalName;

	// Results

	@Action(value = "portal-add", results = { @Result(name = "success", location = "/jsp/portal/portal-add.jsp") })
	public String execute() throws Exception {

		super.putCompanyAllList();
		companyId = SessionHelper.getCompanyId();
		return SUCCESS;
	}

	@Action(value = "portal-add-save", results = {
			@Result(name = "error", location = "/jsp/portal/portal-save-error.jsp"),
			@Result(name = "success", location = "/jsp/portal/portal-save-ok.jsp") })
	public String save() throws Exception {

		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType()) {
			companyId = SessionHelper.getCompanyId();
		}

		boolean flag = portalService.savePortalPackage(companyId, portalName, portalPackage);

		if (flag)
			return SUCCESS;
		else
			return ERROR;
	}

	public String getPortalName() {

		return portalName;
	}

	public void setPortalName(String portalName) {

		this.portalName = portalName;
	}

	public File getPortalPackage() {

		return portalPackage;
	}

	public void setPortalPackage(File portalPackage) {

		this.portalPackage = portalPackage;
	}

}