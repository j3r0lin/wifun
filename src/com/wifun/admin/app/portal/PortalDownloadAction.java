package com.wifun.admin.app.portal;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.model.Portal;
import com.wifun.admin.domain.service.IPortalService;

@SuppressWarnings("serial")
@Namespace(value = "/portal")
@ParentPackage(value = "admin-default")
public class PortalDownloadAction extends ActionSupport {

	// Service
	@Resource(name = "portalService")
	private IPortalService portalService;

	// Parameters
	private int id;
	private Portal portal;

	String filepath;

	@Action(value = "portal-download", results = {
			@Result(name = "success", type = "stream", params = { "contentType",
					"application/octet-stream;charset=utf-8", "inputName", "inputStream",
					"contentDisposition", "attachment;filename=\"${fileName}\"", "bufferSize",
					"4096" }),
			@Result(name = "error", location = "/jsp/portal/portal-download-error.jsp") })
	@Override
	public String execute() throws Exception {

		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType())
			return ERROR;

		portal = portalService.findById(id);

		if (portal == null)
			return ERROR;

		filepath = portal.getPackageUrl();
		File file = new File(filepath);
		if (!file.exists())
			return ERROR;

		return SUCCESS;
	}

	public InputStream getInputStream() throws Exception {

		return new FileInputStream(filepath);
	}

	public String getFileName() {

		String fileName = portal.getName() + ".zip";
		return fileName;
	}

	public void setId(Integer id) {

		this.id = id;
	}
}