package com.wifun.admin.app.portal;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.domain.service.IPortalService;

@SuppressWarnings("serial")
@Namespace(value = "/portal")
@ParentPackage(value = "admin-default")
public class PortalEditAction extends ActionSupport {

	// Service
	@Resource(name = "portalService")
	private IPortalService portalService;
	// Parameters
	private Integer id;

	@Action(value = "portal-pause", results = {
			@Result(name = "success", location = "/jsp/portal/portal-edit-ok.jsp"),
			@Result(name = "error", location = "/jsp/portal/portal-edit-error.jsp") })
	public String pause() throws Exception {

		if (!portalService.updateStatus(id, false))
			return ERROR;

		return SUCCESS;
	}

	@Action(value = "portal-active", results = {
			@Result(name = "success", location = "/jsp/portal/portal-edit-ok.jsp"),
			@Result(name = "error", location = "/jsp/portal/portal-edit-error.jsp") })
	public String active() throws Exception {

		if (!portalService.updateStatus(id, true))
			return ERROR;

		return SUCCESS;
	}

	@Action(value = "portal-delete", results = {
			@Result(name = "success", location = "/jsp/portal/portal-delete-ok.jsp"),
			@Result(name = "error", location = "/jsp/portal/portal-delete-error.jsp") })
	public String delete() throws Exception {

		if (null == id) {
			return ERROR;
		}

		try {
			portalService.deleteById(id);
			return SUCCESS;
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;
		}
	}

	public Integer getId() {

		return id;
	}

	public void setId(Integer id) {

		this.id = id;
	}
}