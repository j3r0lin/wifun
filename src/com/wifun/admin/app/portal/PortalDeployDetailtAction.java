package com.wifun.admin.app.portal;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.domain.model.PortalDeployDetail;
import com.wifun.admin.domain.service.IPortalDeployDetailService;

@Namespace(value = "/portal")
@ParentPackage(value = "admin-default")
public class PortalDeployDetailtAction extends ActionSupport {

	// Service
	@Resource(name = "portalDeployDetailService")
	private IPortalDeployDetailService portalDeployDetailService;
	// Parameters
	private Integer deployId;
	// Results
	private List<PortalDeployDetail> detialList;

	@Action(value = "portal-deploy-detail", results = {
			@Result(name = "success", location = "/jsp/portal/portal-deploy-detail.jsp"),
			@Result(name = "error", location = "/jsp/portal/portal-deploy-detail-error.jsp") })
	public String execute() throws Exception {

		if (null == deployId)
			return ERROR;

		detialList = portalDeployDetailService.loadByDeployId(deployId);
		return SUCCESS;
	}

	public Integer getDeployId() {

		return deployId;
	}

	public void setDeployId(Integer deployId) {

		this.deployId = deployId;
	}

	public List<PortalDeployDetail> getDetialList() {

		return detialList;
	}

	public void setDetialList(List<PortalDeployDetail> detialList) {

		this.detialList = detialList;
	}

}