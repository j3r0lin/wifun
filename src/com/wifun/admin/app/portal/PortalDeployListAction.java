package com.wifun.admin.app.portal;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.model.PortalDeploy;
import com.wifun.admin.domain.service.IPortalDeployService;

@SuppressWarnings("serial")
@Namespace(value = "/portal")
@ParentPackage(value = "admin-default")
public class PortalDeployListAction extends BaseAction {

	// Service
	@Resource(name = "portalDeployService")
	private IPortalDeployService portalDeployService;
	// Parameters
	private String description;
	private String startDate;
	private String endDate;

	// Results
	private List<PortalDeploy> deployList;

	@Action(value = "portal-deploy-list", results = { @Result(name = "success", location = "/jsp/portal/portal-deploy-list.jsp") })
	public String execute() throws Exception {

		super.putCompanyAllList();
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN == SessionHelper.getUserType()) {
			if (companyList != null && companyList.size() > 0)
				companyId = companyList.get(0).getId();
			else
				companyId = 0;
		} else {
			companyId = SessionHelper.getCompanyId();
		}
		deployList = portalDeployService.searchPortal(companyId, description, startDate, endDate);

		return SUCCESS;
	}

	@Action(value = "portal-deploy-search", results = { @Result(name = "success", location = "/jsp/portal/portal-deploy-list.jsp") })
	public String search() throws Exception {

		super.putCompanyAllList();
		deployList = portalDeployService.searchPortal(companyId, description, startDate, endDate);

		return SUCCESS;
	}

	public List<PortalDeploy> getDeployList() {

		return deployList;
	}

	public String getDescription() {

		return description;
	}

	public void setDescription(String description) {

		this.description = description;
	}

	public String getStartDate() {

		return startDate;
	}

	public void setStartDate(String startDate) {

		this.startDate = startDate;
	}

	public String getEndDate() {

		return endDate;
	}

	public void setEndDate(String endDate) {

		this.endDate = endDate;
	}

}