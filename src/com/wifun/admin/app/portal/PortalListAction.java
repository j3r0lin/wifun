package com.wifun.admin.app.portal;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.ResponseHelper;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.model.Portal;
import com.wifun.admin.domain.service.IPortalService;
import com.wifun.admin.util.JSONBean;

@SuppressWarnings("serial")
@Namespace(value = "/portal")
@ParentPackage(value = "admin-default")
public class PortalListAction extends BaseAction {

	// Service
	@Resource(name = "portalService")
	private IPortalService portalService;

	// Parameters
	private String portalName; // 查询使用

	// Results
	private List<Portal> portalList;

	@Action(value = "portal-list", results = { @Result(name = "success", location = "/jsp/portal/portal-list.jsp") })
	public String execute() throws Exception {

		super.putCompanyAllList();
		companyId = SessionHelper.getCompanyId();
		portalList = portalService.searchPortal(companyId, portalName);
		return SUCCESS;
	}

	@Action(value = "portal-search", results = { @Result(name = "success", location = "/jsp/portal/portal-list.jsp") })
	public String search() throws Exception {

		super.putCompanyAllList();
		portalList = portalService.searchPortal(companyId, portalName);
		return SUCCESS;
	}

	@Action(value = "company-portal-list")
	public void portalList() throws Exception {

		List<Portal> portalList1 = portalService.getPortalByCompanyId(1);
		List<Portal> portalList2 = portalService.getPortalByCompanyId(companyId);
		if (portalList1 == null)
			portalList1 = new ArrayList<Portal>();

		portalList1.addAll(portalList2);

		JSONBean j = JSONBean.createJSONBean();
		j.add("portalList", portalList1);
		// 返回josn结果
		ResponseHelper.writeJsons(j.toString());
	}

	public List<Portal> getPortalList() {

		return portalList;
	}

	public String getPortalName() {

		return portalName;
	}

	public void setPortalName(String portalName) {

		this.portalName = portalName;
	}
}