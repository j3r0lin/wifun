package com.wifun.admin.app.system;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.app.ResponseHelper;
import com.wifun.admin.app.vo.FileVo;
import com.wifun.admin.util.FileUtil;
import com.wifun.admin.util.JSONBean;
import com.wifun.admin.util.SysConfig;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;

import java.io.File;
import java.util.List;

@SuppressWarnings("serial")
@Namespace(value = "/file")
public class FileListAction extends ActionSupport {

    // Parameters
    private String filePath;
    private Integer movieId;
    private String type; // //标识打开文件类别；0：movie, 1:hposter, 2: vposter

    // result
    private List<FileVo> filelist;

    @Override
    @Action(value = "file-list", results = {
            @Result(name = "input", location = "/jsp/system/file-list.jsp"),
            @Result(name = "success", location = "/jsp/system/file-finish.jsp") })
    public String execute() throws Exception {

        boolean bfoward = true;
        if (null == filePath) {
            filePath = SysConfig.getSetting("wifun.media.ftp.base");
            bfoward = false;
        }

        File file = new File(filePath);
        if (!file.isDirectory()) { // /如果是文件，进行保存
            return SUCCESS;
        }

        String strKey = "wifun.media.poster.suffix";
        if ("0".equals(type)) {
            strKey = "wifun.media.movie.suffix";
        }

        filelist = FileUtil.getFiles(file, SysConfig.getSetting(strKey));

        // //加向上一级
        return INPUT;
    }

    @Action(value = "file-list-load")
    public void loadFileList() throws Exception {

        filePath = SysConfig.getSetting("wifun.media.ftp.base");
        File file = new File(filePath);

        String strKey = "wifun.media.poster.suffix";
        if ("0".equals(type)) {
            strKey = "wifun.media.movie.suffix";
        }

        filelist = FileUtil.getFiles(file, SysConfig.getSetting(strKey));

        // 返回josn结果
        ResponseHelper.writeJsons(JSONBean.createJSONBean("fileList", filelist).toString());
    }


    public String getFilePath() {

        return filePath;
    }

    public void setFilePath(String filePath) {

        this.filePath = filePath;
    }

    public List<FileVo> getFilelist() {

        return filelist;
    }

    public void setFilelist(List<FileVo> filelist) {

        this.filelist = filelist;
    }

    public Integer getMovieId() {

        return movieId;
    }

    public void setMovieId(Integer movieId) {

        this.movieId = movieId;
    }

    public String getType() {

        return type;
    }

    public void setType(String type) {

        this.type = type;
    }

}