package com.wifun.admin.app.system;

import java.io.PrintWriter;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.service.ICompanyService;

/**
 * 用户登录
 * 
 * @File wifun-admin/com.wifun.admin.app.system.LoginAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/system")
public class LoginAction extends ActionSupport {

	// service
	@Resource(name = "companyService")
	private ICompanyService companyService;

	// Parameters
	private String username;
	private String password;

	// result
	Company company;
	private String name;// 取到的登录用户的姓名
	private String msg = "";

	@Override
	@Action(value = "login", results = { @Result(name = "input", location = "/jsp/system/login.jsp") })
	public String execute() throws Exception {

		return INPUT;
	}

	@Action(value = "do-login", results = {
			@Result(name = "super", type = "redirect", location = "/company/company-list.action"),
			@Result(name = "company", type = "redirect", location = "/group/group-list.action"),
			@Result(name = "error", type = "chain", location = "login") })
	public String doLogin() throws Exception {

		if (username == null || username.isEmpty()) {
			msg = " Please Input Username!";
			return ERROR;
		}

		company = companyService.findByAccount(username);
		if (company == null) {
			msg = "Account is not found!";
			return ERROR;
		}

		if (!company.getIsActive()) {
			msg = "Account is inactive!";
			return ERROR;
		}

		if (!password.equals(company.getPassword())) {
			msg = " Password Error!";
			return ERROR;
		}

		boolean flag = updateLastLoginTime(company);
		if (flag) {
			String returnvalue = "company";
			SessionAccount sa = new SessionAccount();
			sa.setId(company.getId());
			if (1 == company.getId()) {
				sa.setType(SessionAccount.SYSTEM_ROLE_TYPE_ADMIN);
				returnvalue = "super";
			} else {
				sa.setType(SessionAccount.COMPANY_ROLE_TYPE_ADMIN);
				returnvalue = "company";
			}
			sa.setAccount(company.getAccount());
			sa.setCompanyName(company.getName());
			sa.setLastLoginTime(company.getLastLoginTime());
			sa.setInhandId(company.getInhandId());

			SessionHelper.addAccount(sa);

			return returnvalue;
		} else {
			msg = "System Error!";
			return ERROR;
		}
	}

	@Action(value = "login-seach")
	public String checkUserIsExist() throws Exception {

		HttpServletResponse response = ServletActionContext.getResponse();
		PrintWriter pw = response.getWriter();
		company = companyService.findByAccount(username);
		if (company == null) {
			pw.write("1");
		}
		return null;
	}

	public boolean updateLastLoginTime(Company c) {

		boolean flag = companyService.updateLastLoginTime(c);
		return flag;
	}

	public void setUsername(String username) {

		this.username = username;
	}

	public void setPassword(String password) {

		this.password = password;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String getMsg() {

		return msg;
	}

	public String getUsername() {

		return username;
	}

	public String getPassword() {

		return password;
	}

	public void setMsg(String msg) {

		this.msg = msg;
	}

}