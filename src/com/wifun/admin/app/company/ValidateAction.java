package com.wifun.admin.app.company;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.app.ResponseHelper;
import com.wifun.admin.domain.service.ICompanyService;
import com.wifun.admin.util.JSONBean;

/**
 * 检查用户名是否存在
 * 
 * @author ouda
 */
@SuppressWarnings("serial")
@Namespace(value = "/company")
public class ValidateAction extends ActionSupport {

	// Service
	@Resource(name = "companyAccountService")
	private ICompanyService companyAccountService;

	// Parameters
	private String username;

	@Action(value = "validate-company-account")
	public void validaityCheck() {

		boolean flag = companyAccountService.checkUserName(username);
		int result = 0;
		if (!flag)
			result = 1;

		ResponseHelper.writeJsons(JSONBean.createJSONBean("result", result).toString());
	}

	public void setUsername(String username) {

		this.username = username;
	}
}
