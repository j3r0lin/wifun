package com.wifun.admin.app.company;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.service.ICompanyService;

/**
 * 重置公司管理员密码
 * 
 * @File wifun-admin/com.wifun.admin.app.company.CompanyResetPwdAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/company")
@ParentPackage(value = "admin-default")
public class CompanyResetPwdAction extends ActionSupport {

	// Service
	@Resource(name = "companyService")
	private ICompanyService companyService;

	// Parameters
	private Integer id;
	private Integer companyId;
	private String name;
	private String password;

	// Results
	private Company vo;

	@Action(value = "company-pwd-reset", results = { @Result(name = "success", location = "/jsp/company/pwd-reset.jsp") })
	public String execute() throws Exception {

		vo = companyService.findById(companyId);
		return SUCCESS;
	}

	@Action(value = "company-pwd-save", results = {
			@Result(name = "success", location = "/jsp/company/pwd-reset-ok.jsp"),
			@Result(name = "error", location = "/jsp/company/pwd-reset-error.jsp") })
	public String save() throws Exception {

		boolean flag = companyService.updatePwd(id, password);
		if (flag)
			return SUCCESS;
		else
			return ERROR;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	public void setCompanyId(Integer companyId) {

		this.companyId = companyId;
	}

	public void setPassword(String password) {

		this.password = password;
	}

	public Company getVo() {

		return vo;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String getPassword() {

		return password;
	}
}