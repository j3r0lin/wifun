package com.wifun.admin.app.company;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.app.SessionHelper;

/**
 * 账号注销
 * 
 * @author ouda
 */
@SuppressWarnings("serial")
@Namespace(value = "/company")
public class LogoutAction extends ActionSupport {

	@Action(value = "company-logout", results = { @Result(name = "success", type = "redirect", location = "/system/login.action") })
	public String execute() throws Exception {

		SessionHelper.clearSession();

		return SUCCESS;
	}
}
