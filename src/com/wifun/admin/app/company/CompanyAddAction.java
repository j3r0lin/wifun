package com.wifun.admin.app.company;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.app.vo.ResultVo;
import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.service.ICompanyService;

/**
 * 新建公司
 * 
 * @File wifun-admin/com.wifun.admin.app.company.CompanyAddAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/company")
@ParentPackage(value = "admin-default")
public class CompanyAddAction extends ActionSupport {

	// Service
	@Resource(name = "companyService")
	private ICompanyService companyService;

	// Parameters
	private String name;
	private String account;
	private String contact;
	private String phone;
	private String email;
	private boolean isActive;

	// Results
	private String msg;

	@Action(value = "company-add", results = { @Result(name = "success", location = "/jsp/company/company-add.jsp") })
	public String execute() throws Exception {

		return SUCCESS;
	}

	@Action(value = "company-add-save", results = {
			@Result(name = "error", location = "/jsp/company/company-save-error.jsp"),
			@Result(name = "success", location = "/jsp/company/company-save-ok.jsp") })
	public String save() throws Exception {

		if (!companyService.checkCompanyName(name)) {
			msg = "Duplicate company name!";
			return ERROR;
		}

		if (!companyService.checkUserName(account)) {
			msg = "Duplicate user account!";
			return ERROR;
		}

		if (!companyService.checkCompanyEmail(email)) {
			msg = "Duplicate company email!";
			return ERROR;
		}

		Company com = new Company(name, account, contact, phone, email, isActive);
		ResultVo resultVo = companyService.addCompany(com);

		if (resultVo.isOk()) {
			return SUCCESS;
		} else {
			msg = resultVo.getMsg();
			return ERROR;
		}
	}

	public ICompanyService getCompanyService() {

		return companyService;
	}

	public void setCompanyService(ICompanyService companyService) {

		this.companyService = companyService;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public String getAccount() {

		return account;
	}

	public void setAccount(String account) {

		this.account = account;
	}

	public String getContact() {

		return contact;
	}

	public void setContact(String contact) {

		this.contact = contact;
	}

	public String getPhone() {

		return phone;
	}

	public void setPhone(String phone) {

		this.phone = phone;
	}

	public String getEmail() {

		return email;
	}

	public void setEmail(String email) {

		this.email = email;
	}

	public boolean getIsActive() {

		return isActive;
	}

	public void setIsActive(boolean isActive) {

		this.isActive = isActive;
	}

	public String getMsg() {

		return msg;
	}
}