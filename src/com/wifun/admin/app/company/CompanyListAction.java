package com.wifun.admin.app.company;

import java.util.List;

import javax.annotation.Resource;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.service.ICompanyService;

/**
 * 公司列表
 * 
 * @File wifun-admin/com.wifun.admin.app.company.CompanyListAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/company")
@ParentPackage(value = "admin-default")
public class CompanyListAction extends ActionSupport {

	// Service
	@Resource(name = "companyService")
	private ICompanyService companyService;
	// Parameters
	private String name;
	private boolean active;

	// Results
	private List<Company> comList;

	@Action(value = "company-list", results = { @Result(name = "success", location = "/jsp/company/company-list.jsp") })
	public String execute() throws Exception {

		active = true;
		comList = companyService.getCompanyList(active, null);
		return SUCCESS;
	}

	@Action(value = "company-search", results = { @Result(name = "success", location = "/jsp/company/company-list.jsp") })
	public String search() throws Exception {

		comList = companyService.getCompanyList(active, name);
		return SUCCESS;
	}

	public List<Company> getComList() {

		return comList;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public boolean isActive() {

		return active;
	}

	public void setActive(boolean active) {

		this.active = active;
	}
}