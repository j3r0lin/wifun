package com.wifun.admin.app.company;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.app.vo.ResultVo;
import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.service.ICompanyService;

/**
 * 公司修改
 * 
 * @File wifun-admin/com.wifun.admin.app.company.CompanyEditAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/company")
@ParentPackage(value = "admin-default")
public class CompanyEditAction extends ActionSupport {

	// Service
	@Resource(name = "companyService")
	private ICompanyService companyService;
	// Parameters
	private Integer id;
	// Results
	private Company vo;
	private String msg;

	@Action(value = "company-edit", results = { @Result(name = "success", location = "/jsp/company/company-edit.jsp") })
	public String execute() throws Exception {

		vo = companyService.findById(id);
		return SUCCESS;
	}

	@Action(value = "company-edit-save", results = {
			@Result(name = "success", location = "/jsp/company/company-save-ok.jsp"),
			@Result(name = "error", location = "/jsp/company/company-save-error.jsp") })
	public String save() throws Exception {

		ResultVo resultVo = companyService.updateCompany(vo);

		if (resultVo.isOk()) {
			return SUCCESS;
		} else {
			msg = resultVo.getMsg();
			return ERROR;
		}
	}

	public Integer getId() {

		return id;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	public Company getVo() {

		return vo;
	}

	public void setVo(Company vo) {

		this.vo = vo;
	}

	public String getMsg() {

		return msg;
	}
}