package com.wifun.admin.app.location;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.ResponseHelper;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.app.vo.DeviceLocation;
import com.wifun.admin.domain.service.IDeviceService;
import com.wifun.admin.util.JSONBean;

/**
 * 设备当前位置
 * 
 * @File wifun-admin/com.wifun.admin.app.location.DeviceLocationAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/location")
@ParentPackage(value = "admin-default")
public class DeviceLocationAction extends BaseAction {

	@Resource(name = "deviceService")
	private IDeviceService deviceService;

	@Action(value = "device-location", results = { @Result(name = "success", location = "/jsp/location/device-location.jsp") })
	public String execute() throws Exception {

		super.putCompanyAllList();
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN == SessionHelper.getUserType()) {
			if (companyList != null && companyList.size() > 0)
				companyId = companyList.get(0).getId();
			else
				companyId = 0;
		} else {
			companyId = SessionHelper.getCompanyId();
		}
		return SUCCESS;
	}

	/**
	 * 根据公司查询设备列表
	 * 
	 * @throws Exception
	 */
	@Action(value = "device-list")
	public void getDeviceListByCompanyId() throws Exception {

		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType())
			companyId = SessionHelper.getCompanyId();

		List<DeviceLocation> locationList = deviceService.getDeviceLocations(companyId);
		// 返回josn结果
		ResponseHelper.writeJsons(JSONBean.createJSONBean("deviceList", locationList).toString());
	}
}
