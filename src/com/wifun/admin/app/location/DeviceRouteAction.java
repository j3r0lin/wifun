package com.wifun.admin.app.location;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.ResponseHelper;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.app.vo.DeviceLocation;
import com.wifun.admin.domain.service.IDeviceService;
import com.wifun.admin.util.JSONBean;
import com.wifun.admin.util.TimeUtils;

/**
 * 查询单台设备的运行轨迹
 * 
 * @File wifun-admin/com.wifun.admin.app.location.DeviceRouteAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/route")
@ParentPackage(value = "admin-default")
public class DeviceRouteAction extends BaseAction {

	@Resource(name = "deviceService")
	private IDeviceService deviceService;

	// Parameters
	private int deviceId;
	private String date;

	@Action(value = "device-routeIndex", results = { @Result(name = "success", location = "/jsp/location/device-route.jsp") })
	public String execute() throws Exception {

		super.putCompanyAllList();
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN == SessionHelper.getUserType()) {
			if (companyList != null && companyList.size() > 0)
				companyId = companyList.get(0).getId();
			else
				companyId = 0;
		} else {
			companyId = SessionHelper.getCompanyId();
		}

		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("MM/dd/yyyy");
		this.setDate(formatter.format(currentTime));

		return SUCCESS;
	}

	/**
	 * 根据公司查询设备列表
	 * 
	 * @throws Exception
	 */
	@Action(value = "device-list")
	public void getDeviceListByCompanyId() throws Exception {

		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType())
			companyId = SessionHelper.getCompanyId();

		List<DeviceLocation> locationList = deviceService.getDeviceLocations(companyId);
		// 返回josn结果
		ResponseHelper.writeJsons(JSONBean.createJSONBean("deviceList", locationList).toString());
	}

	/**
	 * 查询单台设备在一天中某个时间段的运行轨迹
	 * 
	 * @throws Exception
	 */
	@Action(value = "device-route")
	public void getDeviceRouteByDeviceId() throws Exception {

		if (StringUtils.isBlank(date)) {
			return;
		}

		// 格式化参数
		Date paramDate = TimeUtils.strUsToDate(date);
		String dateStr = TimeUtils.dateToStr(paramDate);

		List<DeviceLocation> locationList = deviceService.getDeviceRoute(deviceId, dateStr);
		// 返回josn结果
		ResponseHelper.writeJsons(JSONBean.createJSONBean("deviceRoute", locationList).toString());
	}

	public void setDeviceId(int deviceId) {

		this.deviceId = deviceId;
	}

	public void setDate(String date) {

		this.date = date;
	}

	public String getDate() {

		return this.date;
	}
}
