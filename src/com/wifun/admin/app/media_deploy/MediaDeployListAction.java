package com.wifun.admin.app.media_deploy;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.model.MediaDeploy;
import com.wifun.admin.domain.service.IMediaDeployService;
import com.wifun.admin.util.TimeUtils;

/**
 * 查询所有媒体资源发布
 * 
 * @File wifun-admin/com.wifun.admin.app.media_deploy.MediaDeployListAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class MediaDeployListAction extends BaseAction {

	// Service
	@Resource(name = "mediaDeployService")
	private IMediaDeployService mediaDeployService;
	// Parameters
	private String startDate;
	private String endDate;

	// Results
	private List<MediaDeploy> deployList;

	@Action(value = "media-deploy-list", results = { @Result(name = "success", location = "/jsp/media-deploy/media-deploy-list.jsp") })
	public String execute() throws Exception {

		super.putCompanyAllList();
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN == SessionHelper.getUserType()) {
			if (companyList != null && companyList.size() > 0)
				companyId = companyList.get(0).getId();
			else
				companyId = 0;
		} else {
			companyId = SessionHelper.getCompanyId();
		}

		startDate = TimeUtils.getDateToUsTow(TimeUtils.getCurrMonthFirstDay());
		endDate = TimeUtils.getCurrentDateShort();

		deployList = mediaDeployService.searchMediaDeployList(companyId, startDate, endDate);

		return SUCCESS;
	}

	@Action(value = "media-deploy-search", results = { @Result(name = "success", location = "/jsp/media-deploy/media-deploy-list.jsp") })
	public String search() throws Exception {

		super.putCompanyAllList();
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType()) {
			companyId = SessionHelper.getCompanyId();
		}

		deployList = mediaDeployService.searchMediaDeployList(companyId, startDate, endDate);

		return SUCCESS;
	}

	public List<MediaDeploy> getDeployList() {

		return deployList;
	}

	public String getStartDate() {

		return startDate;
	}

	public void setStartDate(String startDate) {

		this.startDate = startDate;
	}

	public String getEndDate() {

		return endDate;
	}

	public void setEndDate(String endDate) {

		this.endDate = endDate;
	}
}