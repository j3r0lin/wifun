package com.wifun.admin.app.media_deploy;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.model.MediaDeploy;
import com.wifun.admin.domain.service.IMediaDeployService;

/**
 * 新建SD卡媒体资源发布
 * 
 * @File wifun-admin/com.wifun.admin.app.media_deploy.SDDeployAddAction.java
 * @Desc SD卡媒体资源发布只在后台生成vlist文件，操作员需要在明细页面手工将vlist文件和媒体资源压缩包下载到SD卡中
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class SDDeployAddAction extends BaseAction {

	// Service
	@Resource(name = "mediaDeployService")
	private IMediaDeployService mediaDeployService;

	// Parameters
	private MediaDeploy mediaDeploy;
	private String[] devices;

	// Results
	private String msg;

	@Action(value = "sd-deploy-add", results = { @Result(name = "success", location = "/jsp/media-deploy/sd-deploy-add.jsp") })
	public String execute() throws Exception {

		super.putCompanyActiveList();
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN == SessionHelper.getUserType()) {
			if (companyList != null && companyList.size() > 0)
				companyId = companyList.get(0).getId();
			else
				companyId = 0;
		} else {
			return ERROR;
		}
		return SUCCESS;
	}

	@Action(value = "sd-deploy-save", results = {
			@Result(name = "error", location = "/jsp/media-deploy/sd-deploy-error.jsp"),
			@Result(name = "success", type = "redirect", location = "/media/sd-deploy-detail.action?deployId=${mediaDeploy.id}") })
	public String save() throws Exception {

		if (null == mediaDeploy)
			return ERROR;

		if (mediaDeploy.getMediaListId() == null || mediaDeploy.getMediaListId() == 0) {
			msg = "Please select media list.";
			return ERROR;
		}

		if (devices == null || devices.length == 0) {
			msg = "Please select devices.";
			return ERROR;
		}

		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType()) {
			companyId = SessionHelper.getCompanyId();
			mediaDeploy.setCompanyId(companyId);
		}

		mediaDeploy.setType(MediaDeploy.DEPLOY_TYPE_SD);
		mediaDeploy = mediaDeployService.deploySD(mediaDeploy, devices);

		if (mediaDeploy != null && mediaDeploy.getId() != null)
			return SUCCESS;
		else
			return ERROR;
	}

	public MediaDeploy getMediaDeploy() {

		return mediaDeploy;
	}

	public void setMediaDeploy(MediaDeploy mediaDeploy) {

		this.mediaDeploy = mediaDeploy;
	}

	public String getMsg() {

		return msg;
	}

	public String[] getDevices() {

		return devices;
	}

	public void setDevices(String[] devices) {

		this.devices = devices;
	}
}
