package com.wifun.admin.app.media_deploy;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.util.SysConfig;

/**
 * 下载vlist文件
 * 
 * @File wifun-admin/com.wifun.admin.app.media_deploy.VlistDownloadAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class VlistDownloadAction extends ActionSupport {

	// Parameters
	private int deployId;
	private String filepath;

	@Action(value = "vlist-download", results = {
			@Result(name = "success", type = "stream", params = { "contentType",
					"application/octet-stream;charset=utf-8", "inputName", "inputStream",
					"contentDisposition", "attachment;filename=\"${fileName}\"", "bufferSize",
					"4096" }),
			@Result(name = "error", location = "/jsp/media-deploy/sd-deploy-download-error.jsp") })
	public String execute() throws Exception {

		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType())
			return ERROR;

		if (deployId == 0)
			return ERROR;

		String path = SysConfig.getSetting("wifun.media.deploy.directory");
		filepath = path + "/vlist_" + deployId;
		File file = new File(filepath);
		if (!file.exists())
			return ERROR;

		return SUCCESS;
	}

	public InputStream getInputStream() throws Exception {

		return new FileInputStream(filepath);
	}

	public String getFileName() {

		String fileName = "vlist";
		return fileName;
	}

	public void setDeployId(int deployId) {

		this.deployId = deployId;
	}
}