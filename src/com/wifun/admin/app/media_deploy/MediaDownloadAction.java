package com.wifun.admin.app.media_deploy;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.util.SysConfig;

/**
 * 下载媒体资源压缩包，用于SD卡发布
 * 
 * @File wifun-admin/com.wifun.admin.app.media_deploy.MediaDownloadAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class MediaDownloadAction extends ActionSupport {

	// Parameters
	private int mediaId;
	private String mediaVersion;

	private String filepath;

	@Action(value = "media-download", results = {
			@Result(name = "success", type = "stream", params = { "contentType",
					"application/octet-stream;charset=utf-8", "inputName", "inputStream",
					"contentDisposition", "attachment;filename=\"${fileName}\"", "bufferSize",
					"4096" }),
			@Result(name = "error", location = "/jsp/media-deploy/sd-deploy-download-error.jsp") })
	@Override
	public String execute() throws Exception {

		if (mediaId == 0 || StringUtils.isBlank(mediaVersion))
			return ERROR;

		String path = SysConfig.getSetting("wifun.media.zip.directory");
		filepath = path + "/media_" + mediaId + "_" + mediaVersion + ".zip";

		File file = new File(filepath);
		if (!file.exists())
			return ERROR;

		return SUCCESS;
	}

	public InputStream getInputStream() throws Exception {

		return new FileInputStream(filepath);
	}

	// 媒体资源压缩包命名格式： "media_"+媒体资源id+"_"+媒体资源版本号+".zip"
	public String getFileName() {

		String fileName = "media_" + mediaId + "_" + mediaVersion + ".zip";
		return fileName;
	}

	public void setMediaId(int mediaId) {

		this.mediaId = mediaId;
	}

	public void setMediaVersion(String mediaVersion) {

		this.mediaVersion = mediaVersion;
	}
}