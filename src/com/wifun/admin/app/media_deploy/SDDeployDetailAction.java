package com.wifun.admin.app.media_deploy;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.vo.SDDeployDetailVo;
import com.wifun.admin.domain.model.MediaDeploy;
import com.wifun.admin.domain.service.ISDDeployDetailService;
import com.wifun.admin.domain.service.ISDDeployService;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import javax.annotation.Resource;
import java.util.List;

/**
 * SD卡发布页面
 * 
 * @File wifun-admin/com.wifun.admin.app.media_deploy.SDDeployDetailAction.java
 * @Desc 可以在该页面下载本次发布的vlist文件和媒体资源文件
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class SDDeployDetailAction extends BaseAction {

	@Resource(name = "sdDeployService")
	private ISDDeployService sdDeployService;

	@Resource(name = "sdDeployDetailService")
	private ISDDeployDetailService sdDeployDetailService;

	// Parameters
	private int deployId;

	// Results
	private String vlistUrl;
	private List<SDDeployDetailVo> detailList;

	@Action(value = "sd-deploy-detail", results = { @Result(name = "success", location = "/jsp/media-deploy/sd-deploy-detail.jsp") })
	public String execute() throws Exception {

		MediaDeploy mediaDeploy = sdDeployService.findById(deployId);
		vlistUrl = "http://video.ubt.io/vlist/vlist_"
				+ formateVersion(mediaDeploy.getVersion());
		detailList = sdDeployDetailService.getSDDeployDetailVo(deployId);

		for (int i = 0; i < detailList.size(); i++) {

			SDDeployDetailVo tmp = detailList.get(i);
			String url = "http://video.ubt.io/download/media_" + tmp.getMediaId() + "_"
					+ tmp.getMediaVersion() + ".zip";
			tmp.setUrl(url);
		}

		return SUCCESS;
	}

	private String formateVersion(String version) {

		String[] versionArr = version.split("\\.");
		if (versionArr.length > 0) {
			version = versionArr[0];
		}
		version = version.substring(1);
		return version;
	}

	public String getVlistUrl() {

		return vlistUrl;
	}

	public int getDeployId() {

		return deployId;
	}

	public void setDeployId(int deployId) {

		this.deployId = deployId;
	}

	public List<SDDeployDetailVo> getDetailList() {

		return detailList;
	}
}
