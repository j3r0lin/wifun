package com.wifun.admin.app.media_deploy;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.domain.model.MediaDeployDetail;
import com.wifun.admin.domain.service.IMediaDeployDetailService;

/**
 * 查看某次发布的明细情况
 * 
 * @File 
 *       wifun-admin/com.wifun.admin.app.media_deploy.MediaDeployDetailAction.java
 * @Desc 可以查询到哪些电影在哪些设备上已经发布完成
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class MediaDeployDetailAction extends ActionSupport {

	// Service
	@Resource(name = "mediaDeployDetailService")
	private IMediaDeployDetailService mediaDeployDetailService;
	// Parameters
	private Integer deployId;
	// Results
	private List<MediaDeployDetail> detailList;

	@Action(value = "media-deploy-detail", results = { @Result(name = "success", location = "/jsp/media-deploy/media-deploy-detail.jsp") })
	public String execute() throws Exception {

		detailList = mediaDeployDetailService.loadByDeployId(deployId);

		return SUCCESS;
	}

	public Integer getDeployId() {

		return deployId;
	}

	public void setDeployId(Integer deployId) {

		this.deployId = deployId;
	}

	public List<MediaDeployDetail> getDetailList() {

		return detailList;
	}

	public void setDetailList(List<MediaDeployDetail> detailList) {

		this.detailList = detailList;
	}
}