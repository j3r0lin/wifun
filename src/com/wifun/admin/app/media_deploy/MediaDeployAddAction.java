package com.wifun.admin.app.media_deploy;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.MediaDeploy;
import com.wifun.admin.domain.model.MediaList;
import com.wifun.admin.domain.service.IDeviceService;
import com.wifun.admin.domain.service.IMediaDeployService;
import com.wifun.admin.domain.service.IMediaListService;

/**
 * 新建媒体资源发布
 * 
 * @File wifun-admin/com.wifun.admin.app.media_deploy.MediaDeployAddAction.java
 * @Desc 新建媒体资源发布就是将某个媒体列表中的所有媒体资源通过4g网络发布到某个分组下的所有设备
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class MediaDeployAddAction extends BaseAction {

	// Service
	@Resource(name = "mediaDeployService")
	private IMediaDeployService mediaDeployService;

	@Resource(name = "mediaListService")
	private IMediaListService mediaListService;

	@Resource(name = "deviceService")
	private IDeviceService deviceService;

	// Parameters
	private MediaDeploy mediaDeploy;

	// Results
	private List<MediaList> mediaList;
	private String msg;

	@Action(value = "media-deploy-add", results = { @Result(name = "success", location = "/jsp/media-deploy/media-deploy-add.jsp") })
	public String execute() throws Exception {

		super.putCompanyActiveList();
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN == SessionHelper.getUserType()) {
			if (companyList != null && companyList.size() > 0)
				companyId = companyList.get(0).getId();
			else
				companyId = 0;
		} else {
			companyId = SessionHelper.getCompanyId();
		}
		mediaList = mediaListService.loadAll();
		return SUCCESS;
	}

	@Action(value = "media-deploy-save", results = {
			@Result(name = "error", location = "/jsp/media-deploy/media-deploy-error.jsp"),
			@Result(name = "success", location = "/jsp/media-deploy/media-deploy-ok.jsp") })
	public String save() throws Exception {

		if (null == mediaDeploy)
			return ERROR;

		if (mediaDeploy.getMediaListId() == null || mediaDeploy.getMediaListId() == 0) {
			msg = "Please select media list.";
			return ERROR;
		}

		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType()) {
			companyId = SessionHelper.getCompanyId();
			mediaDeploy.setCompanyId(companyId);
		}
		mediaDeploy.setType(MediaDeploy.DEPLOY_TYPE_NET);

		List<Device> devices = deviceService.getDeviceList(mediaDeploy.getCompanyId(),
				mediaDeploy.getGroupId());

		if (devices == null || devices.size() == 0) {
			msg = "No device at the group.";
			return ERROR;
		}

		boolean flag = mediaDeployService.deploy(mediaDeploy);

		if (flag)
			return SUCCESS;
		else
			return ERROR;
	}

	public MediaDeploy getMediaDeploy() {

		return mediaDeploy;
	}

	public void setMediaDeploy(MediaDeploy mediaDeploy) {

		this.mediaDeploy = mediaDeploy;
	}

	public List<MediaList> getMediaList() {

		return mediaList;
	}

	public void setMediaList(List<MediaList> mediaList) {

		this.mediaList = mediaList;
	}

	public String getMsg() {

		return msg;
	}

}