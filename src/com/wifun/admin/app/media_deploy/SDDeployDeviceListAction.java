package com.wifun.admin.app.media_deploy;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.domain.model.MediaDeployDetail;
import com.wifun.admin.domain.service.ISDDeployDetailService;

/**
 * SD卡媒体资源发布结果查询
 * 
 * @File 
 *       wifun-admin/com.wifun.admin.app.media_deploy.SDDeployDeviceListAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/media")
@ParentPackage(value = "admin-default")
public class SDDeployDeviceListAction extends BaseAction {

	@Resource(name = "sdDeployDetailService")
	private ISDDeployDetailService sdDeployDetailService;

	// Parameters
	private int deployId;
	private int mediaId;

	// Results
	private List<MediaDeployDetail> detailList;

	@Action(value = "sd-deploy-device-list", results = { @Result(name = "success", location = "/jsp/media-deploy/sd-deploy-device-list.jsp") })
	public String execute() throws Exception {

		detailList = sdDeployDetailService.getSDDeployDeviceDetail(deployId, mediaId);
		return SUCCESS;
	}

	public List<MediaDeployDetail> getDetailList() {

		return detailList;
	}

	public void setDeployId(int deployId) {

		this.deployId = deployId;
	}

	public void setMediaId(int mediaId) {

		this.mediaId = mediaId;
	}
}
