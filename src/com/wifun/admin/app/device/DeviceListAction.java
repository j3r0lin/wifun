package com.wifun.admin.app.device;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.ResponseHelper;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.Group;
import com.wifun.admin.domain.service.IDeviceService;
import com.wifun.admin.domain.service.IGroupService;
import com.wifun.admin.util.JSONBean;

/**
 * 查询设备列表
 * 
 * @File wifun-admin/com.wifun.admin.app.device.DeviceListAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/device")
@ParentPackage(value = "admin-default")
public class DeviceListAction extends BaseAction {

	// Service
	@Resource(name = "deviceService")
	private IDeviceService deviceService;
	@Resource(name = "groupService")
	private IGroupService groupService;
	// Parameters
	private int groupId = -1;
	private String sn;
	private String name;
	private boolean active;;
	// Results
	private List<Device> deviceList;
	private List<Group> groupList;

	@Action(value = "device-list", results = { @Result(name = "success", location = "/jsp/device/device-list.jsp") })
	public String execute() throws Exception {

		active = true;
		super.putCompanyAllList();

		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN == SessionHelper.getUserType()) {
			if (companyList != null && companyList.size() > 0)
				companyId = companyList.get(0).getId();
			else
				companyId = 0;
		} else {
			companyId = SessionHelper.getCompanyId();
		}

		groupList = groupService.getGroupsByCompanyId(companyId);
		deviceList = deviceService.searchDevices(active, companyId, groupId, sn, name);
		return SUCCESS;
	}

	@Action(value = "device-search", results = { @Result(name = "success", location = "/jsp/device/device-list.jsp") })
	public String search() throws Exception {

		super.putCompanyAllList();
		groupList = groupService.getGroupsByCompanyId(companyId);
		deviceList = deviceService.searchDevices(active, companyId, groupId, sn, name);
		return SUCCESS;
	}

	@Action(value = "company-group-device-list")
	public void getDeviceByGroupId() throws Exception {

		List<Device> deviceList = deviceService.searchDevices(true, companyId, groupId, null, null);
		// 返回josn结果
		ResponseHelper.writeJsons(JSONBean.createJSONBean("deviceList", deviceList).toString());
	}

	public List<Device> getDeviceList() {

		return deviceList;
	}

	public String getSn() {

		return sn;
	}

	public void setSn(String sn) {

		this.sn = sn;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public int getGroupId() {

		return groupId;
	}

	public void setGroupId(int groupId) {

		this.groupId = groupId;
	}

	public List<Group> getGroupList() {

		return groupList;
	}

	public void setGroupList(List<Group> groupList) {

		this.groupList = groupList;
	}

	public boolean isActive() {

		return active;
	}

	public void setActive(boolean active) {

		this.active = active;
	}
}