package com.wifun.admin.app.device;

import java.util.Date;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.vo.ResultVo;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.service.IDeviceService;
import com.wifun.admin.util.TimeUtils;

/**
 * 新增设备
 * 
 * @File wifun-admin/com.wifun.admin.app.device.DeviceAddAction.java
 * @Desc 新增wifi设备，首先判断SN是否存在，如果不存在则加入inhand平台和业务平台
 */
@SuppressWarnings("serial")
@Namespace(value = "/device")
@ParentPackage(value = "admin-default")
public class DeviceAddAction extends BaseAction {

	// Service
	@Resource(name = "deviceService")
	private IDeviceService deviceService;

	// Parameters
	private Device bo;
	private String startDate;

	// Results
	private String msg;

	@Action(value = "device-add", results = { @Result(name = "success", location = "/jsp/device/device-add.jsp") })
	public String execute() throws Exception {

		super.putCompanyAllList();
		if (companyList != null && companyList.size() > 0)
			companyId = companyList.get(0).getId();
		else
			companyId = 0;

		return SUCCESS;
	}

	@Action(value = "device-add-save", results = {
			@Result(name = "error", location = "/jsp/device/device-save-error.jsp"),
			@Result(name = "success", location = "/jsp/device/device-save-ok.jsp") })
	public String save() throws Exception {

		if (null == bo.getCompanyId()) {
			msg = ResultVo.MSG_SYSTEM_ERROR;
			return ERROR;
		}
		if (null != startDate) {
			bo.setStartDate(TimeUtils.strUsTwoDate(startDate));
		}
		bo.setCreateDate(new Date());
		bo.setUpdateDate(new Date());

		ResultVo resultVo = deviceService.addDevice(bo);

		if (resultVo.isOk()) {
			return SUCCESS;
		} else {
			msg = resultVo.getMsg();
			return ERROR;
		}
	}

	public Device getBo() {

		return bo;
	}

	public void setBo(Device bo) {

		this.bo = bo;
	}

	public String getStartDate() {

		return startDate;
	}

	public void setStartDate(String startDate) {

		this.startDate = startDate;
	}

	public String getMsg() {

		return msg;
	}
}