package com.wifun.admin.app.device;

import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.ResponseHelper;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.app.vo.ResultVo;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.Group;
import com.wifun.admin.domain.service.IDeviceService;
import com.wifun.admin.domain.service.IGroupService;
import com.wifun.admin.util.JSONBean;
import com.wifun.admin.util.TimeUtils;

/**
 * 修改设备
 * 
 * @File wifun-admin/com.wifun.admin.app.device.DeviceEditAction.java
 * @Desc
 */
@SuppressWarnings("serial")
@Namespace(value = "/device")
@ParentPackage(value = "admin-default")
public class DeviceEditAction extends BaseAction {

	// Service
	@Resource(name = "deviceService")
	private IDeviceService deviceService;
	@Resource(name = "groupService")
	private IGroupService groupService;
	// Parameters
	private Integer id;
	private String startDate;
	// Results
	private Device vo;
	private List<Group> groupList;
	private String msg;

	@Action(value = "device-edit", results = {
			@Result(name = "noright", location = "/jsp/global/noright.jsp"),
			@Result(name = "company", location = "/jsp/device/device-company-edit.jsp"),
			@Result(name = "super", location = "/jsp/device/device-edit.jsp") })
	public String execute() throws Exception {

		vo = deviceService.findById(id);

		deviceService.updateInfoFromInhaned(vo);

		groupList = groupService.getGroupsByCompanyId(vo.getCompanyId());
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN == SessionHelper.getUserType()) {
			return "super";
		} else {
			return "company";
		}
	}

	/**
	 * 超级管理员保存设备修改
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "device-edit-save", results = {
			@Result(name = "success", location = "/jsp/device/device-save-ok.jsp"),
			@Result(name = "error", location = "/jsp/device/device-save-error.jsp") })
	public String save() throws Exception {

		Device device = deviceService.findById(id);
		device.updateInfo(vo);

		if (StringUtils.isNotBlank(startDate))
			device.setStartDate(TimeUtils.strUsTwoDate(startDate));

		ResultVo resultVo = deviceService.updateDevice(device);

		if (resultVo.isOk()) {
			return SUCCESS;
		} else {
			msg = resultVo.getMsg();
			return ERROR;
		}
	}

	/**
	 * 公司管理员保存设备修改
	 * 
	 * @return
	 * @throws Exception
	 */
	@Action(value = "device-company-edit-save", results = {
			@Result(name = "success", location = "/jsp/device/device-save-ok.jsp"),
			@Result(name = "error", location = "/jsp/device/device-save-error.jsp") })
	public String save4Company() throws Exception {

		Device device = deviceService.findById(id);
		device.updateInfoCompany(vo);

		ResultVo resultVo = deviceService.updateDevice(device);

		if (resultVo.isOk()) {
			return SUCCESS;
		} else {
			msg = resultVo.getMsg();
			return ERROR;
		}
	}

	@Action(value = "device-delete")
	public void delete() throws Exception {

		// 0,系统错误,1 成功,3参数错误
		int result = 1;
		if (id == null) {
			result = 0;
		} else {
			ResultVo resultVo = deviceService.deleteDeviceById(id);
			if (resultVo.isOk()) {
				result = 1;
			} else {
				msg = resultVo.getMsg();
				result = 0;
			}
		}

		// 返回josn结果
		ResponseHelper.writeJsons(JSONBean.createJSONBean("result", result).toString());
	}

	public Integer getId() {

		return id;
	}

	public void setId(Integer id) {

		this.id = id;
	}

	public Device getVo() {

		return vo;
	}

	public void setVo(Device vo) {

		this.vo = vo;
	}

	public List<Group> getGroupList() {

		return groupList;
	}

	public void setGroupList(List<Group> groupList) {

		this.groupList = groupList;
	}

	public String getStartDate() {

		return startDate;
	}

	public void setStartDate(String startDate) {

		this.startDate = startDate;
	}

	public String getMsg() {

		return msg;
	}
}