package com.wifun.admin.app;

import java.util.List;

import javax.annotation.Resource;

import com.opensymphony.xwork2.ActionSupport;
import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.service.ICompanyService;

@SuppressWarnings("serial")
public class BaseAction extends ActionSupport {

	// Service
	@Resource(name = "companyService")
	private ICompanyService companyService;

	protected List<Company> companyList;
	protected int companyId;

	/*
	 * 查询可用的公司
	 */
	public void putCompanyActiveList() {

		companyList = companyService.getCompanyList(true);
	}

	/**
	 * 查询所有公司，包括有效和无效
	 */
	public void putCompanyAllList() {

		companyList = companyService.getCompanyList(false);
	}

	public int getCompanyId() {

		return companyId;
	}

	public void setCompanyId(int companyId) {

		this.companyId = companyId;
	}

	public List<Company> getCompanyList() {

		return companyList;
	}

	public void setCompanyList(List<Company> companyList) {

		this.companyList = companyList;
	}
}
