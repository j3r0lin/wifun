package com.wifun.admin.app.report;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.app.vo.DailyReportVo;
import com.wifun.admin.domain.model.Group;
import com.wifun.admin.domain.service.IGroupService;
import com.wifun.admin.domain.service.IReportService;
import com.wifun.admin.util.TimeUtils;
/**
 * 每日报表查询
 *
 */
@SuppressWarnings("serial")
@Namespace(value = "/report")
@ParentPackage(value = "admin-default")
public class DailyReportAction extends BaseAction {

	@Resource(name = "reportService")
	private IReportService reportService;

	@Resource(name = "groupService")
	private IGroupService groupService;

	// Parameters
	private int groupId = -1;
	private String date;
	private int maxTrafficValue;

	private List<Group> groupList;
	private List<DailyReportVo> dailyList;

	@Action(value = "daily-report", results = { @Result(name = "success", location = "/jsp/report/daily-report.jsp") })
	public String execute() throws Exception {

		super.putCompanyAllList();

		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN == SessionHelper.getUserType()) {
			if (companyList != null && companyList.size() > 0)
				companyId = companyList.get(0).getId();
			else
				companyId = 0;
		} else {
			companyId = SessionHelper.getCompanyId();
		}

		groupList = groupService.getGroupsByCompanyId(companyId);

		date = TimeUtils.getFormatterDate(TimeUtils.getYesterday(), "MM/dd/yyyy");

		this.dailyList = reportService.getReportDailyVo(companyId, groupId, date);
		return SUCCESS;
	}

	@Action(value = "daily-report-search", results = { @Result(name = "success", location = "/jsp/report/daily-report.jsp") })
	public String search() throws Exception {

		super.putCompanyAllList();
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType())
			companyId = SessionHelper.getCompanyId();

		groupList = groupService.getGroupsByCompanyId(companyId);

		dailyList = reportService.getReportDailyVo(companyId, groupId, date);

		return SUCCESS;
	}

	public String getDate() {

		return date;
	}

	public void setDate(String date) {

		this.date = date;
	}

	public int getMaxTrafficValue() {

		return maxTrafficValue;
	}

	public void setMaxTrafficValue(int maxTrafficValue) {

		this.maxTrafficValue = maxTrafficValue;
	}

	public List<DailyReportVo> getDailyList() {

		return dailyList;
	}

	public void setDailyList(List<DailyReportVo> dailyList) {

		this.dailyList = dailyList;
	}

	public int getGroupId() {

		return groupId;
	}

	public void setGroupId(int groupId) {

		this.groupId = groupId;
	}

	public List<Group> getGroupList() {

		return groupList;
	}
}
