package com.wifun.admin.app.report;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.app.vo.PvuvMovieVo;
import com.wifun.admin.domain.model.MediaList;
import com.wifun.admin.domain.service.IMediaListService;
import com.wifun.admin.domain.service.IPvuvMovieReportService;

/**
 * 查询电影pvuv值
 * 
 */
@SuppressWarnings("serial")
@Namespace(value = "/pvuvReport")
@ParentPackage(value = "admin-default")
public class PvuvMoiveAction extends BaseAction {

	@Resource(name = "pvuvMovieReportService")
	private IPvuvMovieReportService pvuvMovieReportService;

	@Resource(name = "mediaListService")
	private IMediaListService mediaListService;

	ArrayList<PvuvMovieVo> vos;
	List<MediaList> mediaLists;

	private int mediaListId = 0;

	String seriesStr = "";

	String errCode = "";

	String xdata = "";

	// index
	@Action(value = "report-pvuv", results = { @Result(name = "success", location = "/jsp/report/pvuv-movie.jsp") })
	public String execute() throws Exception {

		super.putCompanyAllList();
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN == SessionHelper.getUserType()) {
			if (companyList != null && companyList.size() > 0)
				companyId = companyList.get(0).getId();
			else
				companyId = 0;
		} else {
			companyId = SessionHelper.getCompanyId();
		}

		mediaLists = mediaListService.getCompanyMedias(null, companyId);
		if (mediaLists != null && mediaLists.size() > 0)
			mediaListId = mediaLists.get(0).getId();

		MakePvuvChartData(companyId, mediaListId);
		return SUCCESS;
	}

	// search
	@Action(value = "report-pvuv-search", results = { @Result(name = "success", location = "/jsp/report/pvuv-movie.jsp") })
	public String search() throws Exception {

		super.putCompanyAllList();
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType())
			companyId = SessionHelper.getCompanyId();

		mediaLists = mediaListService.getCompanyMedias(null, companyId);

		MakePvuvChartData(companyId, mediaListId);
		return SUCCESS;
	}

	/*
	 * 为pvuv all 准备页面echarts数据Data
	 */
	public void MakePvuvChartData(int companyId, int mediaListId) {

		String token = SessionHelper.getToken(companyId);
		try {
			vos = pvuvMovieReportService.queryMoviePvuv(token, mediaListId);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String s1data = "name : 'pv',type : 'bar',data :";
		String s2data = "name : 'uv',type : 'bar',data :";

		String tmp1 = "";
		String tmp2 = "";

		if (null == vos) {
			errCode = "data is null,please check request data";
			return;
		}

		for (int i = vos.size() - 1; i >= 0; i--) {
			PvuvMovieVo model = (PvuvMovieVo) vos.get(i);
			xdata = xdata + "'" + model.getTitle() + "',";
			tmp1 = tmp1 + "," + model.getTotalPv();
			tmp2 = tmp2 + "," + model.getTotalUv();
		}

		if (tmp1.length() > 0) {
			s1data = s1data + "[" + tmp1.substring(1) + "]";
		}
		if (tmp2.length() > 0) {
			s2data = s2data + "[" + tmp2.substring(1) + "]";
		}

		seriesStr = "{" + s1data + "}" + "," + "{" + s2data + "}";

		// y轴从下往上显示，与table中的顺序刚好是反的，倒序排一下

		if (xdata.length() > 0) {
			xdata = xdata.substring(0, xdata.length() - 1);
		} else {
			// errCode =
			// "Failed to fetch data , please try again or connect the administrator";
			errCode = "No search results!";
		}

	}

	public String getSeriesStr() {

		return seriesStr;
	}

	public void setSeriesStr(String seriesStr) {

		this.seriesStr = seriesStr;
	}

	public String getXdata() {

		return xdata;
	}

	public void setXdata(String xdata) {

		this.xdata = xdata;
	}

	public String getErrCode() {

		return errCode;
	}

	public void setErrCode(String errCode) {

		this.errCode = errCode;
	}

	public ArrayList<PvuvMovieVo> getVos() {

		return vos;
	}

	public List<MediaList> getMediaLists() {

		return mediaLists;
	}

	public int getMediaListId() {

		return mediaListId;
	}

	public void setMediaListId(int mediaListId) {

		this.mediaListId = mediaListId;
	}
}
