package com.wifun.admin.app.report;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.ResponseHelper;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.app.vo.TrafficDailyVo;
import com.wifun.admin.app.vo.TrafficMonthVo;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.Group;
import com.wifun.admin.domain.service.IDeviceService;
import com.wifun.admin.domain.service.IGroupService;
import com.wifun.admin.util.JSONBean;
import com.wifun.admin.util.MathUtil;
import com.wifun.admin.util.TimeUtils;
import com.wifun.inhand.response.statistic.TrafficDailyRespModel;
import com.wifun.inhand.response.statistic.TrafficDailyResponse;
import com.wifun.inhand.response.statistic.TrafficMonthRespModel;
import com.wifun.inhand.service.InhandTrafficService;

@SuppressWarnings("serial")
@Namespace(value = "/trafficReport")
@ParentPackage(value = "admin-default")
public class TrafficReportAction extends BaseAction {

	// Service
	@Resource(name = "deviceService")
	private IDeviceService deviceService;
	@Resource(name = "groupService")
	private IGroupService groupService;

	List<TrafficMonthVo> vos = new ArrayList<TrafficMonthVo>();

	private String inhandId;
	private String month;
	private int groupId = -1;

	private List<Group> groupList;

	/*
	 * 流量统计首页面，页面打开只显示下拉框，日期框等条件，不进行报表查询。
	 */
	@Action(value = "trafficSum-list", results = { @Result(name = "list", location = "/jsp/report/traffic-report.jsp") })
	public String list() {

		// 初始化页面查询数据
		super.putCompanyAllList();
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN == SessionHelper.getUserType()) {
			if (companyList != null && companyList.size() > 0)
				companyId = companyList.get(0).getId();
			else
				companyId = 0;
		} else {
			companyId = SessionHelper.getCompanyId();
		}
		groupList = groupService.getGroupsByCompanyId(companyId);
		month = TimeUtils.getFormatterDate(new Date(), "yyyyMM");

		List<Device> deviceList = deviceService.loadByCompany(companyId);
		Map<String, Device> deviceMap = deviceService.getDeviceMapByInhandId(companyId);

		String[] resourceIds = new String[deviceList.size()];
		for (int i = 0; i < deviceList.size(); i++) {
			Device device = deviceList.get(i);
			resourceIds[i] = device.getInhandId();
		}
		String token = SessionHelper.getToken(companyId);

		// String[] resourceIds = {"55822a9be4b0e85f6ce22bc7"};
		// String token = SessionHelper.getToken("ilikebus@itm-cn.cn");

		ArrayList<TrafficMonthRespModel> results = InhandTrafficService.trafficMonth(token, month,
				resourceIds);

		for (int i = 0; i < results.size(); i++) {

			TrafficMonthRespModel model = results.get(i);
			// 补充设备名称到各TrafficMonthRespModel
			String inhandId = model.getDeviceId();
			Device device = deviceMap.get(inhandId.toLowerCase());
			// device = new Device();
			// device.setId(29);
			// device.setCompanyId(51);
			// device.setSn("PF3121506267843");
			// device.setInhandId("55822a9be4b0e85f6ce22bc7");
			// device.setInhandSiteId("55822a9be4b0e85f6ce22bc8");
			// device.setGroupId(11);
			// device.setGroupName("test");

			if (device == null)
				continue;

			TrafficMonthVo vo = new TrafficMonthVo(device, model);
			vos.add(vo);
		}

		return "list";
	}

	@Action(value = "trafficSum-search", results = { @Result(name = "success", location = "/jsp/report/traffic-report.jsp") })
	public String search() throws Exception {

		super.putCompanyAllList();

		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType())
			companyId = SessionHelper.getCompanyId();
		groupList = groupService.getGroupsByCompanyId(companyId);

		List<Device> deviceList = deviceService.loadByCompany(companyId);
		Map<String, Device> deviceMap = deviceService.getDeviceMapByInhandId(companyId);

		String[] resourceIds = new String[deviceList.size()];
		for (int i = 0; i < deviceList.size(); i++) {
			Device device = deviceList.get(i);
			resourceIds[i] = device.getInhandId();
		}
		String token = SessionHelper.getToken(companyId);

		ArrayList<TrafficMonthRespModel> results = InhandTrafficService.trafficMonth(token, month,
				resourceIds);

		for (int i = 0; i < results.size(); i++) {

			TrafficMonthRespModel model = results.get(i);
			// 补充设备名称到各TrafficMonthRespModel
			String inhandId = model.getDeviceId();
			Device device = deviceMap.get(inhandId.toLowerCase());

			if (device == null)
				continue;

			// filter by groupid
			if (this.groupId != -1) {
				if (device.getGroupId() != groupId) {
					continue;
				}
			}

			TrafficMonthVo vo = new TrafficMonthVo(device, model);
			vos.add(vo);
		}

		return "success";
	}

	@Action(value = "trafficDailyDetail")
	public void getTrafficDailyDetail() throws Exception {

		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType())
			companyId = SessionHelper.getCompanyId();

		String token = SessionHelper.getToken(companyId);
		// String token = SessionHelper.getToken("ilikebus@itm-cn.cn");

		TrafficDailyResponse response = InhandTrafficService.trafficDaily(token,
				inhandId.toUpperCase(), month);
		List<TrafficDailyVo> voDetails = new ArrayList<TrafficDailyVo>();
		if (null != response) {

			List<TrafficDailyRespModel> models = response.getRespResult();
			if (null != models) {

				for (int i = 0; i < models.size(); i++) {

					TrafficDailyRespModel model = models.get(i);
					String dateStr = model.getDate();
					dateStr = TimeUtils.DateToString(TimeUtils.StringToDate(dateStr, "yyyyMMdd"),
							"MM/dd/yyyy");

					TrafficDailyVo vo = new TrafficDailyVo();
					vo.setInhandId(model.getDeviceId());
					vo.setTotal(MathUtil.bytes2MB(model.getTotal(), 3));
					vo.setSend(MathUtil.bytes2MB(model.getSend(), 3));
					vo.setReceive(MathUtil.bytes2MB(model.getReceive(), 3));
					vo.setDate(dateStr);

					voDetails.add(vo);
				}
			}
		}
		// 返回json结果
		ResponseHelper.writeJsons(JSONBean.createJSONBean("dailyList", voDetails).toString());
	}

	public List<TrafficMonthVo> getVos() {

		return vos;
	}

	public String getInhandId() {

		return inhandId;
	}

	public void setInhandId(String inhandId) {

		this.inhandId = inhandId;
	}

	public String getMonth() {

		return month;
	}

	public void setMonth(String month) {

		this.month = month;
	}

	public List<Group> getGroupList() {

		return groupList;
	}

	public void setGroupList(List<Group> groupList) {

		this.groupList = groupList;
	}

	public int getGroupId() {

		return groupId;
	}

	public void setGroupId(int groupId) {

		this.groupId = groupId;
	}
}
