package com.wifun.admin.app.report;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.domain.model.TaskTmobileDataUse;
import com.wifun.admin.domain.service.ITmobilePageReportService;
import com.wifun.admin.util.TimeUtils;

@SuppressWarnings("serial")
@Namespace(value = "/TmobileDataUsageReport")
@ParentPackage(value = "admin-default")
public class TmobileDataUsageAction extends BaseAction {

	@Resource(name = "tmobilePageService")
	private ITmobilePageReportService tmobilePageService;

	private String date;

	List<TaskTmobileDataUse> vos;
	String errCode = "";

	// index
	@Action(value = "report-tmoible-datausage", results = { @Result(name = "success", location = "/jsp/report/tmobile-datausage-page.jsp") })
	public String execute() throws Exception {

		// date=TimeUtils.getNowDateStrShort();
		date = TimeUtils.getFormatterDate(TimeUtils.getYesterday(), "MM/dd/yyyy");
		vos = tmobilePageService.getDataUsageByDate(date);
		if ((null == vos) || (0 == vos.size())) {
			errCode = "No result data found";
		}

		return SUCCESS;
	}

	// search
	@Action(value = "report-tmoible-datausage-search", results = { @Result(name = "success", location = "/jsp/report/tmobile-datausage-page.jsp") })
	public String search() throws Exception {

		// if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN !=
		// SessionHelper.getUserType())
		// companyId = SessionHelper.getCompanyId();

		vos = tmobilePageService.getDataUsageByDate(date);
		if ((null == vos) || (0 == vos.size())) {
			errCode = "No result data found";
		}

		return SUCCESS;
	}

	public List<TaskTmobileDataUse> getVos() {

		return vos;
	}

	public String getErrCode() {

		return errCode;
	}

	public void setErrCode(String errCode) {

		this.errCode = errCode;
	}

	public String getDate() {

		return date;
	}

	public void setDate(String date) {

		this.date = date;
	}

}
