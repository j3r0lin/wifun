package com.wifun.admin.app.report;

import java.util.List;

import javax.annotation.Resource;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.app.vo.DailyReportCompanyVo;
import com.wifun.admin.domain.service.IReportService;
import com.wifun.admin.util.TimeUtils;

/**
 * 公司的每日报表
 * 
 */
@SuppressWarnings("serial")
@Namespace(value = "/report")
@ParentPackage(value = "admin-default")
public class DailyReportCompanyAction extends BaseAction {

	@Resource(name = "reportService")
	private IReportService reportService;

	// Parameters
	private String date;

	private List<DailyReportCompanyVo> dailyList;

	@Action(value = "daily-report-company", results = { @Result(name = "success", location = "/jsp/report/daily-report-company.jsp") })
	public String execute() throws Exception {

		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType())
			return ERROR;

		date = TimeUtils.getFormatterDate(TimeUtils.getYesterday(), "MM/dd/yyyy");

		dailyList = reportService.getCompanyDailyReport(date);
		return SUCCESS;
	}

	@Action(value = "daily-report-company-search", results = { @Result(name = "success", location = "/jsp/report/daily-report-company.jsp") })
	public String search() throws Exception {

		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType())
			return ERROR;

		dailyList = reportService.getCompanyDailyReport(date);

		return SUCCESS;
	}

	public String getDate() {

		return date;
	}

	public void setDate(String date) {

		this.date = date;
	}

	public List<DailyReportCompanyVo> getDailyList() {

		return dailyList;
	}
}
