package com.wifun.admin.app.report;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.apache.commons.lang.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Namespace;
import org.apache.struts2.convention.annotation.ParentPackage;
import org.apache.struts2.convention.annotation.Result;

import com.wifun.admin.app.BaseAction;
import com.wifun.admin.app.ResponseHelper;
import com.wifun.admin.app.SessionAccount;
import com.wifun.admin.app.SessionHelper;
import com.wifun.admin.app.vo.PvuvPageVo;
import com.wifun.admin.domain.service.IPvuvPageReportService;
import com.wifun.admin.util.JSONBean;
import com.wifun.admin.util.TimeUtils;
import com.wifun.inhand.model.Pvuv;
import com.wifun.inhand.service.InhandPVUVService;

@SuppressWarnings("serial")
@Namespace(value = "/pvuvPageReport")
@ParentPackage(value = "admin-default")
public class PvuvPageAction extends BaseAction {

	@Resource(name = "pvuvPageService")
	private IPvuvPageReportService pvuvPageService;

	private String type;
	private String startDate;
	private String endDate;

	List<PvuvPageVo> vos;
	String errCode = "";

	// index
	@Action(value = "report-pagepvuv", results = { @Result(name = "success", location = "/jsp/report/pvuv-page.jsp") })
	public String execute() throws Exception {

		super.putCompanyAllList();
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN == SessionHelper.getUserType()) {
			if (companyList != null && companyList.size() > 0)
				companyId = companyList.get(0).getId();
			else
				companyId = 0;
		} else {
			companyId = SessionHelper.getCompanyId();
		}

		vos = pvuvPageService.getTypesPvuvSum(companyId);

		endDate = TimeUtils.getCurrentDateShort();
		startDate = TimeUtils.addDayReturnMMddyyyy(new Date(), -7);

		// vos = pvuvPageService. .getReportDailyVo(companyId, "");
		if ((null == vos) || (0 == vos.size())) {
			errCode = "No result data found";
		}

		return SUCCESS;
	}

	// search
	@Action(value = "report-pagepvuv-search", results = { @Result(name = "success", location = "/jsp/report/pvuv-page.jsp") })
	public String search() throws Exception {

		super.putCompanyAllList();
		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType())
			companyId = SessionHelper.getCompanyId();

		vos = pvuvPageService.getTypesPvuvSum(companyId);

		endDate = TimeUtils.getCurrentDateShort();
		startDate = TimeUtils.addDayReturnMMddyyyy(new Date(), -7);

		return SUCCESS;
	}

	@Action(value = "page-pvuv-detail")
	public void getPvuvDetail() throws Exception {

		if (SessionAccount.SYSTEM_ROLE_TYPE_ADMIN != SessionHelper.getUserType())
			companyId = SessionHelper.getCompanyId();

		String token = SessionHelper.getToken(companyId);
		List<Pvuv> list = null;

		if (StringUtils.isNotBlank(startDate) && StringUtils.isNotBlank(endDate)
				&& StringUtils.isNotBlank(token)) {
			long startTime = TimeUtils.strUsToDate(startDate).getTime();
			long endTime = TimeUtils.strUsToDate(endDate).getTime();

			list = InhandPVUVService.queryPvuvDetail(token, type, "0", null, startTime, endTime);
		}

		// 返回json结果
		ResponseHelper.writeJsons(JSONBean.createJSONBean("pvuvList", list).toString());
	}

	public List<PvuvPageVo> getVos() {

		return vos;
	}

	public String getErrCode() {

		return errCode;
	}

	public void setErrCode(String errCode) {

		this.errCode = errCode;
	}

	public void setType(String type) {

		this.type = type;
	}

	public String getStartDate() {

		return startDate;
	}

	public void setStartDate(String startDate) {

		this.startDate = startDate;
	}

	public String getEndDate() {

		return endDate;
	}

	public void setEndDate(String endDate) {

		this.endDate = endDate;
	}
}
