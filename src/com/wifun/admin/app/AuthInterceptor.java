package com.wifun.admin.app;

import com.opensymphony.xwork2.Action;
import com.opensymphony.xwork2.ActionInvocation;
import com.opensymphony.xwork2.interceptor.Interceptor;

/**
 * 后台用户权限拦截
 * 
 * @author wanghc
 * 
 */
@SuppressWarnings("serial")
public class AuthInterceptor implements Interceptor {

	public AuthInterceptor() {

	}

	public void destroy() {

	}

	public void init() {

	}

	public String intercept(ActionInvocation action) throws Exception {

		// 验证是否登录
		if (SessionHelper.getCurrentAccount() != null) {
			return action.invoke();
		} else {
			// 重新登陆
			return Action.LOGIN;
		}
	}
}
