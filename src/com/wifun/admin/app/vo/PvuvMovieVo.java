package com.wifun.admin.app.vo;

public class PvuvMovieVo {

	private String rId;
	private String tId;
	private String title;
	private String updateTime;
	// private HashMap<String, String> current ;
	// private HashMap<String, String> last ;
	// private HashMap<String, String> total ;

	// Pvuv vaules
	private int currentPv;
	private int lastPv;
	private int totalPv;

	private int currentUv;
	private int lastUv;
	private int totalUv;

	public PvuvMovieVo() {

	}

	public PvuvMovieVo(String rid, String title) {

		this.tId = "movie";
		this.rId = rid;
		this.title = title;
	}

	public String getTId() {

		return tId;
	}

	public void setTId(String tId) {

		this.tId = tId;
	}

	public String getRId() {

		return rId;
	}

	public void setRId(String rId) {

		this.rId = rId;
	}

	public String getTitle() {

		return title;
	}

	public void setTitle(String title) {

		this.title = title;
	}

	public int getCurrentPv() {

		return currentPv;
	}

	public void setCurrentPv(int currentPv) {

		this.currentPv = currentPv;
	}

	public int getLastPv() {

		return lastPv;
	}

	public void setLastPv(int lastPv) {

		this.lastPv = lastPv;
	}

	public int getTotalPv() {

		return totalPv;
	}

	public void setTotalPv(int totalPv) {

		this.totalPv = totalPv;
	}

	public int getCurrentUv() {

		return currentUv;
	}

	public void setCurrentUv(int currentUv) {

		this.currentUv = currentUv;
	}

	public int getLastUv() {

		return lastUv;
	}

	public void setLastUv(int lastUv) {

		this.lastUv = lastUv;
	}

	public int getTotalUv() {

		return totalUv;
	}

	public void setTotalUv(int totalUv) {

		this.totalUv = totalUv;
	}

	public String getUpdateTime() {

		return updateTime;
	}

	public void setUpdateTime(String updateTime) {

		this.updateTime = updateTime;
	}
}
