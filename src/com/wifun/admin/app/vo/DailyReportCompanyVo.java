package com.wifun.admin.app.vo;

//报表显示View
public class DailyReportCompanyVo {

	// report
	private int companyId;

	// pvuv
	private int pvHome;
	private int uvHome;
	private int pvMain;
	private int uvMain;
	private int pvMovie;
	private int uvMovie;

	// 终端
	private int deviceDailyAdd;
	private int deviceTotal;
	private int deviceOneHourOnline;

	// 用户
	private int userDailyAdd;
	private int userTotal;

	// 流量
	private long trafficSend;
	private long trafficRecv;
	private long trafficTotal;

	// extent field
	private String companyName;

	// 构造
	public DailyReportCompanyVo() {

	}

	public int getCompanyId() {

		return companyId;
	}

	public void setCompanyId(int companyId) {

		this.companyId = companyId;
	}

	public int getPvHome() {

		return pvHome;
	}

	public void setPvHome(int pvHome) {

		this.pvHome = pvHome;
	}

	public int getUvHome() {

		return uvHome;
	}

	public void setUvHome(int uvHome) {

		this.uvHome = uvHome;
	}

	public int getPvMain() {

		return pvMain;
	}

	public void setPvMain(int pvMain) {

		this.pvMain = pvMain;
	}

	public int getUvMain() {

		return uvMain;
	}

	public void setUvMain(int uvMain) {

		this.uvMain = uvMain;
	}

	public int getPvMovie() {

		return pvMovie;
	}

	public void setPvMovie(int pvMovie) {

		this.pvMovie = pvMovie;
	}

	public int getUvMovie() {

		return uvMovie;
	}

	public void setUvMovie(int uvMovie) {

		this.uvMovie = uvMovie;
	}

	public int getDeviceDailyAdd() {

		return deviceDailyAdd;
	}

	public void setDeviceDailyAdd(int deviceDailyAdd) {

		this.deviceDailyAdd = deviceDailyAdd;
	}

	public int getDeviceTotal() {

		return deviceTotal;
	}

	public void setDeviceTotal(int deviceTotal) {

		this.deviceTotal = deviceTotal;
	}

	public int getDeviceOneHourOnline() {

		return deviceOneHourOnline;
	}

	public void setDeviceOneHourOnline(int deviceOneHourOnline) {

		this.deviceOneHourOnline = deviceOneHourOnline;
	}

	public int getUserDailyAdd() {

		return userDailyAdd;
	}

	public void setUserDailyAdd(int userDailyAdd) {

		this.userDailyAdd = userDailyAdd;
	}

	public int getUserTotal() {

		return userTotal;
	}

	public void setUserTotal(int userTotal) {

		this.userTotal = userTotal;
	}

	public long getTrafficSend() {

		return trafficSend;
	}

	public void setTrafficSend(long trafficSend) {

		this.trafficSend = trafficSend;
	}

	public long getTrafficRecv() {

		return trafficRecv;
	}

	public void setTrafficRecv(long trafficRecv) {

		this.trafficRecv = trafficRecv;
	}

	public long getTrafficTotal() {

		return trafficTotal;
	}

	public void setTrafficTotal(long trafficTotal) {

		this.trafficTotal = trafficTotal;
	}

	public String getCompanyName() {

		return companyName;
	}

	public void setCompanyName(String companyName) {

		this.companyName = companyName;
	}
}
