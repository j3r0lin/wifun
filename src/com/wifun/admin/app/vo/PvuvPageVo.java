package com.wifun.admin.app.vo;

import java.util.Date;
import java.util.HashMap;

import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.TaskReportDaily;
import com.wifun.admin.util.MathUtil;
import com.wifun.admin.util.TimeUtils;

//报表显示View
public class PvuvPageVo {

	// report
	// private int id;
	// private int deviceId;
	// private int companyId;

	// page types
	private String type;

	// pvuv
	private String currentPv;
	private String lastPv;
	private String totalPv;

	private String currentUv;
	private String lastUv;
	private String totalUv;

	private String date;

	// // extent field
	// private String deviceSN;
	// private String deviceName;
	// private String groupName;

	public String getDate() {

		return date;
	}

	public void setDate(String date) {

		this.date = date;
	}

	public String getType() {

		return type;
	}

	public void setType(String type) {

		this.type = type;
	}

	public String getCurrentPv() {

		return currentPv;
	}

	public void setCurrentPv(String currentPv) {

		this.currentPv = currentPv;
	}

	public String getLastPv() {

		return lastPv;
	}

	public void setLastPv(String lastPv) {

		this.lastPv = lastPv;
	}

	public String getTotalPv() {

		return totalPv;
	}

	public void setTotalPv(String totalPv) {

		this.totalPv = totalPv;
	}

	public String getCurrentUv() {

		return currentUv;
	}

	public void setCurrentUv(String currentUv) {

		this.currentUv = currentUv;
	}

	public String getLastUv() {

		return lastUv;
	}

	public void setLastUv(String lastUv) {

		this.lastUv = lastUv;
	}

	public String getTotalUv() {

		return totalUv;
	}

	public void setTotalUv(String totalUv) {

		this.totalUv = totalUv;
	}

}
