package com.wifun.admin.app.vo;

import com.wifun.admin.domain.model.Device;
import com.wifun.admin.util.MathUtil;
import com.wifun.inhand.response.statistic.TrafficDailyRespModel;

public class TrafficDailyVo {

	private float total;
	private float receive;
	private float send;
	private String inhandId;
	private String date;

	// private String _id;

	// public TrafficDailyVo(Device device, TrafficDailyRespModel model) {
	//
	// this.date = model.getDate();
	// this.inhandId = model.getDeviceId();
	//
	// this.total = MathUtil.bytes2MB(model.getTotal(), 2);
	// this.receive = MathUtil.bytes2MB(model.getReceive(), 2);
	// this.send = MathUtil.bytes2MB(model.getSend(), 2);
	// }

	public String getDate() {

		return date;
	}

	public void setDate(String date) {

		this.date = date;
	}

	public String getInhandId() {

		return inhandId;
	}

	public void setInhandId(String inhandId) {

		this.inhandId = inhandId;
	}

	public float getTotal() {

		return total;
	}

	public void setTotal(float total) {

		this.total = total;
	}

	public float getReceive() {

		return receive;
	}

	public void setReceive(float receive) {

		this.receive = receive;
	}

	public float getSend() {

		return send;
	}

	public void setSend(float send) {

		this.send = send;
	}

}
