package com.wifun.admin.app.vo;
@SuppressWarnings("unused")
public class MediaVo {

	// Fields
	
	private Integer id;
	private String title;
	private Integer showIndex;
	private String mediaUrl;
	private String vposterUrl;
	private String hposterUrl;

	private String entertainment;
	private String year;
	private String writer;
	private String director;
	private String starring;
	private Integer length;

	private String introduction;

}
