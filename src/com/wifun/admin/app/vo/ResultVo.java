package com.wifun.admin.app.vo;

public class ResultVo {

	public static final int CODE_OK = 0;
	public static final int CODE_SYSTEM_ERROR = 99;

	public static final int CODE_API_ERROR = 10;

	public static final String MSG_SYSTEM_ERROR = "System Error.";

	private int code;
	private String msg;
	private Object data;

	public ResultVo() {

		this.code = CODE_OK;
	}

	public int getCode() {

		return code;
	}

	public void setCode(int code) {

		this.code = code;
	}

	public String getMsg() {

		return msg;
	}

	public void setMsg(String msg) {

		this.msg = msg;
	}

	public Object getData() {

		return data;
	}

	public void setData(Object data) {

		this.data = data;
	}

	public boolean isOk() {

		if (code == CODE_OK) {
			return true;
		}
		return false;
	}
}