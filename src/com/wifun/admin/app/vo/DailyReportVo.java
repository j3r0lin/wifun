package com.wifun.admin.app.vo;

import com.wifun.admin.domain.model.Company;
import com.wifun.admin.domain.model.Device;
import com.wifun.admin.domain.model.TaskReportDaily;
import com.wifun.admin.util.MathUtil;
import com.wifun.admin.util.TimeUtils;

//报表显示View
public class DailyReportVo {

	// report
	private int id;
	private int deviceId;
	private int companyId;

	// pvuv
	private int pvHome;
	private int uvHome;
	private int pvMain;
	private int uvMain;
	private int pvMovie;
	private int uvMovie;

	// 终端
	private int deviceDailyAdd;
	private int deviceTotal;
	private int deviceOneHourOnline;

	// 用户
	private int userDailyAdd;
	private int userTotal;

	// 流量
	private float trafficSend;
	private float trafficRecv;
	private float trafficTotal;

	private String date;

	// extent field
	private String companyName;
	private int groupId;
	private String deviceSN;
	private String deviceName;
	private String groupName;

	// 构造
	public DailyReportVo(TaskReportDaily report, Company company, Device device) {

		this.id = report.getId();
		this.deviceId = report.getDeviceId();
		this.deviceName = device.getName();
		this.deviceSN = device.getSn();
		this.groupName = device.getGroupName();

		this.companyId = report.getCompanyId();
		this.companyName = company.getName();

		this.pvHome = report.getPvHome();
		this.uvHome = report.getUvHome();
		this.pvMain = report.getPvMain();
		this.uvMain = report.getUvMain();
		this.pvMovie = report.getPvMovie();
		this.uvMovie = report.getUvMovie();
		this.deviceDailyAdd = report.getDeviceDailyAdd();
		this.deviceTotal = report.getDeviceTotal();
		this.deviceOneHourOnline = report.getDeviceOneHourOnline();
		this.userDailyAdd = report.getUserDailyAdd();
		this.userTotal = report.getUserTotal();

		this.trafficSend = MathUtil.bytes2MB(report.getTrafficSend(), 3);
		this.trafficRecv = MathUtil.bytes2MB(report.getTrafficRecv(), 3);
		this.trafficTotal = MathUtil.bytes2MB(report.getTrafficTotal(), 3);

		this.date = TimeUtils.getDateToUsTow(TimeUtils.strToDate(report.getDate()));
	}

	public int getId() {

		return id;
	}

	public void setId(int id) {

		this.id = id;
	}

	public int getDeviceId() {

		return deviceId;
	}

	public void setDeviceId(int deviceId) {

		this.deviceId = deviceId;
	}

	public int getCompanyId() {

		return companyId;
	}

	public void setCompanyId(int companyId) {

		this.companyId = companyId;
	}

	public int getPvHome() {

		return pvHome;
	}

	public void setPvHome(int pvHome) {

		this.pvHome = pvHome;
	}

	public int getUvHome() {

		return uvHome;
	}

	public void setUvHome(int uvHome) {

		this.uvHome = uvHome;
	}

	public int getPvMain() {

		return pvMain;
	}

	public void setPvMain(int pvMain) {

		this.pvMain = pvMain;
	}

	public int getUvMain() {

		return uvMain;
	}

	public void setUvMain(int uvMain) {

		this.uvMain = uvMain;
	}

	public int getPvMovie() {

		return pvMovie;
	}

	public void setPvMovie(int pvMovie) {

		this.pvMovie = pvMovie;
	}

	public int getUvMovie() {

		return uvMovie;
	}

	public void setUvMovie(int uvMovie) {

		this.uvMovie = uvMovie;
	}

	public int getDeviceDailyAdd() {

		return deviceDailyAdd;
	}

	public void setDeviceDailyAdd(int deviceDailyAdd) {

		this.deviceDailyAdd = deviceDailyAdd;
	}

	public int getDeviceTotal() {

		return deviceTotal;
	}

	public void setDeviceTotal(int deviceTotal) {

		this.deviceTotal = deviceTotal;
	}

	public int getDeviceOneHourOnline() {

		return deviceOneHourOnline;
	}

	public void setDeviceOneHourOnline(int deviceOneHourOnline) {

		this.deviceOneHourOnline = deviceOneHourOnline;
	}

	public int getUserDailyAdd() {

		return userDailyAdd;
	}

	public void setUserDailyAdd(int userDailyAdd) {

		this.userDailyAdd = userDailyAdd;
	}

	public int getUserTotal() {

		return userTotal;
	}

	public void setUserTotal(int userTotal) {

		this.userTotal = userTotal;
	}

	public float getTrafficSend() {

		return trafficSend;
	}

	public void setTrafficSend(float trafficSend) {

		this.trafficSend = trafficSend;
	}

	public float getTrafficRecv() {

		return trafficRecv;
	}

	public void setTrafficRecv(float trafficRecv) {

		this.trafficRecv = trafficRecv;
	}

	public float getTrafficTotal() {

		return trafficTotal;
	}

	public void setTrafficTotal(float trafficTotal) {

		this.trafficTotal = trafficTotal;
	}

	public String getDate() {

		return date;
	}

	public void setDate(String date) {

		this.date = date;
	}

	public String getCompanyName() {

		return companyName;
	}

	public void setCompanyName(String companyName) {

		this.companyName = companyName;
	}

	public int getGroupId() {

		return groupId;
	}

	public void setGroupId(int groupId) {

		this.groupId = groupId;
	}

	public String getDeviceSN() {

		return deviceSN;
	}

	public void setDeviceSN(String deviceSN) {

		this.deviceSN = deviceSN;
	}

	public String getDeviceName() {

		return deviceName;
	}

	public void setDeviceName(String deviceName) {

		this.deviceName = deviceName;
	}

	public String getGroupName() {

		return groupName;
	}

	public void setGroupName(String groupName) {

		this.groupName = groupName;
	}

}
