package com.wifun.admin.app.vo;

import com.wifun.admin.domain.model.Device;
import com.wifun.inhand.model.Location;

public class DeviceLocation {

	private int deviceId;
	private int companyId;
	private String sn;
	private String name;
	private int groupId;
	private String groupName;
	private int online;
	private double longitude;
	private double latitude;
	private double speed;
	private double course;
	private String time; // yyyy-mm-dd hh:mi:ss

	public DeviceLocation() {

	}

	public DeviceLocation(Device device, Location location, String time) {

		this.deviceId = device.getId();
		this.companyId = device.getCompanyId();
		this.sn = device.getSn();
		this.longitude = location.getLongitude();
		this.latitude = location.getLatitude();
		this.speed = location.getSpeed();
		this.course = location.getCourse();
		this.time = time;
	}

	public DeviceLocation(Device device, Location location, int online, String time) {

		this.deviceId = device.getId();
		this.companyId = device.getCompanyId();
		this.sn = device.getSn();
		this.name = device.getName();
		this.groupId = device.getGroupId();
		this.groupName = device.getGroupName();
		this.online = online;
		this.longitude = location.getLongitude();
		this.latitude = location.getLatitude();
		this.speed = location.getSpeed();
		this.course = location.getCourse();
		this.time = time;
	}

	public int getDeviceId() {

		return deviceId;
	}

	public void setDeviceId(int companyId) {

		this.deviceId = companyId;
	}

	public int getcompanyId() {

		return companyId;
	}

	public void companyId(int companyId) {

		this.companyId = companyId;
	}

	public String getSn() {

		return sn;
	}

	public void setSn(String sn) {

		this.sn = sn;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public int getGroupId() {

		return groupId;
	}

	public void setGroupId(int groupId) {

		this.groupId = groupId;
	}

	public String getGroupName() {

		return groupName;
	}

	public void setGroupName(String groupName) {

		this.groupName = groupName;
	}

	public int getOnline() {

		return online;
	}

	public void setOnline(int online) {

		this.online = online;
	}

	public double getLongitude() {

		return longitude;
	}

	public void setLongitude(double longitude) {

		this.longitude = longitude;
	}

	public double getLatitude() {

		return latitude;
	}

	public void setLatitude(double latitude) {

		this.latitude = latitude;
	}

	public double getSpeed() {

		return speed;
	}

	public void setSpeed(double speed) {

		this.speed = speed;
	}

	public double getCourse() {

		return course;
	}

	public void setCourse(double course) {

		this.course = course;
	}

	public String getTime() {

		return time;
	}

	public void setTime(String time) {

		this.time = time;
	}
}
