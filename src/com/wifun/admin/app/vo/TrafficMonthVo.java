package com.wifun.admin.app.vo;

import com.wifun.admin.domain.model.Device;
import com.wifun.admin.util.MathUtil;
import com.wifun.inhand.response.statistic.TrafficMonthRespModel;

public class TrafficMonthVo {

	private int deviceId;
	private String inhandId;
	private String sn;
	private String deviceName;
	private int groupId;
	private String groupName;

	private float total;
	private float receive;
	private float send;
	private float max;

	public TrafficMonthVo(Device device, TrafficMonthRespModel model) {

		this.deviceId = device.getId();
		this.inhandId = model.getDeviceId();
		this.sn = device.getSn();
		this.deviceName = device.getName();
		this.groupId = device.getGroupId();
		this.groupName = device.getGroupName();

		this.total = MathUtil.bytes2MB(model.getTotal(), 3);
		this.receive = MathUtil.bytes2MB(model.getReceive(), 3);
		this.send = MathUtil.bytes2MB(model.getSend(), 3);
		this.max = MathUtil.bytes2MB(model.getMax(), 3);
	}

	public int getDeviceId() {

		return deviceId;
	}

	public void setDeviceId(int deviceId) {

		this.deviceId = deviceId;
	}

	public String getInhandId() {

		return inhandId;
	}

	public void setInhandId(String inhandId) {

		this.inhandId = inhandId;
	}

	public String getSn() {

		return sn;
	}

	public void setSn(String sn) {

		this.sn = sn;
	}

	public String getDeviceName() {

		return deviceName;
	}

	public void setDeviceName(String deviceName) {

		this.deviceName = deviceName;
	}

	public int getGroupId() {

		return groupId;
	}

	public void setGroupId(int groupId) {

		this.groupId = groupId;
	}

	public String getGroupName() {

		return groupName;
	}

	public void setGroupName(String groupName) {

		this.groupName = groupName;
	}

	public float getTotal() {

		return total;
	}

	public void setTotal(float total) {

		this.total = total;
	}

	public float getReceive() {

		return receive;
	}

	public void setReceive(float receive) {

		this.receive = receive;
	}

	public float getSend() {

		return send;
	}

	public void setSend(float send) {

		this.send = send;
	}

	public float getMax() {

		return max;
	}

	public void setMax(float max) {

		this.max = max;
	}
}
