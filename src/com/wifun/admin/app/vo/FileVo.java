package com.wifun.admin.app.vo;

public class FileVo {

	private String path;
	private String name;
	private boolean isDirectory;

	public String getPath() {

		return path;
	}

	public void setPath(String path) {

		this.path = path;
	}

	public String getName() {

		return name;
	}

	public void setName(String name) {

		this.name = name;
	}

	public boolean isDirectory() {

		return isDirectory;
	}

	public void setDirectory(boolean isDirectory) {

		this.isDirectory = isDirectory;
	}
}
