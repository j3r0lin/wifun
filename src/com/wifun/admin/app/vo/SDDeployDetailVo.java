package com.wifun.admin.app.vo;

import java.util.Date;

public class SDDeployDetailVo {

	private Integer deployId;
	private Integer mediaId;
	private String mediaTitle;
	private String mediaVersion;
	// private int deviceStatus;
	private Date deployTime;
	private String url;

	public SDDeployDetailVo() {

	}

	public Integer getDeployId() {

		return deployId;
	}

	public void setDeployId(Integer deployId) {

		this.deployId = deployId;
	}

	public Integer getMediaId() {

		return mediaId;
	}

	public void setMediaId(Integer mediaId) {

		this.mediaId = mediaId;
	}

	public String getMediaTitle() {

		return mediaTitle;
	}

	public void setMediaTitle(String mediaTitle) {

		this.mediaTitle = mediaTitle;
	}

	public String getMediaVersion() {

		return mediaVersion;
	}

	public void setMediaVersion(String mediaVersion) {

		this.mediaVersion = mediaVersion;
	}

	// public int getDeviceStatus() {
	//
	// return deviceStatus;
	// }
	//
	// public void setDeviceStatus(int deviceStatus) {
	//
	// this.deviceStatus = deviceStatus;
	// }

	public Date getDeployTime() {

		return deployTime;
	}

	public void setDeployTime(Date deployTime) {

		this.deployTime = deployTime;
	}

	public String getUrl() {

		return url;
	}

	public void setUrl(String url) {

		this.url = url;
	}
}
