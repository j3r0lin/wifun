package com.wifun.admin.app;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletOutputStream;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

/**
 * 响应辅助类
 * 
 * @author wanghc
 * 
 */
public final class ResponseHelper {

	/**
	 * 获取响应对象
	 * 
	 * @return 响应对象
	 */
	public static ServletResponse getResponse() {

		return ServletActionContext.getResponse();
	}

	/**
	 * 输出字符串
	 * 
	 * @param str
	 *            字符串
	 * @param contentType
	 *            类型
	 */
	public static void writeString(String str, String contentType) {

		HttpServletResponse response = (HttpServletResponse) getResponse();
		if (contentType != null && contentType.trim().length() > 0) {
			response.setContentType(contentType);
		}
		response.setCharacterEncoding("UTF-8");
		response.setHeader("Charset", "UTF-8");
		response.setHeader("Cache-Control", "no-cache");
		PrintWriter out = null;
		try {
			out = response.getWriter();
			out.write(str);
			out.flush();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (out != null)
				out.close();
		}
	}

	/**
	 * 输出字节数组
	 * 
	 * @param buffer
	 *            字节数组
	 * @param contentType
	 *            类型
	 */
	public static void writeByte(byte[] buffer, String contentType) {

		if (buffer == null || buffer.length == 0) {
			new NullPointerException(ResponseHelper.class.getName() + "类的writeByte方法中buffer参数为空!");
			return;
		}
		ServletResponse response = getResponse();
		if (contentType != null && contentType.length() > 0)
			response.setContentType(contentType);
		ServletOutputStream out = null;
		try {
			out = response.getOutputStream();
			out.write(buffer);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (out != null)
					out.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * 输出Json数据
	 * 
	 * @param json
	 *            json字符串
	 */
	public static void writeJsons(String json) {

		writeString(json, "text/html;charset=UTF-8");
	}

	/**
	 * 输出图片
	 * 
	 * @param buffer
	 *            字节数组
	 */
	public static void writeImage(byte[] buffer) {

		writeByte(buffer, "image/*");
	}

	/**
	 * 输出XML格式(需要带头<?xml version='1.0' encoding='UTF-8'?>)
	 * 
	 * @param xml
	 *            xml格式内容
	 */
	public static void writeXml(String xml) {

		writeString(xml, "text/xml");
	}

	/**
	 * 输出文本内容
	 * 
	 * @param text
	 *            文本内容
	 */
	public static void writeHtmlText(String text) {

		writeString(text, "text/html");
	}

	/**
	 * 
	 * @param cookie
	 */
	public static void addCookie(Cookie cookie) {

		HttpServletResponse response = (HttpServletResponse) getResponse();
		response.addCookie(cookie);
	}

	public static void sendRedirect(String url) {

		HttpServletResponse response = (HttpServletResponse) getResponse();
		try {
			response.sendRedirect(url);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
