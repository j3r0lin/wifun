package com.wifun.admin.domain.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.wifun.admin.domain.model.SubBusline;
import com.wifun.admin.domain.model.SubSchedule;
import com.wifun.admin.domain.service.ISubScheduleService;
import com.wifun.admin.util.TimeUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext.xml")
public class TestSubScheduleService {
	
	@Resource(name = "subScheduleService")
	private ISubScheduleService subScheduleService;//getBookingList
	
	//@Test
	public void testGetScheduleList() {
		Date date = TimeUtils.strUsToDate("09/14/2014");
//		List<SubSchedule> list  =subScheduleService.getBookingList(1, date);
//		System.out.println(list);
	}
	
	@Test
	public void testGetSubScheduleByDateList() {
//		Date startdate = TimeUtils.strUsToDate("09/15/2014");
//		Date endDate = TimeUtils.nearDay(startdate,7);
		Date startdate = new Date();
		System.out.println(">>>>>>>"+startdate);
		Date endDate = TimeUtils.nearDay(startdate,7);
		System.out.println(endDate);
		List<SubSchedule> subScheduleList = subScheduleService.getSubScheduleByDateList(startdate,endDate,1, 0, true);
		System.out.println(subScheduleList);
	}
	
	@Test
	public void testGetSubBuslineById() {
		SubSchedule subSchedule = subScheduleService.getsSubSchedule(1);
		System.out.println(subSchedule);
	}
}
