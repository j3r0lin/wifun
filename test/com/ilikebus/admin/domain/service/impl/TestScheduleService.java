package com.wifun.admin.domain.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.wifun.admin.domain.model.Schedule;
import com.wifun.admin.domain.service.IScheduleService;
import com.wifun.admin.util.TimeUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext.xml")
public class TestScheduleService {

	@Resource(name = "scheduleService")
	private IScheduleService scheduleService;

	@Test
	public void testGetScheduleList() {

		Date fromDate = TimeUtils.strUsToDate("09/14/2014");
		Date toDate = TimeUtils.strUsToDate("9/14/2014");
		int buslineId = 2;
		boolean active = true;

		List list = scheduleService.getScheduleList(fromDate, toDate, buslineId, active, 1);

		System.out.println(list);
	}

	// @Test
	public void testGetBuslineSchedule() {

		Integer id = 2;
		Schedule bs = scheduleService.getBuslineSchedule(id);
		System.out.println(bs);
	}

	// @Test
	public void testGetBookingList() {

		int companyId = 1;
		Date date = TimeUtils.strUsToDate("09/14/2014");

		List<Schedule> list = scheduleService.getBookingList(companyId, date, true);
		System.out.println(list);
	}

	// @Test
	public void testUpdateBuslineSchedule() {

		// int id=2;
		// //Schedule schedule = new Schedule(2,555);
		//
		// boolean flag = scheduleService.updateBuslineSchedule(id, schedule);
		// System.out.println(flag);
	}

}
