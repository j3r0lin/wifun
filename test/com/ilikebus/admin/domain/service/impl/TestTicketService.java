package com.wifun.admin.domain.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.wifun.admin.app.vo.ReportSimpleVo;
import com.wifun.admin.app.vo.SaleReportVo;
import com.wifun.admin.domain.model.Ticket;
import com.wifun.admin.domain.service.ITicketService;
import com.wifun.admin.util.TimeUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext.xml")
public class TestTicketService {

	@Resource(name = "ticketService")
	private ITicketService ticketService;// getBookingList

	// @Test
	public void testGetScheduleList() {

		String passengername = "22";
		String email = "";
		String ticketId = null;
//		List<Ticket> busTicketList = ticketService.getTicketList(passengername, email, ticketId, 1);
//		System.out.println(busTicketList);

	}

	// @Test
	public void testGetTicketByEmail() {

		Integer ticketId = 2;
		Ticket busTicket = ticketService.getTicketByEmail(ticketId);
		System.out.println(busTicket);

	}

	// @Test
	public void testGetBusLineTickets() {

		Integer buslineId = 2;
		Date dt = TimeUtils.strUsToDate("09/14/2014");
		List<Ticket> busTicketList = ticketService.getBusLineTickets(dt, buslineId);
		System.out.println(busTicketList);

	}

	// @Test
	public void testGetSalesByCity() {

		// List<ReportSimpleVo> list = ticketService.getSalesByCity(9, 2014, 6,
		// 3);
		// System.out.println(list.get(14).getTicketPrice());
		// System.out.println(list.get(14).getDate());
		// System.out.println(list.get(14).getServiceCharge());

	}

	@Test
	public void testGetDaySalesByCity() {

		// List<SaleReportVo> list = ticketService.getRewarSaleReportVo(1, 2,
		// "", "");
		// System.out.println(list.size());
		// System.out.println(list.get(0).getLineName());
		// System.out.println(list.get(0).getQuantity());
		// System.out.println(list.get(0).getScheduledate());
		// System.out.println(list.get(0).getTotal());
	}

	// @Test
	public void testGetSubscheduleList() {

		Date dt = TimeUtils.strUsToDate("09/15/2014");
		List<Ticket> SubBusTicketList = ticketService.getSubBusLineTickets(dt, 3);
		System.out.println(SubBusTicketList);

	}
}
