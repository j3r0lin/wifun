package com.wifun.admin.domain.service.impl;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.wifun.admin.domain.model.Station;
import com.wifun.admin.domain.service.IStationService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext.xml")
public class TestStationService {

	@Resource(name = "stationService")
	private IStationService stationService;

	@Test
	public void testBuslineDao() {
		
		Station station = stationService.getStation(5);
		System.out.println(station.getName());
	}
}
