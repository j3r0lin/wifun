package com.wifun.admin.domain.service.impl;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.wifun.admin.domain.model.SystemAccount;
import com.wifun.admin.domain.service.ILoginService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext.xml")
public class TestLoginService {
	@Resource(name="loginService")
	private ILoginService loginService;
	
	@Test
	public void testGetSystemLoginInfo(){
		SystemAccount user= loginService.getSystemLoginInfo("admin");
		System.out.println("----------------"+user);
	}
}
