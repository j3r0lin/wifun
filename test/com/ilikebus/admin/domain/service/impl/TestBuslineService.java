package com.wifun.admin.domain.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.wifun.admin.domain.service.IBuslineService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/applicationContext.xml")
public class TestBuslineService {

	@Resource(name = "buslineService")
	private IBuslineService buslineService;

	@Test
	public void testBuslineDao() {
		
		List list = buslineService.getBusLineByCompanyId(1);
		System.out.println(list.toString());
	}
	
}
